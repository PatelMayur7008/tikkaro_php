-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 04, 2017 at 03:44 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikkaro`
--

-- --------------------------------------------------------

--
-- Table structure for table `tk_admin`
--

CREATE TABLE `tk_admin` (
  `id` int(11) NOT NULL,
  `var_fname` varchar(100) NOT NULL,
  `var_lname` varchar(100) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `var_password` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `txt_address` text,
  `dt_dob` datetime DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_admin`
--

INSERT INTO `tk_admin` (`id`, `var_fname`, `var_lname`, `var_email`, `var_password`, `bint_phone`, `var_profile_image`, `txt_address`, `dt_dob`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@admin.com', 'e6e061838856bf47e1de730719fb2609', '9727190205', NULL, 'Lorem Ipsum text here...', '1994-05-19 00:00:00', 'YES', '2017-11-03 00:00:00', '2017-11-03 09:44:35');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barbers`
--

CREATE TABLE `tk_barbers` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `txt_address` text,
  `var_rating` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barbers`
--

INSERT INTO `tk_barbers` (`id`, `var_name`, `bint_phone`, `txt_address`, `var_rating`, `created_at`, `updated_at`) VALUES
(1, 'Jignesh vanand', '8487546789', 'Lorem ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(2, 'Rutvik Vanand', '5454878952', 'Lorem Ipsum text here', 4, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(3, 'Nirav patel', '9854894556', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(4, 'Mayur Patel', '7894855896', 'Lorem ipsum text here..', 4, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(5, 'Gaurav Prajapati', '13213132132132', 'Lorem Ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35'),
(6, 'Jayesh Mojidra', '54654654321321', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barber_types`
--

CREATE TABLE `tk_barber_types` (
  `id` int(11) NOT NULL,
  `var_type` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barber_types`
--

INSERT INTO `tk_barber_types` (`id`, `var_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53'),
(2, 'Worker', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_migrations`
--

CREATE TABLE `tk_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_migrations`
--

INSERT INTO `tk_migrations` (`version`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `tk_services`
--

CREATE TABLE `tk_services` (
  `id` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_services`
--

INSERT INTO `tk_services` (`id`, `var_title`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Hair cut', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:42:38'),
(2, 'Saving', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:42:38'),
(3, 'Clipper Cut ', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:43:47'),
(4, 'The Sides', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:43:47'),
(5, 'Hot Towel Head Shave', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:44:22'),
(6, 'Hot Towel Face Shave', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:44:22');

-- --------------------------------------------------------

--
-- Table structure for table `tk_stores`
--

CREATE TABLE `tk_stores` (
  `id` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `txt_address` text,
  `var_latitude` varchar(255) NOT NULL,
  `var_longitude` varchar(255) NOT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `open_time` time DEFAULT NULL,
  `close_time` time DEFAULT NULL,
  `var_rating` varchar(20) DEFAULT NULL,
  `enum_store_type` enum('MEN','WOMEN','BOTH') NOT NULL DEFAULT 'MEN',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_stores`
--

INSERT INTO `tk_stores` (`id`, `var_title`, `txt_address`, `var_latitude`, `var_longitude`, `bint_phone`, `open_time`, `close_time`, `var_rating`, `enum_store_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Hairitage Unisex Salon', 'GF.10, Dev-preet Complex Opp.Brewberrys Coffee Bar N.F.D Circle, Cross Raod Bodakdev, Bodakdev, Ahmedabad, Gujarat 380054', '23.043459', '72.520108', '456798231312', '09:00:00', '09:00:00', '5', 'BOTH', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:56:05'),
(2, 'Truefitt & Hill', ' Shop No 101, Asian Square, Opposite Shilp Aaron, Near Armida, Sindhu Bhavan Road, Off S.G. Highway, Bodekdev, Ahmedabad, Gujarat 380054', '23.040034', '72.508811', '8974654123', '09:00:00', '20:00:00', '4', 'MEN', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:56:05'),
(3, 'Classic Hair Care', 'Newyork Trade Centre, S.G. Highway, Thaltej, Ahmedabad, Gujarat 380059', '23.040054', '72.513088', '5313213211321', '10:00:00', '21:00:00', '3.2', 'WOMEN', 'YES', '2017-11-04 00:00:00', '2017-11-04 08:59:26'),
(4, 'Ai Shree Khodiyar Hair Style', 'Maharana Pratap Rd, Vishwas City 1, Chanakyapuri, Ahmedabad, Gujarat 380061', '23.080365', '72.535798', '213265483141', '09:00:00', '20:00:00', '4.1', 'MEN', 'YES', '2017-11-04 00:00:00', '2017-11-04 08:59:26'),
(5, 'Vaibhav Hair Art', 'Asopalav Apartment, Mangal Murti, Naranpur Telephone Exchange Road, Naranpura, Ahmedabad, Gujarat 380063', '23.060193', '72.547983', '798798564564', '09:00:00', '20:00:00', NULL, 'MEN', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:01:33'),
(6, 'Deepkala Hair Art', '12/B Janta Market Society, Vejalpur, Ahmedabad, Gujarat 380051', '23.003413', '72.518396', '989565656512', '08:00:00', '17:00:00', NULL, 'WOMEN', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:01:33');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_barber`
--

CREATE TABLE `tk_store_has_barber` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_barber` int(11) NOT NULL,
  `fk_barber_type` int(11) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_barber`
--

INSERT INTO `tk_store_has_barber` (`id`, `fk_store`, `fk_barber`, `fk_barber_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(2, 1, 2, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(3, 2, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(4, 2, 4, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(5, 3, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(6, 4, 4, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(7, 5, 5, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57'),
(8, 6, 6, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_services`
--

CREATE TABLE `tk_store_has_services` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(11) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_services`
--

INSERT INTO `tk_store_has_services` (`id`, `fk_store`, `fk_service`, `var_price`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '50', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:05'),
(2, 1, 2, '100', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:05'),
(3, 1, 3, '150', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:35'),
(4, 2, 4, '500', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:35'),
(5, 2, 5, '200', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:05:39'),
(6, 2, 6, '300', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:04:00'),
(7, 3, 1, '50', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:06:12'),
(8, 3, 2, '100', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:06:12'),
(9, 3, 3, '150', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(10, 4, 4, '200', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(11, 4, 5, '300', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(12, 4, 6, '550', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(13, 5, 1, '50', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:08:42'),
(14, 5, 2, '100', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:08:42'),
(15, 5, 3, '200', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(16, 6, 4, '300', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(17, 6, 5, '400', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:37'),
(18, 6, 6, '500', 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:37');

-- --------------------------------------------------------

--
-- Table structure for table `tk_users`
--

CREATE TABLE `tk_users` (
  `id` int(255) NOT NULL,
  `var_fname` varchar(255) DEFAULT NULL,
  `var_lname` varchar(255) DEFAULT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) NOT NULL,
  `int_otp` int(6) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) DEFAULT NULL,
  `var_longitude` varchar(255) DEFAULT NULL,
  `int_areacode` int(11) NOT NULL,
  `txt_address` text,
  `dt_dob` datetime DEFAULT NULL,
  `enum_geneder` enum('M','F') NOT NULL DEFAULT 'M',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_users`
--

INSERT INTO `tk_users` (`id`, `var_fname`, `var_lname`, `var_email`, `bint_phone`, `int_otp`, `var_profile_image`, `var_latitude`, `var_longitude`, `int_areacode`, `txt_address`, `dt_dob`, `enum_geneder`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, '9727190205', 235689, NULL, NULL, NULL, 382415, NULL, NULL, 'M', 'YES', '2017-11-04 10:34:55', '2017-11-04 10:34:55'),
(2, NULL, NULL, NULL, '1232', 932839, NULL, NULL, NULL, 123456, NULL, NULL, 'M', 'YES', '2017-11-04 11:11:27', '2017-11-04 11:11:27'),
(3, NULL, NULL, NULL, '123', 478673, NULL, NULL, NULL, 123456, NULL, NULL, 'M', 'YES', '2017-11-04 11:13:41', '2017-11-04 11:13:41'),
(4, NULL, NULL, NULL, '5', 710270, NULL, NULL, NULL, 555555, NULL, NULL, 'M', 'YES', '2017-11-04 11:14:08', '2017-11-04 11:14:08'),
(5, NULL, NULL, NULL, '222', 105711, NULL, NULL, NULL, 123455, NULL, NULL, 'M', 'YES', '2017-11-04 11:15:16', '2017-11-04 11:15:16'),
(6, NULL, NULL, NULL, '22', 588302, NULL, NULL, NULL, 555555, NULL, NULL, 'M', 'YES', '2017-11-04 11:20:00', '2017-11-04 11:20:00'),
(7, NULL, NULL, NULL, '1', 358550, NULL, NULL, NULL, 111111, NULL, NULL, 'M', 'YES', '2017-11-04 11:20:55', '2017-11-04 11:20:55'),
(8, NULL, NULL, NULL, '2', 180828, NULL, NULL, NULL, 222222, NULL, NULL, 'M', 'YES', '2017-11-04 11:22:04', '2017-11-04 11:22:04'),
(9, NULL, NULL, NULL, '3', 746438, NULL, NULL, NULL, 222222, NULL, NULL, 'M', 'YES', '2017-11-04 11:26:08', '2017-11-04 11:26:08'),
(10, NULL, NULL, NULL, '8888888', 922035, NULL, NULL, NULL, 888888, NULL, NULL, 'M', 'YES', '2017-11-04 11:36:31', '2017-11-04 11:36:31'),
(11, NULL, NULL, NULL, '555', 350745, NULL, NULL, NULL, 888885, NULL, NULL, 'M', 'YES', '2017-11-04 11:37:27', '2017-11-04 11:37:27'),
(12, NULL, NULL, NULL, '9898989898', 580997, NULL, NULL, NULL, 121212, NULL, NULL, 'M', 'YES', '2017-11-04 11:38:58', '2017-11-04 11:38:58'),
(13, NULL, NULL, NULL, '55', 195083, NULL, NULL, NULL, 232222, NULL, NULL, 'M', 'YES', '2017-11-04 12:23:13', '2017-11-04 12:23:13'),
(14, NULL, NULL, NULL, '99245454544', 819750, NULL, NULL, NULL, 123456, NULL, NULL, 'M', 'YES', '2017-11-04 12:58:07', '2017-11-04 12:58:07'),
(15, NULL, NULL, NULL, '5454', 663082, NULL, NULL, NULL, 454444, NULL, NULL, 'M', 'YES', '2017-11-04 13:00:10', '2017-11-04 13:00:10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tk_admin`
--
ALTER TABLE `tk_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_users`
--
ALTER TABLE `tk_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tk_admin`
--
ALTER TABLE `tk_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_services`
--
ALTER TABLE `tk_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_stores`
--
ALTER TABLE `tk_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tk_users`
--
ALTER TABLE `tk_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
