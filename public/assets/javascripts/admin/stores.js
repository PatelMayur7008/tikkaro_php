var storeDatatables = null;
var serviceDatatables = null;
var timeslotDatatables = null;
var timingDatatables = null;
var staffDatatables = null;
var customerDatatables = null;
var holidayDatatables = null;
var bookingDatatables = null;
var bankDatatables = null;
var packageDatetables = null;
var offerDatetables = null;


var Stores = function () {
    var handleDatatable = function () {

        storeDatatables = getDataTable('#stores_list', adminurl + 'stores/getStoreList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [12, "desc"]
                ],
            }
        });
        var oTable = $('#stores_list').dataTable();
        var bVis = oTable.fnSettings().aoColumns[12].bVisible;
        oTable.fnSetColumnVis( 12, bVis ? false : true )
    };

    var handleServiceTable = function () {
        var storeId = $('.storeId').val();
        serviceDatatables = getDataTable('#service_list', adminurl + 'stores/getStoreServiceList/'+storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var handleTimeslotTable = function () {
        var storeId = $('.storeId').val();
        timeslotDatatables = getDataTable('#timeslot_list', adminurl + 'stores/getStoreTimeSlotList/'+storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var general = function () {

    }
    var addStore = function () {
        var form = $('#addStoreFrm');
        var rules = {
            var_title: {required: true},
            var_phone: {required: true,number: true, maxlength: 10, minlength: 10},
            var_address: {required: true},
            opening_time: {required: true},
            closing_time: {required: true},
            store_type: {required: true},
            bussiness_id: {required: true},
            fk_category: {required: true},
            int_no_book_seats: {required: true,number: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form,true);
            return false;
        });
    };

    var add_store_service = function () {
        var form = $('#addStoreServiceFrm');
        var rules = {
            fk_service: {required: true},
            var_price: {required: true,number:true},
            service_time: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    };

    var manageStoreLogoUpdate = function () {

        $("body").on('click', '.userProfilePicture', function () {
            setTimeout(function () {
                $('.cusPicValue').show();
            }, 1000);
        });

        $('body').on('click', '.updatePics', function () {
            var file_data = $("#userProfileImage").prop("files")[0]; // Getting the properties of file from file field

            var form_data = new FormData(); // Creating object of FormData class
            var old_image = $('.oldImage').val();
            var expert_id = $('.user_id').val();
            form_data.append("profile_pic", file_data);
            form_data.append("var_profile_image", old_image);
            form_data.append("store_id", expert_id);
            var data_url = adminurl + 'stores/updateStoreLogoImage';
            // console.log(form_data); return false;
            if (typeof file_data !== 'undefined') {
                App.startPageLoading();
                $.ajax({
                    url: data_url,
                    type: "post",
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: form_data,
                    success: function (output) {
                        var output = output
                        if (output.status == "success") {
                            App.stopPageLoading();
                            $('.cusPicValue').hide();
                        }
                    }
                });
            } else {
                var image_url = baseurl + 'public/upload/expert_profile_pic/' + old_image;
                var img = '<img src=' + image_url + '>';
                $('.expertsProfilePicture').html(img);
                $('.cusPicValue').hide();
            }

        });

        $('body').on('click', '.cancelPics', function () {
            var old_image = $('.oldImage').val();
            if (old_image != '') {
                var image_url = baseurl + 'public/upload/expert_profile_pic/' + old_image;
                $('.expertsProfilePicture').find('img').attr('src', image_url);
            }
            $('.cusPicValue').hide();
        });

        $('body').on('click', '.address', function () {
            $(this).hide();
            $('.addressDiv').show();
        })
        $('body').on('click', '.addressSubmit', function () {
            var lng = $('#lng').val();
            var lat = $('#lat').val();
            var pincode = $('#pincode').val();
            var user_id = $('#u_id').val();
            var address = $('#formated_address').val();
            ajaxcall(adminurl + 'user/updateaddress',{lat:lat,lng:lng,pincode:pincode,address:address,user_id:user_id},function () {
                $('.address').show();
                $('.addressDiv').hide();
            });
        });
        $('body').on('click', '.cancel-add', function () {
            $('.address').show();
            $('.addressDiv').hide();
        });
    }

    var edit_store_service = function () {
        var form = $('#editStoreServiceFrm');
        var rules = {
            var_price: {required: true},
            service_time: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    };

    var add_store_timeslot = function () {
        var form = $('#addStoreTimeslotFrm');
        var rules = {
            fk_timeslot: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    };


    var dateformat = function () {
        $(".datetimepicker").datetimepicker({
            autoclose: true,
            format: "hh:ii",
            pickDate:false,
        });
    };

    var manageAutocomplete = function (){
        var options = {
            componentRestrictions: {
                country: "IN"
            }
        };

        var input = document.getElementById('var_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            //            console.log(place);
            //find state

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    if (place.address_components[i].types[j] == "postal_code") {
                        if(typeof place.address_components[i].long_name !== 'undefined'){
                            $('#pincode').val(place.address_components[i].long_name);
                        }

                    }
                }
            }
            $('#address').val(place.formatted_address);
            $('#lat').val(place.geometry.location.lat());
            $('#lng').val(place.geometry.location.lng());
            return false;
        });
    }

    var handleTimingTable = function(){
        var storeId = $('.storeId').val();
        timingDatatables = getDataTable('#timing_list', adminurl + 'stores/getTimingList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var edit_timing = function () {
        var form = $('#editStoreTimingFrm');
        var rules = {
            opening_time: {required: true},
            closing_time: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    };

    var handleTimePicker = function () {
        $('#opening_time').timepicker({
            showMeridian: false
        });
        $('#closing_time').timepicker({
            showMeridian: false
        });
    }
    var handleStaffDatatable = function(){
        var storeId = $('.storeId').val();
        staffDatatables = getDataTable('#staff_list', adminurl + 'stores/getStaffList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var addStaff = function () {
        var form = $('#addStorestaffFrm');
        var rules = {
            var_name: {required: true},
            var_phone: {required: true,number:true,maxlength:10,minlength:10},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form,true);
            return false;
        });
    };

    var editStaff = function () {
        var form = $('#editStorestaffFrm');
        var rules = {
            var_name: {required: true},
            var_phone: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form,true);
            return false;
        });
    };

    var handleDateFormate =function (){
        $('#var_str_date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
        $('#var_end_date').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    }

    var handleCustomerDatatable = function(){
        var storeId = $('.storeId').val();
        customerDatatables = getDataTable('#customer_list', adminurl + 'stores/getCustomerList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }

    var handleHolidayDatatable = function () {
        var storeId = $('.storeId').val();
        holidayDatatables = getDataTable('#holiday_list', adminurl + 'stores/getHolidayList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }

    var addholiday = function () {
        var form = $('#addHolidayFrm');
        var rules = {
            var_holiday_date: {required: true},
            closing_time: {required: true},
            opening_time: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    var editholiday = function () {
        var form = $('#editHolidayFrm');
        var rules = {
            var_holiday_date: {required: true},
            closing_time: {required: true},
            opening_time: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    var handleBookingDatatable = function(){
        var storeId = $('.storeId').val();
        bookingDatatables = getDataTable('#booking_list', adminurl + 'stores/getBookingList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }
    var handleBankDatatables = function(){
        var storeId = $('.storeId').val();
        bankDatatables = getDataTable('#bank_list', adminurl + 'stores/getBankList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }

    var updateBankDetail = function(){
        var form = $('#updateBankDetail');
        var rules = {
            var_acc_no: {required: true,number:true},
            var_name: {required: true},
            var_ifsc_code: {required: true},
            var_bank_name: {required: true},
            var_branch_name: {required: true},
            var_address: {required: true},
            acc_type: {required: true},
            var_card_name: {required: true},
            var_card_no: {required: true,number:true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }
    var manageOverviewAutocomplete = function (){
        var options = {
            componentRestrictions: {
                country: "IN"
            }
        };

        var input = document.getElementById('var_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            console.log(place);
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    if (place.address_components[i].types[j] == "postal_code") {
                        $('#pincode').val(place.address_components[i].long_name);
                    }
                }
            }
            $('#formated_address').val(place.formatted_address);
            $('#lat').val(place.geometry.location.lat());
            $('#lng').val(place.geometry.location.lng());
            //
            //             console.log(place.formatted_address)
            //            console.log("latlgn: " + place.geometry.location.lat() + "," + place.geometry.location.lng());
            return false;
        });

        $('body').on('click', '.address', function () {
            $(this).hide();
            $('.addressDiv').show();
        })
        $('body').on('click', '.addressSubmit', function () {
            var lng = $('#lng').val();
            var lat = $('#lat').val();
            var pincode = $('#pincode').val();
            var store_id = $('#store_id').val();
            var address = $('#formated_address').val();
            ajaxcall(adminurl + 'stores/updateaddress',{lat:lat,lng:lng,pincode:pincode,address:address,store_id:store_id},function () {
                $('.address').show();
                $('.addressDiv').hide();
            });
        });
        $('body').on('click', '.cancel-add', function () {
            $('.address').show();
            $('.addressDiv').hide();
        });
    }

    var handlePackageDatatable = function () {
        var storeId = $('.storeId').val();
        packageDatetables = getDataTable('#package_list', adminurl + 'stores/getPackageList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }

    var addPackage = function () {
        var form = $('#addPackageFrm');
        var rules = {
            var_name: {required: true},
            services: {required: true},
            var_price: {required: true,number:true},
            "services[]":{required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    var editPackage = function () {
        var form = $('#editPackageFrm');
        var rules = {
            var_name: {required: true},
            services: {required: true},
            var_price: {required: true,number:true},
            "services[]":{required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    //Offers
    var handleOfferDatatable = function () {
        var storeId = $('.storeId').val();
        offerDatetables = getDataTable('#offer_list', adminurl + 'stores/getOfferList/' + storeId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    }

    var addOffer = function () {
        var form = $('#addOfferFrm');
        var rules = {
            var_name: {required: true},
            services: {required: true},
            var_price: {required: true,number:true},
            var_str_date: {required: true},
            var_end_date: {required: true},
            "services[]":{required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    var editOffer = function () {
        var form = $('#editOfferFrm');
        var rules = {
            var_name: {required: true},
            services: {required: true},
            var_price: {required: true,number:true},
            var_str_date: {required: true},
            var_end_date: {required: true},
            "services[]":{required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }
    var handleAddImages = function(){
        var form = $('#uploadimagesfrm');
        var rules = {
            image_file: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
    }

    var handleDeleteImg = function(){
        $('body').on('click', '.delete-image', function () {
            var imgId = $(this).attr('data-galleryId');
            var file = $(this).attr('data-file');
            ajaxcall(adminurl + 'stores/deleteImage',{imgId:imgId,file:file},function (output) {
               handleAjaxResponse(output);
            })
        })
        $('body').on('click', '.make-profile', function () {
            var storeId = $(this).attr('data-storeId');
            var imageId = $(this).attr('data-imageId');
            ajaxcall(adminurl + 'stores/makeImageAsProfile',{storeId:storeId,imageId:imageId},function (output) {
               handleAjaxResponse(output);
            })
        })
    }

    var handleAddPortfolio =  function (){
        var form = $('#addPortfoliofrm');
        var rules = {
            image_file: {required: true},
            var_title: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
    }



    return {
        init: function () {
            handleDatatable();
            general();
            handleDelete();
        },
        Add_init: function () {
            addStore();
            manageAutocomplete();
            dateformat();
        },
        overview_init:function(){
            manageOverviewAutocomplete();
            manageStoreLogoUpdate();
        },
        service_init:function(){
            handleServiceTable();
            handleDelete();
        },
        transaction_init:function(){

        },
        Add_service_init:function(){
            add_store_service();
            handleTimePicker();
        },
        Edit_service_init:function(){
            edit_store_service();
            handleTimePicker();
        },
        timeslot_init:function () {
            handleTimeslotTable();
            handleDelete();
        },
        Add_time_init:function(){
            add_store_timeslot();
        },
        timinig_init:function(){
            handleTimingTable();
            edit_timing();
            handleTimePicker();
        },
        staff_init:function(){
            handleStaffDatatable();
            addStaff();
            handleDateFormate();
            editStaff();
            handleDelete();
        },
        bank_init:function(){
            handleBankDatatables();
            updateBankDetail();
        },
        customer_init:function(){
            handleCustomerDatatable();
        },
        holiday:function(){
            handleHolidayDatatable();
            addholiday();
            handleDateFormate();
            editholiday();
            handleDelete();
            handleTimePicker();
        },
        booking_list:function(){
            handleBookingDatatable();
            updateBankDetail();
        },
        review_init:function(){

        },
        package_init:function(){
            handlePackageDatatable();
            addPackage();
            handleDateFormate();
            editPackage();
            handleDelete();
        },
        offer_init:function(){
            handleOfferDatatable();
            addOffer();
            handleDateFormate();
            editOffer();
            handleDelete();
        },
        gallery_init:function(){
            handleAddImages();
            handleDeleteImg();
        },
        portfolio_init:function () {
            handleAddPortfolio();
        }
    };
}
();
