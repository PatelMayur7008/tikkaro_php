var usermanagerDatatables = null;
var storeDatatables = null;
var bookingDatatables = null;


var User = function () {

    var handleDatatable = function () {
        usermanagerDatatables = getDataTable('#user_list', adminurl + 'user/getUserList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
        
    };

    var dateformat = function () {
        $('#var_Manage_Dob').datepicker({
            format: 'dd/mm/yyyy',
            autoclose: true
        });
    };

    var addUser = function () {
        var form = $('#addUserFrm');
        var rules = {
            var_fname: {required: true},
            var_lname: {required: true},
            var_email: {required: true, email: true},
            var_phone: {required: true,number:true,maxlength:10,minlength:10},
            var_pin: {required: true,number:true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form,true);
            return false;
        });
    }

    var manageAutocomplete = function (){
        var options = {
            componentRestrictions: {
                country: "IN"
            }
        };

        var input = document.getElementById('var_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            //            console.log(place);
            //find state

            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
console.log(place);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    if (place.address_components[i].types[j] == "postal_code") {
                        if(typeof place.address_components[i].long_name !== 'undefined'){
                            $('#pincode').val(place.address_components[i].long_name);
                        }

                    }
                }
            }
            $('#address').val(place.formatted_address);
            $('#lat').val(place.geometry.location.lat());
            $('#lng').val(place.geometry.location.lng());
            //
            //             console.log(place.formatted_address)
            //            console.log("latlgn: " + place.geometry.location.lat() + "," + place.geometry.location.lng());
            return false;
        });
    }

    var manageOverviewAutocomplete = function (){
        var options = {
            componentRestrictions: {
                country: "IN"
            }
        };

        var input = document.getElementById('var_address');
        var autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {

            var place = autocomplete.getPlace();
            console.log(place);
            if (!place.geometry) {
                window.alert("Autocomplete's returned place contains no geometry");
                return;
            }
            console.log(place);
            for (var i = 0; i < place.address_components.length; i++) {
                for (var j = 0; j < place.address_components[i].types.length; j++) {
                    if (place.address_components[i].types[j] == "postal_code") {
                        $('#pincode').val(place.address_components[i].long_name);
                    }
                }
            }
            $('#formated_address').val(place.formatted_address);
            $('#lat').val(place.geometry.location.lat());
            $('#lng').val(place.geometry.location.lng());
            //
            //             console.log(place.formatted_address)
            //            console.log("latlgn: " + place.geometry.location.lat() + "," + place.geometry.location.lng());
            return false;
        });
    }

    var editAccount = function () {

        var form = $('#editAccountManagerFrm');
        var rules = {
            var_usrname: {required: true},
            var_email: {required: true, email: true},
            var_manage_dob: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }

    var userProfileImage = function () {

        $("body").on('click', '.userProfilePicture', function () {
            setTimeout(function () {
                $('.cusPicValue').show();
            }, 1000);
        });

        $('body').on('click', '.updatePics', function () {
            var file_data = $("#userProfileImage").prop("files")[0]; // Getting the properties of file from file field

            var form_data = new FormData(); // Creating object of FormData class
            var old_image = $('.oldImage').val();
            var expert_id = $('.user_id').val();
            form_data.append("profile_pic", file_data);
            form_data.append("var_profile_image", old_image);
            form_data.append("user_id", expert_id);
            var data_url = adminurl + 'user/updateUserProfileImage';
            // console.log(form_data); return false;
            if (typeof file_data !== 'undefined') {
                App.startPageLoading();
                $.ajax({
                    url: data_url,
                    type: "post",
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: form_data,
                    success: function (output) {
                        var output = output
                        if (output.status == "success") {
                            App.stopPageLoading();
                            $('.cusPicValue').hide();
                        }
                    }
                });
            } else {
                var image_url = baseurl + 'public/upload/expert_profile_pic/' + old_image;
                var img = '<img src=' + image_url + '>';
                $('.expertsProfilePicture').html(img);
                $('.cusPicValue').hide();
            }

        });

        $('body').on('click', '.cancelPics', function () {
            var old_image = $('.oldImage').val();
            if (old_image != '') {
                var image_url = baseurl + 'public/upload/expert_profile_pic/' + old_image;
                $('.expertsProfilePicture').find('img').attr('src', image_url);
            }
            $('.cusPicValue').hide();
        });

        $('body').on('click', '.address', function () {
            $(this).hide();
            $('.addressDiv').show();
        })
        $('body').on('click', '.addressSubmit', function () {
           var lng = $('#lng').val();
           var lat = $('#lat').val();
           var pincode = $('#pincode').val();
           var user_id = $('#u_id').val();
           var address = $('#formated_address').val();
           ajaxcall(adminurl + 'user/updateaddress',{lat:lat,lng:lng,pincode:pincode,address:address,user_id:user_id},function () {
               $('.address').show();
               $('.addressDiv').hide();
           });
        });
        $('body').on('click', '.cancel-add', function () {
            $('.address').show();
            $('.addressDiv').hide();
        });
    }

    var handleStoreDatatable = function () {
        var userId = $('#userId').val();
        storeDatatables = getDataTable('#user_list', adminurl + 'user/getStoreList/'+userId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var handleBookingDatatable = function () {
        var userId = $('#userId').val();
        bookingDatatables = getDataTable('#booking_list', adminurl + 'user/getBookingList/'+userId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };




    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        Add_init: function () {
            addUser();
            manageAutocomplete();
            dateformat();
        },
        Edit_init: function () {
            editAccount();
            dateformat();
        },
        overview_init:function () {
            manageOverviewAutocomplete();
            userProfileImage();
		},
        store_list:function (){
            handleStoreDatatable();
        },
        booking_init:function(){
            handleBookingDatatable();
        }

    };
}();
