var categoryDatatables = null;


var Category = function () {

    var handleDatatable = function () {
        categoryDatatables = getDataTable('#category_list', adminurl + 'category/getCategoryList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [0, "desc"]
                ],
            }
        });
    };

    var addCategory = function () {
        var form = $('#addCategoryFrm');
        var rules = {
            var_name: {required: true},
            category_type: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        Add_init: function () {
            addCategory();
        },
        overview_init:function () {
		}

    };
}();
