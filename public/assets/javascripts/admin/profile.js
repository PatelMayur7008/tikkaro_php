var Profile = function () {

    var profile_info = function () {

        var form = $('#admin_info');
        var rules = {
            email: {required: true, email: true},
            fname: {required: true},
            name: {required: true},
            lname: {required: true},
            var_manage_dob: {required: true},
            phone: {number: true},
            address: {required: true}
        };
        handleFormValidate(form, rules);
    };

    var change_pass = function () {
        var form = $('#change_password');
        var rules = {
            old_pwd: {required: true},
            new_pwd: {required: true},
            conf_pwd: {required: true, equalTo: "#new_pwd"}
        };
        handleFormValidate(form, rules);
    };

    var change_image = function () {

        // $('.profilePicture').change(function (e) {
        var form = $('#change_picture');
        var rules = {
            // 'profilePicture': {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
        // });
    };
    return {
        //main function to initiate the module
        init: function () {
            profile_info();
            change_pass();
            change_image();
        },
    };
}();
