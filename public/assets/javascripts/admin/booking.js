var bookingDatatables = null;


var Booking = function () {

    var handleDatatable = function () {
        bookingDatatables = getDataTable('#booking_list', adminurl + 'booking/getBookingList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [0, "desc"]
                ],
            }
        });
    };

    var HandleCancelBooking = function(){
        $('#cancel_model').modal('hide');
        $('body').on('click', '.cancel_booking_link', function() {
            $('#cancel_model').modal('show');
            var bookId = $(this).attr('data-id');
            $('.bookingId').val(bookId);
        });
        $('body').on('click', '.CancelBooking', function() {
            var bookId = $('.bookingId').val();
            ajaxcall(adminurl + 'booking/cacelBooking',{bookId:bookId},function(){
                $('cancel_model').css('display','none');
                $( ".cancel_booking_model" ).trigger( "click" );
                bookingDatatables.refresh();
            })
        });

    }

    return {
        init: function () {
            handleDatatable();
            HandleCancelBooking();
        }

    };
}();
