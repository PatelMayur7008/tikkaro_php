var facilityDatatables = null;


var Facility = function () {

    var handleDatatable = function () {
        facilityDatatables = getDataTable('#facility_list', adminurl + 'facility/getFacilityList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [0, "desc"]
                ],
            }
        });
    };

    var addFacility = function () {
        var form = $('#addFacilityFrm');
        var rules = {
            var_name: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }


    var editAccount = function () {

        var form = $('#editAccountManagerFrm');
        var rules = {
            var_usrname: {required: true},
            var_email: {required: true, email: true},
            var_manage_dob: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        Add_init: function () {
            addFacility();
        },
        overview_init:function () {
		}

    };
}();
