var timeslotDatatables = null;


var Timeslot = function () {

    var handleDatatable = function () {
        timeslotDatatables = getDataTable('#timeslot_list', adminurl + 'time-slot/getTimeSlotList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [0, "asc"]
                ],
            }
        });
    };

    var dateformat = function () {
        $(".datetimepicker").datetimepicker({
            autoclose: true,
            format: "hh:ii",
        });
    };

    var addTime = function () {
        var form = $('#addTimeslotFrm');
        var rules = {
            time_slot: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }

    var editTime = function () {
        var form = $('#editTimeslotFrm');
        var rules = {
            time_slot: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();
            dateformat();
        },
        Add_init: function () {
            addTime();
            dateformat();
        },
        edit_init: function () {
            editTime();
            dateformat();
        },
        overview_init: function () {
        }

    };
}();
