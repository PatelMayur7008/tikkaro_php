var serviceDatatables = null;


var Service = function () {

    var handleDatatable = function () {
        serviceDatatables = getDataTable('#service_list', adminurl + 'service/getServiceList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [0, "desc"]
                ],
            }
        });
    };

    var addService = function () {
        var form = $('#addServiceFrm');
        var rules = {
            var_title: {required: true},
            fk_category: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form,true);
            return false;
        });
    }


    var editAccount = function () {

        var form = $('#editAccountManagerFrm');
        var rules = {
            var_usrname: {required: true},
            var_email: {required: true, email: true},
            var_manage_dob: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        Add_init: function () {
            addService();
        },
        overview_init:function () {
		}

    };
}();
