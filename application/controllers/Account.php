<?php

class Account extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('AuthLibrary');
		$this->load->helper('cookie');
	}

	function index()
	{
		if (isset($this->session->userdata['valid_login_user'])) {
			redirect('dashboard');
		} else {
			$this->login();
		}
	}

	function login()
	{
		$data['page'] = "admin/account/login";
		$data['var_meta_title'] = 'Login';
		$data['var_meta_description'] = 'Login';
		$data['var_meta_keyword'] = 'Login';
		$data['js'] = array(
			'admin/login.js'
		);
		$data['js_plugin'] = array();
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['init'] = array(
			'Login.init()'
		);
		if ($this->input->post()) {
			$this->email = $this->input->post('username');
			$this->password = $this->input->post('password');
			$this->remember = $this->input->post('remember');
			if ($this->email && $this->password) {

				if ($row = $this->authlibrary->loginAttempt($this->email, $this->password)) {

					if (!empty($row)) {
						if ($row['enum_enable'] == 'YES') {
							$this->json_response['status'] = 'success';
							$this->json_response['message'] = 'Well Done Login Successfully Done..';
							$this->json_response['redirect'] = base_url() . 'dashboard';
							echo json_encode($this->json_response);
							exit;
						} else {
							$this->json_response['status'] = 'warning';
							$this->json_response['message'] = 'Account has been disabled please contact to Administrator';
							echo json_encode($this->json_response);
							exit;
						}
					} else {
						$this->json_response['status'] = 'error';
						$this->json_response['message'] = 'Username and password does not match';
						echo json_encode($this->json_response);
						exit;
					}
				} else {
					$this->json_response['status'] = 'error';
					$this->json_response['message'] = 'Username and password does not match';
					echo json_encode($this->json_response);
					exit;
				}
			}
		} else {

			$this->load->view(ADMIN_LAYOUT_LOGIN, $data);
		}
	}

	function logout($type)
	{

		$this->session->unset_userdata('valid_' . $type);
		redirect(base_url());
	}

	function forgot_password()
	{

		$email = $this->input->post('forgetemail');

		$query = $this->db->get_where('admin', array('var_email' => $email));

//		$query1 = $this->db->get_where('account_manager', array('var_email' => $email));

		if ($query->num_rows() > 0) {

			$rowArray = $query->row_array();
			$data['forgot_data'] = $rowArray;


			$auto_pass = autoGeneratePassword();

			$data['reset_password'] = $auto_pass;


			$updatepassword = array(
				'var_password' => md5($auto_pass)
			);
			$this->db->where('id', $rowArray['id']);
			$this->db->update('admin', $updatepassword);

			$mail_body = $this->load->view('email-templates/admin-reset-password', $data, TRUE);


			$this->load->library('Mylibrary');
			$configs['to'] = $rowArray['var_email'];
			$configs['subject'] = 'Admin reset password';
			$configs['mail_body'] = $mail_body;
			$sendMail = $this->mylibrary->sendMail($configs);


			$redirect = base_url('admin');
			$json_response['status'] = 'success';
			$json_response['redirect'] = $redirect;
			$json_response['message'] = 'Successfully Admin Reset Password';

		} else {


			$query1 = $this->db->get_where('account_manager', array('var_email' => $email));


			if ($query1->num_rows() > 0) {

				$rowArray = $query1->row_array();
				$data['forgot_data'] = $rowArray;


				$auto_pass = autoGeneratePassword();

				$data['reset_password'] = $auto_pass;


				$updatepassword = array(
					'var_password' => md5($auto_pass)
				);
				$this->db->where('id', $rowArray['id']);
				$this->db->update('account_manager', $updatepassword);

				$mail_body = $this->load->view('email-templates/admin-reset-password', $data, TRUE);


				$this->load->library('Mylibrary');
				$configs['to'] = $rowArray['var_email'];
				$configs['subject'] = 'Account Manager reset password';
				$configs['mail_body'] = $mail_body;
				$sendMail = $this->mylibrary->sendMail($configs);


				$redirect = base_url('admin');
				$json_response['status'] = 'success';
				$json_response['redirect'] = $redirect;
				$json_response['message'] = 'Successfully Account Manager Reset Password';
				echo json_encode($json_response);
				exit;

			}

			$json_response['status'] = 'error';
			$json_response['message'] = 'Email Not Available in Admin';
		}

		echo json_encode($json_response);

	}
}

?>
