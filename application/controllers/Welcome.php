<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller
{

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *        http://example.com/index.php/welcome
     *    - or -
     *        http://example.com/index.php/welcome/index
     *    - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->load->view('email-templates/client/bio-sent');
    }

    function calls()
    {

        $startTime = '2017-06-11T19:06:47.605369+00:00';
        $endTime = '2017-06-11T20:06:47.605371+00:00';

        $client = new GuzzleHttp\Client();
        $body = ['start' => $startTime,
            'end' => $endTime];

        $res = $client->request(
            'POST',
            'https://hqjqqoap0i.execute-api.eu-west-1.amazonaws.com/dev/call',
            ['headers' => [
                'Accept' => 'application/json',
                'Content-type' => 'application/json',
//				'x-api-key' => 'XXXX-XXXX-XXXX-XXXX',
//				'x-api-key' => '0000-0000--0000-0000-0000'
            ],
                'body' => json_encode($body)
            ]
        );
//        $contents = (string) $res->getBody();
//        $contents = $res->getBody()->getContents();
        echo $res->getBody();
        exit;

//        $this->load->library('twiliocallslibrary');
//
//        $start = date('Y-m-d H:i:s');
//        $end = date('Y-m-d H:i:s', strtotime('+2 hour', strtotime($start)));
//
//        $d = new DateTime($start);
//        $startDate = $d->format('Y-m-d\TH:i:s.u+00.00'); // 2011-01-01T15:03:01.012345
//
//        $d = new DateTime($end);
//        $endDate = $d->format('Y-m-d\TH:i:s.u+00.00'); // 2011-01-01T15:03:01.012345
//
////        echo $startDate .' =>'.  $endDate; exit;
////        echo "<pre>";
//        $test = $this->twiliocallslibrary->createCall('2017-06-11T19:06:47.605369+00:00', '2017-06-11T20:06:47.605371+00:00');
//        echo "<pre>";
//        print_r($test->body);
//        exit;

    }

    function getCall()
    {
//        echo "enter";exit;
        $id = '597c1ffabebafe00090bf79b';
        $this->load->library('TwilioCallsLibrary');

        $response = $this->twiliocallslibrary->getCall($id);
        $response = json_decode($response);
//        echo $response->read(2048);
//        var_export($response->eof());
//        exit;
        echo "<pre>";
        print_r($response);
        exit;
    }

    function createCall()
    {
        $this->load->library('TwilioCallsLibrary');
        $dataTime = date('Y-m-d H:i:s');

        $startDate = date('Y-m-d\TH:i:s.u+00.00', strtotime($dataTime));
        $endDate = date('Y-m-d\TH:i:s.u+00.00', strtotime('+1 hour', strtotime($dataTime)));
//        echo $startDate .' => '.$endDate; exit;
        $response = $this->twiliocallslibrary->createCall($startDate, $endDate);
        $response = json_decode($response);

        echo "<pre>";
        print_r($response);
        exit;
    }


    function deleteCalls()
    {
        $id = '597c1ffabebafe00090bf79b';
        $this->load->library('TwilioCallsLibrary');
        $response = $this->twiliocallslibrary->deleteCall($id);
        echo "<pre>";
        print_r($response);
        exit;
    }

    function callresponse()
    {
        $jsonstring = '{"number": "+442382148999", "call_id": "F4X0UZ882LPM905VIBH8", "passcode": "9890", "day": "06/06/17", "status": "scheduled", "lock-interval": {"start": "2017-06-06T18:06:47.605369+00:00", "end": "2017-06-06T21:06:47.605371+00:00"}, "talk-interval": {"start": "2017-06-06T19:06:47.605369+00:00", "end": "2017-06-06T20:06:47.605371+00:00"}}';
        $arr = json_decode($jsonstring);
        echo "<pre>";
        print_r($arr);
        exit;
    }

    function dateTime()
    {

        $dateTime = convert_date('2017-06-16 04:00:00', 'America/Adak', 'UTC');
        $dateTime1 = convert_date($dateTime, 'UTC', 'America/Adak');
        echo date('Y-m-d H:i:s', strtotime($dateTime));
//        echo "<pre>";
//        echo date('Y-m-d H:i:s', strtotime($dateTime1));

        exit;
    }
}
