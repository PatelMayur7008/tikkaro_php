<?php

header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');

class Api_front extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('API_model');
    }

    public function index() {
        
    }

    function checkAuthentication() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);

        $res = $this->API_model->checkAuthentication($postData);
        echo json_encode($res);
        exit;
    }

    function authentication() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->authentication($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreList() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStoreList($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreListDetailById() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);

        $res = $this->API_model->getStoreList($postData);
        echo json_encode($res);
        exit;
    }

    function getTimeSlotsByStoreId() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);

        $res = $this->API_model->getTimeSlotsByStoreId($postData);
        echo json_encode($res);
        exit;
    }

    function getAppoinmentbyUserId() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getAppoinmentbyUserId($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getBookingTimeSlote() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getBookingTimeSlote($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function bookStoreService() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->bookStoreService($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateUserAccountDetail() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->updateUserAccountDetail($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getUserAccountDetail() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserAccountDetail($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateUserAddressDetail() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->updateUserAddressDetail($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function userFavoriteStore() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->userFavoriteStore($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addReview() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->addReview($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreReview() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStoreReview($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreReviewByUserId() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStoreReviewByUserId($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getUserFavoriteStore() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserFavoriteStore($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateUserProfilePic() {
        $res = $this->API_model->updateUserProfilePic($this->input->post());
        echo json_encode($res);
        exit;
    }

    function getStoreCategory() {
        $res = $this->API_model->getStoreCategory();
        echo json_encode($res);
        exit;
    }

    function getUserLastAppoinment() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserLastAppoinment($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getNearestStore() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getNearestStore($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function sentPushyId() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->sentPushyId($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getPaymentPopupInit() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getPaymentPopupInit($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStorePortfolio(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStorePortfolio($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addReviewOnPortfolio(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->addReviewOnPortfolio($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function toggleUserNotification(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->toggleUserNotification($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getUserNotificationData(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserNotificationData($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getUserDataFromMob(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserDataFromMob($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStorePackages(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStorePackages($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreOffers(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getStoreOffers($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function makePayment(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->makePayment($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getUserPaymentHistory(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->getUserPaymentHistory($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function calculationOfStoreReview($storeId){
        $res = $this->API_model->calculationOfStoreReview($storeId);
    }

    function togglePortfolioLike(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->API_model->togglePortfolioLike($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

}

?>
