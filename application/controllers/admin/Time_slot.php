<?php

class Time_slot extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Timeslot_model');
	}

	function index()
	{
		$data['page'] = "admin/timeslot/list";
		$data['breadcrumb'] = 'TimeSlot';
		$data['breadcrumb_sub'] = 'TimeSlot';
		$data['breadcrumb_list'] = array(
			array('TimeSlot', ''),
			array('List', ''),
		);
		$data['timeslot'] = 'active open';
		$data['timeslot'] = 'active';
		$data['var_meta_title'] = 'Time List';
		$data['var_meta_description'] = 'Time List';
		$data['var_meta_keyword'] = 'Time List';
		$data['js'] = array(
			'admin/timeslot.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Timeslot.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getTimeSlotList()
	{
		$this->load->library('Datatables');
		echo $this->Timeslot_model->getTimeslotList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/timeslot/add";
		$data['breadcrumb'] = 'Timeslot';
		$data['breadcrumb_sub'] = 'Timeslot';
		$data['breadcrumb_list'] = array(
			array('Timeslot', 'Timeslot'),
			array('List', ''),
		);
		$data['timeslot'] = 'active open';
		$data['timeslot'] = 'active';
		$data['var_meta_title'] = 'Timeslot Add';
		$data['var_meta_description'] = 'Timeslot Add';
		$data['var_meta_keyword'] = 'Timeslot Add';

		$data['js'] = array(
			'admin/timeslot.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
			'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',

        );
		$data['js_plugin'] = array(
			'jquery.form.min.js',
			'ajaxfileupload.js',
			'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
		);
		$data['init'] = array(
			'Timeslot.Add_init()',
			'ComponentsDateTimePickers.init()',
		);

		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->Timeslot_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

    function edit($id)
    {
        $data['page'] = "admin/timeslot/edit";
        $data['breadcrumb'] = 'Timeslot';
        $data['breadcrumb_sub'] = 'Timeslot';
        $data['breadcrumb_list'] = array(
            array('Timeslot', 'Timeslot'),
            array('List', ''),
        );
        $data['timeslot'] = 'active open';
        $data['timeslot'] = 'active';
        $data['var_meta_title'] = 'Timeslot Edit';
        $data['var_meta_description'] = 'Timeslot Edit';
        $data['var_meta_keyword'] = 'Timeslot Edit';

        $data['js'] = array(
            'admin/timeslot.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',

        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js',
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['init'] = array(
            'Timeslot.edit_init()',
            'ComponentsDateTimePickers.init()',
        );
        $data['time_slot'] = $this->db->get_where('timeslots',array('id'=>$id))->row_array();
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Timeslot_model->edit($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

	function overview($id){
            $userData = $this->toval->idtorowarr('id', $id, 'users');
            if (empty($userData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'User Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('user'));
            }

            $data['page'] = "admin/user/overview";
            $data['breadcrumb'] = 'User';
            $data['breadcrumb_sub'] = 'User';
            $data['breadcrumb_list'] = array(
                array('User', 'client'),
                array('Overview', ''),
            );
            $data['user'] = 'active open';
            $data['user'] = 'active';
            $data['var_meta_title'] = 'User Overview';
            $data['var_meta_description'] = 'User Overview';
            $data['var_meta_keyword'] = 'User Overview';
            $data['js'] = array(
                'admin/user.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array();
            $data['js_plugin'] = array();
            $data['userId'] = $id;
            $data['userData'] = $this->Timeslot_model->getUserCommonHeader($id);
            $data['init'] = array(
                'User.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }

	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Timeslot_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
