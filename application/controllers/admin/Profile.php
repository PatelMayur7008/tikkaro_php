<?php

class Profile extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('authlibrary');
        $this->load->model('admin/Profile_model');
    }

    function index()
    {
        $data['page'] = "admin/profile/profile";
        $data['breadcrumb'] = 'Profile';
        $data['breadcrumb_sub'] = 'Profile';
        $data['breadcrumb_list'] = array(
            array('Profile', ''),
        );

        $data['var_meta_title'] = 'Profile';
        $data['var_meta_description'] = 'Profile';
        $data['var_meta_keyword'] = 'Profile';
        $data['js'] = array(
            'admin/profile.js'
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'ComponentsDateTimePickers.init()',
            'Profile.init()',
        );
        $data['profiles'] = $this->db->get_where('admin', array('id' => $this->session->userdata['valid_login_user']['id']))->row_array();

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function editDetail()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $json_response = $this->Profile_model->editProfile($this->input->post(), $this->json_response);
            echo json_encode($json_response);
            exit;
        }
    }

    public function editPassword()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $json_response = $this->Profile_model->editProfilePassword($this->input->post(), $this->json_response);
            echo json_encode($json_response);
            exit;
        }
    }

    public function editPicture()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $json_response = $this->Profile_model->editProfilePicture($this->input->post(), $this->json_response);
            echo json_encode($json_response);
            exit;
        }
    }
}
