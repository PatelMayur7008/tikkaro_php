<?php

class Bussiness extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Bussiness_model');
	}

	function index()
	{
		$data['page'] = "admin/bussiness/list";
		$data['breadcrumb'] = 'Bussiness';
		$data['breadcrumb_sub'] = 'Bussiness';
		$data['breadcrumb_list'] = array(
			array('Bussiness', ''),
			array('List', ''),
		);
		$data['bussiness'] = 'active open';
		$data['bussiness'] = 'active';
		$data['var_meta_title'] = 'Bussiness List';
		$data['var_meta_description'] = 'Bussiness List';
		$data['var_meta_keyword'] = 'Bussiness List';
		$data['js'] = array(
			'admin/bussiness.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Bussiness.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getBussinessList()
	{
		$this->load->library('Datatables');
		echo $this->Bussiness_model->getBussinessList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/bussiness/add";
		$data['breadcrumb'] = 'Bussiness';
		$data['breadcrumb_sub'] = 'Bussiness';
		$data['breadcrumb_list'] = array(
			array('Bussiness', ''),
			array('ADD', ''),
		);
		$data['bussiness'] = 'active open';
		$data['bussiness'] = 'active';
		$data['var_meta_title'] = 'Bussiness Add';
		$data['var_meta_description'] = 'Bussiness Add';
		$data['var_meta_keyword'] = 'Bussiness Add';

		$data['js'] = array(
			'admin/bussiness.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
			'bootstrap-fileinput/bootstrap-fileinput.css'
		);
		$data['js_plugin'] = array(
			'jquery.form.min.js',
			'ajaxfileupload.js',
			'bootstrap-fileinput/bootstrap-fileinput.js'
		);
		$data['init'] = array(
			'Bussiness.Add_init()',
			'ComponentsDateTimePickers.init()',
		);
        $data['category'] = $this->Bussiness_model->getAllCategory();
		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->Bussiness_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function overview($id){
            $userData = $this->toval->idtorowarr('id', $id, 'bussiness');
            if (empty($userData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'Bussiness Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('bussiness'));
            }

            $data['page'] = "admin/bussiness/overview";
            $data['breadcrumb'] = 'Bussiness';
            $data['breadcrumb_sub'] = 'Bussiness';
            $data['breadcrumb_list'] = array(
                array('Bussiness', 'client'),
                array('Overview', ''),
            );
            $data['bussiness'] = 'active open';
            $data['bussiness'] = 'active';
            $data['var_meta_title'] = 'Bussiness Overview';
            $data['var_meta_description'] = 'Bussiness Overview';
            $data['var_meta_keyword'] = 'Bussiness Overview';
            $data['js'] = array(
                'admin/bussiness.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array(
                'bootstrap-fileinput/bootstrap-fileinput.css',
            );
            $data['js_plugin'] = array(
                'jquery.form.min.js',
                'ajaxfileupload.js',
                'bootstrap-fileinput/bootstrap-fileinput.js'
            );
            $data['bussinessId'] = $id;
            $data['BussinessData'] = $this->Bussiness_model->getBussinessData($id);
            $data['BussinessCategory'] = $this->Bussiness_model->getBussinessCategories($id);
            $data['Allcategory'] = $this->Bussiness_model->getAllCategory();

    //            echo '<pre/>';
    //            print_r($data['BussinessData']);
    //            print_r($data['BussinessCategory']);
    //            print_r($data['Allcategory']);
    //            exit;
            $data['init'] = array(
                'Bussiness.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }


	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Bussiness_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
