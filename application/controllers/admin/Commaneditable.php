<?php

class Commaneditable extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Comman_editable');
    }

    function editable()
    {

        if ($this->input->post()) {
            $result = $this->Comman_editable->inlineEdit($this->input->post());
            if ($this->input->post('field') == 'var_project_name') {
                echo "success";
            }
            if (is_array($result)) {
                echo json_encode($result);
            }
        }
    }

    function editableFnameLname()
    {
        if ($this->input->post()) {
            $result = $this->Comman_editable->inlineEditFnameLname($this->input->post());
        }
    }

    function editableSelectbox()
    {
        if ($this->input->post()) {
            $result = $this->Comman_editable->inlineEditSelect($this->input->post());
        }
    }

    function handleCompliance()
    {
        if ($this->input->post()) {
            $result = $this->Comman_editable->inlineCompliance($this->input->post());
            if (is_array($result)) {
                echo json_encode($result);
            }
        }
    }

    function handleDuplicate()
    {
        if ($this->input->post()) {
            $result = $this->Comman_editable->handleDuplicate($this->input->post());
            echo json_encode($result);
            exit;
        }
    }
    function handleInvoiceEditable()
    {
        if ($this->input->post()) {
            $result = $this->Comman_editable->inlineEditInvoice($this->input->post());
            if (is_array($result)) {
                echo json_encode($result);
            }
            exit;
        }
    }
}

?>