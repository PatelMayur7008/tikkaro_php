<?php

class Stores extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Store_model');
    }

    function index()
    {
        $data['page'] = "admin/stores/list";
        $data['breadcrumb'] = 'Stores';
        $data['breadcrumb_sub'] = 'Stores';
        $data['breadcrumb_list'] = array(
            array('Stores', ''),
            array('List', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Stores List';
        $data['var_meta_description'] = 'Stores List';
        $data['var_meta_keyword'] = 'Stores List';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
            "bootstrap-switch/css/bootstrap-switch.css"
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            "bootstrap-switch/js/bootstrap-switch.js"
        );
        $data['init'] = array(
            'Stores.init()',
            'ComponentsDateTimePickers.init()',
        );
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getStoreList()
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getStoreList();
        exit;
    }

    function add()
    {
        $data['page'] = "admin/stores/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Add';
        $data['var_meta_description'] = 'Store Add';
        $data['var_meta_keyword'] = 'Store Add';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'Stores.Add_init()',
            'ComponentsDateTimePickers.init()',
        );
        $data['bussinessData'] = $this->db->get('bussiness')->result_array();
        $data['categoryData'] = $this->db->get_where('master_category',array('enum_type'=>'STORE'))->result_array();
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->add($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function overview($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/overview";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Overview';
        $data['var_meta_description'] = 'Store Overview';
        $data['var_meta_keyword'] = 'Store Overview';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['storeId'] = $id;
        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.overview_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function updateStoreLogoImage(){
        $var_image = ($this->input->post('var_profile_image')) ? $this->input->post('var_profile_image') : '';
        if ($_FILES['profile_pic']['name'] != "") {
            $image = upload_single_image($_FILES, 'store_logo', STORE_LOGO_IMG, FALSE);
            $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

            if ($this->input->post('var_profile_image') != '') {
                delete_single_image(STORE_LOGO_IMG, $this->input->post('var_profile_image'));
            }
        }

        $data_array = array(
            'var_logo' => $var_image,
        );

        $this->db->where('id',$this->input->post('store_id'));
        $result =  $this->db->update('stores',$data_array);
        if($result){
            $json_response['status'] = 'success';
            echo json_encode($json_response);
            exit;
        }
    }

    function updateaddress(){
        $res = $this->Store_model->updateStoreAddress($this->input->post());
        echo json_encode($res);
        exit;
    }

    function timing($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/timing";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Timing';
        $data['var_meta_description'] = 'Store Timing';
        $data['var_meta_keyword'] = 'Store Timing';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.timinig_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getTimingList($id)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getTimingList($id);
        exit;
    }

    function edit_timing($id)
    {
        $data['page'] = "admin/stores/timing_edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Edit';
        $data['var_meta_description'] = 'Store Edit';
        $data['var_meta_keyword'] = 'Store Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['init'] = array(
            'Stores.timinig_init()',
            'ComponentsDateTimePickers.init()',
        );
        $data['timing_id'] = $id;
        $data['timing_detail'] = $this->db->get_where('store_has_timing', array('id' => $id))->row_array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->editTiming($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }


    function services($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/service";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Service';
        $data['var_meta_description'] = 'Store Service';
        $data['var_meta_keyword'] = 'Store Service';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.service_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_service($storeId)
    {
        $data['page'] = "admin/stores/service_add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Service Add';
        $data['var_meta_description'] = 'Store Service Add';
        $data['var_meta_keyword'] = 'Store Service Add';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['storeId'] = $storeId;
        $data['service_list'] = $this->Store_model->getAllService($storeId);
        $data['init'] = array(
            'Stores.Add_service_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addService($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function edit_service($id)
    {
        $data['page'] = "admin/stores/service_edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Edit';
        $data['var_meta_description'] = 'Store Edit';
        $data['var_meta_keyword'] = 'Store Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['init'] = array(
            'Stores.Edit_service_init()',
            'ComponentsDateTimePickers.init()',
        );
        $data['service_id'] = $id;
        $data['service_detail'] = $this->db->get_where('store_has_services', array('id' => $id))->row_array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->editService($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getStoreServiceList($id)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getStoreServiceList($id);
        exit;
    }

    function transactions($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/transactions";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Transactions';
        $data['var_meta_description'] = 'Store Transactions';
        $data['var_meta_keyword'] = 'Store Transactions';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.transaction_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function time_slot($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/time_slot";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store TimeSlot';
        $data['var_meta_description'] = 'Store TimeSlot';
        $data['var_meta_keyword'] = 'Store TimeSlot';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.timeslot_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_timeslot($storeId)
    {
        $data['page'] = "admin/stores/add_timeslot";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store TimeSlot Add';
        $data['var_meta_description'] = 'Store TimeSlot Add';
        $data['var_meta_keyword'] = 'Store TimeSlot Add';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['storeId'] = $storeId;
        $data['timeslot_list'] = $this->Store_model->getAllTimeSlot();
        $data['init'] = array(
            'Stores.Add_time_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addTimeslot($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function staff($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/staff/staff";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Staff';
        $data['var_meta_description'] = 'Store Staff';
        $data['var_meta_keyword'] = 'Store Staff';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.staff_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_staff($storeId)
    {
        $data['page'] = "admin/stores/staff/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store staff Add';
        $data['var_meta_description'] = 'Store staff Add';
        $data['var_meta_keyword'] = 'Store staff Add';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['storeId'] = $storeId;
        $data['init'] = array(
            'Stores.staff_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addStaff($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function edit_staff($staffId)
    {
        $data['page'] = "admin/stores/staff/edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store staff Edit';
        $data['var_meta_description'] = 'Store staff Edit';
        $data['var_meta_keyword'] = 'Store staff Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['staff_data'] = $this->db->get_where('store_has_staff', array('id' => $staffId))->row_array();
        $data['storeId'] = $data['staff_data']['fk_store'];
        $data['init'] = array(
            'Stores.staff_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->updateStaff($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function package($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/package/list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('package', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store package';
        $data['var_meta_description'] = 'Store package';
        $data['var_meta_keyword'] = 'Store package';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.package_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getPackageList($storeId){
        $this->load->library('Datatables');
        echo $this->Store_model->getPackageList($storeId);
        exit;
    }

    function add_package($storeId)
    {
        $data['page'] = "admin/stores/package/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store package Add';
        $data['var_meta_description'] = 'Store package Add';
        $data['var_meta_keyword'] = 'Store package Add';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            //'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
        );
        $data['js_plugin'] = array(
            //'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['storeId'] = $storeId;
        $data['service'] = $this->Store_model->getStoreService($storeId);
        $data['init'] = array(
            'Stores.package_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addPackage($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function delete_package(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->deletePackage($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function edit_package($packageId)
    {
        $data['page'] = "admin/stores/package/edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store package Edit';
        $data['var_meta_description'] = 'Store package Edit';
        $data['var_meta_keyword'] = 'Store package Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
         //   'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
        );
        $data['js_plugin'] = array(
         //   'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['storeId'] = $this->db->get_where('store_has_package',array('id'=>$packageId))->row_array()['fk_store'];
        $data['package_data'] = $this->Store_model->getPackageInfo($packageId);
        $data['service'] = $this->Store_model->getStoreService($data['storeId']);
//        print_array($data['package_data']);
        $data['init'] = array(
            'Stores.package_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->updatePackage($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function offer($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/offer/list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('package', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store offer';
        $data['var_meta_description'] = 'Store offer';
        $data['var_meta_keyword'] = 'Store offer';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.offer_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getOfferList($storeId){
        $this->load->library('Datatables');
        echo $this->Store_model->getOfferList($storeId);
        exit;
    }

    function add_offer($storeId)
    {
        $data['page'] = "admin/stores/offer/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store offer Add';
        $data['var_meta_description'] = 'Store offer Add';
        $data['var_meta_keyword'] = 'Store offer Add';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['storeId'] = $storeId;
        $data['service'] = $this->Store_model->getStoreService($storeId);
        $data['init'] = array(
            'Stores.offer_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addOffer($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function delete_offer(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->deleteOffer($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function edit_offer($offerId)
    {
        $data['page'] = "admin/stores/offer/edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store offer Edit';
        $data['var_meta_description'] = 'Store offer Edit';
        $data['var_meta_keyword'] = 'Store offer Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
               'bootstrap-datetimepicker/css/bootstrap-datetimepicker.css',
        );
        $data['js_plugin'] = array(
               'bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js',
        );
        $data['storeId'] = $this->db->get_where('store_has_offer',array('id'=>$offerId))->row_array()['fk_store'];
        $data['offer_data'] = $this->Store_model->getOfferInfo($offerId);
//        print_array($data['offer_data']);
        $data['service'] = $this->Store_model->getStoreService($data['storeId']);
        $data['init'] = array(
            'Stores.offer_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->updateOffer($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function gallery($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/gallery/list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Gallery', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Gallery';
        $data['var_meta_description'] = 'Store Gallery';
        $data['var_meta_keyword'] = 'Store Gallery';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
        $data['gallery_data'] = $this->Store_model->getStoreGallery($id);
//        print_array($data['gallery_data']);
        $data['init'] = array(
            'Stores.gallery_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_image($storeId){
        $data['page'] = "admin/stores/gallery/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', '')
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'gallery';
        $data['var_meta_description'] = 'gallery';
        $data['var_meta_keyword'] = 'gallery';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['store_id'] = $storeId;
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
        );
        $data['init'] = array(
            'Stores.gallery_init()',
        );


        $this->load->view(ADMIN_LAYOUT, $data);
    }
    function uploadImages(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->addImages($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteImage(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->deleteImage($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }
    function makeImageAsProfile(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->makeImageAsProfile($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function bankdetail($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/bankdetail";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Bank';
        $data['var_meta_description'] = 'Store Bank';
        $data['var_meta_keyword'] = 'Store Bank';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
        $data['bankData'] = $this->Store_model->getBankDetail($id);
        $data['init'] = array(
            'Stores.bank_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getBankList($storeId){
        $this->load->library('Datatables');
        echo $this->Store_model->getBankList($storeId);
        exit;
    }

    function updateBankDetail($storeId){
        $data['page'] = "admin/stores/update_bank";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Update Bank Details';
        $data['var_meta_description'] = 'Store Update Bank Details';
        $data['var_meta_keyword'] = 'Store Update Bank Details';
        $data['storeId'] = $storeId;
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['bankData'] = $this->Store_model->getBankDetail($storeId);
        $data['init'] = array(
            'Stores.bank_init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->updateBankDetail($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function customer($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/customer";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Customer';
        $data['var_meta_description'] = 'Store Customer';
        $data['var_meta_keyword'] = 'Store Customer';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.customer_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function holiday_manager($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/holiday_manager/list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store holiday';
        $data['var_meta_description'] = 'Store holiday';
        $data['var_meta_keyword'] = 'Store holiday';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.holiday()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getHolidayList($storeId)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getHolidayList($storeId);
        exit;
    }

    function add_holiday($storeId)
    {
        $data['page'] = "admin/stores/holiday_manager/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Holiday Add';
        $data['var_meta_description'] = 'Store Holiday Add';
        $data['var_meta_keyword'] = 'Store Holiday Add';
        $data['storeId'] = $storeId;
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['init'] = array(
            'Stores.holiday()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->addHoliday($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function edit_holiday($holidayId)
    {
        $data['page'] = "admin/stores/holiday_manager/edit";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', 'Store'),
            array('Store', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store staff Edit';
        $data['var_meta_description'] = 'Store staff Edit';
        $data['var_meta_keyword'] = 'Store staff Edit';

        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-timepicker/css/bootstrap-timepicker.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-timepicker/js/bootstrap-timepicker.min.js',
        );
        $data['holiday_data'] = $this->db->get_where('store_has_holidays', array('id' => $holidayId))->row_array();
        $data['holiday'] = $holidayId;
        $data['storeId'] = $data['holiday_data']['fk_store'];
        $data['init'] = array(
            'Stores.holiday()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Store_model->editHoliday($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function delete_holiday(){
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->delete_holiday($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getCustomerList($storeId)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getCustomerList($storeId);
        exit;
    }

    function booking($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/booking_list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Booking';
        $data['var_meta_description'] = 'Store Booking';
        $data['var_meta_keyword'] = 'Store Booking';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
        $data['init'] = array(
            'Stores.booking_list()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getBookingList($storeId)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getBookingList($storeId);
        exit;
    }

    function review($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/review";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Overview', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Review';
        $data['var_meta_description'] = 'Store Review';
        $data['var_meta_keyword'] = 'Store Review';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
//        $data['storeData'] = $this->Store_model->getStoreCommonHeader($id);
        $data['init'] = array(
            'Stores.review_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }


    function delete_staff()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->deletestaff($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }


    function getStaffList($id)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getStaffList($id);
        exit;
    }

    function getStoreTimeSlotList($id)
    {
        $this->load->library('Datatables');
        echo $this->Store_model->getStoreTimeSlotList($id);
        exit;
    }


    function deletetimeslot()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->deletetimeslot($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function delete()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Store_model->delete($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function portfolio($id)
    {
        $storeData = $this->toval->idtorowarr('id', $id, 'stores');
        if (empty($storeData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Store Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('stores'));
        }

        $data['page'] = "admin/stores/portfolio/list";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', ''),
            array('Portfolio', ''),
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Store Portfolio';
        $data['var_meta_description'] = 'Store Portfolio';
        $data['var_meta_keyword'] = 'Store Portfolio';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['storeId'] = $id;
        $data['portfolio_data'] = $this->Store_model->getStorePortfolio($id);
//        print_array($data['gallery_data']);
        $data['init'] = array(
            'Stores.portfolio_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function add_portfolio($storeId){
        $data['page'] = "admin/stores/portfolio/add";
        $data['breadcrumb'] = 'Store';
        $data['breadcrumb_sub'] = 'Store';
        $data['breadcrumb_list'] = array(
            array('Store', '')
        );
        $data['stores'] = 'active open';
        $data['stores'] = 'active';
        $data['var_meta_title'] = 'Portfolio';
        $data['var_meta_description'] = 'Portfolio';
        $data['var_meta_keyword'] = 'Portfolio';
        $data['js'] = array(
            'admin/stores.js',
        );
        $data['store_id'] = $storeId;
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
        );
        $data['init'] = array(
            'Stores.portfolio_init()',
        );

        if ($this->input->post()) {
            $this->json_response = $this->Store_model->addPortfolio($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function deletePortfolio(){

    }

}
