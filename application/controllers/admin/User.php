<?php

class User extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/User_model');
	}

	function index()
	{
		$data['page'] = "admin/user/list";
		$data['breadcrumb'] = 'User';
		$data['breadcrumb_sub'] = 'User';
		$data['breadcrumb_list'] = array(
			array('User', ''),
			array('List', ''),
		);
		$data['user'] = 'active open';
		$data['user'] = 'active';
		$data['var_meta_title'] = 'User List';
		$data['var_meta_description'] = 'User List';
		$data['var_meta_keyword'] = 'User List';
		$data['js'] = array(
			'admin/user.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'User.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getUserList()
	{
		$this->load->library('Datatables');
		echo $this->User_model->getUserList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/user/add";
		$data['breadcrumb'] = 'User';
		$data['breadcrumb_sub'] = 'User';
		$data['breadcrumb_list'] = array(
			array('User', 'User'),
			array('List', ''),
		);
		$data['user'] = 'active open';
		$data['user'] = 'active';
		$data['var_meta_title'] = 'User Add';
		$data['var_meta_description'] = 'User Add';
		$data['var_meta_keyword'] = 'User Add';

		$data['js'] = array(
			'admin/user.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
			'bootstrap-fileinput/bootstrap-fileinput.css'
		);
		$data['js_plugin'] = array(
			'jquery.form.min.js',
			'ajaxfileupload.js',
			'bootstrap-fileinput/bootstrap-fileinput.js'
		);
		$data['init'] = array(
			'User.Add_init()',
			'ComponentsDateTimePickers.init()',
		);

		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->User_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function overview($id){
            $userData = $this->toval->idtorowarr('id', $id, 'users');
            if (empty($userData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'User Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('user'));
            }

            $data['page'] = "admin/user/overview";
            $data['breadcrumb'] = 'User';
            $data['breadcrumb_sub'] = 'User';
            $data['breadcrumb_list'] = array(
                array('User', 'client'),
                array('Overview', ''),
            );
            $data['user'] = 'active open';
            $data['user'] = 'active';
            $data['var_meta_title'] = 'User Overview';
            $data['var_meta_description'] = 'User Overview';
            $data['var_meta_keyword'] = 'User Overview';
            $data['js'] = array(
                'admin/user.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array(
                'bootstrap-fileinput/bootstrap-fileinput.css',
            );
            $data['js_plugin'] = array(
                'jquery.form.min.js',
                'ajaxfileupload.js',
                'bootstrap-fileinput/bootstrap-fileinput.js'
            );
            $data['userId'] = $id;
            $data['userData'] = $this->User_model->getUserCommonHeader($id);
            $data['init'] = array(
                'User.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }

    function updateUserProfileImage(){
        $var_image = ($this->input->post('var_profile_image')) ? $this->input->post('var_profile_image') : '';
        if ($_FILES['profile_pic']['name'] != "") {
            $image = upload_single_image($_FILES, 'user', USER_PROFILE_IMG, FALSE);
            $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

            if ($this->input->post('var_profile_image') != '') {
                delete_single_image(USER_PROFILE_IMG, $this->input->post('var_profile_image'));
            }
        }

        $data_array = array(
            'var_profile_image' => $var_image,
        );

        $this->db->where('id',$this->input->post('user_id'));
        $result =  $this->db->update('users',$data_array);
        if($result){
            $json_response['status'] = 'success';
            echo json_encode($json_response);
            exit;
        }
    }

    function updateaddress(){
        $res = $this->User_model->updateUserAddress($this->input->post());
        echo json_encode($res);
        exit;
    }

    function store($id){
        $userData = $this->toval->idtorowarr('id', $id, 'users');
        if (empty($userData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'User Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('user'));
        }

        $data['page'] = "admin/user/store";
        $data['breadcrumb'] = 'User';
        $data['breadcrumb_sub'] = 'User';
        $data['breadcrumb_list'] = array(
            array('User', 'client'),
            array('Overview', ''),
        );
        $data['user'] = 'active open';
        $data['user'] = 'active';
        $data['var_meta_title'] = 'User Store List';
        $data['var_meta_description'] = 'User Store List';
        $data['var_meta_keyword'] = 'User Store List';
        $data['js'] = array(
            'admin/user.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['userId'] = $id;
        $data['init'] = array(
            'User.store_list()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function booking($id){
        $userData = $this->toval->idtorowarr('id', $id, 'users');
        if (empty($userData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'User Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('user'));
        }

        $data['page'] = "admin/user/booking_list";
        $data['breadcrumb'] = 'User';
        $data['breadcrumb_sub'] = 'User';
        $data['breadcrumb_list'] = array(
            array('User', 'client'),
            array('Overview', ''),
        );
        $data['user'] = 'active open';
        $data['user'] = 'active';
        $data['var_meta_title'] = 'User Booking List';
        $data['var_meta_description'] = 'User Booking List';
        $data['var_meta_keyword'] = 'User Booking List';
        $data['js'] = array(
            'admin/user.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['userId'] = $id;
        $data['init'] = array(
            'User.booking_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function review($id){
        $userData = $this->toval->idtorowarr('id', $id, 'users');
        if (empty($userData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'User Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('user'));
        }

        $data['page'] = "admin/user/booking_list";
        $data['breadcrumb'] = 'User';
        $data['breadcrumb_sub'] = 'User';
        $data['breadcrumb_list'] = array(
            array('User', 'client'),
            array('Overview', ''),
        );
        $data['user'] = 'active open';
        $data['user'] = 'active';
        $data['var_meta_title'] = 'User Booking List';
        $data['var_meta_description'] = 'User Booking List';
        $data['var_meta_keyword'] = 'User Booking List';
        $data['js'] = array(
            'admin/user.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css',
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['userId'] = $id;
        $data['init'] = array(
            'User.booking_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getStoreList($userId)
    {
        $this->load->library('Datatables');
        echo $this->User_model->getStoreList($userId);
        exit;
    }

    function getBookingList($userId)
    {
        $this->load->library('Datatables');
        echo $this->User_model->getBookingList($userId);
        exit;
    }


	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->User_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
