<?php

class Booking extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Booking_model');
	}

	function index()
	{
		$data['page'] = "admin/booking/list";
		$data['breadcrumb'] = 'Booking';
		$data['breadcrumb_sub'] = 'Booking';
		$data['breadcrumb_list'] = array(
			array('Booking', ''),
			array('List', ''),
		);
		$data['booking'] = 'active open';
		$data['booking'] = 'active';
		$data['var_meta_title'] = 'Booking List';
		$data['var_meta_description'] = 'Booking List';
		$data['var_meta_keyword'] = 'Booking List';
		$data['js'] = array(
			'admin/booking.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Booking.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getBookingList()
	{
		$this->load->library('Datatables');
		echo $this->Booking_model->getBookingList();
		exit;
	}



	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Service_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

	function cacelBooking(){
        if ($this->input->post()) {
            $this->json_response = $this->Booking_model->CancelBooking($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

}
