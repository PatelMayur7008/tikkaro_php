<?php

class Category extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Category_model');
	}

	function index()
	{
		$data['page'] = "admin/category/list";
		$data['breadcrumb'] = 'Category';
		$data['breadcrumb_sub'] = 'Category';
		$data['breadcrumb_list'] = array(
			array('Category', ''),
			array('List', ''),
		);
		$data['category'] = 'active open';
		$data['category'] = 'active';
		$data['var_meta_title'] = 'Category List';
		$data['var_meta_description'] = 'Category List';
		$data['var_meta_keyword'] = 'Category List';
		$data['js'] = array(
			'admin/category.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Category.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getCategoryList()
	{
		$this->load->library('Datatables');
		echo $this->Category_model->getCategoryList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/category/add";
		$data['breadcrumb'] = 'Category';
		$data['breadcrumb_sub'] = 'Category';
		$data['breadcrumb_list'] = array(
			array('Category', ''),
			array('List', ''),
		);
		$data['facility'] = 'active open';
		$data['facility'] = 'active';
		$data['var_meta_title'] = 'Category Add';
		$data['var_meta_description'] = 'Category Add';
		$data['var_meta_keyword'] = 'Category Add';

		$data['js'] = array(
			'admin/category.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
		);
		$data['js_plugin'] = array(
		);
		$data['init'] = array(
			'Category.Add_init()',
			'ComponentsDateTimePickers.init()',
		);

		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->Category_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function overview($id){
            $serviceData = $this->toval->idtorowarr('id', $id, 'master_category');
            if (empty($serviceData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'Category Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('category'));
            }

            $data['page'] = "admin/category/overview";
            $data['breadcrumb'] = 'Category';
            $data['breadcrumb_sub'] = 'Category';
            $data['breadcrumb_list'] = array(
                array('Category', ''),
                array('Overview', ''),
            );
            $data['category'] = 'active open';
            $data['category'] = 'active';
            $data['var_meta_title'] = 'Category Overview';
            $data['var_meta_description'] = 'Category Overview';
            $data['var_meta_keyword'] = 'Category Overview';
            $data['js'] = array(
                'admin/category.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array();
            $data['js_plugin'] = array();
            $data['category_id'] = $id;
            $data['categoryData'] = $this->Category_model->getCategory($id);
            $data['init'] = array(
               // 'Facility.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }

	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Category_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
