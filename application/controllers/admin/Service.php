<?php

class Service extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Service_model');
	}

	function index()
	{
		$data['page'] = "admin/service/list";
		$data['breadcrumb'] = 'Service';
		$data['breadcrumb_sub'] = 'Service';
		$data['breadcrumb_list'] = array(
			array('Service', ''),
			array('List', ''),
		);
		$data['service'] = 'active open';
		$data['service'] = 'active';
		$data['var_meta_title'] = 'Service List';
		$data['var_meta_description'] = 'Service List';
		$data['var_meta_keyword'] = 'Service List';
		$data['js'] = array(
			'admin/service.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Service.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getServiceList()
	{
		$this->load->library('Datatables');
		echo $this->Service_model->getServiceList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/service/add";
		$data['breadcrumb'] = 'Service';
		$data['breadcrumb_sub'] = 'Service';
		$data['breadcrumb_list'] = array(
			array('Service', 'Service'),
			array('List', ''),
		);
		$data['service'] = 'active open';
		$data['service'] = 'active';
		$data['var_meta_title'] = 'Service Add';
		$data['var_meta_description'] = 'Service Add';
		$data['var_meta_keyword'] = 'Service Add';

		$data['js'] = array(
			'admin/service.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
			'bootstrap-fileinput/bootstrap-fileinput.css'
		);
		$data['js_plugin'] = array(
			'jquery.form.min.js',
			'ajaxfileupload.js',
			'bootstrap-fileinput/bootstrap-fileinput.js'
		);
		$data['init'] = array(
			'Service.Add_init()',
			'ComponentsDateTimePickers.init()',
		);
        $data['categoryData'] = $this->db->get_where('master_category',array('enum_type'=>'SERVICE'))->result_array();
		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->Service_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function overview($id){
            $serviceData = $this->toval->idtorowarr('id', $id, 'services');
            if (empty($serviceData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'Service Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('service'));
            }

            $data['page'] = "admin/service/overview";
            $data['breadcrumb'] = 'Service';
            $data['breadcrumb_sub'] = 'Service';
            $data['breadcrumb_list'] = array(
                array('Service', 'Service'),
                array('Overview', ''),
            );
            $data['service'] = 'active open';
            $data['service'] = 'active';
            $data['var_meta_title'] = 'Service Overview';
            $data['var_meta_description'] = 'Service Overview';
            $data['var_meta_keyword'] = 'Service Overview';
            $data['js'] = array(
                'admin/service.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array();
            $data['js_plugin'] = array();
            $data['serviceId'] = $id;
            $data['serviceData'] = $this->Service_model->getServiceCommonHeader($id);
//            print_array($data['serviceData']);
            $data['init'] = array(
                'Service.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }

	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Service_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
