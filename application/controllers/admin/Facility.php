<?php

class Facility extends Admin_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/Facility_model');
	}

	function index()
	{
		$data['page'] = "admin/facility/list";
		$data['breadcrumb'] = 'Facility';
		$data['breadcrumb_sub'] = 'Facility';
		$data['breadcrumb_list'] = array(
			array('Facility', ''),
			array('List', ''),
		);
		$data['facility'] = 'active open';
		$data['facility'] = 'active';
		$data['var_meta_title'] = 'Facility List';
		$data['var_meta_description'] = 'Facility List';
		$data['var_meta_keyword'] = 'Facility List';
		$data['js'] = array(
			'admin/facility.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array();
		$data['js_plugin'] = array();
		$data['init'] = array(
			'Facility.init()',
		);

		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function getFacilityList()
	{
		$this->load->library('Datatables');
		echo $this->Facility_model->getFacilityList();
		exit;
	}

	function add()
	{
		$data['page'] = "admin/facility/add";
		$data['breadcrumb'] = 'Facility';
		$data['breadcrumb_sub'] = 'Facility';
		$data['breadcrumb_list'] = array(
			array('Facility', ''),
			array('List', ''),
		);
		$data['facility'] = 'active open';
		$data['facility'] = 'active';
		$data['var_meta_title'] = 'Facility Add';
		$data['var_meta_description'] = 'Facility Add';
		$data['var_meta_keyword'] = 'Facility Add';

		$data['js'] = array(
			'admin/facility.js',
		);
		$data['css'] = array();
		$data['css_plugin'] = array(
//			'bootstrap-fileinput/bootstrap-fileinput.css'
		);
		$data['js_plugin'] = array(
//			'jquery.form.min.js',
//			'ajaxfileupload.js',
//			'bootstrap-fileinput/bootstrap-fileinput.js'
		);
		$data['init'] = array(
			'Facility.Add_init()',
			'ComponentsDateTimePickers.init()',
		);

		if ($this->input->post() && $this->input->is_ajax_request()) {
			$this->json_response = $this->Facility_model->add($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}


		$this->load->view(ADMIN_LAYOUT, $data);
	}

	function overview($id){
            $serviceData = $this->toval->idtorowarr('id', $id, 'extra_facility');
            if (empty($serviceData)) {
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'Facility Not Found';
                $this->session->set_flashdata($this->json_response);
                return redirect(admin_url('facility'));
            }

            $data['page'] = "admin/facility/overview";
            $data['breadcrumb'] = 'Facility';
            $data['breadcrumb_sub'] = 'Facility';
            $data['breadcrumb_list'] = array(
                array('Facility', ''),
                array('Overview', ''),
            );
            $data['facility'] = 'active open';
            $data['facility'] = 'active';
            $data['var_meta_title'] = 'Facility Overview';
            $data['var_meta_description'] = 'Facility Overview';
            $data['var_meta_keyword'] = 'Facility Overview';
            $data['js'] = array(
                'admin/facility.js',
            );
            $data['css'] = array();
            $data['css_plugin'] = array();
            $data['js_plugin'] = array();
            $data['facility_id'] = $id;
            $data['facilityData'] = $this->Facility_model->getFacility($id);
            $data['init'] = array(
               // 'Facility.overview_init()',
                'ComponentsDateTimePickers.init()',
            );

            $this->load->view(ADMIN_LAYOUT, $data);
    }

	function delete()
	{
		if ($this->input->post()) {
			$this->json_response = $this->Facility_model->delete($this->input->post(), $this->json_response);
			echo json_encode($this->json_response);
			exit;
		}
	}

}
