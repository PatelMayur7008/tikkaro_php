<?php

class Migration extends CI_Controller
{

    public function index()
    {
        //php_sapi_name() == 'cli'

		if (APP_ENVIRONMENT != 'demo' && (in_array($this->input->ip_address(), json_decode(VALID_IP)) || APP_ENVIRONMENT == 'local')) {
            // load migration library
			$this->load->library('migration');

            if (!$this->migration->current()) {
                echo 'Error' . $this->migration->error_string();
            } else {
                $migrations = $this->migration->find_migrations();
                end($migrations);
                $version = $this->config->item('migration_version');
//                $version = key($migrations);
                echo 'Migrations ran successfully!';
                echo '<br>';
                echo 'Last Migration Was: ' . $version;
            }
        } else {
            echo "Not Allowed";
        }

    }
}
