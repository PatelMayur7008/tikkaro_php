<?php

header('Access-Control-Allow-Origin: *');
//header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');

class Apibiz extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('APIbiz_model');
    }

    public function index() {
        
    }

    function checkAuthentication() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);

        $res = $this->APIbiz_model->checkAuthentication($postData);
        echo json_encode($res);
        exit;
    }

    function authentication() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);

        $res = $this->APIbiz_model->authentication($postData);
        echo json_encode($res);
        exit;
    }

    function addBankDetails() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addBankDetails($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStoreStaffWithImage() {
//        $data_json = file_get_contents('php://input');
//        $postData = json_decode($data_json, true);
//        if (!empty($postData)) {
        $res = $this->APIbiz_model->addStoreStaff($this->input->post());
        echo json_encode($res);
        exit;
//        } else {
//            echo json_encode('POST');
//            exit;
//        }
    }

    function addStoreStaffWithoutImage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStoreStaff($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getAllStaffByStoreId() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getAllStaffByStoreId($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStoreHoliday() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStoreHoliday($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreHoliday() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreHoliday($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getAllFacility() {

        $res = $this->APIbiz_model->getAllFacility();
        echo json_encode($res);
        exit;
    }

    function getStoreDetailById() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreDetailById($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getBookingRecordByStoreCode() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getBookingRecordByStoreCode($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deleteStoreHoliday() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deleteStoreHoliday($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deleteStoreStaff() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deleteStoreStaff($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateStoreStaffWithoutImage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateStoreStaff($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateStoreStaffWithImage() {
//        $data_json = file_get_contents('php://input');
//        $postData = json_decode($data_json, true);
//        if (!empty($postData)) {
        $res = $this->APIbiz_model->updateStoreStaff($this->input->post());
        echo json_encode($res);
        exit;
//        } else {
//            echo json_encode('POST');
//            exit;
//        }
    }

    function updateStoreHoliday() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateStoreHoliday($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getAllSetting() {

        $res = $this->APIbiz_model->getAllSetting();
        echo json_encode($res);
        exit;
    }

    function getStoreSetting() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreSetting($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateStoreSetting() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateStoreSetting($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateBussinessProfileWithImage() {
//        $data_json = file_get_contents('php://input');
//        $postData = json_decode($data_json, true);
//        if (!empty($postData)) {
        $res = $this->APIbiz_model->updateBussinessProfile($this->input->post());
        echo json_encode($res);
        exit;
//        } else {
//            echo json_encode('POST');
//            exit;
//        }
    }

    function updateBussinessProfileWithoutImage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateBussinessProfile($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreTiming() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreTiming($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateStoreTiming() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateStoreTiming($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getAllservice() {
        $res = $this->APIbiz_model->getAllService();
        echo json_encode($res);
        exit;
    }

    function getStoreService() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreService($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function updateStoreService() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->updateStoreService($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deleteStoreService() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deleteStoreService($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getCustomerStats() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getCustomerStats($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getCustomerBookingDetail() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getCustomerBookingDetail($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreImages() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreImages($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStoreImageold() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStoreImage($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    public function addStoreImage_14032018() {

        header('Access-Control-Allow-Origin: *');
        $target_path = "public/upload/store_pic/";
        $filename = randomNumber(4);
        $target_path = $target_path . basename($filename . $_FILES['file']['name']);
        $imgname = $filename . $_FILES['file']['name'];
        $store_code = strstr($_FILES['file']['name'], '.', true);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            $storeData = getStoreDataByStoreCode($store_code);
            $imgArr = array(
                'fk_store' => $storeData['id'],
                'var_file' => $imgname,
                'enum_isProfile' => 'No',
                'enum_ismain' => 'YES',
                'enum_enable' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_gallery', $imgArr);
            $lastid = $this->db->insert_id();
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }
    }

    public function addStoreImage() {
//        $data_json = file_get_contents('php://input');
//        $postData = json_decode($data_json, true);
        $res = $this->APIbiz_model->addStoreImage($this->input->post());
        echo json_encode($res);
        exit;
    }

    function deleteStoreImage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deleteStoreImage($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function makeImageAsProfile() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->makeImageAsProfile($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function makeImageActiveInActive() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->makeImageActiveInActive($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStorePackage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStorePackage($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStorePackage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStorePackage($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deletePackage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deletePackage($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getStoreOffer() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStoreOffer($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStoreOffer() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStoreOffer($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deleteOffer() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deleteOffer($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getAllStoreByBussCode() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getAllStoreByBussCode($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function cancelBooking() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->cancelBooking($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function getBankDetails() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getBankDetails($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function sentPushyId() {

        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->sentPushyId($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function addStorePortfolioWithImage() {
//        $data_json = file_get_contents('php://input');
//        $postData = json_decode($data_json, true);
//        if (!empty($postData)) {
        $res = $this->APIbiz_model->addStorePortfolio($this->input->post());
        echo json_encode($res);
        exit;
//        } else {
//            echo json_encode('POST');
//            exit;
//        }
    }

    function addStorePortfolioWithoutImage() {
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->addStorePortfolio($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }



    function getStorePortfolio(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->getStorePortfolio($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function togglePortfolioStatus(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->togglePortfolioStatus($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

    function deletePortfolio(){
        $data_json = file_get_contents('php://input');
        $postData = json_decode($data_json, true);
        if (!empty($postData)) {
            $res = $this->APIbiz_model->deletePortfolio($postData);
            echo json_encode($res);
            exit;
        } else {
            echo json_encode('POST');
            exit;
        }
    }

}

?>
