<?php

use Mailgun\Mailgun;
use Sabre\VObject;

class Test extends MY_Controller {

    function __construct() {
        parent::__construct();
    }

    function test() {
        echo convert_date('2017-07-31 09:00:00', 'UTC', 'europe/london');
        exit;
    }

    function getRandomNumber() {
        $this->load->helper('general_helper');
        $call_extra_data = generateRandomString();
        print_r($call_extra_data);
        exit;
    }

    function sentPushyNotification() {
        // App Type : front / biz
        $res = $this->toval->CallPushyAPI('biz', 6, 'Hi Test Push notifaction!..');
        print_r($res);
        exit;
    }

}
