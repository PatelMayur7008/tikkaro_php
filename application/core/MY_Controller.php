<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public $json_response = null;

    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->json_response = array('status' => 'error', 'message' => 'something went wrong!');
    }

}


class Front_Controller extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
    }

}

class Admin_Controller extends MY_Controller
{

    public $user_data = null;
    public $user_type = null;
    public $user_id = null;

    function __construct()
    {
        parent::__construct();
        $this->load->library('AuthLibrary');


        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isset($this->session->userdata['valid_login_user'])) {

                redirect(admin_url('login'));
            }
            $this->user_data = $this->authlibrary->getLoginUserData();
            $this->user_type = $this->authlibrary->getLoginUserType();
            $this->user_id = $this->authlibrary->getLoginUserId();
            $this->user_data = $this->session->userdata('valid_login_user');
        }
    }

}
