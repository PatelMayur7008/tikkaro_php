<?php

class API_model extends CI_Model
{

    function checkAuthentication($data)
    {
        if (!empty($data)) {
            $this->db->where('bint_phone', $data['phone_number']);
            $checkUser = $this->db->get('users')->row_array();
            $checkUser['var_profile_image'] = (empty($checkUser['var_profile_image'])) ?
                base_url() . 'public/upload/no_image.png'
                : base_url() . USER_PROFILE_IMG . '/' . $checkUser['var_profile_image'];
            if (!empty($checkUser)) {
                $response['status'] = 'success';
                $response['data'] = $checkUser;
                $response['message'] = 'Login successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Authentication Failed!..';
            }
        }
        return $response;
    }

    function authentication($data)
    {
        if (!empty($data)) {
            $this->db->where('bint_phone', $data['phone_number']);
            $checkUser = $this->db->get('users')->row_array();

            if (!empty($checkUser)) {
                // Login User
                $username = explode(' ', $data['user_name']);
                $userData = array(
                    'int_otp' => $data['otp'],
                    'var_fname' => (!empty($username[0])) ? $username[0] : null,
                    'var_lname' => (!empty($username[1])) ? $username[1] : null,
                    'var_email' => (!empty($data['user_email'])) ? $data['user_email'] : null,
                );
                $this->db->where('id', $checkUser['id']);
                $res = $this->db->update('users', $userData);
                if ($this->db->affected_rows() > 0) {
                    $response['status'] = 'success';
                    $response['message'] = 'Login successfully!';
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'OTP not matched...';
                }
            } else {
                $username = explode(' ', $data['user_name']);
                $userData = array(
                    'bint_phone' => $data['phone_number'],
                    'var_fname' => (!empty($username[0])) ? $username[0] : null,
                    'var_lname' => (!empty($username[1])) ? $username[1] : null,
                    'var_email' => (!empty($data['user_email'])) ? $data['user_email'] : null,
                    'int_otp' => $data['otp'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $res = $this->db->insert('users', $userData);
                if ($this->db->insert_id() > 0) {
                    $response['status'] = 'success';
                    $response['message'] = 'User Registered successfully!';
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'Registration failed!...';
                }
            }
        }
        return $response;
    }

    function getStoreList($data)
    {

        if (!empty($data['user_id'])) {
            $this->db->select('stores.*,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
            $this->db->from('user_has_favorite_store');
            $this->db->join('stores', 'user_has_favorite_store.fk_store = stores.id');
            $this->db->join('master_category', 'master_category.id = stores.fk_category');
            $this->db->where('user_has_favorite_store.fk_user', $data['user_id']);
            $result = $this->db->get()->result_array();
        } else {
            $this->db->select('stores.*,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
            $this->db->join('master_category', 'master_category.id = stores.fk_category');
            $this->db->where('stores.enum_enable', 'YES');
            $result = $this->db->get('stores')->result_array();
        }

        foreach ($result as $key => $res) {

            $fav = $this->db->get_where('user_has_favorite_store', array('fk_store' => $res['id'], 'fk_user' => $data['user_id']))->num_rows();
            if ($fav > 0) {
                $result[$key]['is_favorite'] = true;
            } else {
                $result[$key]['is_favorite'] = false;
            }

            //CODE FOR STORE REVIEW
            $result[$key]['reviewCalculation'] = $this->calculationOfStoreReview($res['id']);
            //CODE FOR STORE REVIEW

            $result[$key]['var_logo'] = (!empty(getStoreMainImage($res['id']))) ? base_url() . 'public/upload/store_pic/' . getStoreMainImage($res['id']) : base_url() . 'public/upload/no_image.png';

            $this->db->select('store_has_staff.*,stores.var_title as barber_name,stores.bint_phone as barber_phone');
            $this->db->from('store_has_staff');
            $this->db->join('stores', 'stores.id = store_has_staff.fk_store', 'left');
            $this->db->where('store_has_staff.fk_store', $res['id']);
            $this->db->where('store_has_staff.enum_active', 'YES');
            $this->db->where('store_has_staff.enum_isFrontView', 'YES');
            $result[$key]['store_has_barbers'] = $this->db->get()->result_array();

            $this->db->select('store_has_services.*,services.var_title as service_title,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
            $this->db->from('store_has_services');
            $this->db->join('services', 'services.id = store_has_services.fk_service', 'left');
            $this->db->join('master_category', 'master_category.id = services.fk_category', 'left');
            $this->db->where('store_has_services.fk_store', $res['id']);
            $this->db->where('store_has_services.enum_enable', 'YES');
            $result[$key]['store_has_services'] = $this->db->get()->result_array();

            $this->db->select('store_has_timing.*');
            $this->db->from('store_has_timing');
            $this->db->where('store_has_timing.fk_store', $res['id']);
            $this->db->where('store_has_timing.int_week !=', $res['int_weekoff']);
            $this->db->order_by('store_has_timing.int_week');
            $result[$key]['store_has_timing'] = $this->db->get()->result_array();

            $result[$key]['store_has_review'] = $this->getStoreReview(array('store_id' => $res['id']));
            $result[$key]['store_has_portfolio'] = $this->getStorePortfolio(array('store_id' => $res['id']));
            $result[$key]['store_has_offers'] = $this->getStoreOffers(array('store_id' => $res['id']));
            $result[$key]['store_has_packages'] = $this->getStorePackages(array('store_id' => $res['id']));


            $this->db->select('store_has_gallery.*');
            $this->db->from('store_has_gallery');
            $this->db->join('stores', 'stores.id = store_has_gallery.fk_store', 'left');
            $this->db->where('store_has_gallery.fk_store', $res['id']);
            $this->db->where('store_has_gallery.enum_ismain', 'YES');
            $this->db->limit(5);
            $result[$key]['temp'] = $this->db->get()->result_array();
            $result[$key]['store_has_images'] = array();
            if (count($result[$key]['temp']) > 0) {
                foreach ($result[$key]['temp'] as $val) {
                    $images = ($val['var_file']) ? base_url() . 'public/upload/store_pic/' . $val['var_file'] : base_url() . 'public/upload/no_image.png';
                    array_push($result[$key]['store_has_images'], $images);
                }
            } else {
                $result[$key]['store_has_images'] = array(base_url() . 'public/upload/no_image.png');
            }
        }
        return $result;
    }

    function getTimeSlotsByStoreId($data)
    {

        $storeId = $data['store_id'];
        $this->db->where('id', $storeId);
        $storeDetails = $this->db->get('stores')->row_array();

        $openTime = $storeDetails['open_time'];
        $closeTime = $storeDetails['close_time'];
        $this->db->select('id,slot_time');
        $timeSlots = $this->db->get('timeslots')->result_array();
        $storeTimeSlots = array();
        for ($i = 0; $i < count($timeSlots); $i++) {

            if ($timeSlots[$i]['slot_time'] >= $openTime && $timeSlots[$i]['slot_time'] <= $closeTime) {
                $storeTimeSlots[] = $timeSlots[$i];
            }
        }
        print_array($storeTimeSlots);
        return $storeTimeSlots;
    }

    function getAppoinmentbyUserId($data)
    {

        $this->db->select('booking.dt_booking_date,
                           booking.var_price,
                           booking.enum_book_type,
                           CONCAT(' . TABLE_PREFIX . 'users.var_fname," ",' . TABLE_PREFIX . 'users.var_lname) as username
                           ,users.bint_phone
                           ,users.var_email
                           ,users.id
                           ,booking.id as bookId
                           ,stores.var_title as storename
                           ,stores.id as storeId');
        $this->db->from('booking');
        $this->db->join('users', 'users.id = booking.fk_user');
        $this->db->join('stores', 'stores.id = booking.fk_store');
        $this->db->where('users.id', $data['user_id']);
        $result = $this->db->get()->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $result[$i]['month'] = date('M', strtotime($result[$i]['dt_booking_date']));
            $result[$i]['date'] = date('d', strtotime($result[$i]['dt_booking_date']));
            $result[$i]['services'] = getServiceByBookingId($result[$i]['bookId']);
            $result[$i]['servicesIds'] = getServiceIdByBookingId($result[$i]['bookId'], $result[$i]['storeId']);
            $result[$i]['timeslot'] = getTimeSlotByBookingId($result[$i]['bookId']);
            $slottime = explode('-', $result[$i]['timeslot']);
            $result[$i]['time'] = date('h:i A', strtotime($slottime[0]));

            $this->db->select('store_has_services.*,
                                services.id as id,
                                services.var_title as service_title,
                                master_category.id as catId,
                                master_category.var_name as category_name,
                                master_category.enum_type as category_type');
            $this->db->from('store_has_services');
            $this->db->join('services', 'services.id = store_has_services.fk_service', 'left');
            $this->db->join('master_category', 'master_category.id = services.fk_category', 'left');
            $this->db->where('store_has_services.fk_store', $result[$i]['storeId']);
            $this->db->where('store_has_services.enum_enable', 'YES');
            $result[$i]['store_has_services'] = $this->db->get()->result_array();
        }

        $record['past'] = array();
        $record['current'] = array();
        $record['future'] = array();

        for ($j = 0; $j < count($result); $j++) {
            if (date('Y-m-d') == date('Y-m-d', strtotime($result[$j]['dt_booking_date']))) {
                array_push($record['current'], $result[$j]);
            } elseif (date('Y-m-d') > date('Y-m-d', strtotime($result[$j]['dt_booking_date']))) {
                array_push($record['past'], $result[$j]);
            } else {
                array_push($record['future'], $result[$j]);
            }
        }

        return $record;
    }

    function getBookingTimeSlote($data)
    {
        date_default_timezone_set('Asia/Kolkata');
        $day = date('l', strtotime($data['book_date']));
        $daynumber = getNumberFromDayName($day);
        $getCurrtime = date("h:i:sa");
        $aftertime = date("h:i:sa", strtotime('+30 minutes', strtotime($getCurrtime)));

//echo $aftertime; exit;

        $timing = $this->db->get_where('store_has_timing', array('fk_store' => $data['store_id'], 'int_week' => $daynumber))->result_array();

        $this->db->select('timeslots.id as master_timeslot_id,
                           DATE_FORMAT(tk_timeslots.slot_time,"%h:%i %p") as slot_time,
                           store_has_timeslots.id as store_has_slottime_id,
                           store_has_timeslots.fk_store,
                           store_has_timeslots.int_week_day,
                           store_has_timeslots.int_book_limit,
                           store_has_timeslots.enum_isfull
                            ');
        $this->db->from('timeslots');
        $this->db->join('store_has_timeslots', 'store_has_timeslots.fk_timeslot = timeslots.id');
        $this->db->where('store_has_timeslots.enum_enable', 'YES');
        $this->db->where('fk_store', $data['store_id']);
        $this->db->where('int_week_day', $daynumber);
        $this->db->where('timeslots.slot_time >=', $timing[0]['dt_open_time']);
        $this->db->where('timeslots.slot_time <=', $timing[0]['dt_close_time']);
        $result = $this->db->get()->result_array();
//print_array($result);
        $storeholiday = $this->db->get_where('store_has_holidays', array('fk_store' => $data['store_id'], 'dt_holiday_date' => $data['book_date']))->row_array();

//        print_array($storeholiday);
        for ($k = 0; $k < count($result); $k++) {
            if ($storeholiday['enum_holiday'] == 'YES') {
                $result[$k]['enum_isholiday'] = 'YES';
                $result[$k]['enum_isfull'] = 'YES';
            } else {
                if (date('H:i:s', strtotime($result[$k]['slot_time'])) >= date('H:i:s', strtotime($storeholiday['dt_start_time']))) {
                    if (date('H:i:s', strtotime($result[$k]['slot_time'])) <= date('H:i:s', strtotime($storeholiday['dt_end_time']))) {
                        $result[$k]['enum_isholiday'] = 'YES';
                        $result[$k]['enum_isfull'] = 'YES';
                    } else {
                        $result[$k]['enum_isholiday'] = 'NO';
                    }
                } else {
                    $result[$k]['enum_isholiday'] = 'NO';
                }
            }
            if (date('H:i:s', strtotime($result[$k]['slot_time'])) < date('H:i:s', strtotime($aftertime))) {
                if (date('Y-m-d') == ($data['book_date'])) {
                    $result[$k]['enum_isfull'] = 'YES';
                }
            }
        }
        return $result;
    }

    function bookStoreService($data)
    {

//        print_array($data['service_id'][0]['id']);
        $servicesId = array();
        for ($i = 0; $i < count($data['service_id']); $i++) {
            array_push($servicesId, $data['service_id'][$i]['id']);
        }
        $serviceIdString = implode(',', array_filter($servicesId));

        $day = date('l', strtotime($data['book_date']));
        $daynumber = getNumberFromDayName($day);

        $this->db->select('var_price,dt_service_time');
        $this->db->from('store_has_services');
        $this->db->where('fk_store', $data['store_id']);
        $this->db->where_in('fk_service', $servicesId);
        $storeServiceData = $this->db->get()->result_array();
        $min = 0;
        for ($j = 0; $j < count($storeServiceData); $j++) {
            $timesplit = explode(':', $storeServiceData[$j]['dt_service_time']);
            $min += ($timesplit[0] * 60) + ($timesplit[1]) + ($timesplit[2] > 30 ? 1 : 0);
        }

//        echo 'min' . $min . '----';

        $this->db->select('timeslots.slot_time');
        $this->db->from('timeslots');
        $this->db->join('store_has_timeslots', 'store_has_timeslots.fk_timeslot = timeslots.id');
        $this->db->where('store_has_timeslots.id', $data['store_has_timeslot_id']);
        $storetimeslot = $this->db->get()->row_array();
        $starttime = $storetimeslot['slot_time'];
        $endTime = date('h:i:s', strtotime("+" . $min . " minutes", strtotime($starttime)));

//        echo $starttime . '----';
//        echo $endTime;
//        echo '---------';

        $this->db->select('GROUP_CONCAT(tk_timeslots.id SEPARATOR ",") as ids');
        $this->db->from('timeslots');
        $this->db->where('slot_time >=', $starttime);
        $this->db->where('slot_time <=', $endTime);
        $this->db->order_by('id', 'asc');
        $timeslotdata = $this->db->get()->result_array();

//        echo '<pre/>';
//        print_r($timeslotdata);

        $timeslotIds = explode(',', $timeslotdata[0]['ids']);

        $this->db->select('store_has_timeslots.*');
        $this->db->where('fk_store', $data['store_id']);
        $this->db->where('int_week_day', $daynumber);
        $this->db->where_in('fk_timeslot', $timeslotIds);
        $store_has_timeslot = $this->db->get('store_has_timeslots')->result_array();

        for ($i = 0; $i < count($store_has_timeslot); $i++) {
            if (($store_has_timeslot[$i]['int_book_limit'] == $store_has_timeslot[$i]['int_book_count']) || $store_has_timeslot[$i]['enum_isfull'] == 'YES') {
                $response['status'] = 'error';
                $response['message'] = 'Store is full';
                return $response;
            } else {
                if ($store_has_timeslot[$i]['int_book_limit'] - $store_has_timeslot[$i]['int_book_count'] == 1) {
                    $udateArr = array(
                        'int_book_count' => $store_has_timeslot[$i]['int_book_count'] + 1,
                        'enum_isfull' => 'YES'
                    );
                } else {
                    $udateArr = array(
                        'int_book_count' => $store_has_timeslot[$i]['int_book_count'] + 1
                    );
                }
                $this->db->where('id', $store_has_timeslot[$i]['id']);
                $this->db->update('store_has_timeslots', $udateArr);
            }
        }

        $this->db->select('timeslots.id');
        $this->db->from('timeslots');
        $this->db->where('slot_time >=', $starttime);
        $this->db->where('slot_time <=', $endTime);
        $this->db->order_by('id', 'asc');
        $timeslotdata = $this->db->get()->result_array();

        $bookingArr = array(
            'fk_store' => $data['store_id'],
            'fk_user' => $data['user_id'],
            'dt_booking_date' => date('Y-m-d', strtotime($data['book_date'])),
            'var_price' => $data['var_price'],
            'enum_book_type' => 'UPCOMING',
            'enum_booking_type' => ($data['offer'] == 'YES') ? 'Offer' : 'Normal',
            'created_at' => date('Y-m-d')
        );
        $this->db->insert('booking', $bookingArr);
        $lastBookId = $this->db->insert_id();
        if ($lastBookId > 0) {
            for ($k = 0; $k < count($data['service_id']); $k++) {
                $arr = array(
                    'fk_booking' => $lastBookId,
                    'fk_service' => $data['service_id'][$k]['id'],
                    'created_at' => date('Y-m-d')
                );
                $this->db->insert('booking_has_service', $arr);
                $lastId = $this->db->insert_id();
            }
            for ($l = 0; $l < count($timeslotdata); $l++) {
                $resArr = array(
                    'fk_timeslot' => $timeslotdata[$l]['id'],
                    'fk_booking' => $lastBookId,
                    'created_at' => date('Y-m-d')
                );
                $this->db->insert('booking_has_timeslot', $resArr);
                $timeslotlastId = $this->db->insert_id();
            }

        }
        if ($timeslotlastId > 0) {
            $response['status'] = 'success';
            $response['message'] = 'Booking is Successfully';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something will be wrong';
        }
        return $response;
    }

    function updateUserAccountDetail($data)
    {

        $userData = $this->db->get_where('users', array('id' => $data['user_id']))->row_array();
        $pieces = explode(" ", $data['name']);
        $updateArr = array(
            'var_fname' => ($data['name']) ? $pieces[0] : $userData['var_fname'],
            'var_lname' => ($data['name']) ? $pieces[1] : $userData['var_lname'],
            'var_email' => ($data['email']) ? $data['email'] : $userData['var_email'],
            'bint_phone' => ($data['phone']) ? $data['phone'] : $userData['bint_phone'],
            'dt_dob' => ($data['dob']) ? date('Y-m-d', strtotime($data['dob'])) : $userData['dt_dob'],
            'enum_service_type' => ($data['enum_service_type']) ? $data['enum_service_type'] : $userData['enum_service_type']
        );
        $this->db->where('id', $data['user_id']);
        $update = $this->db->update('users', $updateArr);
        if ($update) {
            $response['status'] = 'success';
            $response['message'] = 'User updated successfully';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something will be wrong';
        }
        return $response;
    }

    function updateUserAddressDetail($data)
    {
        $userData = $this->db->get_where('users', array('id' => $data['user_id']))->row_array();
        $updateArr = array(
            'var_latitude' => ($data['lat']) ? $data['lat'] : $userData['var_latitude'],
            'var_longitude' => ($data['long']) ? $data['long'] : $userData['var_longitude'],
            'txt_address' => ($data['address']) ? $data['address'] : $userData['txt_address'],
            'int_areacode' => ($data['pincode']) ? $data['pincode'] : $userData['int_areacode'],
        );
        $this->db->where('id', $data['user_id']);
        $update = $this->db->update('users', $updateArr);
        if ($update) {
            $response['status'] = 'success';
            $response['message'] = 'User updated successfully';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something will be wrong';
        }
        return $response;
    }

    function userFavoriteStore($data)
    {
        $getData = $this->db->get_where('user_has_favorite_store', array('fk_store' => $data['store_id'], 'fk_user' => $data['user_id']))->row_array();
        if (!empty($getData)) {
            $this->db->where('id', $getData['id']);
            $result = $this->db->delete('user_has_favorite_store');
        } else {
            $insertArr = array(
                'fk_store' => $data['store_id'],
                'fk_user' => $data['user_id'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('user_has_favorite_store', $insertArr);
            $lastid = $this->db->insert_id();
        }
        if ($result || $lastid > 0) {
            $response['status'] = 'success';
            $response['message'] = 'updated successfully';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something will be wrong';
        }
        return $response;
    }

    function addReview($data)
    {
        if (!empty($data)) {
            $arr = array(
                'fk_store' => $data['store_id'],
                'fk_user' => $data['user_id'],
                'int_review' => $data['review_star'],
                'txt_text' => $data['review_text'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_review', $arr);
            $lastid = $this->db->insert_id();
            if ($lastid > 0) {
                $response['status'] = 'success';
                $response['message'] = 'updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        }
        return $response;
    }

    function calculationOfStoreReview($storeId)
    {
        $this->db->select('store_has_review.fk_store,
                           tk_store_has_review.int_review as reviewStar,
                           COUNT(tk_store_has_review.int_review) as reviewStarCount');
        $this->db->from('store_has_review');
        $this->db->where('store_has_review.fk_store', $storeId);
        $this->db->group_by('store_has_review.int_review');
        $data = $this->db->get()->result_array();
        $data['total_reviews'] = 0;
        for ($i = 0; $i < count($data); $i++) {
            $data['total_reviews'] += $data[$i]['reviewStarCount'];
            if ($data[$i]['reviewStar'] == 5) {
                $fivestar = $data[$i]['reviewStarCount'];
            } elseif ($data[$i]['reviewStar'] == 4) {
                $fourstar = $data[$i]['reviewStarCount'];
            } elseif ($data[$i]['reviewStar'] == 3) {
                $threestar = $data[$i]['reviewStarCount'];
            }
            if ($data[$i]['reviewStar'] == 2) {
                $twostar = $data[$i]['reviewStarCount'];
            }
            if ($data[$i]['reviewStar'] == 1) {
                $onestar = $data[$i]['reviewStarCount'];
            }
        }
        $data['rating'] = round(((5 * $fivestar) + (4 * $fourstar) + (3 * $threestar) + (2 * $twostar) + (1 * $onestar)) / $data['total_reviews']);
        $data['nonrating'] = 5 - $data['rating'];
        $data['var_rating'] = array_fill(0, $data['rating'], 'test');
        $data['var_non_rating'] = array_fill(0, $data['nonrating'], 'test');
        return $data;
    }

    function getStoreReview($data)
    {
        if (!empty($data)) {
            $this->db->select('CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,users.var_email as usermail,users.bint_phone as usercontact,
                                users.var_profile_image as userprofile,
                                DATE_FORMAT(tk_store_has_review.created_at,"%b %d, %Y") as reviewdate,tk_store_has_review.created_at as revieworigindate,store_has_review.int_review as reviewstar,store_has_review.txt_text as reviewtext
                                ');
            $this->db->from('store_has_review');
            $this->db->join('users', 'users.id = store_has_review.fk_user');
            $this->db->where('store_has_review.fk_store', $data['store_id']);
            return $this->db->get()->result_array();
        } else {
            return 'no review';
        }
    }

    function getStoreReviewByUserId($data)
    {
        if (!empty($data)) {
            $this->db->select('stores.id as store_id,stores.var_title,stores.var_email,stores.bint_phone,stores.txt_address,CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,users.var_email as usermail,users.bint_phone as usercontact,
                                store_has_review.created_at as reviewdate,store_has_review.int_review as reviewstar,store_has_review.txt_text as reviewtext,
                                store_has_gallery.var_file');
            $this->db->from('store_has_review');
            $this->db->join('users', 'users.id = store_has_review.fk_user');
            $this->db->join('stores', 'stores.id = store_has_review.fk_store');
            $this->db->join('store_has_gallery', 'store_has_gallery.fk_store = stores.id and store_has_gallery.enum_isProfile ="YES"', 'left');
            $this->db->where('store_has_review.fk_user', $data['user_id']);
            return $this->db->get()->result_array();
        } else {
            return 'no review';
        }
    }

    function getUserFavoriteStore($data)
    {
        if (!empty($data)) {
            $this->db->select('stores.*');
            $this->db->from('user_has_favorite_store');
            $this->db->join('stores', 'user_has_favorite_store.fk_store = stores.id');
            $this->db->where('user_has_favorite_store.fk_user', $data['user_id']);
            $result = $this->db->get()->result_array();

            foreach ($result as $key => $res) {

                $result[$key]['var_logo'] = (!empty(getStoreMainImage($res['id']))) ? base_url() . 'public/upload/store_pic/' . getStoreMainImage($res['id']) : base_url() . 'public/upload/no_image.png';

                $this->db->select('store_has_staff.*,stores.var_title as barber_name,stores.bint_phone as barber_phone');
                $this->db->from('store_has_staff');
                $this->db->join('stores', 'stores.id = store_has_staff.fk_store', 'left');
                $this->db->where('store_has_staff.fk_store', $res['id']);
                $this->db->where('store_has_staff.enum_active', 'YES');
                $this->db->where('store_has_staff.enum_isFrontView', 'YES');
                $result[$key]['store_has_barbers'] = $this->db->get()->result_array();

                $this->db->select('store_has_services.*,services.var_title as service_title,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
                $this->db->from('store_has_services');
                $this->db->join('services', 'services.id = store_has_services.fk_service', 'left');
                $this->db->join('master_category', 'master_category.id = services.fk_category', 'left');
                $this->db->where('store_has_services.fk_store', $res['id']);
                $this->db->where('store_has_services.enum_enable', 'YES');
                $result[$key]['store_has_services'] = $this->db->get()->result_array();

                $this->db->select('store_has_timing.*');
                $this->db->from('store_has_timing');
                $this->db->where('store_has_timing.fk_store', $res['id']);
                $this->db->where('store_has_timing.int_week !=', $res['int_weekoff']);
                $this->db->order_by('store_has_timing.int_week');
                $result[$key]['store_has_timing'] = $this->db->get()->result_array();

                $result[$key]['store_has_review'] = $this->getStoreReview(array('store_id' => $res['id']));
                $result[$key]['store_has_portfolio'] = $this->getStorePortfolio(array('store_id' => $res['id']));
                $result[$key]['store_has_offers'] = $this->getStoreOffers(array('store_id' => $res['id']));
                $result[$key]['store_has_packages'] = $this->getStorePackages(array('store_id' => $res['id']));

            }

            return $result;
        } else {
            return 'Store Not Found';
        }
    }

    function getUserAccountDetail($data)
    {
        if (!empty($data)) {
            $userdata = $this->db->get_where('users', array('id' => $data['user_id']))->row_array();
        }
        return $userdata;
    }

    function updateUserProfilePic($data)
    {
        $this->load->library('Mylibrary');
        $userData = $this->db->get_where('users', array('id' => $data['user_id']))->row_array();
        if (!empty($userData)) {
            if (!empty($_FILES['file'])) {
                $imgexten = explode('.', $_FILES['file']['name']);
                $img_name = 'file';
                $img_last_key = end($imgexten);
                $image_name = url_title($imgexten[0]) . time();
                $array = array(
                    'image_pet_name' => $img_name,
                    'image_name' => $image_name,
                    'path' => USER_PROFILE_IMG,
                    'extension' => $img_last_key,
                );
                $result = $this->mylibrary->upload_image($array);
                if (!empty($result)) {
                    $var_logo = $result['orig_name'];
                } else {
                    $var_logo = $userData['var_profile_image'];
                }
            } else {
                $var_logo = $userData['var_profile_image'];
            }

            $updateArr = array(
                'var_profile_image' => $var_logo
            );
            $this->db->where('id', $data['user_id']);
            $res = $this->db->update('users', $updateArr);
            if ($res) {
                $response['status'] = 'success';
                $response['message'] = 'User Updated successfully!';
                $response['data'] = base_url() . USER_PROFILE_IMG . '/' . $var_logo;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'User not found';
        }
        return $response;
    }

    function getStoreCategory()
    {
        $this->db->where('enum_type', 'STORE');
        return $this->db->get('master_category')->result_array();
    }

    function getUserLastAppoinment($data)
    {
        $this->db->select('booking.dt_booking_date,
                           booking.var_price,
                           booking.enum_book_type,
                           CONCAT(' . TABLE_PREFIX . 'users.var_fname," ",' . TABLE_PREFIX . 'users.var_lname) as username
                           ,users.bint_phone
                           ,users.var_email
                           ,users.id
                           ,booking.id as bookId
                           ,stores.var_title as storename');
        $this->db->from('booking');
        $this->db->join('users', 'users.id = booking.fk_user');
        $this->db->join('stores', 'stores.id = booking.fk_store');
        $this->db->order_by('booking.id', 'desc');
        $this->db->limit(1);
        $this->db->where('users.id', $data['user_id']);
        $result = $this->db->get()->result_array();
        if (!empty($result)) {
            $result[0]['month'] = date('M', strtotime($result[0]['dt_booking_date']));
            $result[0]['date'] = date('d', strtotime($result[0]['dt_booking_date']));
            $result[0]['time'] = date('h:i A', strtotime($result[0]['dt_booking_date']));
            $result[0]['services'] = getServiceByBookingId($result[0]['bookId']);
            $result[0]['timeslot'] = getTimeSlotByBookingId($result[0]['bookId']);

        } else {
            $result = [];
        }
        return $result;
    }

    function getNearestStore($data)
    {
        if (!empty($data['lat']) && !empty($data['lang'])) {
            $query = $this->db->query('SELECT *, SQRT(
                POW(69.1 * (var_latitude - ' . $data['lat'] . '), 2) +
                POW(69.1 * (' . $data['lang'] . ' - var_longitude) * COS(var_latitude / 57.3), 2)) AS distance
                FROM  tk_stores  ORDER BY distance;');
            $result = $query->result_array();
        } else {
            $this->db->select('stores.*,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
            $this->db->join('master_category', 'master_category.id = stores.fk_category');
            $this->db->where('stores.enum_enable', 'YES');
            $result = $this->db->get('stores')->result_array();
        }


        foreach ($result as $key => $res) {

            if (!empty($data['user_id'])) {
                $fav = $this->db->get_where('user_has_favorite_store', array('fk_store' => $res['id'], 'fk_user' => $data['user_id']))->num_rows();
                if ($fav > 0) {
                    $result[$key]['is_favorite'] = true;
                } else {
                    $result[$key]['is_favorite'] = false;
                }
            } else {
                $result[$key]['is_favorite'] = false;
            }

            //CODE FOR STORE REVIEW
            $result[$key]['reviewCalculation'] = $this->calculationOfStoreReview($res['id']);
            //CODE FOR STORE REVIEW

            $result[$key]['var_logo'] = (!empty(getStoreMainImage($res['id']))) ? base_url() . 'public/upload/store_pic/' . getStoreMainImage($res['id']) : base_url() . 'public/upload/no_image.png';

            $this->db->select('store_has_staff.*,stores.var_title as barber_name,stores.bint_phone as barber_phone');
            $this->db->from('store_has_staff');
            $this->db->join('stores', 'stores.id = store_has_staff.fk_store', 'left');
            $this->db->where('store_has_staff.fk_store', $res['id']);
            $this->db->where('store_has_staff.enum_active', 'YES');
            $this->db->where('store_has_staff.enum_isFrontView', 'YES');
            $result[$key]['store_has_barbers'] = $this->db->get()->result_array();

            for ($i = 0; $i < count($result[$key]['store_has_barbers']); $i++) {
                $result[$key]['store_has_barbers'][$i]['var_image'] = (!empty($result[$key]['store_has_barbers'][$i]['var_image'])) ?
                    base_url() . STAFF_PROFILE_IMG . '/' . $result[$key]['store_has_barbers'][$i]['var_image'] :
                    base_url() . 'public/upload/profile_pic/download.jpg';
            }

            $this->db->select('store_has_services.*,
                                TIME_FORMAT(tk_store_has_services.dt_service_time,"%H") as service_hours,
                                TIME_FORMAT(tk_store_has_services.dt_service_time,"%i") as service_minutes,
                                services.var_title as service_title,master_category.id as catId,master_category.var_name as category_name,master_category.enum_type as category_type');
            $this->db->from('store_has_services');
            $this->db->join('services', 'services.id = store_has_services.fk_service', 'left');
            $this->db->join('master_category', 'master_category.id = services.fk_category', 'left');
            $this->db->where('store_has_services.fk_store', $res['id']);
            $this->db->where('store_has_services.enum_enable', 'YES');
            $result[$key]['store_has_services'] = $this->db->get()->result_array();

            $this->db->select('store_has_timing.*');
            $this->db->from('store_has_timing');
            $this->db->where('store_has_timing.fk_store', $res['id']);
            $this->db->where('store_has_timing.int_week !=', $res['int_weekoff']);
            $this->db->order_by('store_has_timing.int_week');
            $result[$key]['store_has_timing'] = $this->db->get()->result_array();

            $result[$key]['store_has_review'] = $this->getStoreReview(array('store_id' => $res['id']));

            for ($h = 0; $h < count($result[$key]['store_has_review']); $h++) {
                $result[$key]['store_has_review'][$h]['userprofile'] = (!empty($result[$key]['store_has_review'][$h]['userprofile']))
                    ? base_url() . USER_PROFILE_IMG . '/' . $result[$key]['store_has_review'][$h]['userprofile']
                    : base_url() . 'public/upload/profile_pic/download.jpg';
            }
            $result[$key]['store_has_portfolio'] = $this->getStorePortfolio(array('store_id' => $res['id']));
            $result[$key]['store_has_offers'] = $this->getStoreOffers(array('store_id' => $res['id']));
            $result[$key]['store_has_packages'] = $this->getStorePackages(array('store_id' => $res['id']));

//            print_array($result[$key]['store_has_review']);

            $this->db->select('store_has_gallery.*');
            $this->db->from('store_has_gallery');
            $this->db->join('stores', 'stores.id = store_has_gallery.fk_store', 'left');
            $this->db->where('store_has_gallery.fk_store', $res['id']);
            $this->db->where('store_has_gallery.enum_ismain', 'YES');
            $this->db->limit(5);
            $result[$key]['temp'] = $this->db->get()->result_array();
            $result[$key]['store_has_images'] = array();
            if (count($result[$key]['temp']) > 0) {
                foreach ($result[$key]['temp'] as $val) {
                    $images = ($val['var_file']) ? base_url() . 'public/upload/store_pic/' . $val['var_file'] : base_url() . 'public/upload/no_image.png';
                    array_push($result[$key]['store_has_images'], $images);
                }
            } else {
                $result[$key]['store_has_images'] = array(base_url() . 'public/upload/no_image.png');
            }
        }
        return $result;
    }

    function sentPushyId($data)
    {
        if (!empty($data)) {
            $userData = array(
                'var_pushy_device_id' => $data['pushyId'],
            );
            $this->db->where('id', $data['user_id']);
            $res = $this->db->update('users', $userData);
            if ($this->db->affected_rows() > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Pushy Id Updated!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Pushy Id not Updated';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'data not found...';
        }
    }


    function getPaymentPopupInit($data)
    {
        if (!empty($data)) {
            $headers = array(
                'Content-Type: application/json'
            );
            // Initialize curl handle
            $ch = curl_init();

            // Set URL to Pushy endpoint
            curl_setopt($ch, CURLOPT_URL, PUSHY_API_URL . '?api_key=' . $apiKey);

            // Set request method to POST
            curl_setopt($ch, CURLOPT_POST, true);

            // Set our custom headers
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

            // Get the response back as string instead of printing it
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Set post data as JSON
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            // Actually send the push
            $result = curl_exec($ch);

            // Display errors
            if (curl_errno($ch)) {
                echo curl_error($ch);
            }

            // Close curl handle
            curl_close($ch);

            // Debug API response
            return $result;
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something Went Wrong!..';
            return $response;
        }
    }

    function getStorePortfolio($data)
    {
        $this->db->select('store_has_portfolio.*,stores.var_title');
        $this->db->from('store_has_portfolio');
        $this->db->join('stores', 'stores.id = store_has_portfolio.fk_store');
        $this->db->where('store_has_portfolio.fk_store', $data['store_id']);
        $data = $this->db->get()->result_array();
        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['nooflikes'] = $this->db->get_where('portfolio_has_likes', array('fk_portfolio' => $data[$i]['id']))->num_rows();
            $data[$i]['var_file'] = base_url() . PORTFOLIO_IMG . '/' . $data[$i]['var_file'];
            $this->db->select('portfolio_has_review.*,
                CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,
                users.var_profile_image');
            $this->db->from('portfolio_has_review');
            $this->db->where('portfolio_has_review.fk_portfolio', $data[$i]['id']);
            $this->db->join('users', 'users.id = portfolio_has_review.fk_user');
            $data[$i]['review'] = $this->db->get()->result_array();

            for ($j = 0; $j < count($data[$i]['review']); $j++) {
                $data[$i]['review'][$j]['var_profile_image'] = !empty($data[$i]['review'][$j]['var_profile_image']) ?
                    base_url() . USER_PROFILE_IMG . '/' . $data[$i]['review'][$j]['var_profile_image'] :
                    base_url() . 'public/upload/no_image.png';
            }
        }
        return $data;
    }

    function addReviewOnPortfolio($data)
    {
        if (!empty($data)) {
            $insertArr = array(
                'fk_user' => $data['user_id'],
                'fk_portfolio' => $data['portfolio_id'],
                'txt_review' => $data['review'],
                'created_at' => date('Y-m-d h:i:s')
            );
            $this->db->insert('portfolio_has_review', $insertArr);
            $lastId = $this->db->insert_id();
            if ($lastId > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Comment Added Successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'something went wrong';
            }
            return $response;
        }
    }

    function toggleUserNotification($data)
    {
        if (!empty($data)) {
            $getNotificationFlag = $this->db->get_where('users', array('id' => $data['user_id']))->row_array()['enum_notification'];
            $updateArr = array(
                'enum_notification' => ($getNotificationFlag == 'YES') ? 'NO' : 'YES'
            );
            $this->db->where('id', $data['user_id']);
            $update = $this->db->update('users', $updateArr);
            if ($update) {
                $response['status'] = 'success';
                $response['message'] = 'User Notification updated Successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'something went wrong';
            }
            return $response;
        }
    }

    function getUserNotificationData($data)
    {
        if (!empty($data['user_id'])) {
            $getNotificationFlag = $this->db->get_where('users', array('id' => $data['user_id']))->row_array()['enum_notification'];
            if (!empty($getNotificationFlag)) {
                $response['data'] = ($getNotificationFlag == 'YES') ? true : false;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'something went wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'user not found';
        }
        return $response;
    }

    function getUserDataFromMob($data)
    {
        if (!empty($data['user_phone'])) {
            $userdata = $this->db->get_where('users', array('bint_phone' => $data['user_phone']))->row_array();
            if (!empty($userdata)) {
                $response['data'] = $userdata;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'user with this number not found';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'something went wrong';
        }
        return $response;
    }

    function getStorePackages($data)
    {
        if (!empty($data['store_id'])) {
            $this->db->select('store_has_package.id as packageId,store_has_package.fk_store as storeId,store_has_package.var_name as packagename,
                               store_has_package.var_price,store_has_package.txt_note as extranote');
            $this->db->from('store_has_package');
            $this->db->where('store_has_package.enum_enable', 'YES');
            $this->db->where('store_has_package.fk_store', $data['store_id']);
            $packages = $this->db->get()->result_array();
            for ($i = 0; $i < count($packages); $i++) {
                $this->db->select('package_has_service.var_price as servicepackprice,
                services.id as masterserviceid,services.var_title as servicename,
                store_has_services.var_price as serviceprice,store_has_services.dt_service_time as servicetime');
                $this->db->from('package_has_service');
                $this->db->join('services', 'services.id = package_has_service.fk_service');
                $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
                $this->db->where('package_has_service.fk_package', $packages[$i]['packageId']);
                $this->db->group_by('masterserviceid');
                $packages[$i]['package_has_services'] = $this->db->get()->result_array();


                $this->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'services.var_title) as services');
                $this->db->from('package_has_service');
                $this->db->join('services', 'services.id = package_has_service.fk_service');
                $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
                $this->db->where('package_has_service.fk_package', $packages[$i]['packageId']);
                $packages[$i]['services'] = $this->db->get()->row_array();

            }
            if (!empty($packages)) {
                $response = $packages;
            } else {
                $response = array();
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'store id not found';
        }
        return $response;
    }

    function getStoreOffers($data)
    {
        if (!empty($data['store_id'])) {
            $this->db->select('store_has_services.*,
                               services.var_title as service_title,
                               GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'services.var_title) as service_title,
                               master_category.id as catId,
                               master_category.var_name as category_name,
                               master_category.enum_type as category_type,
                               store_has_offer.id as offerId,
                               store_has_offer.fk_store as storeId,
                               store_has_offer.var_name as offername,
                               store_has_offer.var_price,
                               store_has_offer.var_offer_price,
                               store_has_offer.txt_note as extranote,
                               DATE_FORMAT(tk_store_has_offer.dt_start_date,"%d %b %Y") as offerstartdate,
                               DATE_FORMAT(tk_store_has_offer.dt_end_date,"%d %b %Y") as offerenddate
                               ');
            $this->db->from('store_has_offer');
            $this->db->join('offer_has_service', 'offer_has_service.fk_offer = store_has_offer.id');
            $this->db->join('services', 'offer_has_service.fk_service = services.id');
            $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
            $this->db->join('master_category', 'master_category.id = services.fk_category', 'left');
            $this->db->where('store_has_offer.enum_enable', 'YES');
            $this->db->where('store_has_offer.fk_store', $data['store_id']);
            $this->db->group_by('offerId');
            $offers = $this->db->get()->result_array();
            for ($i = 0; $i < count($offers); $i++) {
                $this->db->select('offer_has_service.var_price as serviceactualofferprice,
                offer_has_service.var_offer_price as serviceofferprice,
                services.id as masterserviceid,services.var_title as servicename,
                store_has_services.var_price as store_has_serviceprice,store_has_services.dt_service_time as servicetime');
                $this->db->from('offer_has_service');
                $this->db->join('services', 'services.id = offer_has_service.fk_service');
                $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
                $this->db->where('offer_has_service.fk_offer', $offers[$i]['offerId']);
                $this->db->group_by('masterserviceid');
                $offers[$i]['offer_has_services'] = $this->db->get()->result_array();
            }
            if (!empty($offers)) {
                $response = $offers;
            } else {
                $response = array();
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'store id not found';
        }

        return $response;
    }

    function makePayment($data, $json_response)
    {
        if (!empty($data)) {
            $insertArr = array(
                'fk_user' => $data['user_id'],
                'fk_booking' => $data['booking_id'],
                'fk_store' => $data['store_id'],
                'var_orderID' => $data['order_id'],
                'dt_booking_date' => date('Y-m-d', strtotime($data['booking_date'])),
                'tm_booking_time' => date('H:i:s', strtotime($data['booking_time'])),
                'var_price' => $data['price'],
                'enum_status' => $data['status'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('payment', $insertArr);
            $lastId = $this->db->insert_id();
            if ($lastId > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Payment added successfully';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'store id not found';
            }
        }
        return $json_response;
    }

    function getUserPaymentHistory($data, $json_response)
    {
        $this->db->select('CONCAT(' . TABLE_PREFIX . 'users.var_fname," ",' . TABLE_PREFIX . 'users.var_lname) as username,
                           users.bint_phone,
                           users.var_email,
                           users.var_profile_image,
                           stores.var_title as store_title,
                           payment.dt_booking_date,
                           payment.tm_booking_time,
                           payment.var_price,
                           payment.enum_status,
                           payment.fk_booking,
                           payment.fk_user,
                           payment.fk_store');
        $this->db->from('payment');
        $this->db->join('users', 'users.id = payment.fk_user');
        $this->db->join('stores', 'stores.id = payment.fk_store');
        $this->db->join('booking', 'booking.id = payment.fk_booking');
        $this->db->where('payment.fk_user', $data['user_id']);
        $paymentData = $this->db->get()->result_array();
        for ($i = 0; $i < count($paymentData); $i++) {
            $paymentData[$i]['var_profile_image'] = (empty($paymentData[$i]['var_profile_image']))
                ? base_url() . 'public/upload/no_image.png'
                : base_url() . USER_PROFILE_IMG . '/' . $paymentData[$i]['var_profile_image'];
            $paymentData[$i]['service'] = getServiceForPaymentByBookId($paymentData[$i]['fk_booking']);
        }
        if (!empty($paymentData)) {
            return $paymentData;
        } else {
            return [];
        }
    }

    function togglePortfolioLike($data)
    {
        $getLike = $this->db->get_where('portfolio_has_likes', array('fk_user' => $data['user_id'], 'fk_portfolio' => $data['portfolio_id']))->row_array();
        if(!empty($getLike)){
            $this->db->where('id',$getLike['id']);
            $del = $this->db->delete('portfolio_has_likes');
        }else{
            $insrtArr = array(
                'fk_user'=>$data['user_id'],
                'fk_portfolio'=>$data['portfolio_id'],
                'created_at'=>date('Y-m-d H:i:s')
            );
            $this->db->insert('portfolio_has_likes',$insrtArr);
            $lastId = $this->db->insert_id();
        }
        if ($lastId > 0 || $del) {
            $json_response['status'] = 'success';
            $json_response['nooflikes'] = $this->db->get_where('portfolio_has_likes', array('fk_portfolio' => $data['portfolio_id']))->num_rows();
            $json_response['message'] = 'portfolio like successfully';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'store id not found';
        }
        return $json_response;
    }
}
