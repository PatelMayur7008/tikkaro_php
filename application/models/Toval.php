<?php

class Toval extends CI_Model {

    // id to val
    function idtoval($id, $val, $name, $table) {
        if (isset($val)) {
            $row = $this->idtorow($id, $val, $table, $name);
            if (isset($row->$name)) {
                return $row->$name;
            }
        }
    }

    function idtorow($id, $val, $table, $name = '*') {
        $this->db->select($name);
        $this->db->from($table);
        $this->db->where($id, $val);
        $query = $this->db->get();
//        print_r($query);exit
//        echo  $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->row();
//             $row = $query->result_array();exit;
            return $row;
        }
    }

    function idtorowarr($id, $val, $table, $name = '*') {

        $this->db->select($name);
        $this->db->from($table);
        $this->db->where($id, $val);
        $query = $this->db->get();
//        echo  $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            $row = $query->row_array();
            return $row;
        }
    }

    function idtorowarray($id, $val, $table, $name = '*', $type = "data") {

        $this->db->select($name);
        $this->db->from($table);

        $this->db->where($id, $val);
        $query = $this->db->get();
        //echo  $this->db->last_query();
        if ($type == "count") {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
//                echo $row['title'];
//                echo $row['name'];
//                echo $row['body'];
                $result[] = $row;
            }
        }
        return $result;
    }

    function idtorowarray_in($id, $val, $table, $name = '*', $type = "data") {

        $this->db->select($name);
        $this->db->from($table);

        $this->db->where_in($id, $val);
        $query = $this->db->get();
        //echo  $this->db->last_query();        
        if ($type == "count") {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
//                echo $row['title'];
//                echo $row['name'];
//                echo $row['body'];
                $result[] = $row;
            }
        }
        return $result;
    }

    function fetchArrayMC($condiion, $table, $name = '*', $type = "data", $condiion_in = array()) {

        $this->db->select($name);
        $this->db->from($table);
        if (!empty($condiion)) {
            foreach ($condiion as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        if (!empty($condiion_in)) {
            foreach ($condiion_in as $key => $value) {
                $this->db->where_in($key, $value);
            }
        }
        $query = $this->db->get();
//        echo  $this->db->last_query();
        if ($type == "count") {
            return $query->num_rows();
        }
        if ($query->num_rows() > 0) {
            foreach ($query->result_array() as $row) {
//                echo $row['title'];
//                echo $row['name'];
//                echo $row['body'];
                $result[] = $row;
            }
        }
        return $result;
    }

    function id_delete($id, $tb_name, $clm) {
        $this->db->where($clm, $id);
        $sql = $this->db->delete($tb_name);
        return $sql;
        //echo  $this->db->last_query();
    }

    function id_delete_array($id_array, $tb_name, $clm, $condition = array()) {

        if (!empty($condition)) {
            $this->db->where($condition);
        }
        $this->db->where_in($clm, $id_array);
        $sql = $this->db->delete($tb_name);
        return TRUE;
    }

    function update_delete($id_array, $tb_name, $clm, $data, $condition) {
        if (!empty($condition)) {
            $this->db->where($condition);
        }
        $this->db->where_in($clm, $id_array);
        $sql = $this->db->update($tb_name, $data);
//        echo  $this->db->last_query();        
    }

    function createPNGFromBase64($param = array()) {
        $data = $param['data'];
        list($type, $data) = explode(';', $data);
        list(, $data) = explode(',', $data);

        $data = base64_decode($data);

        $savePath = $param['path'] . $param['filename'];

        file_put_contents($savePath, $data);
    }

    function calculateResizeImage($width, $height, $thumb_width, $thumb_height) {
        $image_height = floor(($height / $width) * $thumb_width);
        $image_width = $thumb_width;

        if ($image_height > $thumb_height) {
            $image_width = floor(($width / $height) * $thumb_height);
            $image_height = $thumb_height;
        }
        $resultArray = array($image_width, $image_height);

        return $resultArray;
    }

    function search_array($array, $key, $value) {
        // print_r($array);
        $results = array();

        if (is_array($array)) {
            echo $array[$key];
            echo $value;
            exit;
            if (isset($array[$key]) && $array[$key] == $value) {
                $results[] = $array;
            }

            foreach ($array as $subarray) {
                $results = array_merge($results, $this->search_array($subarray, $key, $value));
            }
        }
        // print_r($results);exit;
        return $results;
    }

    function in_multiarray($array, $field, $elem) {
        $top = sizeof($array) - 1;
        $bottom = 0;
        while ($bottom <= $top) {
            if ($array[$bottom][$field] == $elem)
                return $bottom;
            else
            if (is_array($array[$bottom][$field]))
                if (in_multiarray($elem, ($array[$bottom][$field])))
                    return $bottom;

            $bottom++;
        }
        return FALSE;
    }

    function valid_url($url) {
        if (!filter_var($url)) {

            if (empty($url)) {
                $img = 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image';
            } else {
                $img = $this->base_path . $url;
            }
        } else {
            $img = $url;
        }
        return $img;
    }

    function str_replace_from_array($str, $replace_arr, $pattern = '/<p>&nbsp;<\/p>/') {
        $count = 0;
        $str1 = preg_replace_callback($pattern, function ($match) use ($replace_arr, &$count) {
            return count($replace_arr) > $count ? $replace_arr[$count++] : $match[0];
        }, $str);
        return $str1;
    }

    function download_csv($dataArray, $fileName) {

        if (count($dataArray) > 0) {
            $this->load->helper('csv');
            array_unshift($dataArray, array_keys($dataArray[0]));
            array_to_csv($dataArray, $fileName . '.csv');
            exit;
        } else {
            $dataArray[0]['No data available in table'] = '';
            $this->load->helper('csv');
            array_unshift($dataArray, array_keys($dataArray[0]));
            array_to_csv($dataArray, $fileName . '.csv');
            exit;
        }
    }

    function customerDetail() {
        $loginCustomerId = getLoginCustomerUserId();
        $this->db->select('user.var_profile_image,CONCAT(' . TABLE_PREFIX . 'user.var_fname," ",' . TABLE_PREFIX . 'user.var_lname) as UserName,
            client.var_company_name');
        $this->db->join('client', 'client.id = user.fk_client');
        $this->db->where('user.id', $loginCustomerId);
        $userDetail = $this->db->get('user')->result_array();
        return $userDetail;
    }

    function upcommingProjects() {
        $loginCustomerId = getLoginCustomerUserId();
        $this->db->select('client_has_project.*,project_has_user.fk_user as userid');
        $this->db->join('project_has_user', 'client_has_project.id = project_has_user.fk_project');
        $this->db->where('project_has_user.fk_user', $loginCustomerId);
        $projects = $this->db->order_by('id', 'DESC')->get('client_has_project')->result_array();
        return $projects;
    }

    function CallPushyAPI($appType, $userId, $message) {

        // Default post data to provided options or empty array
        $post = $options ? : array();

        if ($appType == 'front') {
            $tbl = 'users';
            $apiKey = PUSHY_API_FRONT_KEY;
        } else if ($appType == 'biz') {
            $tbl = 'bussiness';
            $apiKey = PUSHY_API_BIZ_KEY;
        }
        $this->db->select('var_pushy_device_id');
        $this->db->where('id', $userId);
        $res = $this->db->get($tbl)->row_array();
//        print_r($res); exit;
        // Set notification payload and recipients
        $post['to'] = array($res['var_pushy_device_id']);

        $post['data'] = array('message' => $message);

        // Set Content-Type header since we're sending JSON
        $headers = array(
            'Content-Type: application/json'
        );

        // Initialize curl handle
        $ch = curl_init();

        // Set URL to Pushy endpoint
        curl_setopt($ch, CURLOPT_URL, PUSHY_API_URL . '?api_key=' . $apiKey);

        // Set request method to POST
        curl_setopt($ch, CURLOPT_POST, true);

        // Set our custom headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // Get the response back as string instead of printing it
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Set post data as JSON
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post, JSON_UNESCAPED_UNICODE));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Actually send the push
        $result = curl_exec($ch);

        // Display errors
        if (curl_errno($ch)) {
            echo curl_error($ch);
        }

        // Close curl handle
        curl_close($ch);

        // Debug API response
        return $result;
    }

}
