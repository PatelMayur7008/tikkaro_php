<?php
header('Access-Control-Allow-Origin: *');

class APIbiz_model extends CI_Model
{

    function checkAuthentication($data)
    {
        if (!empty($data)) {
            $this->db->where('var_phone', $data['phone_number']);
            $this->db->where('var_bussiness_code', $data['store_code']);
            $checkStore = $this->db->get('bussiness')->row_array();
            $stores = $this->getAllStoreByBussCode($checkStore['id']);
            $otp = $this->db->get_where('bussiness', array('id' => $checkStore['id']))->row_array();

            for ($j = 0; $j < count($stores['data']); $j++) {
                if (empty($stores['data'][$j]['var_logo'])) {
                    $stores['data'][$j]['var_logo'] = base_url() . 'public/upload/no_image.png';
                } else {
                    $stores['data'][$j]['var_logo'] = base_url() . 'public/upload/store_logo/' . $stores['data'][$j]['var_logo'];
                }
            }
            if (!empty($checkStore)) {
                $response['status'] = 'success';
                $response['data'] = $otp;
                $response['store_data'] = $stores['data'];
                $response['message'] = 'Login successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Authentication Failed!..';
            }
        }
        return $response;
    }

    function authentication($data)
    {
        if (!empty($data)) {
            $this->db->where('var_phone', $data['phone_number']);
            $this->db->where('var_bussiness_code', $data['store_code']);
            $checkStore = $this->db->get('bussiness')->row_array();
            if (!empty($checkStore)) {
                // Login Store
                $storeData = array(
                    'int_otp' => $data['otp'],
                );
                $this->db->where('id', $checkStore['id']);
                $res = $this->db->update('bussiness', $storeData);
                if ($this->db->affected_rows() > 0) {
                    $response['status'] = 'success';
                    $response['message'] = 'Login successfully!';
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'OTP not matched...';
                }
            }
        }
        return $response;
    }

    function addBankDetails($data)
    {
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_id']))->row_array()['id'];
        $bankDetail = $this->db->get_where('store_has_bank_details', array('fk_store' => $storeId))->row_array();
        if (!empty($data)) {
            if ($storeId > 0) {
                if (empty($bankDetail)) {
                    $bankArr = array(
                        'fk_store' => $storeId,
                        'var_accountno' => $data['acc_no'],
                        'var_ifsc_code' => $data['ifsc_code'],
                        'var_name' => $data['name'],
                        'var_bank_name' => $data['bank_name'],
                        'var_branch_name' => $data['branch_name'],
                        'txt_address' => $data['address'],
                        'enum_acc_type' => $data['acc_type'],
                        'var_debit_card_type' => $data['card_type'],
                        'var_card_number' => $data['card_number'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('store_has_bank_details', $bankArr);
                    $lastId = $this->db->insert_id();
                } else {
                    $bankArr = array(
                        'var_accountno' => ($data['acc_no']) ? $data['acc_no'] : $bankDetail['var_accountno'],
                        'var_ifsc_code' => ($data['ifsc_code']) ? $data['ifsc_code'] : $bankDetail['var_ifsc_code'],
                        'var_name' => ($data['name']) ? $data['name'] : $bankDetail['var_name'],
                        'var_bank_name' => ($data['bank_name']) ? $data['bank_name'] : $bankDetail['var_bank_name'],
                        'var_branch_name' => ($data['branch_name']) ? $data['branch_name'] : $bankDetail['var_branch_name'],
                        'txt_address' => ($data['address']) ? $data['address'] : $bankDetail['txt_address'],
                        'enum_acc_type' => ($data['acc_type']) ? $data['acc_type'] : $bankDetail['enum_acc_type'],
                        'var_debit_card_type' => ($data['card_type']) ? $data['card_type'] : $bankDetail['var_debit_card_type'],
                        'var_card_number' => ($data['card_number']) ? $data['card_number'] : $bankDetail['var_card_number']
                    );
                    $this->db->where('id', $bankDetail['id']);
                    $result = $this->db->update('store_has_bank_details', $bankArr);
                }

            }
            if ($lastId > 0 || $result) {
                $response['status'] = 'success';
                $response['message'] = 'Bank Detail Added successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function addStoreStaff($data)
    {
        $this->load->library('Mylibrary');
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_id']))->row_array()['id'];
        if (!empty($data)) {
            if (!empty($_FILES['file'])) {
                $imgexten = explode('.', $_FILES['file']['name']);
                $img_name = 'file';
                $img_last_key = end($imgexten);
                $image_name = url_title($imgexten[0]) . time();
                $array = array(
                    'image_pet_name' => $img_name,
                    'image_name' => $image_name,
                    'path' => STAFF_PROFILE_IMG,
                    'extension' => $img_last_key,
                );
                $result = $this->mylibrary->upload_image($array);
                if (!empty($result)) {
                    $var_logo = $result['orig_name'];
                } else {
                    $var_logo = null;
                }
            } else {
                $var_logo = null;
            }
            $bankArr = array(
                'fk_store' => $storeId,
                'var_name' => $data['name'],
                'var_image' => $var_logo,
                'var_phone' => $data['phone'],
                'dt_startdate' => date('Y-m-d H:i:s', strtotime($data['start_date'])),
                'dt_enddate' => date('Y-m-d H:i:s', strtotime($data['end_date'])),
                'enum_isFrontView' => $data['enum_frontview'],
                'enum_active' => $data['enum_active'],
                'enum_current' => $data['enum_current'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_staff', $bankArr);
            $lastId = $this->db->insert_id();
            if ($lastId > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Staff Store Added successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function addStoreHoliday($data)
    {
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_id']))->row_array()['id'];
        if (!empty($data)) {
            $date = date('Y-m-d', strtotime($data['holiday_date']));
            $Holidayrecord = $this->db->get_where('store_has_holidays', array('fk_store' => $storeId, 'dt_holiday_date' => $date))->row_array();
            if (!empty($Holidayrecord)) {
                $holidayArr = array(
                    'fk_store' => $storeId,
                    'dt_holiday_date' => date('Y-m-d', strtotime($data['holiday_date'])),
                    'dt_start_time' => date('H:i:s', strtotime($data['start_time'])),
                    'dt_end_time' => date('H:i:s', strtotime($data['end_time'])),
                    'txt_note' => ($data['holiday_note']) ? $data['holiday_note'] : NULL,
                    'enum_holiday' => ($data['enum_holiday']) ? 'YES' : 'NO',
                );
                $this->db->where('id', $Holidayrecord['id']);
                $update = $this->db->update('store_has_holidays', $holidayArr);
            } else {
                $holidayArr = array(
                    'fk_store' => $storeId,
                    'dt_holiday_date' => date('Y-m-d', strtotime($data['holiday_date'])),
                    'dt_start_time' => date('H:i:s', strtotime($data['start_time'])),
                    'dt_end_time' => date('H:i:s', strtotime($data['end_time'])),
                    'txt_note' => ($data['holiday_note']) ? $data['holiday_note'] : NULL,
                    'enum_holiday' => ($data['enum_holiday']) ? 'YES' : 'NO',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_holidays', $holidayArr);
                $lastId = $this->db->insert_id();
            }
            if ($lastId > 0 || $update) {
                $response['status'] = 'success';
                $response['message'] = 'Store Holiday Added successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function getAllFacility()
    {
        return $this->db->get_where('extra_facility', array('enum_enable' => 'YES'))->result_array();
    }

    function getStoreDetailById($data)
    {
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array()['id'];

        $this->db->select('stores.*');
        $this->db->from('stores');
        $this->db->where('stores.id', $storeId);
        $res['store_detail'] = $this->db->get()->row_array();

        if (empty($res['store_detail']['var_logo'])) {
            $res['store_detail']['var_logo'] = base_url() . 'public/upload/no_image.png';
        } else {
            $res['store_detail']['var_logo'] = base_url() . 'public/upload/store_logo/' . $res['store_detail']['var_logo'];
        }

        $this->db->select('extra_facility.var_name,extra_facility.id');
        $this->db->from('extra_facility');
        $this->db->join('store_has_facility', 'store_has_facility.fk_facility = extra_facility.id', 'left');
        $this->db->where('store_has_facility.fk_store', $storeId);
        $res['store_detail']['facility'] = $this->db->get()->result_array();
        return $res;
    }

    function getBookingRecordByStoreCode($data)
    {
        $this->db->select('booking.*,CONCAT(' . TABLE_PREFIX . 'users.var_fname," ",' . TABLE_PREFIX . 'users.var_lname) as username,users.var_email,users.bint_phone,
                            
                            ');
        $this->db->from('booking');
        $this->db->join('booking_has_service', 'booking.id= booking_has_service.fk_booking', 'left');
        $this->db->join('booking_has_timeslot', 'booking.id = booking_has_timeslot.fk_booking', 'left');
        //    $this->db->join('timeslots','timeslots.id = booking_has_timeslot.fk_timeslot','left');
        //   $this->db->join('services','services.id = booking_has_service.fk_service','left');
        $this->db->join('users', 'users.id = booking.fk_user', 'left');
        $this->db->join('stores', 'stores.id = booking.fk_store', 'left');
        //   $this->db->join('store_has_services','stores.id = store_has_services.fk_store','left');
        $this->db->where('stores.var_store_code', $data['store_code']);
        $this->db->group_by('booking.id');
        $result['booking'] = $this->db->get()->result_array();
        for ($i = 0; $i < count($result['booking']); $i++) {
            $this->db->select('services.id,services.var_title,store_has_services.var_price as servicePrice');
            $this->db->from('services');
            $this->db->join('booking_has_service', 'booking_has_service.fk_service= services.id', 'left');
            $this->db->join('store_has_services', 'store_has_services.fk_service = services.id', 'left');
            $this->db->where('booking_has_service.fk_booking', $result['booking'][$i]['id']);
            $this->db->group_by('booking_has_service.fk_service');
            $result['booking'][$i]['services'] = $this->db->get()->result_array();


            $this->db->select('timeslots.id,timeslots.slot_time as timeslot');
            $this->db->from('timeslots');
            $this->db->join('booking_has_timeslot', 'booking_has_timeslot.fk_timeslot = timeslots.id');
            $this->db->where('booking_has_timeslot.fk_booking', $result['booking'][$i]['id']);
            $this->db->group_by('booking_has_timeslot.fk_timeslot');
            $result['booking'][$i]['timeslot'] = $this->db->get()->result_array();
        }
        return $result;
    }

    function getAllStaffByStoreId($data)
    {
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array()['id'];
        if (!empty($storeId)) {
            $staffdata = $this->db->get_where('store_has_staff', array('fk_store' => $storeId))->result_array();
            for ($i = 0; $i < count($staffdata); $i++) {
                if (empty($staffdata[$i]['var_image'])) {
                    $staffdata[$i]['var_image'] = base_url() . 'public/upload/no_image.png';
                } else {
                    $staffdata[$i]['var_image'] = base_url() . 'public/upload/staff_profile_pic/' . $staffdata[$i]['var_image'];
                }
            }

            if (!empty($staffdata)) {
                return $staffdata;
            } else {
                return [];
            }
        } else {
            return 'store Id Not found';
        }
    }

    function getStoreHoliday($data)
    {
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array()['id'];
        if (!empty($storeId)) {
            $staffdata = $this->db->get_where('store_has_holidays', array('fk_store' => $storeId))->result_array();
            if (!empty($staffdata)) {
                $response['data'] = $staffdata;
            } else {
                $response['data'] = array();
            }
        } else {
            $response['message'] = 'Store Id not Found';
        }
        return $response;
    }

    function deleteStoreHoliday($data)
    {
        $this->db->where('id', $data['holiday_id']);
        $this->db->delete('store_has_holidays');
        if ($this->db->affected_rows()) {
            $response['status'] = 'success';
            $response['message'] = 'Store Holiday Deleted successfully!';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function deleteStoreStaff($data)
    {
        $this->db->where('id', $data['staff_id']);
        $this->db->delete('store_has_staff');
        if ($this->db->affected_rows() > 0) {
            $response['status'] = 'success';
            $response['message'] = 'Store Staff Deleted successfully!';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function updateStoreStaff($data)
    {
        $this->load->library('Mylibrary');
        $staffData = $this->db->get_where('store_has_staff', array('id' => $data['staff_id']))->row_array();
        if (!empty($staffData)) {
            if (!empty($_FILES['file'])) {
                $imgexten = explode('.', $_FILES['file']['name']);
                $img_name = 'file';
                $img_last_key = end($imgexten);
                $image_name = url_title($imgexten[0]) . time();
                $array = array(
                    'image_pet_name' => $img_name,
                    'image_name' => $image_name,
                    'path' => STAFF_PROFILE_IMG,
                    'extension' => $img_last_key,
                );
                $result = $this->mylibrary->upload_image($array);
                if (!empty($result)) {
                    $var_logo = $result['orig_name'];
                } else {
                    $var_logo = $staffData['var_image'];
                }
            } else {
                $var_logo = $staffData['var_image'];
            }
            $updateArr = array(
                'var_name' => (!empty($data['name'])) ? $data['name'] : $staffData['var_name'],
                'var_phone' => (!empty($data['phone'])) ? $data['phone'] : $staffData['var_phone'],
                'var_image' => $var_logo,
                'dt_startdate' => (!empty($data['start_date'])) ? date('Y-m-d H:i:s', strtotime($data['start_date'])) : $staffData['dt_startdate'],
                'dt_enddate' => (!empty($data['end_date'])) ? date('Y-m-d H:i:s', strtotime($data['end_date'])) : $staffData['dt_enddate'],
                'enum_isFrontView' => (!empty($data['enum_frontview'])) ? $data['enum_frontview'] : $staffData['enum_isFrontView'],
                'enum_active' => (!empty($data['enum_active'])) ? $data['enum_active'] : $staffData['enum_active'],
                'enum_current' => (!empty($data['enum_current'])) ? $data['enum_current'] : $staffData['enum_current'],
            );
            $this->db->where('id', $data['staff_id']);
            $res = $this->db->update('store_has_staff', $updateArr);
            if ($res) {
                $response['status'] = 'success';
                $response['message'] = 'Store Staff Updated successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Staff not found';
        }
        return $response;
    }

    function updateStoreHoliday($data)
    {
        $holidayData = $this->db->get_where('store_has_holidays', array('id' => $data['holiday_id']))->row_array();
        if (!empty($holidayData)) {
            $updateArr = array(
                'dt_holiday_date' => (!empty($data['holiday_date'])) ? date('Y-m-d H:i:s', strtotime($data['holiday_date'])) : $holidayData['dt_holiday_date'],
                'dt_start_time' => (!empty($data['start_time'])) ? date('H:i:s', strtotime($data['start_time'])) : $holidayData['dt_start_time'],
                'dt_end_time' => (!empty($data['end_time'])) ? date('H:i:s', strtotime($data['end_time'])) : $holidayData['dt_end_time'],
                'txt_note' => (!empty($data['holiday_note'])) ? $data['holiday_note'] : $holidayData['txt_note'],
                'enum_holiday' => (($data['enum_holiday'])) ? 'YES' : 'NO'
            );
            $this->db->where('id', $data['holiday_id']);
            $res = $this->db->update('store_has_holidays', $updateArr);
            if ($res) {
                $response['status'] = 'success';
                $response['message'] = 'Store holiday Updated successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Record not found';
        }
        return $response;
    }

    function getAllSetting()
    {
        return $this->db->get_where('setting', array('enum_enable' => 'YES'))->result_array();
    }

    function getStoreSetting($data)
    {
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            $settingarr = array(
                'int_no_book_seats' => $storeData['int_no_book_seats'],
                'int_weekoff' => $storeData['int_weekoff'],
                'var_fb_link' => $storeData['var_fb_link'],
                'var_twitter_link' => $storeData['var_twitter_link'],
                'var_whatsapp_no' => $storeData['var_whatsapp_no'],
                'store_code' => $storeData['var_store_code'],
                'var_instagram_link' => $storeData['var_instagram_link'],
                'enum_store_type' => $storeData['enum_store_type'],
                'store_code' => $storeData['var_store_code'],
            );
            $response['data'] = $settingarr;
        } else {
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function updateStoreSetting($data)
    {
//        print_array($data); exit;
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            $settingarr = array(
                'int_no_book_seats' => (!empty($data['int_no_book_seats'])) ? $data['int_no_book_seats'] : $storeData['int_no_book_seats'],
                'int_weekoff' => (is_numeric($data['int_weekoff'])) ? $data['int_weekoff'] : $storeData['int_weekoff'],
                'var_fb_link' => (!empty($data['var_fb_link'])) ? $data['var_fb_link'] : $storeData['var_fb_link'],
                'var_twitter_link' => (!empty($data['var_twitter_link'])) ? $data['var_twitter_link'] : $storeData['var_twitter_link'],
                'var_instagram_link' => (!empty($data['var_instagram_link'])) ? $data['var_instagram_link'] : $storeData['var_instagram_link'],
                'enum_store_type' => (!empty($data['enum_store_type'])) ? $data['enum_store_type'] : $storeData['enum_store_type'],
                'var_whatsapp_no' => (!empty($data['var_whatsapp_no'])) ? $data['var_whatsapp_no'] : $storeData['var_whatsapp_no'],
            );
            $this->db->where('id', $storeData['id']);
            $update = $this->db->update('stores', $settingarr);
            if ($update) {
                $response['status'] = 'success';
                $response['message'] = 'Store setting updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function updateBussinessProfile($data)
    {
        $this->load->library('Mylibrary');
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            if (!empty($_FILES['file'])) {
                $imgexten = explode('.', $_FILES['file']['name']);
                $img_name = 'file';
                $img_last_key = end($imgexten);
                $image_name = url_title($imgexten[0]) . time();
                $array = array(
                    'image_pet_name' => $img_name,
                    'image_name' => $image_name,
                    'path' => STORE_LOGO_IMG,
                    'extension' => $img_last_key,
                );
                $result = $this->mylibrary->upload_image($array);
                if (!empty($result)) {
                    $var_logo = $result['orig_name'];
                } else {
                    $var_logo = $storeData['var_logo'];
                }
            } else {
                $var_logo = $storeData['var_logo'];
            }


            $settingarr = array(
                'var_title' => (!empty($data['var_title'])) ? $data['var_title'] : $storeData['var_title'],
                'txt_address' => (!empty($data['txt_address'])) ? $data['txt_address'] : $storeData['txt_address'],
                'var_latitude' => (!empty($data['var_latitude'])) ? $data['var_latitude'] : $storeData['var_latitude'],
                'var_longitude' => (!empty($data['var_longitude'])) ? $data['var_longitude'] : $storeData['var_longitude'],
                'int_areacode' => (!empty($data['int_areacode'])) ? $data['int_areacode'] : $storeData['int_areacode'],
                'bint_phone' => (!empty($data['bint_phone'])) ? $data['bint_phone'] : $storeData['bint_phone'],
                'var_logo' => $var_logo,
                'var_email' => (!empty($data['var_email'])) ? $data['var_email'] : $storeData['var_email'],
                'txt_note' => (!empty($data['txt_note'])) ? $data['txt_note'] : $storeData['txt_note']
            );
            $this->db->where('id', $storeData['id']);
            $update = $this->db->update('stores', $settingarr);
            if (!empty($data['facility'])) {
                $this->db->where('fk_store', $storeData['id']);
                $this->db->delete('store_has_facility');
                for ($i = 0; $i < count($data['facility']); $i++) {
                    $insertArr = array(
                        'fk_store' => $storeData['id'],
                        'fk_facility' => $data['facility'][$i]['id']
                    );
                    $this->db->insert('store_has_facility', $insertArr);
                }
            } else {
                $this->db->where('fk_store', $storeData['id']);
                $this->db->delete('store_has_facility');
            }

            if ($update) {
                $response['status'] = 'success';
                $response['message'] = 'Store Bussiness Profile updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function getStoreTiming($data)
    {
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            $timings = $this->db->get_where('store_has_timing', array('fk_store' => $storeData['id'], 'int_week !=' => $storeData['int_weekoff']))->result_array();
            if (!empty($timings)) {
                $response['data'] = $timings;
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Store not have any timing yet';
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function updateStoreTiming($data)
    {
        if (!empty($data)) {
            for ($i = 0; $i < count($data); $i++) {
                $updateArr = array(
                    'dt_open_time' => $data[$i]['dt_open_time'],
                    'dt_close_time' => $data[$i]['dt_close_time']
                );
                $this->db->where('id', $data[$i]['id']);
                $result = $this->db->update('store_has_timing', $updateArr);


                $int_week_day = $this->db->get_where('store_has_timing', array('id' => $data[$i]['id']))->row_array()['int_week'];

                $timesolt = $this->db->get('timeslots')->result_array();;

                for ($k = 0; $k < count($timesolt); $k++) {
                    if ((strtotime($data['dt_open_time']) > strtotime($timesolt[$k]['slot_time']))
                        || ((strtotime($data['dt_close_time']) < strtotime($timesolt[$k]['slot_time'])))
                    ) {
                        $enable = 'NO';
                    } else {
                        $enable = 'YES';
                    }
                    $updatearr = array(
                        'enum_enable' => $enable,
                    );
//                    $this->db->where('fk_store', $data['fk_store']);
                    $this->db->where('int_week_day', $int_week_day);
                    $this->db->where('fk_timeslot', $timesolt[$k]['id']);
                    $this->db->update('store_has_timeslots', $updatearr);
                }
            }
            if ($result) {
                $response['status'] = 'success';
                $response['message'] = 'Store Timing updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something will be wrong';
        }
        return $response;
    }

    function getAllService()
    {
        return $this->db->get_where('services', array('enum_enable' => 'YES'))->result_array();
    }

    function getStoreService($data)
    {
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            $this->db->select('services.id,services.var_title,store_has_services.enum_enable,store_has_services.var_price,store_has_services.dt_service_time');
            $this->db->from('services');
            $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
//            $this->db->join('stores','store_has_services.fk_store = stores.id');
            $this->db->where('store_has_services.fk_store', $storeData['id']);
            $storeServices = $this->db->get()->result_array();
            if (!empty($storeServices)) {
                $response['data'] = $storeServices;
            } else {
                $response['message'] = 'Store has no services found';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function updateStoreService($data)
    {
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            if ($data['service']['is_new'] == true) {
                $service = array(
                    'var_title' => $data['service']['var_title'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('services', $service);
                $lastid = $this->db->insert_id();
                $storeService = array(
                    'fk_service' => $lastid,
                    'fk_store' => $storeData['id'],
                    'var_price' => $data['service']['var_price'],
                    'dt_service_time' => $data['service']['dt_service_time'],
                    'enum_enable' => 'NO',
                    'created_at' => date('Y-m-d H:i:s')
                );
                $this->db->insert('store_has_services', $storeService);
                $lastStoreid = $this->db->insert_id();
            } else {
                $store_has_service = $this->db->get_where('store_has_services', array('fk_store' => $storeData['id'], 'fk_service' => $data['service']['id']))->row_array();
                if (empty($store_has_service)) {
                    $storeService = array(
                        'fk_service' => $data['service']['id'],
                        'fk_store' => $storeData['id'],
                        'var_price' => $data['service']['var_price'],
                        'dt_service_time' => $data['service']['dt_service_time'],
                        'enum_enable' => 'NO',
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('store_has_services', $storeService);
                    $lastStoreid = $this->db->insert_id();
                } else {
                    $storeService = array(
                        'var_price' => $data['service']['var_price'],
                        'dt_service_time' => $data['service']['dt_service_time'],
                        'enum_enable' => $data['service']['enum_enable']
                    );
                    $this->db->where('fk_store', $storeData['id']);
                    $this->db->where('fk_service', $data['service']['id']);
                    $update = $this->db->update('store_has_services', $storeService);
                }
            }

            if ($lastStoreid > 0 || $update) {
                $response['status'] = 'success';
                $response['message'] = 'Store Services updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }


        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function deleteStoreService($data)
    {
        $storeData = $this->db->get_where('stores', array('var_store_code' => $data['store_code']))->row_array();
        if (!empty($storeData)) {
            $this->db->where('fk_store', $storeData['id']);
            $this->db->where('fk_service', $data['service_id']);
            $this->db->delete('store_has_services');
            if ($this->db->affected_rows() > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Store Service deleted successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will bo wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function getCustomerStats($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        $this->db->select('booking.dt_booking_date,
                            SUM(' . TABLE_PREFIX . 'booking.var_price) as total,
                            count(' . TABLE_PREFIX . 'booking.id) as noofbooking,
                            CONCAT(' . TABLE_PREFIX . 'users.var_fname," ",' . TABLE_PREFIX . 'users.var_lname) as username
                            ,users.bint_phone
                            ,users.var_email
                            ,users.var_profile_image
                            ,users.id');
        $this->db->from('booking');
        $this->db->join('users', 'users.id = booking.fk_user');
        $this->db->where('booking.fk_store', $storeData['id']);
        $this->db->group_by('booking.fk_user');
        $userdata = $this->db->get()->result_array();
//            echo $this->db->last_query(); exit;
        for ($k = 0; $k < count($userdata); $k++) {
            $userdata[$k]['var_profile_image'] = (!empty($userdata[$k]['var_profile_image']))
                ? base_url() . USER_PROFILE_IMG . '/' . $userdata[$k]['var_profile_image']
                : base_url() . 'public/upload/no_image.png';

        }
        return $userdata;

    }

    function getCustomerBookingDetail($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        $this->db->select('booking.dt_booking_date,
		                            booking.var_price,
		                            booking.enum_book_type,  
		                            booking.id as bookId,
		                            ');
        $this->db->from('booking');
        $this->db->join('stores', 'stores.id = booking.fk_store');
        $this->db->where('booking.fk_user', $data['user_id']);
        $this->db->where('booking.fk_store', $storeData['id']);
        $data = $this->db->get()->result_array();
        for ($i = 0; $i < count($data); $i++) {
            $data[$i]['services'] = getServiceByBookingId($data[$i]['bookId']);
            $data[$i]['timeslot'] = getTimeSlotByBookingId($data[$i]['bookId']);
        }
        return $data;
    }

    function getStoreImages($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $this->db->select('id,fk_store,var_file,enum_isProfile,enum_ismain,enum_enable,created_at');
            $this->db->from('store_has_gallery');
            $this->db->where('fk_store', $storeData['id']);
            $this->db->order_by('enum_isProfile', 'asc');
            $allImg = $this->db->get()->result_array();
            if (!empty($allImg)) {
                $response['data'] = $allImg;
            } else {
                $response['data'] = array();
                $response['message'] = 'Store has no Images';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function addStoreImageold($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
//            header('Access-Control-Allow-Origin: *');
//            //$target_path = "public/upload/";
//
//            $target_path = STORE_IMG . basename($_FILES['file']['name']);
//
//            if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
//                echo "Upload and move success";
//            } else {
//                echo $target_path;
//                echo "There was an error uploading the file, please try again!";
//            }
            if ($_FILES['file']['name'] != "") {
                $image = upload_single_image($_FILES, 'store', STORE_IMG, FALSE);
                $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

//                    if ($data['var_profile_image'] != '') {
//                        delete_single_image(KEY_ACCOUNT_PROFILE_IMG, $data['var_profile_image']);
//                    }
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function addStoreImage_old()
    {
        echo 'hiii';
        exit;
        $target_path = STORE_IMG;

        $target_path = $target_path . basename($_FILES['file']['name']);

        if (move_uploaded_file($_FILES['file']['tmp_name'], $target_path)) {
            echo "Upload and move success";
        } else {
            echo $target_path;
            echo "There was an error uploading the file, please try again!";
        }

    }

    function addStoreImage($data){
        $this->load->library('Mylibrary');
        header('Access-Control-Allow-Origin: *');
        $target_path = "public/upload/store_pic/";
        $file_name = $_FILES['file']['name'] . '_' . time() . '.jpg';
        $target_path = $target_path . basename($file_name);

        //test

        $imgexten = explode('.', $_FILES['file']['name']);
        $img_name = 'file';
        $img_last_key = end($imgexten);
        $image_name = url_title($imgexten[0]) . time();
        $array = array(
            'image_pet_name' => $img_name,
            'image_name' => $image_name,
            'path' => STORE_IMG ,
            'extension' => $img_last_key,
        );

        $result = $this->mylibrary->upload_image($array);

        if (!empty($result)) {

            $storeData = getStoreDataByStoreCode($data['store_code']);

            $receipt_data = array(
                'fk_store' => $storeData['id'],
                'var_file' => $result['orig_name'],
                'var_width' => $result['image_width'],
                'var_height' => $result['image_height'],
                'enum_isProfile' => 'No',
                'enum_ismain' => 'YES',
                'enum_enable' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_gallery', $receipt_data);
            $lastid = $this->db->insert_id();

            $small_size = '100';
            $medium_size = '200';
            $large_size = '500';

            $thumb_array = array(
                array('width' => $small_size, 'height' => $small_size, 'image_type' => 'SMALL'),
                array('width' => $medium_size, 'height' => $medium_size, 'image_type' => 'MEDIUM'),
                array('width' => $large_size, 'height' => $large_size, 'image_type' => 'LARGE'));

            for ($j = 0; $j < count($thumb_array); $j++) {

                $data_thump['config']['source_image'] = STORE_IMG . '/' . $result['orig_name'];
                $data_thump['config']['maintain_ratio'] = TRUE;
                $data_thump['config']['create_thumb'] = TRUE;
                $data_thump['config']['new_image'] = $result['orig_name'];
                $data_thump['config']['thumb_marker'] = TRUE;
                $data_thump['width'] = $thumb_array[$j]['width'];
                $data_thump['height'] = $thumb_array[$j]['height'];
                $data_thump['full_path'] = $result['full_path'];
//                        $data_thump['table'] = 'kg_sell_artwork_has_images';
//                        $data_thump['id'] = $sell_artwork_id;
//                        $data_thump['parent_id'] = $original_img_id;
                $thumb_data = $this->mylibrary->create_thumb($data_thump);
                $imgArr = array(
                    'fk_store' =>  $storeData['id'],
                    'fk_parent' => $lastid,
                    'var_file' => $thumb_data['image_name'],
                    'var_width' => $thumb_array[$j]['width'],
                    'var_height' => $thumb_array[$j]['height'],
                    'enum_isProfile' => 'No',
                    'enum_ismain' => 'NO',
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_gallery', $imgArr);
                $last_id = $this->db->insert_id();
            }

            if ($last_id > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Uploaded successfully..!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Failed to upload Receipt.';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something went wrong..';
        }
        return $response;
    }

    function deleteStoreImage($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $imgId = $data['img_id'];
            $storeImage = $this->db->get_where('store_has_gallery',array('id' => $data['img_id']))->row_array();
            $this->db->where('id', $data['img_id']);
            $this->db->delete('store_has_gallery');
            if ($this->db->affected_rows() > 0) {
                $removefile = STORE_IMG . '/' . $storeImage['var_file'];
                //REtireve all thumb images
                $thumbimages = $this->db->get_where('store_has_gallery', array('fk_parent' => $imgId))->result_array();
                for ($i = 0; $i < count($thumbimages); $i++) {
                    $this->db->where('id', $thumbimages[$i]['id']);
                    $this->db->delete('store_has_gallery');
                    $thumbimg = STORE_IMG . '/' . $thumbimages[$i]['var_file'];
                    if (file_exists($thumbimg)) {
                        unlink($thumbimg);
                    }
                }
                if (file_exists($removefile)) {
                    unlink($removefile);
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Image deleted Successfully!..';
                    $json_response['jscode'] = " setTimeout(function () { location.reload() }, 100);";
                }

                $response['status'] = 'success';
                $response['message'] = 'Store image Deleted successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function makeImageAsProfile($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $getProfileImg = $this->db->get_where('store_has_gallery', array('enum_isProfile' => 'YES', 'fk_store' => $storeData['id']))->row_array();
            if (!empty($getProfileImg)) {
                $this->db->where('id', $getProfileImg['id']);
                $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'NO'));
                if ($update) {
                    $this->db->where('id', $data['img_id']);
                    $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'YES'));
                }
            } else {
                $this->db->where('id', $data['img_id']);
                $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'YES'));
            }
            if ($update) {
                $response['status'] = 'success';
                $response['message'] = 'Store image updated successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function makeImageActiveInActive($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $getImg = $this->db->get_where('store_has_gallery', array('id' => $data['img_id'], 'fk_store' => $storeData['id']))->row_array();
            if (!empty($getImg)) {
                if ($getImg['enum_enable'] == 'YES') {
                    $updateArr = array(
                        'enum_enable' => 'NO'
                    );
                    $this->db->where('id', $data['img_id']);
                    $update = $this->db->update('store_has_gallery', $updateArr);
                } else {
                    $updateArr = array(
                        'enum_enable' => 'YES'
                    );
                    $this->db->where('id', $data['img_id']);
                    $update = $this->db->update('store_has_gallery', $updateArr);
                }
                if ($update) {
                    $response['status'] = 'success';
                    $response['message'] = 'Store image updated successfully!';
                } else {
                    $response['status'] = 'error';
                    $response['message'] = 'Something is wrong';
                }
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function getStorePackage($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $this->db->select('store_has_package.id,
                               store_has_package.var_name,
                               store_has_package.var_price,
                               store_has_package.txt_note,
                               store_has_package.enum_enable,
                               store_has_package.fk_store,
                                ');
            $this->db->from('store_has_package');
            $this->db->where('store_has_package.fk_store', $storeData['id']);
            $returnData['packages'] = $this->db->get()->result_array();
            for ($i = 0; $i < count($returnData['packages']); $i++) {
                $this->db->select('services.id,services.var_title');
                $this->db->from('services');
                $this->db->join('package_has_service', 'package_has_service.fk_service = services.id');
                $this->db->where('package_has_service.fk_package', $returnData['packages'][$i]['id']);
                $returnData['packages'][$i]['services'] = $this->db->get()->result_array();
                for ($j = 0; $j < count($returnData['packages'][$i]['services']); $j++) {
                    $this->db->select('var_price');
                    $this->db->from('package_has_service');
                    $this->db->where('package_has_service.fk_service', $returnData['packages'][$i]['services'][$j]['id']);
                    $this->db->where('package_has_service.fk_package', $returnData['packages'][$i]['id']);
                    $returnData['packages'][$i]['services'][$j]['var_price'] = $this->db->get()->row_array()['var_price'];
                }
            }
            if (!empty($returnData)) {
                $response['data'] = $returnData;
            } else {
                $response['message'] = 'Store has no Packages';
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function addStorePackage($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            if ($data['package']['is_new']) {
                $storePack = array(
                    'fk_store' => $storeData['id'],
                    'var_name' => $data['package']['var_name'],
                    'var_price' => $data['package']['var_price'],
                    'txt_note' => $data['package']['txt_note'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_package', $storePack);
                $lastStoreid = $this->db->insert_id();
                if ($lastStoreid > 0) {
                    for ($i = 0; $i < count($data['package']['services']); $i++) {
                        $serviceArr = array(
                            'fk_package' => $lastStoreid,
                            'fk_service' => $data['package']['services'][$i]['id'],
                            'var_price' => $data['package']['services'][$i]['var_price'],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('package_has_service', $serviceArr);
                        $lastid = $this->db->insert_id();
                    }
                }
            } else {
                $storePack = array(
                    'var_name' => $data['package']['var_name'],
                    'var_price' => $data['package']['var_price'],
                    'txt_note' => $data['package']['txt_note'],
                    'enum_enable' => ($data['package']['enum_enable'] == 'YES') ? 'YES' : 'NO',
                );
                $this->db->where('id', $data['package']['id']);
                $update = $this->db->update('store_has_package', $storePack);
                if ($update) {
                    $this->db->where('fk_package', $data['package']['id']);
                    $this->db->delete('package_has_service');
                    for ($i = 0; $i < count($data['package']['services']); $i++) {
                        $serviceArr = array(
                            'fk_package' => $data['package']['id'],
                            'fk_service' => $data['package']['services'][$i]['id'],
                            'var_price' => $data['package']['services'][$i]['var_price'],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('package_has_service', $serviceArr);
                        $lastid = $this->db->insert_id();
                    }
                }

            }
            if ($lastStoreid > 0 || $update) {
                $response['status'] = 'success';
                $response['message'] = 'Package updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function deletePackage($data)
    {
        $this->db->where('id', $data['package_id']);
        $this->db->delete('store_has_package');
        if ($this->db->affected_rows() > 0) {
            $response['status'] = 'success';
            $response['message'] = 'Store package Deleted successfully!';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function getStoreOffer($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            $this->db->select('store_has_offer.id,
                               store_has_offer.var_name,
                               store_has_offer.var_price,
                               store_has_offer.var_offer_price,
                               store_has_offer.txt_note,
                               store_has_offer.dt_start_date,
                               store_has_offer.dt_end_date,
                               store_has_offer.enum_enable,
                               store_has_offer.fk_store,
                                ');
            $this->db->from('store_has_offer');
            $this->db->where('store_has_offer.fk_store', $storeData['id']);
            $returnData['offers'] = $this->db->get()->result_array();
            for ($i = 0; $i < count($returnData['offers']); $i++) {
                $this->db->select('services.id,services.var_title');
                $this->db->from('services');
                $this->db->join('offer_has_service', 'offer_has_service.fk_service = services.id');
                //  $this->db->join('store_has_services', 'store_has_services.fk_service = services.id');
                $this->db->where('offer_has_service.fk_offer', $returnData['offers'][$i]['id']);
                $returnData['offers'][$i]['services'] = $this->db->get()->result_array();
                for ($j = 0; $j < count($returnData['offers'][$i]['services']); $j++) {
                    $this->db->select('var_price,var_offer_price');
                    $this->db->from('offer_has_service');
                    $this->db->where('offer_has_service.fk_service', $returnData['offers'][$i]['services'][$j]['id']);
                    $this->db->where('offer_has_service.fk_offer', $returnData['offers'][$i]['id']);
                    $offerPrice = $this->db->get()->row_array();
                    $returnData['offers'][$i]['services'][$j]['var_price'] = $offerPrice['var_price'];
                    $returnData['offers'][$i]['services'][$j]['var_offer_price']= $offerPrice['var_offer_price'];
                }
            }
            if (!empty($returnData)) {
                $response['data'] = $returnData;
            } else {
                $response['message'] = 'Store has no Offers';
            }

        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function addStoreOffer($data)
    {
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if (!empty($storeData)) {
            if ($data['offer']['is_new']) {
                $storePack = array(
                    'fk_store' => $storeData['id'],
                    'var_name' => $data['offer']['var_name'],
                    'var_price' => $data['offer']['var_price'],
                    'var_offer_price' => $data['offer']['var_offer_price'],
                    'txt_note' => $data['offer']['txt_note'],
                    'dt_start_date' => date('Y-m-d', strtotime($data['offer']['dt_start_date'])),
                    'dt_end_date' => date('Y-m-d', strtotime($data['offer']['dt_end_date'])),
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_offer', $storePack);
                $lastStoreid = $this->db->insert_id();
                if ($lastStoreid > 0) {
                    for ($i = 0; $i < count($data['offer']['services']); $i++) {
                        $serviceArr = array(
                            'fk_offer' => $lastStoreid,
                            'fk_service' => $data['offer']['services'][$i]['id'],
                            'var_price' => $data['offer']['services'][$i]['var_price'],
                            'var_offer_price' => $data['offer']['services'][$i]['var_offer_price'],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('offer_has_service', $serviceArr);
                        $lastid = $this->db->insert_id();
                    }
                }
            } else {
                $storePack = array(
                    'var_name' => $data['offer']['var_name'],
                    'var_price' => $data['offer']['var_price'],
                    'var_offer_price' => $data['offer']['var_offer_price'],
                    'txt_note' => $data['offer']['txt_note'],
                    'dt_start_date' => date('Y-m-d', strtotime($data['offer']['dt_start_date'])),
                    'dt_end_date' => date('Y-m-d', strtotime($data['offer']['dt_end_date'])),
                    'enum_enable' => ($data['offer']['enum_enable'] == 'YES') ? 'YES' : 'NO',
                );
                $this->db->where('id', $data['offer']['id']);
                $update = $this->db->update('store_has_offer', $storePack);
                if ($update) {
                    $this->db->where('fk_offer', $data['offer']['id']);
                    $this->db->delete('offer_has_service');
                    for ($i = 0; $i < count($data['offer']['services']); $i++) {
                        $serviceArr = array(
                            'fk_offer' => $data['offer']['id'],
                            'fk_service' => $data['offer']['services'][$i]['id'],
                            'var_price' => $data['offer']['services'][$i]['var_price'],
                            'var_offer_price' => $data['offer']['services'][$i]['var_offer_price'],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('offer_has_service', $serviceArr);
                        $lastid = $this->db->insert_id();
                    }
                }

            }
            if ($lastStoreid > 0 || $update) {
                $response['status'] = 'success';
                $response['message'] = 'offer updated successfully';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something will be wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function deleteOffer($data)
    {
        $this->db->where('id', $data['offer_id']);
        $this->db->delete('store_has_offer');
        if ($this->db->affected_rows() > 0) {
            $response['status'] = 'success';
            $response['message'] = 'Store offer Deleted successfully!';
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function getAllStoreByBussCode($data)
    {
        if (!empty($data)) {
            $this->db->select('stores.*');
            $this->db->where('stores.fk_bussiness',$data['id']);
            $stores = $this->db->get('stores')->result_array();
            if (!empty($stores)) {
                $response['data'] = $stores;
            } else {
                $response['message'] = 'Bussiness has no Store';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Bussiness not found';
        }
        return $response;
    }

    function cancelBooking($data){
        if (!empty($data)) {
            $this->db->where('id', $data['bookId']);
            $result = $this->db->update('booking', array('enum_book_type' => 'CANCEL'));
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Booking updated Successfully!..';
//                $json_response['jscode'] = " setTimeout(function () { $('#cancel_model').modal('hide');bookingDatatables.refresh(); }, 100);";
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
            return $json_response;
        }
    }

    function completeBooking($data){
        if (!empty($data)) {
            $this->db->where('id', $data['bookId']);
            $result = $this->db->update('booking', array('enum_book_type' => 'COMPLETED'));
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Booking updated Successfully!..';
//                $json_response['jscode'] = " setTimeout(function () { $('#cancel_model').modal('hide');bookingDatatables.refresh(); }, 100);";
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
            return $json_response;
        }
    }

    function getBankDetails($data){
        $storeData = getStoreDataByStoreCode($data['store_code']);
        if(!empty($storeData)){
                $this->db->select('store_has_bank_details.*');
                $this->db->where('store_has_bank_details.fk_store',$storeData['id']);
                $stores = $this->db->get('store_has_bank_details')->result_array();
                if (!empty($stores)) {
                    $response['data'] = $stores;
                } else {
                    $response['data'] = [];
                }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }

        return $response;
    }
    
    function sentPushyId($data) {
        if (!empty($data)) {
            $userData = array(
                'var_pushy_device_id' => $data['pushyId'],
            );
            $this->db->where('id', $data['user_id']);
            $res = $this->db->update('bussiness', $userData);
            if ($this->db->affected_rows() > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Pushy Id Updated!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Pushy Id not Updated';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'data not found...';
        }
    }

    function addStorePortfolio($data){
        $this->load->library('Mylibrary');
        $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_id']))->row_array()['id'];
        if (!empty($data)) {
                if (!empty($_FILES['file'])) {
                    $imgexten = explode('.', $_FILES['file']['name']);
                    $img_name = 'file';
                    $img_last_key = end($imgexten);
                    $image_name = url_title($imgexten[0]) . time();
                    $array = array(
                        'image_pet_name' => $img_name,
                        'image_name' => $image_name,
                        'path' => PORTFOLIO_IMG,
                        'extension' => $img_last_key,
                    );
                    $result = $this->mylibrary->upload_image($array);
                    if(!empty($result)){
                        $var_logo = $result['orig_name'];
                    }else{
                        $var_logo = null;
                    }
                }else{
                    $var_logo = null;
                }
                $bankArr = array(
                    'fk_store' => $storeId,
                    'var_title' => $data['title'],
                    'var_file' => $var_logo,
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_portfolio', $bankArr);
                $lastId = $this->db->insert_id();

            if ($lastId > 0) {
                $response['status'] = 'success';
                $response['message'] = 'Store Portfolio Added successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Something is wrong';
        }
        return $response;
    }

    function getStorePortfolio($data){
        if(!empty($data)){
            $storeId = $this->db->get_where('stores', array('var_store_code' => $data['store_id']))->row_array()['id'];

            $this->db->select('store_has_portfolio.*');
            $this->db->from('store_has_portfolio');
            $this->db->where('store_has_portfolio.fk_store',$storeId);
            $data = $this->db->get()->result_array();
            for($i=0;$i<count($data);$i++){
                if($data[$i]['enum_enable'] == 'YES'){
                    $data[$i]['enum_enable'] = true;
                }else{
                    $data[$i]['enum_enable'] = false;
                }
                if(empty($data[$i]['var_file'])){
                    $data[$i]['var_file'] = base_url() . 'public/upload/no_image.png';
                }else{
                    $data[$i]['var_file'] = base_url().PORTFOLIO_IMG . '/' . $data[$i]['var_file'];
                }
                $this->db->select('users.var_fname,users.var_lname,users.var_email,users.bint_phone,users.var_profile_image,portfolio_has_review.*');
                $this->db->from('portfolio_has_review');
                $this->db->join('users','users.id = portfolio_has_review.fk_user');
                $this->db->where('portfolio_has_review.fk_portfolio',$data[$i]['id']);
                $data[$i]['review'] = $this->db->get()->result_array();
                for($j=0;$j<count($data[$i]['review']);$j++){
                    if(empty($data[$i]['review'][$j]['var_profile_image'])){
                        $data[$i]['review'][$j]['var_profile_image'] = base_url() . 'public/upload/no_image.png';
                    }else{
                        $data[$i]['review'][$j]['var_profile_image'] == base_url().USER_PROFILE_IMG . '/' . $data[$i]['review'][$j]['var_profile_image'];
                    }
                }
            }
            return $data;
        }
    }

    function togglePortfolioStatus($data){
        if(!empty($data)){
            $portfolioDetail = $this->db->get_where('store_has_portfolio',array('id',$data['portfolio_id']))->row_array();
            if(!empty($portfolioDetail)){
                $updateArr = array(
                    'enum_enable'=>($portfolioDetail['enum_enable'] == 'YES') ? 'NO' : 'YES'
                );

                $this->db->where('id',$data['portfolio_id']);
                $update = $this->db->update('store_has_portfolio',$updateArr);
                if($update){
                    $response['status'] = 'success';
                    $response['message'] = 'portfolio status updated successfully';
                }else{
                    $response['status'] = 'error';
                    $response['message'] = 'something went wrong';
                }
            }else{
                $response['status'] = 'error';
                $response['message'] = 'portfolio not found';
            }
        }
        return $response;
    }

    function deletePortfolio($data){
        $portfolioData = $this->db->get_where('store_has_portfolio',array('id'=>$data['portfolio_id']))->row_array();
        if(!empty($portfolioData)){
            $portfolioimg = PORTFOLIO_IMG . '/' . $portfolioData['var_file'];
            $this->db->where('id',$data['portfolio_id']);
            $result= $this->db->delete('store_has_portfolio');
            if (file_exists($portfolioimg)) {
                unlink($portfolioimg);
            }
            if($result){
                $response['status'] = 'success';
                $response['message'] = 'portfolio deleted successfully!!..';
            }else{
                $response['status'] = 'error';
                $response['message'] = 'something went wrong..';
            }
        }else{
            $response['status'] = 'error';
            $response['message'] = 'portfolio not found';
        }
        return $response;
    }




}
