<?php

class Category_model extends CI_Model
{

    function getCategoryList()
    {
        $this->datatables->select('master_category.id,
		                            master_category.var_name,
		                            master_category.enum_type,
		                            master_category.enum_enable,
		                            master_category.created_at,
		                            master_category.id as id2');
        $this->datatables->from('master_category');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('category/overview/').$res["data"][$i][0]. '">' . $res["data"][$i][1] . '</a>';
            if($res["data"][$i][3] == 'YES'){
                $res["data"][$i][3] = 'Enable';
            }else{
                $res["data"][$i][3] = 'Disable';
            }
            if($res["data"][$i][2] == 'SERVICE'){
                $res["data"][$i][2] = 'Service';
            }else{
                $res["data"][$i][2] = 'Store';
            }
            $res["data"][$i][4] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('category/delete') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_name', 'label' => 'Category Name', 'rules' => 'required'),
            array('field' => 'category_type', 'label' => 'Category Type', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
                $AccountData = array(
                    'var_name' => $data['var_name'],
                    'enum_type' => $data['category_type'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("master_category", $AccountData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Category added Successfully!..';
                    $json_response['redirect'] = admin_url('category');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }
    function getCategory($categoryId){
        return $this->db->get_where('master_category',array('id'=>$categoryId))->row_array();
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'master_category', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Category Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');categoryDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
