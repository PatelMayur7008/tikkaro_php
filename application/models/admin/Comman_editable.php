<?php

class Comman_editable extends CI_Model
{

    function inlineEdit($data)
    {

        $field = $data['field'];
        $table = $data['table'];
        $value = $data['value'];
        $id = $data['id'];

        if ($data['checkDuplicate'] == true) {
            $this->db->where($field, $value);
            $this->db->where('id !=', $id);
            $checkCnt = $this->db->get($table)->num_rows();
            if ($checkCnt > 0) {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Email already exits!';
                return $json_response;
            }
        }

        if ($data['checkUniqueInvoiceNumber'] == true) {
            $this->db->where($field, $value);
            $this->db->where('id !=', $id);
            $checkCnt = $this->db->get($table)->num_rows();
            if ($checkCnt > 0) {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Invoice number already exists!..';
                return $json_response;
            }
        }


        if ($data['type'] == 'date') {
            $value = DateTime::createFromFormat('d/m/Y', $value)->format('Y-m-d');
        }
        if (!empty($data['id'])) {
            $data_array = array(
                $field => $value,
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }

        if($table = 'stores' && $field = 'int_no_book_seats' ){
            $arr= array(
                'int_book_limit'=>$value
            );
            $this->db->where('fk_store',$id);
            $this->db->update('store_has_timeslots', $arr);
        }
        return TRUE;
    }

    function inlineEditFnameLname($data)
    {
        $varfname = $data['field1'];
        $varlname = $data['field2'];
        $table = $data['table'];
        $id = $data['id'];
        $value = explode(' ', $data['value']);
        $fname = $value[0];
        $lname = $value[1];

        if (!empty($data['id'])) {
            $data_array = array(
                $varfname => $fname,
                $varlname => ($lname) ? ($lname) : '',
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }
        return TRUE;
    }

    function inlineEditSelect($data)
    {
        $table = $data['table'];
        $id = $data['id'];

        if (!empty($data['id'])) {
            $data_array = array(
                $data['field1'] => $data['value'],
                $data['field2'] => $data['value'],
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }
        return TRUE;
    }

    function inlineCompliance($data)
    {

        $checkOfficer = $this->db->get_where($data['table'], array($data['field1'] => $data['client']))->row_array();

        //Check dulication first then do other things
        $table = $data['table'];
        $field = $data['field'];
        $value = $data['value'];
        $id = $data['id'];

        if (empty($checkOfficer)) {
            if ($data['checkDuplicate'] == "true") {

                $this->db->where($field, $value);
                $checkCnt = $this->db->get($table)->num_rows();

                if ($checkCnt > 0) {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Email already exits!';
                    return $json_response;
                }
            }
            $data_array = array(
                $data['field'] => $data['value'],
                $data['field1'] => $data['client'],
                'created_at' => date('Y-m-d H:i:s'),
                'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
            );
            $this->db->insert($data['table'], $data_array);
        } else {

            if ($data['checkDuplicate'] == "true") {
                $this->db->where($field, $value);
                $this->db->where('id !=', $id);
                $checkCnt = $this->db->get($table)->num_rows();

                if ($checkCnt > 0) {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Email already exits!';
                    return $json_response;
                }
            }
            $data_array = array(
                $data['field'] => $data['value'],
            );
            $this->db->where($data['field1'], $data['client']);
            $this->db->update($data['table'], $data_array);
        }
        return TRUE;
    }

    function handleDuplicate($data, $json_response)
    {
        if ($data) {

            $this->db->where($data['check-duplicate-field'], $data['value']);
            $this->db->where('id !=', $data['check-duplicate-id']);
            $checkcnt = $this->db->get($data['check-duplicate-table'])->num_rows();
            if ($checkcnt > 0) {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Email already exits!';
            } else {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Email address available';
            }

        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'something went wrong!';
        }
        return $json_response;
    }

    function inlineEditInvoice($data)
    {
        $table = $data['table'];
        $id = $data['invoiceid'];

        if (!empty($id)) {
            $data_array = array(
                $data['field'] => $data['value']
            );
            $this->db->where('var_invoice', $id);
            $this->db->update($table, $data_array);

        }
//        echo $this->db->last_query();exit;


        return TRUE;
    }

}
