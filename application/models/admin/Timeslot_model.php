<?php

class Timeslot_model extends CI_Model
{

    function getTimeslotList()
    {
        $this->datatables->select('id,slot_time,id');
        $this->datatables->from('timeslots');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('time-slot/edit/') . $res["data"][$i][0] . '">' . $res["data"][$i][1] . '</a>';
            $res["data"][$i][2] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('time_slot/delete') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function add($data, $json_response)
    {
//        print_array($data);
        $config = array(
            array('field' => 'time_slot', 'label' => 'Time Slot', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

                $TimeslotData = array(
                    'slot_time' => date('H:i:s', strtotime($data['time_slot'])),
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("timeslots", $TimeslotData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Time added Successfully!..';
                    $json_response['redirect'] = admin_url('time-slot');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function edit($data, $json_response)
    {
        $config = array(
            array('field' => 'time_slot', 'label' => 'Time Slot', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $TimeslotData = array(
                'slot_time' => date('H:i:s', strtotime($data['time_slot'])),
            );
            $this->db->where('id',$data['id']);
            $update = $this->db->update('timeslots',$TimeslotData);
            if ($update) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Time Updated Successfully!..';
                $json_response['redirect'] = admin_url('time-slot');
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }

        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getUserCommonHeader($userId)
    {
        return $this->db->get_where('users', array('id' => $userId))->row_array();
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'timeslots', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Timeslot Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');timeslotDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
