<?php

class Service_model extends CI_Model
{

    function getServiceList()
    {
        $this->datatables->select('services.id,
		                            services.var_title,
		                            master_category.var_name,
		                            services.enum_enable,
		                            services.id');
        $this->datatables->from('services');
        $this->datatables->join('master_category','master_category.id = services.fk_category');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('service/overview/').$res["data"][$i][0]. '">' . $res["data"][$i][1] . '</a>';
            if($res["data"][$i][3] == 'YES'){
                $res["data"][$i][3] = 'Enable';
            }else{
                $res["data"][$i][3] = 'Disable';
            }
            $res["data"][$i][4] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('service/delete') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_title', 'label' => 'Service Title', 'rules' => 'required'),
            array('field' => 'fk_category', 'label' => 'Category', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $checkAccount = $this->db->get_where('services', array('var_title' => $data['var_title']))->num_rows();
            if ($checkAccount == 0) {
                $AccountData = array(
                    'var_title' => $data['var_title'],
                    'fk_category' => $data['fk_category'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("services", $AccountData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Service added Successfully!..';
                    $json_response['redirect'] = admin_url('service');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

            } else {
                $json_response['message'] = 'Service Name alredy exist!..';
                $json_response['status'] = 'warning';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }
    function getServiceCommonHeader($serviceId){
//        return $this->db->get_where('services',array('id'=>$serviceId))->row_array();

        $this->db->select('services.*,master_category.id as catid,master_category.var_name');
        $this->db->from('services');
        $this->db->join('master_category','master_category.id = services.fk_category');
        $this->db->where('services.id',$serviceId);
        $data = $this->db->get()->row_array();
        return $data;
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'services', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Service Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');serviceDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
