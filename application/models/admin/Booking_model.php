<?php

class Booking_model extends CI_Model
{

    function getBookingList()
    {
        $this->datatables->select('booking.id,
		                            CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,
		                            stores.var_title,
		                            booking.var_price as price,
		                            booking.fk_store,
		                            booking.fk_user,
		                            DATE_FORMAT(' . TABLE_PREFIX . 'booking.dt_booking_date, \'%d/%m/%Y\') AS initiate,
		                            booking.enum_book_type,
		                            booking.created_at,
		                            users.id as userId,
		                            stores.id as storeId
		                            ');
        $this->datatables->from('booking');
        $this->datatables->join('users', 'booking.fk_user = users.id');
        $this->datatables->join('stores', 'booking.fk_store = stores.id');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('user/overview/') . $res['data'][$i][9] . '"> ' . $res['data'][$i][1] . ' </a>';
            $res["data"][$i][2] = '<a  href="' . admin_url('stores/overview/') . $res['data'][$i][10] . '"> ' . $res['data'][$i][2] . ' </a>';
            $res["data"][$i][4] = getServiceByBookingId($res["data"][$i][0]);
            $res["data"][$i][5] = getTimeSlotByBookingId($res["data"][$i][0]);
            if($res["data"][$i][7] == 'UPCOMING'){
                $res["data"][$i][8] = '<a title="Delete" class="btn blue btn-xs cancel_booking_link" data-href="' . admin_url('booking/cancelBooking') . '" data-id="' . $res["data"][$i][0] . '" href="javascript:;"><i class="fa fa-trash"></i> Cancel </a>';
            }else{
                $res["data"][$i][8] = '-';
            }

        }
        return json_encode($res);
    }


    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'services', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Service Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');serviceDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function CancelBooking($data, $json_response)
    {
        if (!empty($data)) {
            $this->db->where('id', $data['bookId']);
            $result = $this->db->update('booking', array('enum_book_type' => 'CANCEL'));
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Booking updated Successfully!..';
//                $json_response['jscode'] = " setTimeout(function () { $('#cancel_model').modal('hide');bookingDatatables.refresh(); }, 100);";
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
            return $json_response;
        }
    }
}
