<?php

class Store_model extends CI_Model
{

    function getStoreList()
    {
        $this->datatables->select('stores.var_title,
		                            stores.txt_address,
		                            stores.bint_phone,
		                            stores.var_rating,
		                            master_category.var_name,
		                            stores.enum_store_type,
		                            stores.int_no_book_seats,
		                            stores.int_weekoff,
		                            "" as noofstaff,
		                            "" as totalcustomer,
		                            "" as totalservice,
		                            stores.enum_enable,
		                            stores.id,
		                            stores.id as storeId');
        $this->datatables->from('stores');
        $this->datatables->join('store_has_staff', 'store_has_staff.fk_store = stores.id', 'left');
        $this->datatables->join('master_category', 'master_category.id = stores.fk_category');
        $this->datatables->group_by('stores.id');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $storeId = $res["data"][$i][12];
            $res["data"][$i][0] = '<a href="' . admin_url('stores/overview/') . $res["data"][$i][12] . '">' . $res["data"][$i][0] . '</a>';
            if ($res["data"][$i][11] == 'YES') {
                $res["data"][$i][11] = 'Enable';
            } else {
                $res["data"][$i][11] = 'Disable';
            }
            $res["data"][$i][7] = getDayNameFromNumber($res["data"][$i][6]);
            $res["data"][$i][8] = getCountTotalStaff($storeId);
            $res["data"][$i][9] = getStoreTotalCustomer($storeId);
            $res["data"][$i][10] = getStoreTotalServices($storeId);
            $res["data"][$i][12] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/delete') . '" data-id="' . $res["data"][$i][12] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function getStoreServiceList($id)
    {
        $this->datatables->select('store_has_services.id,
		                            services.var_title,
		                            store_has_services.var_price,     
		                            store_has_services.dt_service_time,     
		                            store_has_services.enum_enable,
		                            store_has_services.fk_store,
		                            store_has_services.fk_service,
		                            ');
        $this->datatables->from('store_has_services');
        $this->datatables->join('services', 'services.id = store_has_services.fk_service');
        $this->datatables->where(TABLE_PREFIX . "store_has_services.fk_store", $id);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            if ($res["data"][$i][4] == 'YES') {
                $res["data"][$i][4] = 'Enable';
            } else {
                $res["data"][$i][4] = 'Disable';
            }
            $res["data"][$i][5] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_service/') . $res["data"][$i][0] . '"><i class="fa fa-eye"></i> Edit </a> <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/deleteService') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function getStoreTimeSlotList($id)
    {
        $this->datatables->select('store_has_timeslots.id,
		                            timeslots.slot_time,
		                            store_has_timeslots.store_id,     
		                            store_has_timeslots.timeslot_id,
		                            ');
        $this->datatables->from('store_has_timeslots');
        $this->datatables->join('timeslots', 'timeslots.id = store_has_timeslots.timeslot_id');
        $this->datatables->where(TABLE_PREFIX . "store_has_timeslots.store_id", $id);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][2] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/deletetimeslot') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function getAllService($storeId)
    {

        $this->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'store_has_services.fk_service) as serviceid');
        $this->db->where('store_has_services.fk_store', $storeId);
        $serviceIds = $this->db->get('store_has_services')->row_array();

//        print_array();
        $this->db->select('*');
        $this->db->from('services');
        if ($serviceIds['serviceid'] != '') {
            $this->db->where_not_in('id', explode(',', $serviceIds['serviceid']));
        }
        return $this->db->get()->result_array();
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_title', 'label' => 'Title', 'rules' => 'required'),
            array('field' => 'var_phone', 'label' => 'Phone no', 'rules' => 'required'),
            array('field' => 'var_address', 'label' => 'Address', 'rules' => 'required'),
            array('field' => 'store_type', 'label' => 'Store Type', 'rules' => 'required'),
            array('field' => 'fk_category', 'label' => 'CAtegory Type', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $count = $this->db->get_where('stores',array('fk_bussiness'=>$data['bussiness_id']))->num_rows();
            if($count <= 8 ){
                if ($_FILES['store_logo']['name'] != "") {
                    $image = upload_single_image($_FILES, 'store_logo', STORE_LOGO_IMG, FALSE);
                    $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

//                    if ($data['var_profile_image'] != '') {
//                        delete_single_image(KEY_ACCOUNT_PROFILE_IMG, $data['var_profile_image']);
//                    }
                }

                $StoreData = array(
                    'var_title' => $data['var_title'],
                    'fk_bussiness' => $data['bussiness_id'],
                    'fk_category' => $data['fk_category'],
                    'txt_address' => $data['var_address'],
                    'var_logo' => $var_image,
                    'bint_phone' => ($data['var_phone']) ? $data['var_phone'] : NULL,
                    'var_latitude' => ($data['lat']) ? $data['lat'] : NULL,
                    'var_longitude' => ($data['lng']) ? $data['lng'] : NULL,
                    'var_store_code' => (generateRandomString()) ? generateRandomString() : NULL,
                    'var_rating' => ($data['var_rating']) ? $data['var_rating'] : NULL,
                    'enum_enable' => 'YES',
                    'enum_store_type' => $data['store_type'],
                    'int_weekoff' => ($data['int_weekoff']) ? $data['int_weekoff'] : NULL,
                    'int_no_book_seats' => ($data['int_no_book_seats']) ? $data['int_no_book_seats'] : NULL,
                    'int_areacode' => ($data['pincode']) ? $data['pincode'] : NULL,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("stores", $StoreData);
                $laststoreid = $this->db->insert_id();
                if ($laststoreid > 0) {
                    for ($j = 0; $j < 7; $j++) {
                        $data['opening_time'] = '09:00:00';
                        $data['closing_time'] = '20:00:00';
                        $insArr = array(
                            'fk_store' => $laststoreid,
                            'int_week' => $j,
                            'dt_open_time' => date('H:i:s', strtotime($data['opening_time'])),
                            'dt_close_time' => date('H:i:s', strtotime($data['closing_time'])),
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert("store_has_timing", $insArr);
                        $lastid = $this->db->insert_id();

                        $timesolt = $this->db->get('timeslots')->result_array();
                        $data['opening_time'] = '09:00:00';
                        $data['closing_time'] = '20:00:00';
                        for ($k = 0; $k < count($timesolt); $k++) {
                            if (( strtotime($data['opening_time']) > strtotime($timesolt[$k]['slot_time']))
                                || ((strtotime($data['closing_time']) < strtotime($timesolt[$k]['slot_time'])))
                            ) {
                                $enable = 'NO';
                            } else {
                                $enable = 'YES';
                            }
                            $arr = array(
                                'fk_store'=>$laststoreid,
                                'fk_timeslot'=>$timesolt[$k]['id'],
                                'int_week_day'=>$j,
                                'enum_enable'=>$enable,
                                'enum_isfull'=> 'NO',
                                'int_book_limit'=>$data['int_no_book_seats'],
                                'created_at' => date('Y-m-d H:i:s')
                            );
                            $this->db->insert("store_has_timeslots", $arr);
                            $lastid = $this->db->insert_id();
                        }
                    }

                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Store added Successfully!..';
                    $json_response['redirect'] = admin_url('stores');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }else{
                $json_response['status'] = 'error';
                $json_response['message'] = 'No more store allowed in this bussiness';
            }

        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function addService($data, $json_response)
    {
        $config = array(
            array('field' => 'fk_service', 'label' => 'Service', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'service_time', 'label' => 'Service Time', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $StoreserviceData = array(
                'fk_store' => $data['store_id'],
                'fk_service' => $data['fk_service'],
                'var_price' => ($data['var_price']) ? $data['var_price'] : NULL,
                'dt_service_time' => ($data['service_time']) ? $data['service_time'] : NULL,
                'enum_enable' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert("store_has_services", $StoreserviceData);
            $lastid = $this->db->insert_id();
            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Service added Successfully!..';
                $json_response['redirect'] = admin_url('stores/services/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function addTimeslot($data, $json_response)
    {
        $config = array(
            array('field' => 'fk_timeslot', 'label' => 'Time slot', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $StoreserviceData = array(
                'store_id' => $data['store_id'],
                'timeslot_id' => $data['fk_timeslot'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert("store_has_timeslots", $StoreserviceData);
            $lastid = $this->db->insert_id();
            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Timeslot added Successfully!..';
                $json_response['redirect'] = admin_url('stores/time_slot/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function editService($data, $json_response)
    {

        $config = array(
            array('field' => 'enum_enable', 'label' => 'Status', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'service_time', 'label' => 'Service Time', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $StoreserviceData = array(
                'var_price' => ($data['var_price']) ? $data['var_price'] : NULL,
                'dt_service_time' => ($data['service_time']) ? $data['service_time'] : NULL,
                'enum_enable' => $data['enum_enable'],
            );
            $this->db->where('id', $data['service_id']);
            $update = $this->db->update("store_has_services", $StoreserviceData);

            if (!empty($update)) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Service update Successfully!..';
                $json_response['redirect'] = admin_url('stores/services/') . $data['fk_store'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getStoreCommonHeader($id)
    {

//        return $this->db->get_where('stores', array('id' => $id))->row_array();
        $this->db->select('stores.*,bussiness.var_name as bussinessName,master_category.id as catId,master_category.var_name as catname');
        $this->db->from('stores');
        $this->db->join('bussiness', 'bussiness.id = stores.fk_bussiness', 'left');
        $this->db->join('master_category', 'master_category.id = stores.fk_category', 'left');
        $this->db->where('stores.id', $id);
        return $this->db->get()->row_array();
    }

    function updateStoreAddress($data)
    {
        $updateArray = array(
            'var_latitude' => $data['lat'],
            'var_longitude' => $data['lng'],
            'int_areacode' => $data['pincode'],
            'txt_address' => $data['address']
        );
        $this->db->where('id', $data['store_id']);
        $this->db->update('stores', $updateArray);
    }

    function getAllTimeSlot()
    {
        return $this->db->get('timeslots')->result_array();
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'stores', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');storeDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function deletetimeslot($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'store_has_timeslots', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');timeslotDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getTimingList($storeId)
    {
        $storeData = $this->db->get_where('stores', array('id' => $storeId))->row_array();
        $this->datatables->select('id,int_week,dt_open_time,dt_close_time,fk_store,id as timingId');
        $this->datatables->from('store_has_timing');
        $this->datatables->where('fk_store', $storeId);
        $this->datatables->where('int_week !=', $storeData['int_weekoff']);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
//        print_array($res['data']);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][2] = date('h:i A',strtotime($res["data"][$i][2]));
            $res["data"][$i][3] = date('h:i A',strtotime($res["data"][$i][3]));
            $res["data"][$i][1] = '<a href="' . admin_url('stores/edit_timing/') . $res["data"][$i][0] . '">' . getDayNameFromNumber($res["data"][$i][1]) . '</a>';
            $res["data"][$i][4] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_timing/') . $res["data"][$i][0] . '"><i class="fa fa-eye"></i> Edit </a>';
        }
        return json_encode($res);
    }

    function getStaffList($storeId)
    {
        $this->datatables->select('var_name,
                                   var_phone,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'store_has_staff.dt_startdate, \'%d/%m/%Y\') AS startDate,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'store_has_staff.dt_enddate, \'%d/%m/%Y\') AS endDate,  
                                   enum_isFrontView,
                                   enum_current,
                                   enum_active,
                                   id,
                                   fk_store,
                                   created_at
                                   ');
        $this->datatables->from('store_has_staff');
        $this->datatables->where('fk_store', $storeId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            if ($res["data"][$i][6] == 'YES') {
                $res["data"][$i][6] = 'Enable';
            } else {
                $res["data"][$i][6] = 'Disable';
            }
            $id = $res["data"][$i][7];
            $res["data"][$i][7] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_staff/') . $res["data"][$i][7] . '"><i class="fa fa-eye"></i> Edit </a>
                                    <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/delete_staff') . '" data-id="' . $id . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function addStaff($data, $json_response)
    {
        $config = array(
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_phone', 'label' => 'Phonr', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $var_image = ($data['profile_pic']) ? $data['profile_pic'] : '';

            if ($_FILES['profile_pic']['name'] != "") {
                $image = upload_single_image($_FILES, 'staff', STAFF_PROFILE_IMG, FALSE);
                $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';
            }

            $StorestaffData = array(
                'fk_store' => $data['store_id'],
                'var_name' => $data['var_name'],
                'var_phone' => $data['var_phone'],
                'var_image' => $var_image,
                'dt_startdate' => ($data['var_str_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_str_date'])->format('Y-m-d') : null,
                'dt_enddate' => ($data['var_end_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_end_date'])->format('Y-m-d') : null,
                'enum_isFrontView' => ($data['is_front'] && $data['is_front'] == 'on') ? 'YES' : 'NO',
                'enum_current' => ($data['is_current'] && $data['is_current'] == 'on') ? 'YES' : 'NO',
                'enum_active' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert("store_has_staff", $StorestaffData);
            $lastid = $this->db->insert_id();
            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Staff added Successfully!..';
                $json_response['redirect'] = admin_url('stores/staff/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function updateStaff($data, $json_response)
    {
        $config = array(
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_phone', 'label' => 'Phonr', 'rules' => 'required')
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            if ($_FILES['profile_pic']['name'] != "") {
                $image = upload_single_image($_FILES, 'staff', STAFF_PROFILE_IMG, FALSE);
                $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

                delete_single_image(STAFF_PROFILE_IMG, $data['old_image']);
            } else {
                $var_image = $data['old_image'];
            }

            $StorestaffData = array(
                'var_name' => $data['var_name'],
                'var_phone' => $data['var_phone'],
                'var_image' => $var_image,
                'dt_startdate' => ($data['var_str_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_str_date'])->format('Y-m-d') : null,
                'dt_enddate' => ($data['var_end_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_end_date'])->format('Y-m-d') : null,
                'enum_isFrontView' => ($data['is_front'] && $data['is_front'] == 'on') ? 'YES' : 'NO',
                'enum_current' => ($data['is_current'] && $data['is_current'] == 'on') ? 'YES' : 'NO',
                'enum_active' => 'YES',
            );
            $this->db->where('id', $data['staff_id']);
            $update = $this->db->update("store_has_staff", $StorestaffData);

            if ($update) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Staff updates Successfully!..';
                $json_response['redirect'] = admin_url('stores/staff/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function editTiming($data, $json_response)
    {
        if (!empty($data)) {
            $updateArr = array(
                'dt_open_time' => date('H:i:s', strtotime($data['opening_time'])),
                'dt_close_time' => date('H:i:s', strtotime($data['closing_time'])),
            );
            $this->db->where('id', $data['timing_id']);
            $update = $this->db->update('store_has_timing', $updateArr);

            $int_week_day = $this->db->get_where('store_has_timing',array('id'=>$data['timing_id']))->row_array()['int_week'];

            $timesolt = $this->db->get('timeslots')->result_array();;

            for ($k = 0; $k < count($timesolt); $k++) {
                if ((strtotime($data['opening_time']) > strtotime($timesolt[$k]['slot_time']))
                    || ((strtotime($data['closing_time']) < strtotime($timesolt[$k]['slot_time'])))
                ) {
                    $enable = 'NO';
                } else {
                    $enable = 'YES';
                }
                $updatearr = array(
                    'enum_enable' => $enable,
                );
                $this->db->where('fk_store', $data['fk_store']);
                $this->db->where('int_week_day', $int_week_day);
                $this->db->where('fk_timeslot', $timesolt[$k]['id']);
                $this->db->update('store_has_timeslots', $updatearr);
            }

            if (!empty($update)) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Timing updated Successfully!..';
                $json_response['redirect'] = admin_url('stores/timing/') . $data['fk_store'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        }
        return $json_response;
    }

    function deletestaff($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'store_has_staff', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Staff deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');staffDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getBankDetail($id)
    {
        return $this->db->get_where('store_has_bank_details', array('fk_store' => $id))->row_array();
    }

    function getCustomerList($id)
    {
        $this->datatables->select('CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,
                                    users.var_email,
                                    users.bint_phone,
                                    "" as address,
                                    users.enum_geneder
                                    ');
        $this->datatables->from('store_has_customer');
        $this->datatables->join('users', 'users.id = store_has_customer.fk_user');
        $this->datatables->where('store_has_customer.fk_store', $id);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            if ($res["data"][$i][4] == 'M') {
                $res["data"][$i][4] = 'Male';
            } else {
                $res["data"][$i][4] = 'Female';
            }
        }
        return json_encode($res);
    }

    function getHolidayList($id)
    {
        $this->datatables->select('DATE_FORMAT(' . TABLE_PREFIX . 'store_has_holidays.dt_holiday_date, \'%d/%m/%Y\') AS startDate,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'store_has_holidays.dt_start_time, \'%H:%i:%s\') AS starttime,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'store_has_holidays.dt_end_time, \'%H:%i:%s\') AS endtime,
                                    store_has_holidays.txt_note,
                                    store_has_holidays.id,
                                    store_has_holidays.fk_store,
                                    store_has_holidays.created_at,
                                    ');
        $this->datatables->from('store_has_holidays');
        $this->datatables->where('store_has_holidays.fk_store', $id);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $id = $res["data"][$i][4];
            $res["data"][$i][4] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_holiday/') . $id . '"><i class="fa fa-eye"></i> Edit </a>
                                    <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/delete_holiday') . '" data-id="' . $id . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function addHoliday($data, $json_response)
    {
        $config = array(
            array('field' => 'var_holiday_date', 'label' => 'Date', 'rules' => 'required'),
            array('field' => 'opening_time', 'label' => 'Opening Time', 'rules' => 'required'),
            array('field' => 'closing_time', 'label' => 'Closing Time', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $StorestaffData = array(
                'fk_store' => $data['storeId'],
                'dt_holiday_date' => ($data['var_holiday_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_holiday_date'])->format('Y-m-d') : null,
                'dt_start_time' => ($data['opening_time']) ? $data['opening_time'] : null,
                'dt_end_time' => ($data['closing_time']) ? $data['closing_time'] : null,
                'txt_note' => ($data['note']) ? $data['note'] : null,
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_holidays', $StorestaffData);
            $lastid = $this->db->insert_id();

            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Holiday Addedd Successfully!..';
                $json_response['redirect'] = admin_url('stores/holiday_manager/') . $data['storeId'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function editHoliday($data, $json_response)
    {

        $config = array(
            array('field' => 'var_holiday_date', 'label' => 'Date', 'rules' => 'required'),
            array('field' => 'opening_time', 'label' => 'Opening Time', 'rules' => 'required'),
            array('field' => 'closing_time', 'label' => 'Closing Time', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $StorestaffData = array(
                'dt_holiday_date' => ($data['var_holiday_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_holiday_date'])->format('Y-m-d') : null,
                'dt_start_time' => ($data['opening_time']) ? $data['opening_time'] : null,
                'dt_end_time' => ($data['closing_time']) ? $data['closing_time'] : null,
                'txt_note' => ($data['note']) ? $data['note'] : null,
            );
            $this->db->where('id', $data['holiday_id']);
            $update = $this->db->update('store_has_holidays', $StorestaffData);

            if ($update) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Holiday Updated Successfully!..';
                $json_response['redirect'] = admin_url('stores/holiday_manager/') . $data['storeId'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getBookingList($storeId)
    {
        $this->datatables->select('DATE_FORMAT(tk_booking.dt_booking_date,"%d/%m/%Y") as bookingDate,
                                    CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,
		                            booking.var_price,
		                            booking.id as bookingId,
		                            booking.id as bookId,
		                            booking.enum_book_type,  
		                            booking.id as bId,
		                            ');
        $this->datatables->from('booking');
        $this->datatables->join('users', 'users.id = booking.fk_user');
        $this->datatables->where('booking.fk_store', $storeId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][3] = getServiceByBookingId($res["data"][$i][6]);
            $res["data"][$i][4] = getTimeSlotByBookingId($res["data"][$i][6]);
        }
        return json_encode($res);
    }

    function getBankList($storeId)
    {
        $this->datatables->select('store_has_bank_details.var_accountno,
                                    store_has_bank_details.var_ifsc_code,
		                            store_has_bank_details.var_name,
		                            store_has_bank_details.var_bank_name,
		                            store_has_bank_details.var_branch_name,
		                            store_has_bank_details.txt_address,  
		                            store_has_bank_details.enum_acc_type,
		                            store_has_bank_details.var_debit_card_type,
		                            store_has_bank_details.var_card_number,
		                            store_has_bank_details.fk_store,
		                            store_has_bank_details.id,
		                            ');
        $this->datatables->from('store_has_bank_details');
        $this->datatables->where('store_has_bank_details.fk_store', $storeId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            if ($res["data"][$i][6] == 'S') {
                $res["data"][$i][6] = 'Saving';
            } else {
                $res["data"][$i][6] = 'Current';
            }
            $res["data"][$i][9] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/updateBankDetail/') . $res["data"][$i][9] . '"><i class="fa fa-eye"></i> Edit </a>';
        }
        return json_encode($res);
    }

    function updateBankDetail($data, $json_response)
    {

        $config = array(
            array('field' => 'var_acc_no', 'label' => 'ACc no', 'rules' => 'required'),
            array('field' => 'var_ifsc_code', 'label' => 'Ifsc code', 'rules' => 'required'),
            array('field' => 'var_bank_name', 'label' => 'Bank Name', 'rules' => 'required'),
            array('field' => 'var_branch_name', 'label' => 'Branch Name', 'rules' => 'required'),
            array('field' => 'acc_type', 'label' => 'Account Type', 'rules' => 'required'),
            array('field' => 'var_card_name', 'label' => 'Card Name', 'rules' => 'required'),
            array('field' => 'var_card_no', 'label' => 'Card no', 'rules' => 'required'),
            array('field' => 'var_address', 'label' => 'Address', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $bankData = $this->db->get_where('store_has_bank_details', array('fk_store' => $data['store_id']))->row_array();
            if (empty($bankData)) {
                $StorestaffData = array(
                    'fk_store' => ($data['store_id']),
                    'var_accountno' => ($data['var_acc_no']) ? $data['var_acc_no'] : null,
                    'var_ifsc_code' => ($data['var_ifsc_code']) ? $data['var_ifsc_code'] : null,
                    'var_name' => ($data['var_name']) ? $data['var_name'] : null,
                    'var_bank_name' => ($data['var_bank_name']) ? $data['var_bank_name'] : null,
                    'var_branch_name' => ($data['var_branch_name']) ? $data['var_branch_name'] : null,
                    'txt_address' => ($data['var_address']) ? $data['var_address'] : null,
                    'enum_acc_type' => ($data['acc_type']) ? $data['acc_type'] : null,
                    'var_debit_card_type' => ($data['var_card_name']) ? $data['var_card_name'] : null,
                    'var_card_number' => ($data['var_card_no']) ? $data['var_card_no'] : null,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("store_has_bank_details", $StorestaffData);
                $lastid = $this->db->insert_id();
            } else {
                $StorestaffData = array(
                    'var_accountno' => ($data['var_acc_no']) ? $data['var_acc_no'] : null,
                    'var_ifsc_code' => ($data['var_ifsc_code']) ? $data['var_ifsc_code'] : null,
                    'var_name' => ($data['var_name']) ? $data['var_name'] : null,
                    'var_bank_name' => ($data['var_bank_name']) ? $data['var_bank_name'] : null,
                    'var_branch_name' => ($data['var_branch_name']) ? $data['var_branch_name'] : null,
                    'txt_address' => ($data['var_address']) ? $data['var_address'] : null,
                    'enum_acc_type' => ($data['acc_type']) ? $data['acc_type'] : null,
                    'var_debit_card_type' => ($data['var_card_name']) ? $data['var_card_name'] : null,
                    'var_card_number' => ($data['var_card_no']) ? $data['var_card_no'] : null,
                );
                $this->db->where('fk_store', $data['store_id']);
                $update = $this->db->update('store_has_bank_details', $StorestaffData);
            }


            if ($update || $lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Bank Details Updated Successfully!..';
                $json_response['redirect'] = admin_url('stores/bankdetail/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getPackageList($storeId)
    {
        $this->datatables->select(' store_has_package.var_name,
                                    "" as services,
		                            store_has_package.var_price,
		                            store_has_package.txt_note,
		                            store_has_package.enum_enable,
		                            store_has_package.fk_store,  
		                            store_has_package.id,
		                            store_has_package.created_at
		                            ');
        $this->datatables->from('store_has_package');
        $this->datatables->where('store_has_package.fk_store', $storeId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][0] = '<a href="' . admin_url('stores/edit_package/') . $res["data"][$i][6] . '">' . $res["data"][$i][0] . '</a>';
            $res["data"][$i][1] = getPackageService($res["data"][$i][6]);
            if ($res["data"][$i][4] == 'YES') {
                $res["data"][$i][4] = 'Enable';
            } else {
                $res["data"][$i][4] = 'Disable';
            }
            $res["data"][$i][5] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_package/') . $res["data"][$i][6] . '"><i class="fa fa-eye"></i> Edit </a>
                                  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/delete_package') . '" data-id="' . $res["data"][$i][6] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function getStoreService($storeId)
    {
        $this->db->select('store_has_services.id,
		                            services.var_title,
		                            store_has_services.var_price,     
		                            store_has_services.enum_enable,
		                            store_has_services.fk_store,
		                            store_has_services.fk_service,
		                            ');
        $this->db->from('store_has_services');
        $this->db->join('services', 'services.id = store_has_services.fk_service');
        $this->db->where("store_has_services.fk_store", $storeId);
        return $this->db->get()->result_array();
    }

    function addPackage($data, $json_response)
    {
        $config = array(
            array('field' => 'store_id', 'label' => 'Store', 'rules' => 'required'),
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $storePack = array(
                'fk_store' => $data['store_id'],
                'var_name' => $data['var_name'],
                'var_price' => $data['var_price'],
                'txt_note' => (!empty($data['note'])) ? $data['note'] : null,
                'enum_enable' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_package', $storePack);
            $lastStoreid = $this->db->insert_id();
            if ($lastStoreid > 0) {
                for ($i = 0; $i < count($data['services']); $i++) {
                    $serviceArr = array(
                        'fk_package' => $lastStoreid,
                        'fk_service' => $data['services'][$i],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('package_has_service', $serviceArr);
                    $lastid = $this->db->insert_id();
                }
            }

            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Package Added Successfully!..';
                $json_response['redirect'] = admin_url('stores/package/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function deletePackage($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'store_has_package', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Package Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');packageDatetables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getPackageInfo($packageId)
    {
        $storeId = $this->db->get_where('store_has_package', array('id' => $packageId))->row_array()['fk_store'];
        $this->db->select('store_has_package.id,
                               store_has_package.var_name,
                               store_has_package.var_price,
                               store_has_package.txt_note,
                               store_has_package.enum_enable,
                               store_has_package.fk_store,
                                ');
        $this->db->from('store_has_package');
        $this->db->where('store_has_package.fk_store', $storeId);
        $returnData['packages'] = $this->db->get()->result_array();
        for ($i = 0; $i < count($returnData['packages']); $i++) {
            $this->db->select('services.var_title,services.id');
            $this->db->from('services');
            $this->db->join('package_has_service', 'package_has_service.fk_service = services.id');
            $this->db->where('package_has_service.fk_package', $returnData['packages'][$i]['id']);
            $returnData['packages'][$i]['services'] = $this->db->get()->result_array();
        }
        return $returnData;
    }

    function updatePackage($data, $json_response)
    {
//        print_array($data);
        $config = array(
            array('field' => 'store_id', 'label' => 'Store', 'rules' => 'required'),
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $storePack = array(
                'var_name' => $data['var_name'],
                'var_price' => $data['var_price'],
                'txt_note' => $data['note'],
                'enum_enable' => ($data['enum_status'] == 'YES') ? 'YES' : 'NO',
            );
            $this->db->where('id', $data['package_id']);
            $update = $this->db->update('store_has_package', $storePack);
            if ($update) {
                $this->db->where('fk_package', $data['package_id']);
                $this->db->delete('package_has_service');
                for ($i = 0; $i < count($data['services']); $i++) {
                    $serviceArr = array(
                        'fk_package' => $data['package_id'],
                        'fk_service' => $data['services'][$i],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('package_has_service', $serviceArr);
                    $lastid = $this->db->insert_id();
                }
            }
            if ($lastid > 0 || $update) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Package updated Successfully!..';
                $json_response['redirect'] = admin_url('stores/package/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getOfferList($storeId)
    {
        $this->datatables->select('store_has_offer.var_name,
                            "" as services,
                            store_has_offer.dt_start_date,
                            store_has_offer.dt_end_date,
                            store_has_offer.var_price,
                            "" as OfferPrice,
                            store_has_offer.txt_note,
                            store_has_offer.enum_enable,
                            store_has_offer.id,
                            store_has_offer.fk_store
                                ');
        $this->datatables->from('store_has_offer');
        $this->datatables->where('store_has_offer.fk_store', $storeId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $offerId = $res["data"][$i][8];
            $storeId = $res["data"][$i][9];
            $res["data"][$i][0] = '<a href="' . admin_url('stores/edit_offer/') . $res["data"][$i][8] . '">' . $res["data"][$i][0] . '</a>';
            $res["data"][$i][2] = date('d/m/Y', strtotime($res["data"][$i][2]));
            $res["data"][$i][3] = date('d/m/Y', strtotime($res["data"][$i][3]));
            $res["data"][$i][1] = getOfferService($offerId);
            $res["data"][$i][5] = getOfferServicePrice($offerId, $storeId);
            if ($res["data"][$i][7] == 'YES') {
                $res["data"][$i][7] = 'Enable';
            } else {
                $res["data"][$i][7] = 'Disable';
            }
            $res["data"][$i][8] = '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('stores/edit_offer/') . $offerId . '"><i class="fa fa-eye"></i> Edit </a>
                                  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('stores/delete_offer') . '" data-id="' . $offerId . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function addOffer($data, $json_response)
    {
//        print_array($data); exit;
        $config = array(
            array('field' => 'store_id', 'label' => 'Store', 'rules' => 'required'),
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'var_str_date', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'var_end_date', 'label' => 'Price', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $storePack = array(
                'fk_store' => $data['store_id'],
                'var_name' => $data['var_name'],
                'var_price' => $data['var_price'],
                'txt_note' => $data['note'],
                'dt_start_date' => ($data['var_str_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_str_date'])->format('Y-m-d') : null,
                'dt_end_date' => ($data['var_end_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_end_date'])->format('Y-m-d') : null,
                'enum_enable' => 'YES',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('store_has_offer', $storePack);
            $lastStoreid = $this->db->insert_id();
            if ($lastStoreid > 0) {
                for ($i = 0; $i < count($data['services']); $i++) {
                    $serviceArr = array(
                        'fk_offer' => $lastStoreid,
                        'fk_service' => $data['services'][$i],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('offer_has_service', $serviceArr);
                    $lastid = $this->db->insert_id();
                }
            }

            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Offer Added Successfully!..';
                $json_response['redirect'] = admin_url('stores/offer/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function deleteOffer($data)
    {
        $result = $this->toval->id_delete($data['id'], 'store_has_offer', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Offer Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');offerDatetables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getOfferInfo($offerId)
    {
        $this->db->select('store_has_offer.id,
                               store_has_offer.var_name,
                               store_has_offer.var_price,
                               store_has_offer.txt_note,
                               store_has_offer.dt_start_date,
                               store_has_offer.dt_end_date,
                               store_has_offer.enum_enable,
                               store_has_offer.fk_store,
                                ');
        $this->db->from('store_has_offer');
        $this->db->where('store_has_offer.id', $offerId);
        $returnData['offers'] = $this->db->get()->row_array();
        $this->db->select('services.id,services.var_title');
        $this->db->from('services');
        $this->db->join('offer_has_service', 'offer_has_service.fk_service = services.id');
        $this->db->where('offer_has_service.fk_offer', $offerId);
        $returnData['offers']['services'] = $this->db->get()->result_array();
        for ($j = 0; $j < count($returnData['offers']['services']); $j++) {
            $this->db->select('var_price');
            $this->db->from('store_has_services');
            $this->db->where('store_has_services.fk_service', $returnData['offers']['services'][$j]['id']);
            $this->db->where('store_has_services.fk_store', $returnData['offers']['fk_store']);
            $returnData['offers']['services'][$j]['var_price'] = $this->db->get()->row_array()['var_price'];
        }
        return $returnData;
    }

    function updateOffer($data, $json_response)
    {
//        print_array($data);
        $config = array(
            array('field' => 'store_id', 'label' => 'Store', 'rules' => 'required'),
            array('field' => 'var_name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'var_price', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'var_str_date', 'label' => 'Price', 'rules' => 'required'),
            array('field' => 'var_end_date', 'label' => 'Price', 'rules' => 'required')
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $storePack = array(
                'fk_store' => $data['store_id'],
                'var_name' => $data['var_name'],
                'var_price' => $data['var_price'],
                'txt_note' => $data['note'],
                'dt_start_date' => ($data['var_str_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_str_date'])->format('Y-m-d') : null,
                'dt_end_date' => ($data['var_end_date']) ? DateTime::createFromFormat('d/m/Y', $data['var_end_date'])->format('Y-m-d') : null,
                'enum_enable' => ($data['enum_status'] == 'YES') ? 'YES' : 'NO',
            );
            $this->db->where('id', $data['offer_id']);
            $update = $this->db->update('store_has_offer', $storePack);
            if ($update) {
                $this->db->where('fk_offer', $data['offer_id']);
                $this->db->delete('offer_has_service');
                for ($i = 0; $i < count($data['services']); $i++) {
                    $serviceArr = array(
                        'fk_offer' => $data['offer_id'],
                        'fk_service' => $data['services'][$i],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('offer_has_service', $serviceArr);
                    $lastid = $this->db->insert_id();
                }
            }

            if ($lastid > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Offer updated Successfully!..';
                $json_response['redirect'] = admin_url('stores/offer/') . $data['store_id'];
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getStoreGallery($storeId)
    {
        $this->db->select('id,fk_store as storeId,var_file,enum_isProfile,enum_ismain,enum_enable,created_at');
        $this->db->from('store_has_gallery');
        $this->db->where('enum_enable', 'YES ');
        $this->db->where('fk_store', $storeId);
        return $this->db->get()->result_array();
    }


    function getStorePortfolio($storeId)
    {
        $this->db->select('id,fk_store as storeId,var_title,var_file,enum_enable,created_at');
        $this->db->from('store_has_portfolio');
        $this->db->where('enum_enable', 'YES');
        $this->db->where('fk_store', $storeId);
        return $this->db->get()->result_array();
    }

    function addImages_old($data, $json_response)
    {
        if (!empty($data)) {
            if ($_FILES['image_file']['name'] != "") {
                $image = upload_single_image($_FILES, 'store', STORE_IMG, FALSE);
                $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';
                $imgArr = array(
                    'fk_store' => $data['store_code'],
                    'var_file' => $var_image,
                    'enum_isProfile' => 'No',
                    'enum_ismain' => 'YES',
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_gallery', $imgArr);
                $lastid = $this->db->insert_id();
            }
        }
        if ($lastid > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Image Added Successfully!..';
            $json_response['redirect'] = admin_url('stores/gallery/') . $data['store_code'];
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function addImages($data, $json_response)
    {
        if (!empty($_FILES['image_file'])) {
            $this->load->library('Mylibrary');
            for ($i = 0; $i < count($_FILES['image_file']['name']); $i++) {
                $_FILES['imagefile']['name'] = $_FILES['image_file']['name'][$i];
                $_FILES['imagefile']['type'] = $_FILES['image_file']['type'][$i];
                $_FILES['imagefile']['tmp_name'] = $_FILES['image_file']['tmp_name'][$i];
                $_FILES['imagefile']['error'] = $_FILES['image_file']['error'][$i];
                $_FILES['imagefile']['size'] = $_FILES['image_file']['size'][$i];

                $imgexten = explode('.', $_FILES['imagefile']['name']);
                $img_name = 'imagefile';
                $img_last_key = end($imgexten);
                $image_name = url_title($imgexten[0]) . time();
                $array = array(
                    'image_pet_name' => $img_name,
                    'image_name' => $image_name,
                    'path' => STORE_IMG,
                    'extension' => $img_last_key,
                );

                $result = $this->mylibrary->upload_image($array);

                if (is_array($result)) {
                    $imgArr = array(
                        'fk_store' => $data['store_code'],
                        'var_file' => $result['orig_name'],
                        'var_width' => $result['image_width'],
                        'var_height' => $result['image_height'],
                        'enum_isProfile' => 'No',
                        'enum_ismain' => 'YES',
                        'enum_enable' => 'YES',
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('store_has_gallery', $imgArr);
                    $lastid = $this->db->insert_id();

                    $small_size = '100';
                    $medium_size = '200';
                    $large_size = '500';

                    $thumb_array = array(
                        array('width' => $small_size, 'height' => $small_size, 'image_type' => 'SMALL'),
                        array('width' => $medium_size, 'height' => $medium_size, 'image_type' => 'MEDIUM'),
                        array('width' => $large_size, 'height' => $large_size, 'image_type' => 'LARGE'));

                    for ($j = 0; $j < count($thumb_array); $j++) {

                        $data_thump['config']['source_image'] = STORE_IMG . '/' . $result['orig_name'];
                        $data_thump['config']['maintain_ratio'] = TRUE;
                        $data_thump['config']['create_thumb'] = TRUE;
                        $data_thump['config']['new_image'] = $result['orig_name'];
                        $data_thump['config']['thumb_marker'] = TRUE;
                        $data_thump['width'] = $thumb_array[$j]['width'];
                        $data_thump['height'] = $thumb_array[$j]['height'];
                        $data_thump['full_path'] = $result['full_path'];
//                        $data_thump['table'] = 'kg_sell_artwork_has_images';
//                        $data_thump['id'] = $sell_artwork_id;
//                        $data_thump['parent_id'] = $original_img_id;
                        $thumb_data = $this->mylibrary->create_thumb($data_thump);
                        $imgArr = array(
                            'fk_store' => $data['store_code'],
                            'fk_parent' => $lastid,
                            'var_file' => $thumb_data['image_name'],
                            'var_width' => $thumb_array[$j]['width'],
                            'var_height' => $thumb_array[$j]['height'],
                            'enum_isProfile' => 'No',
                            'enum_ismain' => 'NO',
                            'enum_enable' => 'YES',
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('store_has_gallery', $imgArr);
                        $last_id = $this->db->insert_id();
                    }
                }
            }
        }
        if ($last_id > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Image Added Successfully!..';
            $json_response['redirect'] = admin_url('stores/gallery/') . $data['store_code'];
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function deleteImage($data, $response)
    {
        $imgId = $data['imgId'];
        $removefile = STORE_IMG . '/' . $data['file'];
        $this->db->where('id', $imgId);
        $this->db->delete('store_has_gallery');
        if ($this->db->affected_rows() > 0) {
            //REtireve all thumb images
            $thumbimages = $this->db->get_where('store_has_gallery', array('fk_parent' => $imgId))->result_array();
            for ($i = 0; $i < count($thumbimages); $i++) {
                $this->db->where('id', $thumbimages[$i]['id']);
                $this->db->delete('store_has_gallery');
                $thumbimg = STORE_IMG . '/' . $thumbimages[$i]['var_file'];
                if (file_exists($thumbimg)) {
                    unlink($thumbimg);
                }
            }
            if (file_exists($removefile)) {
                unlink($removefile);
                $json_response['status'] = 'success';
                $json_response['message'] = 'Image deleted Successfully!..';
                $json_response['jscode'] = " setTimeout(function () { location.reload() }, 100);";
            }
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function makeImageAsProfile($data, $response)
    {
        if (!empty($data)) {
            $getProfileImg = $this->db->get_where('store_has_gallery', array('enum_isProfile' => 'YES', 'fk_store' => $data['storeId']))->row_array();
            if (!empty($getProfileImg)) {
                $this->db->where('id', $getProfileImg['id']);
                $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'NO'));
                if ($update) {
                    $this->db->where('id', $data['imageId']);
                    $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'YES'));
                }
            } else {
                $this->db->where('id', $data['imageId']);
                $update = $this->db->update('store_has_gallery', array('enum_isProfile' => 'YES'));
            }
            if ($update) {
                $response['status'] = 'success';
                $response['message'] = 'Store image updated successfully!';
            } else {
                $response['status'] = 'error';
                $response['message'] = 'Something is wrong';
            }
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Store not found';
        }
        return $response;
    }

    function delete_holiday($data){
        $result = $this->toval->id_delete($data['id'], 'store_has_holidays', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Store Holiday Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');holidayDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function addPortfolio($data, $response){
        if (!empty($data)) {
            if ($_FILES['image_file']['name'] != "") {
                $image = upload_single_image($_FILES, 'portfolio', PORTFOLIO_IMG, FALSE);
                $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';
                $imgArr = array(
                    'fk_store' => $data['store_code'],
                    'var_file' => $var_image,
                    'var_title' => $data['var_title'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('store_has_portfolio', $imgArr);
                $lastid = $this->db->insert_id();
            }
        }
        if ($lastid > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'portfolio Added Successfully!..';
            $json_response['redirect'] = admin_url('stores/portfolio/') . $data['store_code'];
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
