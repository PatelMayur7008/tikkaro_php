<?php

class Facility_model extends CI_Model
{

    function getFacilityList()
    {
        $this->datatables->select('extra_facility.id,
		                            extra_facility.var_name,
		                            extra_facility.enum_enable,
		                            extra_facility.id');
        $this->datatables->from('extra_facility');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('facility/overview/').$res["data"][$i][0]. '">' . $res["data"][$i][1] . '</a>';
            if($res["data"][$i][2] == 'YES'){
                $res["data"][$i][2] = 'Enable';
            }else{
                $res["data"][$i][2] = 'Disable';
            }
            $res["data"][$i][3] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('facility/delete') . '" data-id="' . $res["data"][$i][0] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_name', 'label' => 'Facility Name', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $checkAccount = $this->db->get_where('extra_facility', array('var_name' => $data['var_name']))->num_rows();
            if ($checkAccount == 0) {
                $AccountData = array(
                    'var_name' => $data['var_name'],
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("extra_facility", $AccountData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'FAcility added Successfully!..';
                    $json_response['redirect'] = admin_url('facility');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

            } else {
                $json_response['message'] = 'Facility Name alredy exist!..';
                $json_response['status'] = 'error';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }
    function getFacility($facilityId){
        return $this->db->get_where('extra_facility',array('id'=>$facilityId))->row_array();
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'extra_facility', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Facility Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');facilityDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
