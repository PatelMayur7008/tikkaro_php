<?php

class User_model extends CI_Model
{

    function getUserList()
    {
        $this->datatables->select('CONCAT(tk_users.var_fname," ",tk_users.var_lname) as username,
		                            users.var_email,
		                            users.bint_phone,     
		                            users.int_areacode,
		                            DATE_FORMAT(dt_dob,"%d/%m/%Y") as dt_dob,
		                            users.enum_geneder,
		                            "" as noofBooking,
		                            "" as totalPrice,
		                            "" as noofStore,
		                            users.enum_enable,
		                            users.id');
        $this->datatables->from('users');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][0] = '<a href="' . admin_url('user/overview/') . $res["data"][$i][10] . '">' . $res["data"][$i][0] . '</a>';
            if ($res["data"][$i][5] == 'M') {
                $gender = 'Male';
            } else {
                $gender = 'Female';
            }
            $res["data"][$i][5] = $gender;
            $res["data"][$i][6] = (getCountOfUserBooking($res["data"][$i][10]) > 0) ? getCountOfUserBooking($res["data"][$i][10]): 0;
            $res["data"][$i][7] = (getTotalBookingPrice($res["data"][$i][10]) > 0 )?getTotalBookingPrice($res["data"][$i][10]):0;
            $res["data"][$i][8] = (getCountOfUserStore($res["data"][$i][10]) > 0) ? getCountOfUserStore($res["data"][$i][10]) :0;
            if ($res["data"][$i][9] == 'YES') {
                $res["data"][$i][9] = 'Enable';
            } else {
                $res["data"][$i][9] = 'Disable';
            }
            $res["data"][$i][10] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('user/delete') . '" data-id="' . $res["data"][$i][10] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_fname', 'label' => 'First Name', 'rules' => 'required'),
            array('field' => 'var_lname', 'label' => 'Last Name', 'rules' => 'required'),
            array('field' => 'var_phone', 'label' => 'Phone no', 'rules' => 'required'),
            array('field' => 'gender', 'label' => 'gender', 'rules' => 'required'),
            array('field' => 'var_email', 'label' => 'Email', 'rules' => 'required'),
            array('field' => 'var_pin', 'label' => 'Pincode', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $checkAccount = $this->db->get_where('users', array('var_email' => $data['var_email']))->num_rows();
//            }else{
//			$checkAccountEdit = $this->db->get_where('account_manager', array('id' => $data['accountId'], 'var_email' => $data['var_email']))->num_rows();
//            }
//            echo $checkAccount; exit();

            if ($checkAccount == 0) {

                $var_image = ($data['profile_pic']) ? $data['profile_pic'] : '';

                if ($_FILES['profile_pic']['name'] != "") {
                    $image = upload_single_image($_FILES, 'user', USER_PROFILE_IMG, FALSE);
                    $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

//                    if ($data['var_profile_image'] != '') {
//                        delete_single_image(KEY_ACCOUNT_PROFILE_IMG, $data['var_profile_image']);
//                    }
                }

                $AccountData = array(
                    'var_fname' => $data['var_fname'],
                    'var_lname' => $data['var_lname'],
                    'var_email' => $data['var_email'],
                    'bint_phone' => ($data['var_phone']) ? $data['var_phone'] : NULL,
                    'dt_dob' => ($data['var_manage_dob']) ? DateTime::createFromFormat('d/m/Y', $data['var_manage_dob'])->format('Y-m-d') : NULL,
                    'enum_geneder' => ($data['gender']) ? $data['gender'] : NULL,
                    'enum_enable' => 'YES',
                    'var_profile_image' => $var_image,
                    'int_areacode' => ($data['var_pin']) ? $data['var_pin'] : NULL,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("users", $AccountData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'User added Successfully!..';
                    $json_response['redirect'] = admin_url('User');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

            } else {
                $json_response['message'] = 'Email alredy exist!..';
                $json_response['status'] = 'warning';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getUserCommonHeader($userId)
    {
        return $this->db->get_where('users', array('id' => $userId))->row_array();
    }

    function updateUserAddress($data)
    {
        $updateArray = array(
            'var_latitude' => $data['lat'],
            'var_longitude' => $data['lng'],
            'int_areacode' => $data['pincode'],
            'txt_address' => $data['address']
        );
        $this->db->where('id',$data['user_id']);
        $this->db->update('users',$updateArray);

    }

    function getStoreList($userId)
    {
        $this->datatables->select('stores.var_title,
		                            stores.var_email,
		                            stores.txt_note,     
		                            stores.txt_address,
		                            stores.bint_phone,
		                            stores.int_weekoff,
		                            stores.int_no_book_seats,
		                            stores.var_fb_link,
		                            stores.var_twitter_link,
		                            stores.var_instagram_link,
		                            stores.var_whatsapp_no,
		                            stores.var_rating,
		                            stores.enum_store_type,
		                            stores.id');
        $this->datatables->from('store_has_customer');
        $this->datatables->join('stores','stores.id = store_has_customer.fk_store');
        $this->datatables->where('store_has_customer.fk_user',$userId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][5] = getDayNameFromNumber($res["data"][$i][5]);
            if ($res["data"][$i][12] == 'MEN') {
                $gender = 'Male';
            } else if($res["data"][$i][12] == 'WOMEN'){
                $gender = 'Female';
            }else{
                $gender = 'Both';
            }
            $res["data"][$i][12] = $gender;
        }
        return json_encode($res);
    }

    function getBookingList($userId)
    {
        $this->datatables->select('DATE_FORMAT(tk_booking.dt_booking_date,"%d/%m/%Y") as bookingDate,
                                    stores.var_title,
		                            booking.var_price,
		                            booking.id as bookingId,
		                            booking.id as bookId,
		                            booking.enum_book_type,  
		                            booking.id as bId,
		                            ');
        $this->datatables->from('booking');
        $this->datatables->join('stores','stores.id = booking.fk_store');
        $this->datatables->where('booking.fk_user',$userId);
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][3] = getServiceByBookingId($res["data"][$i][6]);
            $res["data"][$i][4] = getTimeSlotByBookingId($res["data"][$i][6]);
        }
        return json_encode($res);
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'users', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'User Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');usermanagerDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
