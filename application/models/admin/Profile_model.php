<?php

class Profile_model extends CI_Model
{
    /**
     * Check DOB in dash format 25-03-2016
     * @param $value DOB In Format
     * @return bool If dob correct than return true otherwise false
     */
    public function check_dob($value)
    {
        $this->form_validation->set_message('dob_callable', 'Invalid %s.There should be earlier years than current year.');
        if ($time = strtotime($value)) {
            $current_year = date('Y', $time);
            $now_year = date('Y');
            if ($current_year < $now_year) {
                return TRUE;
            }
        }
        return FALSE;
    }

    /**
     * Edit profile
     * @param $data Input array
     * @param $json_response Default JSONResponse
     * @return mixed
     */
    public function editProfile($data, $json_response)
    {

        $config = array(
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
            array('field' => 'fname', 'label' => 'First Name', 'rules' => 'trim|required'),
            array('field' => 'lname', 'label' => 'Last Name', 'rules' => 'trim|required'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'trim|required'),
            array('field' => 'phone', 'label' => 'Phone', 'rules' => 'trim|required'),
            array('field' => 'address', 'label' => 'Address', 'rules' => 'trim|required'),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $userData = array(
                'var_fname' => $data['fname'],
                'var_lname' => $data['lname'],
                'var_email' => $data['email'],
                'bint_phone' => $data['phone'],
                'txt_address' => $data['address'],
                'dt_dob' => date('Y-m-d', strtotime($data['var_manage_dob'])),
            );

            $this->db->where("id", $data['id']);
            $result = $this->db->update('admin', $userData);

            if ($result > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Updated';
                $json_response['reload'] = 'true';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function editProfilePassword($data, $json_response)
    {
        $config = array(
            array('field' => 'old_pwd', 'label' => 'Old password', 'rules' => 'trim|required'),
            array('field' => 'new_pwd', 'label' => 'New password', 'rules' => 'trim|required'),
            array('field' => 'conf_pwd', 'label' => 'Confirm password', 'rules' => 'trim|required|matches[new_pwd]'),
        );
        $this->load->library('form_validation');
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $row = $this->db->get_where('admin', array('id' => $data['admin_id']))->row_array();

            if ($row['var_password'] !== md5($data['old_pwd'])) {
                $json_response['message'] = 'Password didn\'t match with current password';
                $json_response['status'] = 'error';
                return $json_response;
            }
            $userData = array(
                'var_password' => md5($data['new_pwd']),
            );
            $this->db->where("id",$data['admin_id']);
            $result = $this->db->update('admin', $userData);

            if ($result > 0) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Updated';
                $json_response['reload'] = 'true';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function editProfilePicture($data, $json_response)
    {

        if (!empty($_FILES)) {
            $table = 'admin';
            $old_image = $this->db->get_where('admin', ['id' => $data['admin_id']])->row_array();
            if ($old_image['var_profile_image'] !== NULL && trim($old_image['var_profile_image']) !== '') {
                delete_single_image(ADMIN_PROFILE_IMG, $old_image['var_profile_image']);
            }
            $imageError = upload_single_image($_FILES, 'Profile', ADMIN_PROFILE_IMG, FALSE);
            $var_image = ($imageError['error'] == '') ? $imageError['data']['orig_name'] : '';

            $image = array(
                'var_profile_image' => $var_image,
            );
            $this->db->where('id', $data['admin_id']);
            $update = $this->db->update($table, $image);
            if($update){
                $json_response['imageError'] = $imageError;
                $json_response['status'] = 'success';
                $json_response['message'] = 'Updated';
                $json_response['reload'] = 'true';
            }

        } else {
            $json_response['message'] = 'Image is required';
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }
}
