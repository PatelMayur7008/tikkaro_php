<?php

class Bussiness_model extends CI_Model
{

    function getBussinessList()
    {
        $this->datatables->select(' bussiness.var_bussiness_code,
		                            bussiness.var_name,
		                            bussiness.id as tempid,
		                            bussiness.var_phone,
		                            bussiness.var_email,
		                            bussiness.txt_note,
		                            bussiness.enum_enable,
		                            bussiness.created_at,
		                            bussiness.id');
        $this->datatables->from('bussiness');
        $results = $this->datatables->generate();
        $res = (array)json_decode($results);
        for ($i = 0; $i < count($res['data']); $i++) {
            $res["data"][$i][1] = '<a href="' . admin_url('bussiness/overview/') . $res["data"][$i][8] . '">' . $res["data"][$i][1] . '</a>';
            $res["data"][$i][2] = getCategoryFromBussiness($res["data"][$i][8]);
            $res["data"][$i][7] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('bussiness/delete') . '" data-id="' . $res["data"][$i][8] . '" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>';
        }
        return json_encode($res);
    }

    function getAllCategory(){
        return $this->db->get('bussiness_category')->result_array();
    }

    function add($data, $json_response)
    {
        $config = array(
            array('field' => 'var_name', 'label' => ' Name', 'rules' => 'required'),
//            array('field' => 'Category', 'label' => 'Category', 'rules' => 'required'),
            array('field' => 'var_phone', 'label' => 'Phone no', 'rules' => 'required'),
            array('field' => 'var_email', 'label' => 'Email', 'rules' => 'required'),
        );


        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

                $AccountData = array(
                    'var_bussiness_code' => generateRandomBussinessString(),
                    'var_name' => $data['var_name'],
                    'var_phone' => ($data['var_phone']) ? $data['var_phone'] : NULL,
                    'var_email' => $data['var_email'],
                    'txt_note' => ($data['note']) ? $data['note'] : NULL,
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert("bussiness", $AccountData);
                $lastid = $this->db->insert_id();
                if ($lastid > 0) {
                    for($j=0;$j<count($data['Category']);$j++){
                        $insertArr = array(
                            'fk_bussiness'=>$lastid,
                            'fk_category'=>$data['Category'][$j],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert("bussiness_has_category", $insertArr);
                    }
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Bussiness added Successfully!..';
                    $json_response['redirect'] = admin_url('bussiness');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getBussinessData($bussId)
    {
        return $this->db->get_where('bussiness', array('id' => $bussId))->row_array();
    }
    function getBussinessCategories($bussId){
        $this->db->select('bussiness_has_category.id,bussiness_category.var_name');
        $this->db->join('bussiness_category','bussiness_category.id = bussiness_has_category.fk_category');
        $this->db->where('fk_bussiness',$bussId);
        return $this->db->get('bussiness_has_category')->result_array();
    }



    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'bussiness', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Business Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');usermanagerDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
