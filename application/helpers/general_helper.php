<?php

function get_country($ip)
{
	$CI = &get_instance();
	$CI->load->model('curl_function');
	$url = "http://api.wipmania.com/" . $ip . "?" . base_url();
	$result = $CI->curl_function->get($url);
	return $result;
}

function admin_url($url = '')
{
	$CI = &get_instance();
	return $CI->config->config['admin_url'] . $url;
}

function customer_url($url = '')
{
	$CI = &get_instance();
	return $CI->config->config['customer_url'] . $url;
}

function expert_url($url = '')
{
	$CI = &get_instance();
	return $CI->config->config['expert_url'] . $url;
}

function compliance_url($url = '')
{
	$CI = &get_instance();
	return $CI->config->config['compliance_url'] . $url;
}

function user_url($url = '')
{
	$CI = &get_instance();
	return $CI->config->config['user_url'] . $url;
}

function get_project_name()
{
	$CI = &get_instance();
	return $CI->config->config['project_name'];
}

function get_skip($page_no = 1, $per_page = 24)
{
	return (($page_no - 1) * $per_page);
}

function upload_single_image($file, $name, $path, $thumb = FALSE)
{
	$CI = &get_instance();

	$return['error'] = '';
	$image_name = $name . '_' . time();

	$CI->load->helper('form');
	$config['upload_path'] = $path;
	$config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|PNG|JPG';
	$config['file_name'] = $image_name;

	$CI->load->library('upload', $config);
	$CI->upload->initialize($config);

	$CI->upload->set_allowed_types('gif|jpg|png|jpeg|JPEG|PNG|JPG|GIF');

	if (!$CI->upload->do_upload(key($file))) {
		$return['error'] = $CI->upload->display_errors();
	} else {
		$result = $CI->upload->data();
		$return['data'] = $result;
	}

	if ($thumb == TRUE && $return['error'] == '') {
		$CI->load->library('Mylibrary');
		$thumb_array = array(
			array('width' => '100', 'height' => '100', 'image_type' => 'SMALL'),
			array('width' => '250', 'height' => '250', 'image_type' => 'MEDIUM'));
		for ($i = 0; $i < count($thumb_array); $i++) {
			$imageinfo = getimagesize($result['full_path']);
			$thumbSize = $CI->mylibrary->calculateResizeImage($imageinfo[0], $imageinfo[1], $thumb_array[$i]['width'], $thumb_array[$i]['height']);

			$CI->load->library('image_lib');
			$conf['image_library'] = 'gd2';
			$conf['source_image'] = $path . $result['orig_name'];
			$conf['create_thumb'] = TRUE;
			$conf['maintain_ratio'] = TRUE;
			$conf['new_image'] = $result['orig_name'];
			$conf['thumb_marker'] = "_" . $thumb_array[$i]['image_type'];
			$conf['width'] = $thumbSize['width'];
			$conf['height'] = $thumbSize['height'];
			$CI->image_lib->clear();
			$CI->image_lib->initialize($conf);
			if (!$CI->image_lib->resize()) {
				$return['error'] = 'Thumb Not Created';
			}
		}
	}

	return $return;
}
function getSchedulingCount($clientId){
	$CI = &get_instance();
	return $CI->db->get_where('project_has_experts',array('fk_client'=>$clientId,'enum_status'=>'CALLS_IN_SCHEDULING'))->num_rows();

}
function getScheduledCount($clientId){
	$CI = &get_instance();
	return $CI->db->get_where('calls',array('fk_client'=>$clientId,'enum_status'=>'CALLS_SCHEDULED'))->num_rows();
}

function delete_single_image($fullPath, $fileName)
{
	unlink($fullPath . '/' . $fileName);
}

function delete_image($array, $path)
{
	$CI = &get_instance();
	$img = $CI->db->select($array['field'])->where('int_glcode', $array['id'])->get($array['table'])->row_array();
	$mainImg = $img[$array['field']];
	$expImg = explode('.', $mainImg);
	$imgdelete = $path . $mainImg;
	if (file_exists($imgdelete)) {
		unlink($imgdelete);
	}
	return TRUE;
}

function sanitize($string, $force_lowercase = true, $anal = false)
{

	$string = htmlentities($string, ENT_QUOTES, 'UTF-8');
	$string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
	$string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');

	$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
		"}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
		"â€”", "â€“", ",", "<", ".", ">", "/", "?");
	$clean = trim(str_replace($strip, "", strip_tags($string)));
	$clean = preg_replace('/\s+/', "-", $clean);
	$clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
	return ($force_lowercase) ?
		(function_exists('mb_strtolower')) ?
			mb_strtolower($clean, 'UTF-8') :
			strtolower($clean) :
		$clean;
}

function sanitize_title($string)
{

	$strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "=", "+", "[", "{", "]",
		"}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
		"â€”", "â€“", ",", "<", ".", ">", "/", "?");
	$string = trim(str_replace($strip, "", strip_tags($string)));

	$url = $string;
	$url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
	$url = trim($url, "-");
	$url = iconv("utf-8", "us-ascii//TRANSLIT", $url); // TRANSLIT does the whole job
	$url = strtolower($url);
	$url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
	return $url;
}

function apply_lang($string, $delimiter = ' ')
{
	$CI = &get_instance();
	$str_array = explode($delimiter, $string);
	$return_string = '';

	for ($i = 0; $i < count($str_array); $i++) {
		$return_string .= $CI->lang->line($str_array[$i]);
		if ($i < count($str_array)) {
			$return_string .= $delimiter;
		}
	}
	if ($return_string == '') {
		return $CI->lang->line($string);
	}
	return $return_string;
}

function sorttextlen($text, $limit)
{
	if (strlen($text) < $limit) {
		$sort_text = mb_substr($text, 0, $limit);
	} else if (strlen($text) > $limit) {
		$sort_text = mb_substr($text, 0, $limit) . '...';
	}

	return $sort_text;
}

function shortenText($str, $len, $readMore = " ... ")
{

	if (mb_strlen($str) > $len) {
		$str = mb_substr($str, 0, $len);
		//        $str = mb_substr($str, 0, mb_strrpos($str, " "));
		return html_entity_decode(mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8'), ENT_QUOTES, 'UTF-8') . $readMore;
	} else
		return html_entity_decode(mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8'), ENT_QUOTES, 'UTF-8');
}

function date_formate($date)
{
	$date = date('M', strtotime($date)) . ' ' . date('j', strtotime($date)) . "'" . date('y', strtotime($date)) . ' at' . ' ' . date('h:i', strtotime($date));
	return $date;
}

function str_replace_first($from, $to, $subject)
{
	$from = '/' . preg_quote($from, '/') . '/';

	return preg_replace($from, $to, $subject, 1);
}

/* ----   Start Function to get the client IP address ----- */

function get_ip()
{
	$ipaddress = '';
	if (getenv('HTTP_CLIENT_IP')) {
		$ipaddress = getenv('HTTP_CLIENT_IP');
	} else if (getenv('HTTP_X_FORWARDED_FOR')) {
		$ipaddress = getenv('HTTP_X_FORWARDED_FOR');
	} else if (getenv('HTTP_X_FORWARDED')) {
		$ipaddress = getenv('HTTP_X_FORWARDED');
	} else if (getenv('HTTP_FORWARDED_FOR')) {
		$ipaddress = getenv('HTTP_FORWARDED_FOR');
	} else if (getenv('HTTP_FORWARDED')) {
		$ipaddress = getenv('HTTP_FORWARDED');
	} else if (getenv('REMOTE_ADDR')) {
		$ipaddress = getenv('REMOTE_ADDR');
	} else {
		$ipaddress = 'UNKNOWN';
	}
	return $ipaddress;
}

/* ---- End Function to get the client IP address ----- */

function getLoginUserData()
{
	$CI = &get_instance();
	if ($CI->session->userdata['valid_user']) {
		$result = $CI->session->userdata['valid_user'];
	} elseif ($CI->session->userdata['valid_dealer']) {
		$result = $CI->session->userdata['valid_dealer'];
	} elseif ($CI->session->userdata['valid_admin']) {
		$result = $CI->session->userdata['valid_admin'];
	}
	return $result;
}

function get_img($path, $image, $multiple_img = FALSE)
{
	$path = trim($path);
	$image = trim($image);
	if ($multiple_img == TRUE) {
		if (!empty($image)) {
			$img = explode(',', $image);
			if (file_exists($path . $img[0])) {
				$data = base_url() . $path . $img[0];
			} else {
				$data = base_url() . NO_IMAGE;
			}
		} else {
			$data = base_url() . NO_IMAGE;
		}
	} else {
		if (!empty($image) && !empty($path)) {
			if (file_exists($path . $image)) {
				$data = base_url() . $path . $image;
			} else {
				$data = base_url() . NO_IMAGE;
			}
		} else {
			$data = base_url() . NO_IMAGE;
		}
	}
	return $data;
}

function getLoginUserType($userData)
{
	$CI = &get_instance();
	if ($CI->session->userdata['valid_dealer'] && $CI->uri->segment(1) == 'dealer') {
		$result = $CI->session->userdata['valid_dealer'];
	} elseif ($CI->session->userdata['valid_user'] && $CI->uri->segment(1) == 'user') {
		$result = $CI->session->userdata['valid_user'];
	} elseif ($CI->session->userdata['valid_admin'] && $CI->uri->segment(1) == 'admin') {
		$result = $CI->session->userdata['valid_admin'];
	}
	return $result['user_type'];
}

function getbaseURL($url = '')
{
	$CI = &get_instance();
	if ($CI->session->userdata['valid_dealer'] && $CI->uri->segment(1) == 'dealer') {
		return dealer_url($url);
	} elseif ($CI->session->userdata['valid_user'] && $CI->uri->segment(1) == 'user') {
		return user_url($url);
	} elseif ($CI->session->userdata['valid_admin'] && $CI->uri->segment(1) == 'admin') {
		return admin_url($url);
	} else {
		return base_url($url);
	}
}

function tz_date($date, $formate = 'Y-m-d H:i:s', $zone = null)
{
	return date($formate, strtotime($date));
}

function timezone_offset_string($offset)
{
	return sprintf("%s%02d:%02d", ($offset >= 0) ? '+' : '-', abs($offset / 3600), abs($offset % 3600));
}

function getTimezoneOfset($timeZone, $isString = FALSE)
{
	$offset = timezone_offset_get(new DateTimeZone($timeZone), new DateTime());
	if ($isString === TRUE) {
		$offset = timezone_offset_string($offset);
	}
	return $offset;
}

function getTimeZone()
{
	$CI = &get_instance();
	$settings = $CI->authlibrary->getStoreSetting();
	$settings = $settings[0];
	return ($settings['var_time_zone']) ? $settings['var_time_zone'] : "UTC";
}

function date_tz($format = 'Y-m-d H:i:s')
{
	return convert_date_from_utc(date('Y-m-d H:i:s'), getTimeZone(), $format);
}

function array_insert($array, $index, $val)
{
	$size = count($array); //because I am going to use this more than one time
	if (!is_int($index) || $index < 0 || $index > $size) {
		return -1;
	} else {
		$temp = array_slice($array, 0, $index);
		$temp[] = $val;
		return array_merge($temp, array_slice($array, $index, $size));
	}
}

function escapeJavaScriptText($string)
{
	return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
}

function addForeignKey($sourceTable, $sourceField, $targetTable, $targetField = 'id', $onDelete = 'RESTRICT', $onUpdate = 'RESTRICT')
{
	$CI = &get_instance();
	$dbName = $CI->db->database;

	$CI->db->query('ALTER TABLE ' . $dbName . '.`ps_' . $sourceTable . '` ADD INDEX `ps_' . $sourceTable . '_' . $sourceField . '` (`' . $sourceField . '`);');
//    echo $CI->db->last_query();
	$CI->db->query('ALTER TABLE `ps_' . $sourceTable . '` ADD CONSTRAINT `ps_' . $sourceTable . '_' . $sourceField . '` FOREIGN KEY (`' . $sourceField . '`) REFERENCES ' . $dbName . '.`ps_' . $targetTable . '`(`' . $targetField . '`) ON DELETE ' . $onDelete . ' ON UPDATE ' . $onUpdate . '; ');
//    echo $CI->db->last_query();
//    exit;
}

function dropForeignKey($sourceTable, $sourceField, $targetTable, $targetField = 'id')
{
	$CI = &get_instance();
	$CI->db->query('ALTER TABLE ps_' . $sourceTable . ' DROP FOREIGN KEY ps_' . $sourceTable . '_' . $sourceField . ';');
	$CI->db->query('ALTER TABLE ps_' . $sourceTable . ' DROP INDEX ps_' . $sourceTable . '_' . $sourceField . ';');
}

//ALTER TABLE ps_news DROP FOREIGN KEY ps_news_fk_category;

function convertNumber($number)
{
	return preg_replace('/[^\\d.]+/', '', $number);
}

function clientLable($clientLable)
{

	$client_status = json_decode(CLIENT_STATUS);

	if ($clientLable == "LOW") {
		$status = '<span class="label label-sm label-info ">' . $client_status->LOW . '</span>';
	} elseif ($clientLable == 'MEDIUM') {
		$status = '<span class="label label-sm label-success ">' . $client_status->MEDIUM . '</span>';
	} else {
		$status = '<span class="label label-sm label-danger ">' . $client_status->HIGH . '</span>';
	}

	return $status;
}

function callLable($callLable)
{

	$call_status = json_decode(CALL_STATUS);

	if ($callLable == "CALLS_SCHEDULED") {
		$status = '<span class="label label-sm label-info ">' . $call_status->CALLS_SCHEDULED . '</span>';
	} elseif ($callLable == 'CALLS_COMPLETED') {
		$status = '<span class="label label-sm label-success ">' . $call_status->CALLS_COMPLETED . '</span>';
	} elseif ($callLable == 'CALLS_CANCELLED') {
		$status = '<span class="label label-sm label-danger ">' . $call_status->CALLS_CANCELLED . '</span>';
	}

	return $status;
}

function projectLable($projectLable)
{

	$projectStatus = json_decode(PROJECT_STATUS);

	if ($projectLable == "WAITING") {
		$status = '<span class="label label-sm label-info">' . $projectStatus->WAITING . '</span>';
	} elseif ($projectLable == 'ACTIVE') {
		$status = '<span class="label label-sm label-success">' . $projectStatus->ACTIVE . '</span>';
	} else if ($projectLable == 'CLOSED') {
		$status = '<span class="label label-sm label-danger">' . $projectStatus->CLOSED . '</span>';
	} else if ($projectLable == 'PRIORITY') {
		$status = '<span class="label label-sm label-default">' . $projectStatus->PRIORITY . '</span>';
	} else if ($projectLable == 'SCHEDULING') {
		$status = '<span class="label label-sm label-warning">' . $projectStatus->SCHEDULING . '</span>';
	}

	return $status;
}

function getProjectStatus($status)
{
	$ProjectStatus = '';
	$projectStatus = json_decode(PROJECT_STATUS);
	foreach ($projectStatus as $key => $value) {
		if ($status == $key) {
			$ProjectStatus = $value;
		}
	}
	return $ProjectStatus;
}

function getShortlistedStatus($status)
{
	$ListedStatus = '';
	$shortListedStatus = json_decode(SHORTLISTED_STATUS);
	foreach ($shortListedStatus as $key => $value) {
		if ($status == $key) {
			$ListedStatus = $value;
		}
	}
	return $ListedStatus;
}

function age_diff($fromdate, $todate)
{
	if (trim($fromdate) != '0000-00-00' && trim($todate) != '0000-00-00' && date('Y-m-d', strtotime($fromdate)) <= date('Y-m-d', strtotime($todate))) {
		$datediff = (new DateTime(date('Y-m-d', strtotime($todate))))->diff((new DateTime(date('Y-m-d', strtotime($fromdate)))));

		if ($datediff->y == 0 && $datediff->m == 0) {
			if ($datediff->d > 1) {
				$result = $datediff->d . ' days';
			} else if ($datediff->d == 1 || $datediff->d == 0) {
				$result = '1 day';
			}
		} else if ($datediff->y == 0 && $datediff->m > 0) {
			if ($datediff->m > 1) {
				$result = $datediff->m . ' months';
			} else if ($datediff->m == 1 || $datediff->m == 0) {
				$result = '1 month';
			}
		} else {
			if ($datediff->y > 1) {
				$result = $datediff->y . ' years ';
			} else {
				$result = $datediff->y . ' year';
			}
		}
	} else {
		$result = '-';
	}
	return $result;
}

function time_ago($datetime, $todate, $seconds = FALSE)
{
	if (is_numeric($datetime)) {
		$timestamp = $datetime;
	} else {
		$timestamp = strtotime($datetime);
	}
	if (is_numeric($todate)) {
		$to_timestamp = $todate;
	} else {
		$to_timestamp = strtotime($todate);
	}
	$diff = $to_timestamp - $timestamp;

	$min = 60;
	$hour = 60 * 60;
	$day = 60 * 60 * 24;
	$month = $day * 30;
	$year = $month * 12;
	$timeago = '';

	if ($diff < 60) { //Under a min
		if ($seconds) {
			$timeago .= $diff . " seconds";
		}
	} elseif ($diff < $hour) { //Under an hour
		$timeago .= round($diff / $min) . " mins";
	} elseif ($diff < $day) { //Under a day
		$timeago .= round($diff / $hour) . " hours";
	} elseif ($diff < $month) { //Under a day
		$timeago .= round($diff / $day) . " days";
	} elseif ($diff < $year) { //Under a day
		$timeago .= round($diff / $month) . " months";
	} else {
		if (round($diff / $year) > 1) {
			$timeago .= round($diff / $year) . " years";
		} else {
			$timeago .= round($diff / $year) . " year";
		}
	}

	return $timeago;
}

function dateDifference($date_1, $date_2, $days = FALSE)
{
	$datetime1 = date_create($date_1);
	$datetime2 = date_create($date_2);
	$differenceFormat = '';
	$interval = date_diff($datetime1, $datetime2);

	if ($interval->y > 0) {
		if ($interval->y > 1) {
			$differenceFormat .= '%y years';
		} else {
			$differenceFormat .= '1 year';
		}
	}

	if ($interval->m > 0) {
		if ($interval->m > 1) {
			if ($interval->y > 0) {
				$differenceFormat .= ' and %m months';
			} else {
				$differenceFormat .= '%m months';
			}
		} else {
			$differenceFormat .= '1 month';
		}
	}
	if ($days) {
		if ($interval->d > 0) {
			if ($interval->d > 1) {
				if ($interval->m > 0) {
					$differenceFormat .= ' and %d days';
				} else {
					$differenceFormat .= '%d days';
				}
			} else {
				$differenceFormat .= '1 day';
			}
		}
	}

	return $interval->format($differenceFormat);
}

function getPastYears($startFromYear = FALSE, $cnt = 50)
{
	if ($startFromYear) {
		$cur_year = $startFromYear;
	} else {
		$cur_year = date('Y');
	}
	for ($i = 0; $i <= $cnt; $i++) {
		$years[] = $cur_year--;
	}
	return $years;
}

function getMonths($full = FALSE)
{

	$months = array(
		'01' => ($full) ? 'January' : 'Jan',
		'02' => ($full) ? 'February' : 'Feb',
		'03' => ($full) ? 'March' : 'Mar',
		'04' => ($full) ? 'April' : 'Apr',
		'05' => ($full) ? 'May' : 'May',
		'06' => ($full) ? 'June' : 'Jun',
		'07' => ($full) ? 'July' : 'Jul',
		'08' => ($full) ? 'August' : 'Aug',
		'09' => ($full) ? 'September' : 'Sep',
		'10' => ($full) ? 'Octomber' : 'Oct',
		'11' => ($full) ? 'November' : 'Nov',
		'12' => ($full) ? 'December' : 'Dec');

	return $months;
}

function sortMultiArray($arr, $k, $sort)
{
	$tmp = Array();
	foreach ($arr as &$ma)
		$tmp[] = &$ma[$k];
	$tmp = array_map('strtolower', $tmp);      // to sort case-insensitive
	array_multisort($tmp, $sort, $arr);
	return $arr;
}

function cust_base_url()
{
	if (APP_ENVIRONMENT == 'prod') {
		return '//' . $_SERVER['HTTP_HOST'] . '/';
	} else if (APP_ENVIRONMENT == 'local') {
		return '//' . $_SERVER['HTTP_HOST'] . '/prosapient/';
	} else if (APP_ENVIRONMENT == 'demo') {
		return '//' . $_SERVER['HTTP_HOST'] . '/prosapient/';
	} else if (APP_ENVIRONMENT == 'dev') {
		return '//' . $_SERVER['HTTP_HOST'] . '/prosapient/';
	}
}

function convert_date($datetime, $sourceTimeZone, $targetTimezone, $format = 'Y-m-d H:i:s')
{
//    echo "$datetime, $sourceTimeZone, $targetTimezone,";exit;
	if (empty($targetTimezone)) {
		$targetTimezone = "UTC";
	}
	$date = new \DateTime($datetime, new \DateTimeZone($sourceTimeZone));
	$date->setTimezone(new \DateTimeZone($targetTimezone));
	return $date->format($format);
}

function getTimeZoneChoice($selectedzone)
{
	$all = timezone_identifiers_list();

	$html = '';
	$html .= '<option value="0">Select Timezone</option>';
	for ($i = 0; $i < count($all); $i++) {
		$html .= '<option ' . (($selectedzone == $all[$i]) ? 'selected="selected "' : '') . ' value="' . $all[$i] . '">' . $all[$i] . '</option>';
	}
	return $html;

	$i = 0;
	foreach ($all AS $zone) {
		$zone = explode('/', $zone);
		$zonen[$i]['continent'] = isset($zone[0]) ? $zone[0] : '';
		$zonen[$i]['city'] = isset($zone[1]) ? $zone[1] : '';
		$zonen[$i]['subcity'] = isset($zone[2]) ? $zone[2] : '';
		$i++;
	}

	asort($zonen);

	$structure = '';
	foreach ($zonen AS $zone) {
		extract($zone);
		if ($continent == 'Africa' || $continent == 'America' || $continent == 'Antarctica' || $continent == 'Arctic' || $continent == 'Asia' || $continent == 'Atlantic' || $continent == 'Australia' || $continent == 'Europe' || $continent == 'Indian' || $continent == 'Pacific') {
			if (!isset($selectcontinent)) {
				$structure .= '<optgroup label="' . $continent . '">'; // continent
			} elseif ($selectcontinent != $continent) {
				$structure .= '</optgroup><optgroup label="' . $continent . '">'; // continent
			}

			if (isset($city) != '') {
				if (!empty($subcity) != '') {
					$city = $city . '/' . $subcity;
				}
				$structure .= "<option " . ((($continent . '/' . $city) == $selectedzone) ? 'selected="selected "' : '') . " value=\"" . ($continent . '/' . $city) . "\">" . str_replace('_', ' ', $city) . "</option>"; //Timezone
			} else {
				if (!empty($subcity) != '') {
					$city = $city . '/' . $subcity;
				}
				$structure .= "<option " . (($continent == $selectedzone) ? 'selected="selected "' : '') . " value=\"" . $continent . "\">" . $continent . "</option>"; //Timezone
			}

			$selectcontinent = $continent;
		}
	}
	$structure .= '</optgroup>';
	return $structure;
}

function getTimeZoneList()
{
	return timezone_identifiers_list();
}

function getExpertTimeZone($id)
{
	$CI = &get_instance();
	return $CI->db->get_where('experts', array('id' => $id))->row_array()['var_timezone'];
}

function getClientTimeZone($id)
{
	$CI = &get_instance();
	return $CI->db->get_where('client', array('id' => $id))->row_array()['var_timezone'];
}

function formatNumber($number, $countryCode = "uk")
{

	if ($number != '') {
		if ($countryCode == "uk") {
			return sprintf("%s %s %s %s",
				substr($number, 0, 3),
				substr($number, 3, 3),
				substr($number, 6, 3),
				substr($number, 9));
		} else if ($countryCode == "us") {

			return sprintf("%s %s %s %s",
				substr($number, 0, 2),
				substr($number, 2, 3),
				substr($number, 5, 3),
				substr($number, 8));
		}

	} else {
		return $number;
	}
}

function isFileExist($url)
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	// $retcode >= 400 -> not found, $retcode = 200, found.
	curl_close($ch);
	if ($responseCode == 200) {
		return true;
	} else {
		return false;
	}

}

function findJs($value, $module)
{
	$module = !empty($module) ? "{$module}/" : "";

	$url = base_url() . "public/front/js/{$module}{$value}?v=" . FRONT_REVISED_VERSION;
	if (!isFileExist($url)) {
		$url = base_url() . "public/assets/global/scripts/{$value}?v=" . FRONT_REVISED_VERSION;
	}
	return $url;
}

function findJsPlugins($value)
{
	$url = base_url() . "public/front/plugins/{$value}?v=" . FRONT_REVISED_VERSION;
	if (!isFileExist($url)) {
		$url = base_url() . "public/assets/global/plugins/{$value}?v=" . FRONT_REVISED_VERSION;
	}
	return $url;
}

function findCSS($value)
{
	$url = base_url() . "public/front/css/{$value}?v=" . FRONT_REVISED_VERSION;

	if (!isFileExist($url)) {
		$url = base_url() . "public/assets/global/css/{$value}?v=" . FRONT_REVISED_VERSION;
	}
	return $url;
}

function findCSSPlugins($value)
{
	$url = base_url() . "public/front/plugins/{$value}?v=" . FRONT_REVISED_VERSION;
	if (!isFileExist($url)) {
		$url = base_url() . "public/assets/global/plugins/{$value}?v=" . FRONT_REVISED_VERSION;
	}
	return $url;
}

function is_url_exist($url)
{
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_NOBODY, true);
	curl_exec($ch);
	$code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	if ($code == 200) {
		$status = true;
	} else {
		$status = false;
	}
	curl_close($ch);
	return $status;
}

function getProfilePic($loginType)
{
	$CI = &get_instance();


	switch ($loginType) {
		case 'expert':
			$imgFile = $CI->session->userdata['valid_expert']['var_profile_image'];
			$profilePicURL = base_url() . EXPERT_PROFILE_IMG . "/" . $imgFile;
			break;
	}
//    echo "enter".is_url_exist($profilePicURL);exit;
	if (!is_url_exist($profilePicURL)) {
		$profilePicURL = base_url() . "public/front/images/user.png";
	}

	return $profilePicURL;

}

function getProspectStatus($status)
{
	$ProspectStatus = '';
	$prospectStatus = json_decode(PROSPECT_STATUS);
	foreach ($prospectStatus as $key => $value) {
		if ($status == $key) {
			$ProspectStatus = $value;
		}
	}
	return $ProspectStatus;
}

function getProspectStatusImage($status)
{

	$prospectStatus = json_decode(PROSPECT_STATUS);
	$statusArray = array();
	if ($status == 'REACHED_OUT') {

		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation disabled"><i class="fa fa-comments-o"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification disabled"><i class="fa fa-graduation-cap"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'CONVERSATION') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-check-circle right tick"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification disabled"><i class="fa fa-graduation-cap"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'QUALIFICATION') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-check-circle right tick"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification active"><i class="fa fa-graduation-cap"></i><i class="fa fa-check-circle right tick"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'CONTRACT') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-check-circle right tick"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification active"><i class="fa fa-graduation-cap"></i><i class="fa fa-check-circle right tick"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract active"><i class="fa fa-wpforms" aria-hidden="true"></i><i class="fa fa-check-circle right tick"></i>Contract</span>',
		);
	} else if ($status == 'REACHED_OUT_REJECTED') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-times-circle right cross"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation disabled"><i class="fa fa-comments-o"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification disabled"><i class="fa fa-graduation-cap"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'CONVERSATION_REJECTED') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-times-circle right cross"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification disabled"><i class="fa fa-graduation-cap"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'QUALIFICATION_REJECTED') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-check-circle right tick"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification active"><i class="fa fa-graduation-cap"></i><i class="fa fa-times-circle right cross"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	} else if ($status == 'CONTRACT_REJECTED') {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached active"><i class="fa fa-envelope"></i><i class="fa fa-check-circle right tick"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation active"><i class="fa fa-comments-o"></i><i class="fa fa-check-circle right tick"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification active"><i class="fa fa-graduation-cap"></i><i class="fa fa-check-circle right tick"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract active"><i class="fa fa-wpforms" aria-hidden="true"></i><i class="fa fa-times-circle right cross"></i>Contract</span>',
		);
	} else {
		$statusArray = array(
			'REACHED_OUT' => '<span class="reached disabled"><i class="fa fa-envelope"></i>Reached Out</span>',
			'CONVERSATION' => '<span class="conversation disabled"><i class="fa fa-comments-o"></i>Conversation</span>',
			'QUALIFICATION' => '<span class="qualification disabled"><i class="fa fa-graduation-cap"></i>Qualification</span>',
			'CONTRACT' => '<span class="contract disabled"><i class="fa fa-wpforms" aria-hidden="true"></i>Contract</span>',
		);
	}

	return $statusArray;
}

function getExpertProfilePic($image)
{
	$profilePicURL = base_url() . "public/front/images/user.png";
	if (!empty($image)) {
		$profilePicURL = base_url() . EXPERT_PROFILE_IMG . "/" . $image;
	}
	return $profilePicURL;
}

function isMobile()
{
	if (WEBSITE_MODE == "mobile") {
		return true;
	} else {
		return false;
	}
}

function cost_revenue_calculations($calls_id)
{
	$CI = &get_instance();

	$CI->db->select('calls.*
        ,experts.int_units_charges as expert_units_charges
        ,experts.var_rate_per_hour as expert_rate_per_hour
        ,client.dec_unit_price as client_unit_price');
	$CI->db->from('calls');
	$CI->db->join('experts', 'experts.id = calls.fk_expert');
	$CI->db->join('client', 'client.id = calls.fk_client');
	$CI->db->where('calls.id', $calls_id);
	$CI->db->where('calls.enum_status', 'CALLS_COMPLETED');
	$callData = $CI->db->get()->row_array();
	$duration = $callData['int_duration'];

	$duration = ceil($duration / 60);

	$roundHours = ceil($duration / 60);
	$exeprtHourlyRate = $callData['expert_rate_per_hour'];
	$clientUnitPrice = $callData['client_unit_price'];
	$expertUnitCharges = $callData['expert_units_charges'];

	$cost = $duration / 60 * $exeprtHourlyRate;
	$revenue = $roundHours * $expertUnitCharges * $clientUnitPrice;


	return array('cost' => $cost, 'revenue' => $revenue);
}

function generateInvoiceNumber($expertId)
{
	$CI = &get_instance();
	$expertData = $CI->db->where('id', $expertId)->get('experts')->row_array();
	if ($expertData['var_fname'] && $expertData['var_lname']) {
		$tempinvoiceData = $CI->db->select('var_invoice')->get('invoice')->result_array();
		$invoiceData = array_column($tempinvoiceData, 'var_invoice');
		$cnt = 1;
		for ($i = 0; $i < $cnt; $i++) {
			$generateCode = strtoupper(substr($expertData['var_fname'], 0, 2)) . strtoupper(substr($expertData['var_lname'], 0, 2)) . randomNumber(5);
			if (in_array($generateCode, $invoiceData)) {
				$cnt++;
			}
		}
	}
	return $generateCode;
}

function randomNumber($length)
{
	$result = '';

	for ($i = 0; $i < $length; $i++) {
		$result .= mt_rand(0, 9);
	}

	return $result;

}

function getUsNumber($ukNumber)
{
	$CI = &get_instance();

	$clientConfirmation = $CI->db->get_where('ps_call_maps', array('var_uk_number' => $ukNumber))->row_array();
//        print_r($clientConfirmation);exit;
	return $clientConfirmation['var_us_number'];

}
function print_array($arr){
    echo '<pre/>';
    print_r($arr);
    echo '</pre>';
    exit;
}


function autoGeneratePassword()
{
	$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
	$auto_pass = '';
	for ($i = 0; $i < 7; $i++) {
		$auto_pass .= $characters[mt_rand(0, 61)];
	}
	return $auto_pass;
}


function get_call_extra_data($userId, $expertId, $projectId)
{
	$CI = &get_instance();


	$CI->db->select('
					accent.var_accent_code as user_accent_code
					');
	$CI->db->from('user');
	$CI->db->join('accent', 'accent.id = user.fk_accent',"left");
	$CI->db->where('user.id', $userId);
	$extra_call_Data_user = $CI->db->get()->row_array();

	$CI->db->select('
					experts.var_phone as expert_contact_no,
					accent.var_accent_code as expert_accent_code,
					CONCAT(ps_experts.var_fname," ",ps_experts.var_lname) as expertname,
					');
	$CI->db->from('experts');
	$CI->db->where('experts.id', $expertId);
	$CI->db->join('accent', 'accent.id = experts.fk_accent',"left");
	$extra_call_Data_export = $CI->db->get()->row_array();

    $CI->db->select('
					client_has_project.var_target_company,
					client_has_project.var_companies_interest,
					client_has_project.var_industry,
					client.var_company_name,
					CONCAT(ps_user.var_fname," ",ps_user.var_lname) as username,
					');
    $CI->db->from('client_has_project');
    $CI->db->where('client_has_project.id', $projectId);
    $CI->db->join('client', 'client.id = client_has_project.fk_client');
    $CI->db->join('user', 'user.id = client_has_project.fk_user');
    $data = $CI->db->get()->row_array();

    $CI->db->select('
					CONCAT(ps_experts.var_fname," ",ps_experts.var_lname) as expertname,
					');
    $CI->db->from('experts');
    $CI->db->where('experts.id', $expertId);
    $data1 = $CI->db->get()->row_array();

	$expert_number = $extra_call_Data_export['expert_contact_no'];
	$expert_accent = $extra_call_Data_export['expert_accent_code'];
	$user_accent = $extra_call_Data_user['user_accent_code'];
    $finalArray = array($data['var_company_name'],$data['username'],$data1['expertname'],$data['var_target_company'],$data['var_companies_interest'],$data['var_industry']);
    $filter=array_filter($finalArray);
    $speechcontext = implode(",", $filter);

	return array(
	    'expert_number' => $expert_number,
        'expert_accent' => $expert_accent,
        'user_accent' => $user_accent,
        'speech_context' => $speechcontext
    );
}

function checkExpertComplianceTraining($id){
	$CI = &get_instance();
	$expertData = $CI->db->select('dt_compliance_date')->get_where('experts', array('id' => $id))->row_array();
	if(empty($expertData['dt_compliance_date'])||$expertData['dt_compliance_date'] == ''){
		$flag = true;
	}
	if (strtotime($expertData['dt_compliance_date']) < strtotime('-1 Year')) {
		$flag = true;
	} else {
		$flag = false;
	}
	return $flag;
}

function generateRandomString()  {
    $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$@';
    $digits = '1234567890';
    $randomString = '';
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $letters[rand(0, strlen($letters) - 1)];
    }
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $digits[rand(0, strlen($digits) - 1)];
    }
    if(checkNumerExist($randomString)){
        generateRandomString();
    }
    return $randomString;
}

function checkNumerExist($randomNum){
    $CI = &get_instance();
    $count = $CI->db->get_where('stores',array('var_store_code',$randomNum))->num_rows();
    if($count > 0){
        return true;
    }else{
        return false;
    }
}

function generateRandomBussinessString()  {
    $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz$@';
    $digits = '1234567890';
    $randomString = '';
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $letters[rand(0, strlen($letters) - 1)];
    }
    for ($i = 0; $i < 3; $i++) {
        $randomString .= $digits[rand(0, strlen($digits) - 1)];
    }
    if(checkBussinessNumerExist($randomString)){
        generateRandomString();
    }
    return $randomString;
}

function checkBussinessNumerExist($randomNum){
    $CI = &get_instance();
    $count = $CI->db->get_where('bussiness',array('var_bussiness_code',$randomNum))->num_rows();
    if($count > 0){
        return true;
    }else{
        return false;
    }
}

function getStoreIdByStoreCode($storecode){
    $CI = &get_instance();
    return $CI->db->get_where('stores',array('var_store_code',$storecode))->row_array()['id'];
}

function getStoreDataByStoreCode($storecode){
    $CI = &get_instance();
    return $CI->db->get_where('stores',array('var_store_code'=>$storecode))->row_array();
}



function getServiceByBookingId($bookId){
    $CI = &get_instance();
//    $CI->db->select('services.var_title');
    $CI->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'services.var_title) as services');
    $CI->db->join('booking_has_service','booking_has_service.fk_service = services.id');
    $CI->db->join('booking','booking.id = booking_has_service.fk_booking');
//    $CI->db->group_by('booking.id');
    $CI->db->where('booking.id',$bookId);
    return $CI->db->get('services')->row_array()['services'];
}
function getServiceIdByBookingId($bookId,$storeId){
    $CI = &get_instance();
//    $CI->db->select('services.var_title');
    $CI->db->select('services.id,
                    store_has_services.*,
                    services.var_title as service_title,
                    master_category.id as catId,
                    master_category.var_name as category_name,
                    master_category.enum_type as category_type');
    $CI->db->from('services');
    $CI->db->join('store_has_services','store_has_services.fk_service = services.id AND fk_store ='.$storeId);
    $CI->db->join('master_category','master_category.id = services.fk_category');
    $CI->db->join('booking_has_service','booking_has_service.fk_service = services.id');
    $CI->db->join('booking','booking.id = booking_has_service.fk_booking');
//    $CI->db->group_by('booking.id');
    $CI->db->where('booking.id',$bookId);
    return $CI->db->get()->result_array();
}

function getTimeSlotByBookingId($bookId){
    $CI = &get_instance();
    $CI->db->select('timeslots.slot_time');
    $CI->db->join('booking_has_timeslot','booking_has_timeslot.fk_timeslot = timeslots.id');
    $CI->db->join('booking','booking.id = booking_has_timeslot.fk_booking');
    $CI->db->where('booking.id',$bookId);
    $result = $CI->db->get('timeslots')->result_array();
    $startTime = $result[0]['slot_time'];
    $EndTime = $result[count($result) - 1]['slot_time'];
    return $startTime.' - '.$EndTime;
}

function getTimeByBookingId($bookId){
    $CI = &get_instance();
//    $CI->db->select('services.var_title');
    $CI->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'timeslots.slot_time) as timeslot');
    $CI->db->join('booking_has_timeslot','booking_has_timeslot.fk_timeslot = timeslots.id');
    $CI->db->join('booking','booking.id = booking_has_timeslot.fk_booking');
//    $CI->db->group_by('booking.id');
    $CI->db->where('booking.id',$bookId);
    return $CI->db->get('timeslots')->row_array()['timeslot'];
}

function getServiceForPaymentByBookId($bookId){
    $CI = &get_instance();
//    $CI->db->select('services.var_title');
    $CI->db->select('services.id,
                    store_has_services.*,
                    services.var_title as service_title,
                    master_category.id as catId,
                    master_category.var_name as category_name,
                    master_category.enum_type as category_type');
    $CI->db->from('services');
    $CI->db->join('store_has_services','store_has_services.fk_service = services.id');
    $CI->db->join('master_category','master_category.id = services.fk_category');
    $CI->db->join('booking_has_service','booking_has_service.fk_service = services.id');
    $CI->db->join('booking','booking.id = booking_has_service.fk_booking');
//    $CI->db->group_by('booking.id');
    $CI->db->where('booking.id',$bookId);
    return $CI->db->get()->result_array();
}


function getDayNameFromNumber($day) {
    switch ($day) {
        case 0:
            $dayName = "Sunday";
            break;
        case 1:
            $dayName = "Monday";
            break;
        case 2:
            $dayName = "Tuesday";
            break;
        case 3:
            $dayName = "Wednesday";
            break;
        case 4:
            $dayName = "Thursday";
            break;
        case 5:
            $dayName = "Friday";
            break;
        case 6:
            $dayName = "Saturday";
    }
    return $dayName;
}

function   getNumberFromDayName($dayname) {
    switch ($dayname) {
        case 'Sunday':
            $dayNum = 0;
            break;
        case 'Monday':
            $dayNum = 1;
            break;
        case 'Tuesday':
            $dayNum = 2;
            break;
        case 'Wednesday':
            $dayNum = 3;
            break;
        case 'Thursday':
            $dayNum = 4;
            break;
        case 'Friday':
            $dayNum = 5;
            break;
        case 'Saturday':
            $dayNum = 6;
    }
    return $dayNum;
}

function getAllWeekDays(){
    return array(
      '0'=>'Sunday',
      '1'=>'Monday',
      '2'=>'Tuesday',
      '3'=>'Wednesday',
      '4'=>'Thursday',
      '5'=>'Friday',
      '6'=>'Saturday',
    );
}

function getCategoryFromBussiness($bussinessId){
    $CI = &get_instance();
    $CI->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'bussiness_category.var_name) as category');
    $CI->db->join('bussiness_has_category','bussiness_has_category.fk_category = bussiness_category.id');
    $CI->db->join('bussiness','bussiness.id = bussiness_has_category.fk_bussiness');
    $CI->db->where('bussiness.id',$bussinessId);
    return $CI->db->get('bussiness_category')->row_array()['category'];
}

function getPackageService($packageId){
    $CI = &get_instance();
    $CI->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'services.var_title) as services');
    $CI->db->from('services');
    $CI->db->join('package_has_service', 'package_has_service.fk_service = services.id');
    $CI->db->where('package_has_service.fk_package', $packageId);
    return $CI->db->get()->row_array()['services'];
    // echo $CI->db->last_query(); exit;
}

function getOfferService($offerId){
    $CI = &get_instance();
    $CI->db->select('GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'services.var_title) as services');
    $CI->db->from('services');
    $CI->db->join('offer_has_service', 'offer_has_service.fk_service = services.id');
    $CI->db->where('offer_has_service.fk_offer', $offerId);
    return $CI->db->get()->row_array()['services'];
    // echo $CI->db->last_query(); exit;
}

function getOfferServicePrice($offerId,$storeId){
    $CI = &get_instance();
    $CI->db->select('fk_service');
    $CI->db->from('offer_has_service');
    $CI->db->where('offer_has_service.fk_offer', $offerId);
    $fkservices = $CI->db->get()->result_array();
    $total = 0;
    for($i=0;$i<count($fkservices);$i++){
        $total += $CI->db->get_where('store_has_services',array('fk_store'=>$storeId,'fk_service'=>$fkservices[$i]['fk_service']))->row_array()['var_price'];
    }
    return $total;
}

function getStoreTotalServices($storeId){
    $CI = &get_instance();
    $CI->db->select('count('.TABLE_PREFIX.'store_has_services.id) as noofservices');
    $CI->db->from('store_has_services');
    $CI->db->where('store_has_services.fk_store', $storeId);
    return $CI->db->get()->row_array()['noofservices'];
}
function getStoreTotalCustomer($storeId){
    $CI = &get_instance();
    $CI->db->select('count(DISTINCT ('.TABLE_PREFIX.'booking.fk_user)) as noofcustomer');
    $CI->db->from('booking');
    $CI->db->where('booking.fk_store', $storeId);
    return $CI->db->get()->row_array()['noofcustomer'];
}
function getCountOfUserBooking($userId){
    $CI = &get_instance();
    $CI->db->select('count('.TABLE_PREFIX.'booking.id) as noofbooking');
    $CI->db->from('booking');
    $CI->db->where('booking.fk_user', $userId);
    return $CI->db->get()->row_array()['noofbooking'];
}

function getTotalBookingPrice($userId){
    $CI = &get_instance();
    $CI->db->select('SUM('.TABLE_PREFIX.'booking.var_price) as bookprice');
    $CI->db->from('booking');
    $CI->db->where('booking.fk_user', $userId);
    return $CI->db->get()->row_array()['bookprice'];
}

function getCountOfUserStore($userId){
    $CI = &get_instance();
    $CI->db->select('count(DISTINCT ('.TABLE_PREFIX.'booking.fk_store)) as noofuser');
    $CI->db->from('booking');
    $CI->db->where('booking.fk_user', $userId);
    return $CI->db->get()->row_array()['noofuser'];
}

function getBussDataByBussCode($bussCode){
    $CI = &get_instance();
    return $CI->db->get_where('bussiness',array('var_bussiness_code'=>$bussCode))->row_array();
}

function getCountTotalStaff($storeId){
    $CI = &get_instance();
    $CI->db->select('count('.TABLE_PREFIX.'store_has_staff.id) as countofstaff');
    $CI->db->from('store_has_staff');
    $CI->db->where('store_has_staff.fk_store', $storeId);
    return $CI->db->get()->row_array()['countofstaff'];
}
function getStoreMainImage($storeId){
    $CI = &get_instance();
    $CI->db->select('store_has_gallery.var_file as var_logo');
    $CI->db->from('store_has_gallery');
    $CI->db->where('store_has_gallery.fk_store', $storeId);
    $CI->db->where('store_has_gallery.enum_isProfile', 'YES');
    return $CI->db->get()->row_array()['var_logo'];
}


