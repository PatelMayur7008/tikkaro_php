<?php

function clientData($data) {

    $client = $data['var_company_name'];
    $client_email = $data['var_email'];
    $client_id = $data['id'];
    $acMnagerName = $data['account_manager'];
    $team = $data['team_name'];
    $html = '<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Client Name:</b></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $client . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Key Account Manager:</b></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $acMnagerName . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Team:</b></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $team . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div><div class="clearfix"></div>';

    return $html;
}


function staffData($data) {

	$var_name = $data['var_name'];
//	$client_email = $data['var_email'];
//	$client_id = $data['id'];
//	$acMnagerName = $data['account_manager'];
//	$team = $data['team_name'];
	$html = '<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Staff Name:</b></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $var_name . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                </div>
                <div class="clearfix"></div>';

	return $html;
}

function projectData($projectData) {
    $client = $projectData['var_company_name'];
    $user = $projectData['user_fname'] . ' ' . $projectData['user_lname'];
    $account_manager = $projectData['account_manager'];
    $team = $projectData['team_name'];
    $status = $projectData['client_status'];
    $client_status = json_decode(CLIENT_STATUS);
    if ($status == "LOW") {
        $status = '<label class="control-label label label-sm label-info ">' . $client_status->LOW . '</label>';
    } elseif ($status == 'MEDIUM') {
        $status = '<label class="control-label label label-sm label-success ">' . $client_status->MEDIUM . '</label>';
    } else {
        $status = '<label class="control-label label label-danger ">' . $client_status->HIGH . '</label>';
    }
    $project = $projectData['var_project_name'];
    $html = '<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Project Name:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label projectName">' . $project . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Team:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $team . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Client:</strong></label>
                            <div class="col-md-7">
                                <a href="'.admin_url().'client/overview/'.$projectData['fk_client'].'"> ' . $client . ' </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Account Manager:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $account_manager . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>User:</strong></label>
                            <div class="col-md-7">
                                <a href="'.admin_url().'client/user-bio/'.$projectData['fk_user'].'">' . $user . '</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Client Value:</strong></label>
                            <div class="col-md-7">
                                ' . $status . '
                            </div>
                            <div class="clearfix"></div>
                        </div>
                </div>
                <div class="clearfix"></div>';

    return $html;
}

function expertData($expertDataArray) {
    $expertName = $expertDataArray['var_fname'] . ' ' . $expertDataArray['var_lname'];
    $company = $expertDataArray['company'];
    $jobTitle = $expertDataArray['var_position'];
    $areaExpert = $expertDataArray['area_of_expert'];
    $html = '<div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Name:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $expertName . ' </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><strong>Company:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label head-company-name">' . $company . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><strong>Job Title:</strong></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $jobTitle . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><strong>Areas of Expertise :</strong></label>
                            <div class="col-md-7">
                                <label class="control-label">' . $areaExpert . '</label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>';
    return $html;
}
