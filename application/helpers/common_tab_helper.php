<?php

function userMenu($activeClass, $userId)
{
    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    } else if ($activeClass == 'store') {
        $acivestore = 'class="active"';
    } else if ($activeClass == 'booking') {
        $aciveBooking = 'class="active"';
    } else if ($activeClass == 'review') {
        $acivereview = 'class="active"';
    }


    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url() . 'user/overview/' . $userId . '"> Overview </a>
                </li>
                <li ' . $acivestore . '>
                    <a href="' . admin_url() . 'user/store/' . $userId . '"> Stores </a>
                </li>
                <li ' . $aciveBooking . '>
                    <a href="' . admin_url() . 'user/booking/' . $userId . '"> Booking </a>
                </li>
                <li ' . $acivereview . '>
                    <a href="' . admin_url() . 'user/review/' . $userId . '"> Review </a>
                </li>
            </ul>';

    return $html;
}

function serviceMenu($activeClass, $serviceId)
{
    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    }


    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url() . 'service/overview/' . $serviceId . '"> Overview </a>
                </li>
            </ul>';

    return $html;
}

function storeMenu($activeClass, $storeId)
{
    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    } else if ($activeClass == 'service') {
        $aciveservice = 'class="active"';
    } else if ($activeClass == 'timeslot') {
        $acivetimeslot = 'class="active"';
    } else if ($activeClass == 'transaction') {
        $acivetransaction = 'class="active"';
    } else if ($activeClass == 'timing') {
        $acivetiming = 'class="active"';
    } else if ($activeClass == 'staff') {
        $acivestaff = 'class="active"';
    } else if ($activeClass == 'bankdetail') {
        $acivebank = 'class="active"';
    } else if ($activeClass == 'customer') {
        $acivecustomer = 'class="active"';
    } else if ($activeClass == 'holiday') {
        $activeholiday = 'class="active"';
    } else if ($activeClass == 'booking') {
        $activebooking = 'class="active"';
    } else if($activeClass == 'review'){
        $activereview = 'class="active"';
    } else if($activeClass == 'package'){
        $activepack = 'class="active"';
    } else if($activeClass == 'offer'){
        $activeoffer = 'class="active"';
    } else if($activeClass == 'gallery'){
        $activegallery = 'class="active"';
    } else if($activeClass == 'portfolio'){
        $activeportfolio = 'class="active"';
    }


    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url() . 'stores/overview/' . $storeId . '"> Overview </a>
                </li>
                 <li ' . $aciveservice . '>
                    <a href="' . admin_url() . 'stores/services/' . $storeId . '"> services </a>
                </li>
                <li ' . $acivetiming . '>
                    <a href="' . admin_url() . 'stores/timing/' . $storeId . '"> timing </a>
                </li>
                </li>
                 <li ' . $acivestaff . '>
                    <a href="' . admin_url() . 'stores/staff/' . $storeId . '"> staff </a>
                </li>
                <li ' . $acivebank . '>
                    <a href="' . admin_url() . 'stores/bankdetail/' . $storeId . '"> bank detail </a>
                </li>
                <li ' . $activepack . '>
                    <a href="' . admin_url() . 'stores/package/' . $storeId . '"> package </a>
                </li>
                <li ' . $activeoffer . '>
                    <a href="' . admin_url() . 'stores/offer/' . $storeId . '"> offer </a>
                </li> 
                <li ' . $activegallery . '>
                    <a href="' . admin_url() . 'stores/gallery/' . $storeId . '"> gallery </a>
                </li>
                <li ' . $activeportfolio . '>
                    <a href="' . admin_url() . 'stores/portfolio/' . $storeId . '"> portfolio </a>
                </li>
                <li ' . $acivecustomer . '>
                    <a href="' . admin_url() . 'stores/customer/' . $storeId . '"> customer </a>
                </li>
                 <li ' . $activeholiday . '>
                    <a href="' . admin_url() . 'stores/holiday_manager/' . $storeId . '"> holiday </a>
                </li>
                <li ' . $activebooking . '>
                    <a href="' . admin_url() . 'stores/booking/' . $storeId . '"> booking </a>
                </li>
                <!-- <li ' . $acivetimeslot . '>
                    <a href="' . admin_url() . 'stores/time_slot/' . $storeId . '"> time-slot </a>
                </li>-->
                 <li ' . $acivetransaction . '>
                    <a href="' . admin_url() . 'stores/transactions/' . $storeId . '"> transactions </a>
                </li> 
                <li ' . $activereview . '>
                    <a href="' . admin_url() . 'stores/review/' . $storeId . '"> review </a>
                </li>
            </ul>';

    return $html;
}


function staffMenu($activeClass, $staffId)
{
    if ($activeClass == 'statistics') {
        $aciveOverview = 'class="active"';
    }


    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url() . 'client/overview/' . $staffId . '"> Statistics </a>
                </li>
       
            </ul>';

    return $html;
}


function clientUserMenu($activeClass, $userid)
{
    if ($activeClass == 'userbio') {
        $aciveUserBio = 'class="active"';
    } else if ($activeClass == 'usernotes') {
        $aciveUserNotes = 'class="active"';
    } else if ($activeClass == 'callhistory') {
        $aciveCalls = 'class="active"';
    } else if ($activeClass == 'userproject') {
        $aciveProject = 'class="active"';
    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveUserBio . '>
                    <a href="' . admin_url() . 'client/user-bio/' . $userid . '"> User Bio </a>
                </li>
                <li ' . $aciveUserNotes . '>
                    <a href="' . admin_url() . 'client/user-notes/' . $userid . '"> User Notes </a>
                </li>
               
            </ul>';

    return $html;
}

function bussinessMenu($activeClass, $bussinessId)
{

    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    }
//    else if ($activeClass == 'shortlist') {
//        $aciveShortlist = 'class="active"';
//    } else if ($activeClass == 'notes') {
//        $aciveNotes = 'class="active"';
//    } else if ($activeClass == 'prospects') {
//        $aciveProspect = 'class="active"';
//    } else if ($activeClass == 'calls') {
//        $aciveCalls = 'class="active"';
//    } else if ($activeClass == 'rules') {
//        $aciveRules = 'class="active"';
//    } else if ($activeClass == 'projectUser') {
//        $aciveProjectUser = 'class="active"';
//    } else if ($activeClass == 'interest') {
//        $aciveInterest = 'class="active"';
//    } else if ($activeClass == 'email') {
//        $aciveEmail = 'class="active"';
//    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url('bussiness/overview/' . $bussinessId) . '"> Overview </a>
                </li>
                
            </ul>';

    return $html;
}

function facilityMenu($activeClass, $facilityId)
{

    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url('facility/overview/' . $facilityId) . '"> Overview </a>
                </li>
                
            </ul>';

    return $html;
}

function categoryMenu($activeClass, $categoryId)
{

    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url('category/overview/' . $categoryId) . '"> Overview </a>
                </li>
                
            </ul>';

    return $html;
}


function expertsMenu($activeClass, $expertId, $getExperts, $email)
{
    if ($activeClass == 'bio') {
        $aciveBio = 'class="active"';
    } else if ($activeClass == 'email') {
        $aciveEmail = 'class="active"';
    } else if ($activeClass == 'notes') {
        $aciveNotes = 'class="active"';
    } else if ($activeClass == 'projects') {
        $aciveProject = 'class="active"';
    } else if ($activeClass == 'addtoshortlist') {
        $aciveAddtoshortlist = 'class="active"';
    } else if ($activeClass == 'call_history') {
        $aciveCallHistory = 'class="active"';
    }
    if (!empty($getExperts)) {
        $backtosearch = admin_url() . 'find-experts/?clientId=' . $getExperts['clientId'] . '&projectId=' . $getExperts['projectId'] . '&expertsName=' . $getExperts['expertsName'] . '&position=' . $getExperts['position'] . '&geoGraphy=' . $getExperts['geoGraphy'] . '&expretise=' . $getExperts['expretise'] . '&currentCompany=' . $getExperts['currentCompany'] . '&formerCompany=' . $getExperts['formerCompany'] . '';
        $search = '<li ' . $aciveAddtoshortlist . '>
                    <a href="' . $backtosearch . '"> Back to Search </a>
                </li>';

        $addtoShorlist = '<li ' . $aciveAddtoshortlist . '>
                    <a href="javascript:;"> Add to Shortlist </a>
                </li>';
        $biourl = admin_url() . 'experts/bio/' . $expertId . '?clientId=' . $getExperts['clientId'] . '&projectId=' . $getExperts['projectId'] . '&expertsName=' . $getExperts['expertsName'] . '&position=' . $getExperts['position'] . '&geoGraphy=' . $getExperts['geoGraphy'] . '&expretise=' . $getExperts['expretise'] . '&currentCompany=' . $getExperts['currentCompany'] . '&formerCompany=' . $getExperts['formerCompany'] . '';
        $projecturl = admin_url() . 'experts/projects/' . $expertId . '?clientId=' . $getExperts['clientId'] . '&projectId=' . $getExperts['projectId'] . '&expertsName=' . $getExperts['expertsName'] . '&position=' . $getExperts['position'] . '&geoGraphy=' . $getExperts['geoGraphy'] . '&expretise=' . $getExperts['expretise'] . '&currentCompany=' . $getExperts['currentCompany'] . '&formerCompany=' . $getExperts['formerCompany'] . '';
        $noteurl = admin_url() . 'experts/notes/' . $expertId . '?clientId=' . $getExperts['clientId'] . '&projectId=' . $getExperts['projectId'] . '&expertsName=' . $getExperts['expertsName'] . '&position=' . $getExperts['position'] . '&geoGraphy=' . $getExperts['geoGraphy'] . '&expretise=' . $getExperts['expretise'] . '&currentCompany=' . $getExperts['currentCompany'] . '&formerCompany=' . $getExperts['formerCompany'] . '';
    } else {
        $biourl = admin_url() . 'experts/bio/' . $expertId . '';
        $projecturl = admin_url() . 'experts/projects/' . $expertId . '';
        $noteurl = admin_url() . 'experts/notes/' . $expertId . '';
    }

    if ($email != '') {
        $mailExperts = '<li ' . $aciveEmail . '>
                    <a href="mailto:' . $email . '"> Email </a>
                </li>';
    }
    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveBio . '>
                    <a href="' . $biourl . '"> Bio </a>
                </li>
                ' . $mailExperts . '
                <li ' . $aciveProject . '>
                    <a href="' . $projecturl . '"> Projects </a>
                </li>
                <li ' . $aciveNotes . '>
                    <a href="' . $noteurl . '"> Notes </a>
                </li>
                 ' . $addtoShorlist . '
                 <li ' . $aciveCallHistory . '>
                   <a href="' . admin_url() . 'experts/call-history/' . $expertId . '"> Call History </a>
                </li>
                ' . $search . '
            </ul>';

    return $html;
}


