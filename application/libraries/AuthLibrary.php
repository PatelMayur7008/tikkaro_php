<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class AuthLibrary {

    var $ci;
    var $userData = null;
    var $loginUserId = null;

    public function __construct() {
        $this->ci = &get_instance();
        $this->userData = $this->ci->session->userdata;
//        $this->loginUserId = ($this->loginUserId == null) ? $this->getLoginUserId() : $this->loginUserId;
    }

    public function refreshData() {
        $this->userData = $this->ci->session->userdata;
    }

    public function setLoginUserId($id) {
        $this->loginUserId = $id;
    }

    public function resetLoginUserId() {
        $this->loginUserId = $this->getLoginUserId();
    }

    public function getLoginUserData() {
        return $this->userData['valid_login_user'];
    }

    public function getLoginUserType() {
        return $this->getLoginUserData()['user_type'];
    }

    public function getLoginUserId() {
        return $this->getLoginUserData()['id'];
    }

    public function getLoginAuthorId() {
        if ($this->getLoginUserType() == 'AUTHOR') {
            return $this->getLoginUserData()['id'];
        } else {
            return NULL;
        }
    }

    public function getLoginAdminId() {
        if ($this->getLoginUserType() == 'ADMIN') {
            return $this->getLoginUserData()['id'];
        } else {
            return NULL;
        }
    }

    public function getModuleAccess() {
        return $this->ci->db->where(['enum_enable' => 'YES', 'enum_access' => 'YES'])->get('bs_modules')->result_array();
    }

    public function loginAttempt($email, $password) {

        $this->ci->db->select('admin.*');
        $this->ci->db->from('admin');
        $this->ci->db->where("admin.var_email", $email);
        $this->ci->db->where('admin.enum_enable', 'YES');
        $row = $this->ci->db->get()->row_array();
        $finalData = [];

        if ((!empty($row))) {
            if ($row['var_password'] == md5($password)) {
                $row['user_type'] = 'ADMIN';
                $finalData = $row;
            }
        } else {
            $this->ci->db->select('account_manager.*');
            $this->ci->db->from('account_manager');
            $this->ci->db->where("account_manager.var_email", $email);
            $this->ci->db->where('account_manager.enum_enable', 'YES');
            $row = $this->ci->db->get()->row_array();
            if ((!empty($row))) {
                if ($row['var_password'] == md5($password)) {
                    $row['user_type'] = 'KEY_ACCOUNT_MANAGER';
                    $finalData = $row;
                }
            }
        }
        if (!empty($finalData) && $finalData['enum_enable'] == 'YES') {
            if ($finalData['user_type'] == 'ADMIN') {
                $sessionData['valid_login_user'] = [
                    'id' => $row['id'],
                    'username' => $finalData['var_email'],
                    'name' => $finalData['var_name'],
                    'email' => $finalData['var_email'],
                    'enum_enable' => $finalData['enum_enable'],
                    'user_type' => 'ADMIN'
                ];
            } else if ($finalData['user_type'] == 'KEY_ACCOUNT_MANAGER') {
                $sessionData['valid_login_user'] = [
                    'id' => $row['id'],
                    'username' => $finalData['var_email'],
                    'name' => $finalData['var_name'],
                    'first_name' => $finalData['var_name'],
                    'last_name' => '',
                    'email' => $finalData['var_email'],
                    'enum_enable' => $finalData['enum_enable'],
                    'user_type' => 'KEY_ACCOUNT_MANAGER'
                ];
            }

            $this->ci->session->set_userdata($sessionData);
            return $sessionData['valid_login_user'];
        }
        return $finalData;
    }

}

/* End of file AuthLibrary.php */