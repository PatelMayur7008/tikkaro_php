<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_dec_unit_price_field_in_client_table extends CI_Migration
{

    public $table = 'client';

    public function up()
    {
        $fields = array(
            'dec_unit_price' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_timezone');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'dec_unit_price');
    }

}
