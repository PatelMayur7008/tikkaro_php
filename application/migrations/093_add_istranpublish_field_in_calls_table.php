<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_istranpublish_field_in_calls_table extends  CI_Migration
{
	public $table = 'calls';

	public function up()
	{
        $fields = array(
            'en_istranpublish' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
        );
		$this->dbforge->add_column($this->table, $fields, 'en_isGenrated');
        $data = array(
            'en_istranpublish' => "YEs"
        );
        $this->db->update("calls",$data);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'en_istranpublish');
	}
}
