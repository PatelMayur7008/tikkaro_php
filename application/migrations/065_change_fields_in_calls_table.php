<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_fields_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {

        $fields = array(
            'fk_project_has_experts' => array(
                'name' => 'fk_project_has_experts',
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE,
            ),
            'fk_project_has_client_has_availability' => array(
                'name' => 'fk_project_has_client_has_availability',
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE,
            ),
        );

        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'fk_project_has_experts' => array(
                'name' => 'fk_project_has_experts',
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE,
            ),
            'fk_project_has_client_has_availability' => array(
                'name' => 'fk_project_has_client_has_availability',
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE,
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
