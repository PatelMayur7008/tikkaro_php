<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_accent_table extends CI_Migration
{
	public $table = 'accent';

	public function up()
	{
		$this->dbforge->add_field(array(

			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			),
			'var_accent_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'var_accent_code' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'created_at' => array(
				'type' => 'datetime',
			),
			'updated_at' => array(
				'type' => 'timestamp'
			),

		));

		$this->dbforge->add_key('id', TRUE);
		$attributes = array('ENGINE' => 'InnoDB');
		$this->dbforge->create_table($this->table, TRUE, $attributes);

		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Australia)',
				'var_accent_code' => 'en-AU',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Canada)',
				'var_accent_code' => 'en-CA',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Ghana)',
				'var_accent_code' => 'en-GH',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Great Britain)',
				'var_accent_code' => 'en-GB',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (India)',
				'var_accent_code' => 'en-IN',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Ireland)',
				'var_accent_code' => 'en-IE',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Kenya)',
				'var_accent_code' => 'en-KE',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (New Zealand)',
				'var_accent_code' => 'en-NZ',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Nigeria)',
				'var_accent_code' => 'en-NG',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Philippines)',
				'var_accent_code' => 'en-PH',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (South Africa)',
				'var_accent_code' => 'en-ZA',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (Tanzania)',
				'var_accent_code' => 'en-TZ',
				'created_at' => date('Y-m-d H:i:s')
			]
		);
		$this->db->insert($this->table,
			[
				'var_accent_name' => 'English (United States)',
				'var_accent_code' => 'en-US',
				'created_at' => date('Y-m-d H:i:s')
			]
		);

	}

	public function down()
	{
		$this->dbforge->drop_table($this->table, TRUE);
	}
}
