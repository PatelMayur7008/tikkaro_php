<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_client_has_project_table extends CI_Migration
{

    public $table = 'client_has_project';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_user' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_assign_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'var_project_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'txt_description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'var_target_company' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'enum_client_anonymous' => array(
                'type' => 'enum("YES", "NO")',
                'null' => TRUE
            ),
            'enum_target_anonymous' => array(
                'type' => 'enum("YES", "NO")',
                'null' => TRUE
            ),
            'var_country_interest' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_geography_interest' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_companies_interest' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'int_no_of_call_required' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'enum_status' => array(
                'type' => 'enum("ACTIVE", "PRIORITY","WAITING","SCHEDULING","CLOSED")',
                'null' => TRUE
            ),
            'dt_initiate' => array(
                'type' => 'datetime',
                'null' => TRUE,
            ),
            'dt_deadline' => array(
                'type' => 'datetime',
                'null' => TRUE,
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'var_industry' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_user', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
