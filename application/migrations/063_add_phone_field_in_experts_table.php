<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_phone_field_in_experts_table extends CI_Migration
{

	public $table = 'experts';

	public function up()
	{
		$fields = array(
			'var_phone' => array(
				'type' => 'VARCHAR',
				'constraint' => '50',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'var_timezone');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'var_phone');
	}

}
