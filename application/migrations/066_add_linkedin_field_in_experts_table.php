<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_linkedin_field_in_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {
        $fields = array(
            'var_linkedin_link' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_phone');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_linkedin_link');
    }

}
