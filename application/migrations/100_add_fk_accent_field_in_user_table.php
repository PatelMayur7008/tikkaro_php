<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fk_accent_field_in_user_table extends  CI_Migration{


	public $table = 'user';

	public function up()
	{
		$fields = array(
			'fk_accent' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'enum_enable');
		dropForeignKey($this->table, 'fk_accent', 'accent', 'id');
		addForeignKey($this->table, 'fk_accent', 'accent', 'id', 'SET NULL', 'SET NULL');
	}

	public function down()
	{
		dropForeignKey($this->table, 'fk_accent', 'accent', 'id');
		$this->dbforge->drop_column($this->table, 'fk_accentId');
	}

}

