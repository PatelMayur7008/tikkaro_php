<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_two_fields_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {
        $fields = array(
            'flt_cost' => array(
                'type' => 'FLOAT',
                'constraint' => '8'
            ),
            'flt_revenue' => array(
                'type' => 'FLOAT',
                'constraint' => '8'
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'enum_invoice_sent');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'flt_revenue');
        $this->dbforge->drop_column($this->table, 'flt_cost');
    }
}
