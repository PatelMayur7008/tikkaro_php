<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_status_in_calls_table extends CI_Migration
{

	public $table = 'calls';

	public function up()
	{

		$fields = array(
			'enum_status' => array(
				'type' => 'enum("CALLS_SCHEDULED", "CALLS_COMPLETED","CALLS_CANCELLED")',
				'default' => 'CALLS_SCHEDULED',
				'null' => FALSE
			),
		);

		$this->dbforge->modify_column($this->table, $fields);

	}

	public function down()
	{

		$fields = array(
			'enum_status' => array(
				'type' => 'enum("CALLS_SCHEDULED", "CALLS_COMPLETED")',
				'default' => 'CALLS_SCHEDULED',
				'null' => FALSE
			),
		);
		$this->dbforge->modify_column($this->table, $fields);

	}

}
