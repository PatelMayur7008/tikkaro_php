<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_var_profile_image_field_in_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {
        $fields = array(
            'var_profile_image' => array(
                'type' => 'VARCHAR',
                'constraint' => '5000',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'txt_bio');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_profile_image');
    }

}
