<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_industry_table extends CI_Migration
{
	public $table = 'industry';

	public function up()
	{
		$this->dbforge->add_field(array(

			'id' => array(
				'type' => 'INT',
				'constraint' => '11',
				'auto_increment' => TRUE
			),
			'var_industry_name' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
				'null' => TRUE
			),
			'created_at' => array(
				'type' => 'datetime',
			),
			'updated_at' => array(
				'type' => 'timestamp'
			),

		));

		$this->dbforge->add_key('id', TRUE);
		$attributes = array('ENGINE' => 'InnoDB');
		$this->dbforge->create_table($this->table, TRUE, $attributes);

		$this->db->insert($this->table, [
			'var_industry_name' => 'Energy',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Materials',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Capital Goods',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Commercial & Professional Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Transportation',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Automobiles & Components',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Consumer Durables & Apparel',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Consumer Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Consumer Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Media',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Retailing',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Food & Staples Retailing',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Food, Beverage & Tobacco',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Household & Personal Products',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Health Care Equipment & Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Pharmaceuticals, Biotechnology & Life Sciences',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Banks',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Diversified Financials',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Insurance',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Real Estate',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Software & Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Technology Hardware & Equipment',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Semiconductors & Semiconductor Equipment',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Telecommunication Services',
			'created_at' => date('Y-m-d H:i:s')
		]);
		$this->db->insert($this->table, [
			'var_industry_name' => 'Utilities',
			'created_at' => date('Y-m-d H:i:s')
		]);
	}

	public function down()
	{
		$this->dbforge->drop_table($this->table, TRUE);
	}
}
