<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_account_manager_table extends CI_Migration {

    public $table = 'account_manager';

    public function up() {
        $fields = array(
            'bint_phone' => array(
                'type' => 'bigint',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_email');
    }

    public function down() {
        $this->dbforge->drop_column($this->table, 'bint_phone');
    }

}
