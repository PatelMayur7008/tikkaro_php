<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_unitcharges_field_in_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {
        $fields = array(
            'int_units_charges' => array(
                'type' => 'INT',
                'constraint' => '11'
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_rate_per_survey');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'int_units_charges');
    }

}
