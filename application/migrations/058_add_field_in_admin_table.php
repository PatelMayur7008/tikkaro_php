<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_field_in_admin_table extends CI_Migration {

    public $table = 'admin';

    public function up() {
        $fields = array(
            'var_phone' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'dt_dob');
    }

    public function down() {
        $this->dbforge->drop_column($this->table, 'var_phone');
    }

}
