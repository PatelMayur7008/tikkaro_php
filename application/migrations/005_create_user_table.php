<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_user_table extends CI_Migration
{

    public $table = 'user';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_assign_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_team' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_assign_team' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'var_fname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'var_lname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'var_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'var_password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'var_position' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'bint_phone' => array(
                'type' => 'bigint',
                'constraint' => '20',
                'null' => TRUE
            ),
            'var_profile_image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'txt_address' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'dt_dob' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_team', 'team', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_team', 'team', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_team', 'team', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
