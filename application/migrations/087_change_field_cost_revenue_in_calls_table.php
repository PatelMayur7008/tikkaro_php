<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_cost_revenue_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {
        $fields = array(
            'flt_cost' => array(
                'name' => 'flt_cost',
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'null' => TRUE
            ),
            'flt_revenue' => array(
                'name' => 'flt_revenue',
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'flt_cost' => array(
                'name' => 'flt_cost',
                'type' => 'FLOAT',
                'null' => TRUE
            ),
            'flt_revenue' => array(
                'name' => 'flt_cost',
                'type' => 'FLOAT',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
