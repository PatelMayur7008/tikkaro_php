<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_enum_invoice_sent_field_in_calls_table extends CI_Migration {

    public $table = 'calls';

    public function up() {


        $fields = array(
            'enum_invoice_sent' => array(
                'type' => "INT",
                'default' => 0,
                'null' => TRUE
            ),
        );

        $this->dbforge->add_column($this->table, $fields, 'enum_status');
    }

    public function down() {

        $this->dbforge->drop_column($this->table,'enum_invoice_sent');

    }

}
