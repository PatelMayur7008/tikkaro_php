<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_create_project_has_user_table extends CI_Migration
{

    public $table = 'project_has_user';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_project' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_user' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'enum_user_status' => array(
                'type' => 'enum("P", "S")',
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);


        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');

        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_project', 'client_has_project', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_user', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
