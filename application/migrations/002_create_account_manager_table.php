<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_account_manager_table extends CI_Migration
{

    public $table = 'account_manager';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'var_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'var_fname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_lname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => FALSE
            ),
            'var_password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => FALSE
            ),
            'var_profile_image' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'txt_address' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'dt_dob' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'var_register_ip' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_last_login_ip' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'dt_last_login' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);
       
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
         
        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
