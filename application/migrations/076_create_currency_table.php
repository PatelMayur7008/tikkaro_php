<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_currency_table extends CI_Migration
{

    public $table = 'currency';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'cur_code' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'cur_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => FALSE
            ),
            'prize' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);


        $data = array(
            array(
                'cur_code' => 'GBP',
                'cur_name' => '',
                'prize' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => ''
            ),
            array(
                'cur_code' => 'USD',
                'cur_name' => '',
                'prize' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => ''
            ),
            array(
                'cur_code' => 'EUR',
                'cur_name' => '',
                'prize' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => ''
            ),
            array(
                'cur_code' => 'CHF',
                'cur_name' => '',
                'prize' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => ''
            ),
            array(
                'cur_code' => 'AUD',
                'cur_name' => '',
                'prize' => '',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => ''
            ),
        );

        $this->db->insert_batch($this->table, $data);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
