<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_experts_has_education_table extends CI_Migration
{

    public $table = 'experts_has_education';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'var_colleges' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_certificates' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'dt_from_respective_date' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'dt_to_respective_date' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_experts', 'experts', 'id');

        addForeignKey($this->table, 'fk_experts', 'experts', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_experts', 'experts', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
