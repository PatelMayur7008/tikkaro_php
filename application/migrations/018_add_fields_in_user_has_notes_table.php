<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_user_has_notes_table extends CI_Migration {

    public $table = 'user_has_notes';

    public function up() {
        $fields = array(
            'fk_assign_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'fk_account_manager');
    }

    public function down() {
        $this->dbforge->drop_column($this->table, 'fk_assign_account_manager');
    }

}
