<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_field_in_client_table extends CI_Migration
{

    public $table = 'client';

    public function up()
    {
        $fields = array(
            'var_timezone' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'txt_address');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_timezone');
    }

}
