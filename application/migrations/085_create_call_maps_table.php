<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_call_maps_table extends CI_Migration
{

    public $table = 'call_maps';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'var_uk_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'var_us_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

    }

    public function down()
    {

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
