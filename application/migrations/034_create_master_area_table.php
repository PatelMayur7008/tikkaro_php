<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_master_area_table extends CI_Migration
{

    public $table = 'master_area';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'var_area_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'txt_description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

    }

    public function down()
    {
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
