<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_experts_has_time_availability_table extends CI_Migration {

    public $table = 'experts_has_time_availability';

    public function up() {

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_user' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_project' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_expert' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_project_has_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'dt_date_availability' => array(
                'type' => 'DATE',
                'null' => FALSE
            ),
            'dt_time_availability' => array(
                'type' => 'TIME',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_expert', 'experts', 'id');
        dropForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id');

        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_user', 'user', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_project', 'client_has_project', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_expert', 'experts', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id', 'CASCADE', 'CASCADE');

    }

    public function down() {

        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_expert', 'experts', 'id');
        dropForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
