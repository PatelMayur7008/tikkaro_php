<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_experts_block_table extends CI_Migration
{

    public $table = 'experts_block';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'dt_from_block_date' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'dt_to_block_date' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'enum_permanent' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
            'txt_reason' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_experts', 'experts', 'id');

        addForeignKey($this->table, 'fk_experts', 'experts', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_experts', 'experts', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
