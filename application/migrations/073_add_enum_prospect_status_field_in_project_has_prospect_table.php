<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_enum_prospect_status_field_in_project_has_prospect_table extends CI_Migration {

    public $table = 'project_has_prospect';

    public function up() {

        $fields = array(
            'enum_prospect_status' => array(
                'type' => 'enum("REACHED_OUT","CONVERSATION","QUALIFICATION","CONTRACT","REACHED_OUT_REJECTED","CONVERSATION_REJECTED","QUALIFICATION_REJECTED","CONTRACT_REJECTED")',
                'default' => 'REACHED_OUT',
            ),
        );

        $this->dbforge->add_column($this->table, $fields, 'enum_enable');
    }

    public function down() {

        $this->dbforge->drop_column($this->table,'enum_prospect_status');

    }

}
