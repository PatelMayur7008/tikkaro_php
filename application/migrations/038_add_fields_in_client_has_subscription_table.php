<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_client_has_subscription_table extends CI_Migration
{

    public $table = 'client_has_subscription';

    public function up()
    {
        $fields = array(
            'enum_subscription_type' => array(
                'type' => 'enum("PAY-AS-YOU-GO", "UNIT-BLOCK")',
                'default' => 'PAY-AS-YOU-GO',
                'null' => FALSE
            )
        );
        $this->dbforge->add_column($this->table, $fields, 'int_avg_subscription_per_week');

        $this->dbforge->drop_column($this->table, 'var_subscription_type');

    }

    public function down()
    {

        $this->dbforge->drop_column($this->table, 'var_subscription_type');

        $fields = array(
            'enum_subscription_type' => array(
                'type' => 'enum("PAY-AS-YOU-GO", "UNIT-BLOCK")',
                'default' => 'PAY-AS-YOU-GO',
                'null' => FALSE
            )
        );

        $this->dbforge->add_column($this->table, $fields, 'int_avg_subscription_per_week');
    }

}
