<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fk_team_column_in_account_manager_table extends CI_Migration
{

    public $table = 'account_manager';

    public function up()
    {
        $fields = array(
            'fk_team' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            )
        );
        $this->dbforge->add_column($this->table, $fields, 'fk_admin');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'fk_team');
    }

}
