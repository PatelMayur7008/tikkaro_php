<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_one_field_in_ps_invoice_table extends  CI_Migration{


	public $table = 'invoice';

	public function up()
	{
		$fields = array(
			'dt_paid' => array(
				'type' => 'datetime',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'dt_generated');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'dt_paid');
	}

}
