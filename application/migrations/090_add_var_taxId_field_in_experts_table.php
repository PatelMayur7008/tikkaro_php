<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_var_taxId_field_in_experts_table extends  CI_Migration
{
	public $table = 'experts';

	public function up()
	{
		$fields = array(
			'var_taxId' => array(
				'type' => 'VARCHAR',
				'constraint' => '200',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'var_address');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'var_taxId');
	}
}
