<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_client_table extends CI_Migration
{

    public $table = 'client';

    public function up()
    {
        $fields = array(
            'fk_currency' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_timezone');

        dropForeignKey($this->table, 'fk_currency', 'currency', 'id');
        addForeignKey($this->table, 'fk_currency', 'currency', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_currency', 'currency', 'id');
        $this->dbforge->drop_column($this->table, 'fk_currency');
    }

}
