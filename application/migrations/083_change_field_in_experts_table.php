<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_in_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {

        $fields = array(
            'fk_currency' => array(
                'type' => 'INT',
                'constraint' => '11',
                'default' => '2',
                'null' => FALSE
            ),
        );

        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'fk_currency' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
