<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_txt_transcription_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {
        $fields = array(
            'txt_transcription' => array(
                'name' => 'txt_transcription',
                'type' => 'LONGTEXT',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'txt_transcription' => array(
                'name' => 'txt_transcription',
                'type' => 'TEXT',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
