<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_project_has_email_table extends CI_Migration
{

    public $table = 'project_has_email';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'var_subject' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
				'null' => TRUE
            ),
            'txt_body' => array(
				'type' => 'LONGTEXT',
				'null' => TRUE
			),
			'fk_project' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'fk_admin' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'fk_account_manager' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
			'fk_client' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
			'fk_user' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => FALSE
			),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);
    }

    public function down()
    {
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
