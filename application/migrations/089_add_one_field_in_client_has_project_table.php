<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_one_field_in_client_has_project_table extends  CI_Migration{


	public $table = 'client_has_project';

	public function up()
	{
		$fields = array(
			'fk_industry' => array(
				'type' => 'INT',
				'constraint' => '11',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'var_industry');
		dropForeignKey($this->table, 'fk_industry', 'industry', 'id');
		addForeignKey($this->table, 'fk_industry', 'industry', 'id', 'SET NULL', 'SET NULL');
	}

	public function down()
	{
		dropForeignKey($this->table, 'fk_industry', 'industry', 'id');
		$this->dbforge->drop_column($this->table, 'fk_industry');
	}

}
