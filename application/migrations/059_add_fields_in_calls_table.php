<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {
        $fields = array(
            'var_recording_url' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'txt_transcription' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'int_duration' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'var_end_time' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_start_time' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_talk_interval' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_passcode' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_call_number' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_call_id' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'dt_time_availability');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_recording_url');
        $this->dbforge->drop_column($this->table, 'txt_transcription');
        $this->dbforge->drop_column($this->table, 'int_duration');
        $this->dbforge->drop_column($this->table, 'var_end_time');
        $this->dbforge->drop_column($this->table, 'var_start_time');
        $this->dbforge->drop_column($this->table, 'var_talk_interval');
        $this->dbforge->drop_column($this->table, 'var_passcode');
        $this->dbforge->drop_column($this->table, 'var_call_number');
        $this->dbforge->drop_column($this->table, 'var_call_id');
    }

}
