<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_user_has_notes_table extends CI_Migration
{

    public $table = 'user_has_notes';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'txt_note' => array(
                'type' => 'TEXT',
                'null' => FALSE
            ),
            'fk_user' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_user', 'user', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        dropForeignKey($this->table, 'fk_user', 'user', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
