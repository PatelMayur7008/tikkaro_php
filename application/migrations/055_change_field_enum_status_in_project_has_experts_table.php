<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_enum_status_in_project_has_experts_table extends CI_Migration
{

    public $table = 'project_has_experts';

    public function up()
    {
        $fields = array(
            'enum_status' => array(
                'type' => 'enum("EXPERTS_ATTACHED", "BIOS_SENT", "CALLS_IN_SCHEDULING", "CALLS_SCHEDULED", "CALLS_CANCELLED","CALLS_COMPLETED")',
                'default' => 'EXPERTS_ATTACHED',
                'null' => FALSE
            ),
        );

        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {
        $fields = array(
            'enum_status' => array(
                'type' => 'enum("EXPERTS_ATTACHED", "BIOS_SENT", "CALLS_IN_SCHEDULING", "CALLS_SCHEDULED", "CALLS_CANCELLED")',
                'default' => 'EXPERTS_ATTACHED',
                'null' => FALSE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);
    }
}
