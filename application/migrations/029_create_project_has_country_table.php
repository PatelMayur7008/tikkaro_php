<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_project_has_country_table extends CI_Migration
{

    public $table = 'project_has_country';

    public function up()
    {

        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_project' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_country' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);



        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');

        addForeignKey($this->table, 'fk_project', 'client_has_project', 'id', 'CASCADE', 'CASCADE');

        dropForeignKey($this->table, 'fk_country', 'master_country', 'id');

        addForeignKey($this->table, 'fk_country', 'master_country', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_country', 'master_country', 'id');
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
