<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_invoice_table extends CI_Migration
{

    public $table = 'invoice';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_call' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_project' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_expert' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'fk_project_has_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'var_invoice' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE
            ),
            'dt_generated' => array(
                'type' => 'datetime',
                'null' => TRUE
            ),
            'fk_currency' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
            'flt_cost' => array(
                'type' => 'FLOAT',
                'constraint' => '8'
            ),
            'en_isReceived' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
            'en_isPaid' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_call', 'calls', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_expert', 'experts', 'id');
        dropForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id');
        dropForeignKey($this->table, 'fk_currency', 'currency', 'id');

        addForeignKey($this->table, 'fk_call', 'calls', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_project', 'client_has_project', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_expert', 'experts', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id', 'CASCADE', 'CASCADE');
        addForeignKey($this->table, 'fk_currency', 'currency', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_call', 'calls', 'id');
        dropForeignKey($this->table, 'fk_project', 'client_has_project', 'id');
        dropForeignKey($this->table, 'fk_expert', 'experts', 'id');
        dropForeignKey($this->table, 'fk_project_has_experts', 'project_has_experts', 'id');
        dropForeignKey($this->table, 'fk_currency', 'currency', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
