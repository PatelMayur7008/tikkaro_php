<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_phone_in_project_has_prospect_table extends CI_Migration
{

	public $table = 'project_has_prospect';

	public function up()
	{

		$fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
		);

		$this->dbforge->modify_column($this->table, $fields);

	}

	public function down()
	{

		$fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ),
		);
		$this->dbforge->modify_column($this->table, $fields);

	}

}
