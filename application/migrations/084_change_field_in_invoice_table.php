<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_in_invoice_table extends CI_Migration
{

    public $table = 'invoice';

    public function up()
    {

        $fields = array(
            'fk_project_has_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
        );

        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'fk_project_has_experts' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
