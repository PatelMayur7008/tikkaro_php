<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_phone_in_user_table extends CI_Migration
{

    public $table = 'user';

    public function up()
    {
        $fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'bigint',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
