<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_one_field_in_ps_calls_table extends  CI_Migration{


	public $table = 'calls';

	public function up()
	{
		$fields = array(
			'int_notification_cnt' => array(
				'type' => 'INT',
				'constraint' => '11',
				'default' => '0',
				'null' => FALSE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'flt_cost');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'int_notification_cnt');
	}

}
