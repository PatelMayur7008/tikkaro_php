<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_var_password_field_in_client_has_compliance_officer extends CI_Migration
{

    public $table = 'client_has_compliance_officer';

    public function up()
    {
        $fields = array(
            'var_password' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_email');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_password');
    }

}
