<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_calls_table extends CI_Migration
{

    public $table = 'calls';

    public function up()
    {
        $fields = array(
            'en_isGenrated' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'enum_invoice_sent');

    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'en_isGenrated');
    }

}
