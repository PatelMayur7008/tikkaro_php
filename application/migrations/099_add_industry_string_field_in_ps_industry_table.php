<?php


defined('BASEPATH') OR exit('No direct script access allowed');
class Migration_Add_industry_string_field_in_ps_industry_table extends  CI_Migration{

	public $table = 'industry';

	public function up()
	{
		$fields = array(
			'var_industry_string' => array(
				'type' => 'VARCHAR',
				'constraint' => '5000',
				'null' => TRUE
			),
		);
		$this->dbforge->add_column($this->table, $fields, 'var_industry_name');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->table, 'var_industry_string');
	}

}
