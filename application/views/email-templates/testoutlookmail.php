<?php
ob_start();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>ProSaoient</title>
    <!--    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet">-->
    <!--    <link rel="stylesheet" type="text/css"-->
    <!--          href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700"/>-->

    <style>
        /*@import url(http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,300,400,700);*/

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        [style*="Open Sans"] {
            font-family: 'Open Sans', Arial, sans-serif !important;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">
<!--YAHOO CENTER FIX (full page wrapper) -->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">

            <!-- This is your container, you can shrink this if you want, but it's not recommended to go over 600px. 600px is generally the standard width for desktop/web. -->
            <table style="background: #ffffff;" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="600" align="center">
                        Lorem Ipsum Text Here ....
                        <!-- Add your layout tables here. Make sure they all have the attribute align="center" -->

                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>


</body>
</html>
<!--  -->
<?php
$variable = ob_get_clean();
echo $variable;
?>
