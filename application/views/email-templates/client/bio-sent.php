<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ProSapient</title>
    <style>

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">

<?php
ob_start();
?>
<tr>
    <td style="padding: 0 25px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <label style="margin: 0; font-family: 'Open Sans', sans-serif; font-weight: bold; font-weight: 600;">
                                    <strong style="font-family: 'Open Sans', sans-serif; font-size: 14px;">
                                        Dear <?= $userName; ?>
                                        ,</strong>
                                </label></td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif;">We have curated this list
                                of industry
                                experts for your upcoming project: <?= $expertsdata[0]['var_project_name'] ?>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px;"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0"
                           style="width: 100%; border: 1px solid #0d011f;border-radius: 3px; background: #F5F5F5;">
                        <tr>
                            <td colspan="2"
                                style="background-color: #0D011F; font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px 10px; text-align: left;">
                                Shortlisted Experts:
                            </td>
                        </tr>
                        <?php for ($i = 0; $i < count($expertsdata); $i++) { ?>
                            <tr>
                                <td style="border-bottom: 1px solid #ccc; padding: 10px 0 20px;">
                                    <table cellpadding="0" cellspacing="0" border="0"
                                           style="width: 100%;">
                                        <tr>
                                            <td colspan="2" style="padding: 0 10px">
                                                <label style="color: #1a1a1a;font-size: 18px !important; font-family: 'Open Sans', sans-serif; font-weight: 600; ">
                                                    <strong style="font-size: 18px !important; font-family: 'Open Sans', sans-serif;"> <?= $expertsdata[$i]['expertsfname'] . ' ' . $expertsdata[$i]['expertslname']; ?> </strong>
                                                </label><br>
                                                <?php if ($expertsdata[$i]['currentPosition'] != '' && $expertsdata[$i]['currentCompanyName']) { ?>
                                                    <label style="color: #1a1a1a;font-weight: bold;font-family: 'Open Sans', sans-serif;font-size: 13px;"><?= $expertsdata[$i]['currentPosition'] . ' at ' . $expertsdata[$i]['currentCompanyName']; ?></label>
                                                <?php } ?>
                                                <?php if ($expertsdata[$i]['txt_bio'] != '') { ?>
                                                    <p style="margin: 10px 0 0;;font-family: 'Open Sans', sans-serif;"><?= $expertsdata[$i]['txt_bio']; ?></p>
                                                <?php } else if ($expertsdata[$i]['currentDesc'] != '') { ?>
                                                    <p style="margin: 10px 0 0;;font-family: 'Open Sans', sans-serif;"><?= $expertsdata[$i]['currentDesc']; ?></p>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height: 20px"></td>
                                        </tr>
                                        <tr>
                                            <td style="width:50%; text-align:right; padding:0 5px">
                                                <?php $urlString = base64_encode(json_encode(['expertsHasProjectId' => $expertsdata[$i]["id"], 'expertsId' => $expertsdata[$i]["fk_experts"]])); ?>
                                                <a taget="_blank"
                                                   href="<?php echo base_url() . 'scheduling/schedule?data=' . $urlString ?>"
                                                   style="background-color:#fff; font-family: 'Open Sans', sans-serif; border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 130px;padding: 5px;text-align: center;text-decoration: none;">Schedule</a>
                                            </td>
                                            <td style="width:50%; text-align:left;padding:0 5px">
                                                <a target="_blank"
                                                   href="<?php echo base_url() . 'expert/bio/' . $expertsdata[$i]['fk_experts'] ?>"
                                                   style="background-color:#fff; font-family: 'Open Sans', sans-serif;border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 130px;padding: 5px;text-align: center;text-decoration: none;">Bio</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="height: 30px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif;">Best,</td>
                        </tr>
                        <tr>
                            <td style="height: 20px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif;">
                                <span><?= $expertsdata[0]['stafname'] ?></span>, Associate<br/>
                                <a style="color: blue;"
                                   href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                <?php if ($expertsdata[0]['staffphone']) { ?>
                                    <span><?= $expertsdata[0]['staffphone'] ?></span><br/>
                                <?php } ?>
                                <a style="color: blue;"
                                   href="mailto:<?= $expertsdata[0]['staffemail'] ?>"><?= $expertsdata[0]['staffemail'] ?></a><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
        </table>
    </td>
</tr>
<?php
$mailBody = ob_get_clean();
?>

<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                   style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                <?php
                echo $mailBody;
                ?>
                <?php
                $mailTemplatedata['userName'] = $userName;

                $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
