<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProSapient</title>


    <style>

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">

<?php
ob_start();
?>
<tr>
    <td style="padding: 0 25px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <label style="margin: 0; font-family: 'Open Sans', sans-serif;">
                                    <strong style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                                        Dear <?= $userName; ?>
                                        ,</strong>
                                </label></td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                                <span style="font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $ExpertsAndClientUser[0]['expetsFname'] . ' ' . $ExpertsAndClientUser[0]['expetsLname'] ?></span>
                                has come back to us with the following availability
                                for <?= $projectName; ?>:
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <?php if (!empty($WeekData)) {
                            for ($i = 0; $i < count($WeekData); $i++) {
                                ?>
                                <tr>
                                    <td colspan="2">
                                        <label style="font-size: 13px; font-family: 'Open Sans', sans-serif; margin:0; font-weight:600;">
                                            <strong style="font-size: 13px; font-family: 'Open Sans', sans-serif; ">
                                                Week
                                                (<?= date('d/m/y', strtotime($WeekData[$i]['start'])) . ' - ' . date('d/m/y', strtotime($WeekData[$i]['end'])); ?>
                                                )</strong>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="height: 10px"></td>
                                </tr>
                                <?php for ($j = 0; $j < count($WeekData[$i]['time']); $j++) { ?>
                                    <tr>
                                        <td style="background-color: #0D011F;border-bottom: solid 1px #fff; width:40%; border-right: solid 2px #E2E2E2;font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px 10px; text-align: left;">
                                            <strong style="font-size: 13px; font-family: 'Open Sans', sans-serif; "> <?= date('l jS', strtotime($WeekData[$i]['time'][$j]['date'])); ?> </strong>
                                        </td>
                                        <td style="background-color: #0D011F; border-bottom: solid 1px #fff; width:60%; color: #fff; padding: 3px 10px;font-family: 'Open Sans', sans-serif; font-weight: bold; text-align: left;">
                                            <?php for ($k = 0; $k < count($WeekData[$i]['time'][$j]['timeArray']); $k++) {
                                                $urlString = base64_encode(json_encode(['expertsHasTimeAvailabilityId' => $WeekData[$i]['time'][$j]['timeArray'][$k]['id']]));
                                                ?>
                                                <a href="<?php echo base_url() . 'scheduling/clientSelectAvailability?data=' . $urlString ?>"
                                                   style="color: #ffffff; text-decoration: none;">
                                                    <strong style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                                                        <?php
                                                        if (($k + 1) == count($WeekData[$i]['time'][$j]['timeArray'])) {
                                                            echo date("g:i a", strtotime($WeekData[$i]['time'][$j]['timeArray'][$k]['time']));
                                                        } else {
                                                            echo date("g:i a", strtotime($WeekData[$i]['time'][$j]['timeArray'][$k]['time'])) . ' / ';
                                                        }
                                                        ?>
                                                    </strong>
                                                </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                <tr>
                                    <td style="height: 20px"></td>
                                </tr>
                            <?php }
                        } ?>
                    </table>
                </td>
            </tr>

            <tr>
                <td style="height: 30px"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="font-size: 13px; font-family: 'Open Sans', sans-serif;">You can
                                click on your
                                preferred time or
                                just send us an email to schedule the consultation
                            </td>
                        </tr>
						<tr>
							<td style="height: 30px"></td>
						</tr>
						<?php
						if(!empty($expertData['var_position']) && (!empty($expertData['var_company']))){
							$expertdetail = ' - '.$expertData['var_position'].' at '.$expertData['var_company'];
						}
						else{
							$expertdetail = '';
						}
						?>
						<tr>
							<td style="font-weight:bold;font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $expertData['var_fname'].' '.$expertData['var_lname'].$expertdetail?>
							</td>
						</tr>
						<tr>
							<td style="font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $expertData['txt_bio']?>
							</td>
						</tr>
                        <tr>
                            <td style="height: 30px"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; font-family: 'Open Sans', sans-serif;">Best,
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                                <span><?= $ExpertsAndClientUser[0]['stafName'] ?></span>, Associate<br/>
                                <a style="color: blue;"
                                   href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                <?php if ($ExpertsAndClientUser[0]['staffPhone']) { ?>
                                    <span><?= $ExpertsAndClientUser[0]['staffPhone'] ?></span><br/>
                                <?php } ?>
                                <a style="color: blue;"
                                   href="mailto:<?= $ExpertsAndClientUser[0]['staffEmail'] ?>"><?= $ExpertsAndClientUser[0]['staffEmail'] ?></a><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
        </table>
    </td>
</tr>
<?php
$mailBody = ob_get_clean();
?>

<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                   style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                <?php
                echo $mailBody;
                ?>
                <?php
                $mailTemplatedata['fname'] = $ExpertsAndClientUser[0]['userFname'];
                $mailTemplatedata['lname'] = $ExpertsAndClientUser[0]['userLname'];
                $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata);
                ?>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
