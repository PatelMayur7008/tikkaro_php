<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ProSapient</title>

    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        [style*="Open Sans"] {
            font-family: 'Open Sans', Arial, sans-serif !important;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">


<?php
ob_start();
?>
<?php if (!empty($customertData['var_password'])) { ?>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <label style="
                                        margin: 0;
                                        font-weight: 600;
                                        font-family: 'Open Sans';
                                        font-style: normal;
                                        font-variant: normal;
                                        font-weight: 500;
                                        font-size: 14px;
">
                                        <strong style="font-size: 14px; font-family: 'Open Sans', sans-serif;">
                                            Dear <?= $customertData['var_fname'] . ' ' . $customertData['var_lname']; ?>
                                            ,
                                        </strong>
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Thank you for joining
                                    proSapient.
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Your username is your
                                    email address and we require you to set a password which you can do
                                    by following this link :
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>

                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif; text-align: center; height: 50px;">
                                    <a style="
                                                    background-color:#fff;
                                                    font-family: 'Open Sans', sans-serif;
                                                    border: 2px solid #662d91;
                                                    border-radius: 20px;
                                                    color: #662d91;
                                                    display: inline-block;
                                                    font-size: 15px;
                                                    min-width: 130px;
                                                    padding: 5px;
                                                    text-align: center;
                                                    text-decoration: none;"
                                       href="<?= $url; ?>">Reset Password</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Best</td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">
                                    <span><?= $customertData['accountmanagerName'] ?></span>,
                                    Associate<br/>
                                    <a style="color: blue;"
                                       href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                    <?php if ($customertData['accountmanagerPhone']) { ?>
                                        <span><?= $customertData['accountmanagerPhone'] ?></span><br/>
                                    <?php } ?>
                                    <a style="color: blue;"
                                       href="mailto:<?= $customertData['accountmanagerEmail'] ?>"><?= $customertData['accountmanagerEmail'] ?></a><br/>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
<?php } else { ?>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <label style="margin: 0; font-weight: bold;font-family: 'Open Sans', sans-serif;">
                                        <strong style="font-size: 14px; font-family: 'Open Sans', sans-serif;">
                                            Dear <?= $customertData['var_fname'] . ' ' . $customertData['var_lname']; ?>
                                            ,
                                        </strong></label></td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Thank you for joining
                                    proSapient.
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Your username is your
                                    email
                                    address and we require you to set a password which you can do by
                                    following this
                                    link:
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>

                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif; text-align: center; height: 50px;">
                                    <a style="
                                                    background-color:#fff;
                                                    font-family: 'Open Sans', sans-serif;
                                                    border: 2px solid #662d91;
                                                    border-radius: 20px;
                                                    color: #662d91;
                                                    display: inline-block;
                                                    font-size: 15px;
                                                    min-width: 130px;
                                                    padding: 5px;
                                                    text-align: center;
                                                    text-decoration: none;"
                                       href="<?= $url; ?>">Reset Password</a>
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">

                            <tr>
                                <td style="height: 30px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">Best</td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">
                                    <span><?= $customertData['accountmanagerName'] ?></span>,
                                    Associate<br/>
                                    <a style="color: blue;"
                                       href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                    <?php if ($customertData['accountmanagerPhone']) { ?>
                                        <span><?= $customertData['accountmanagerPhone'] ?></span><br/>
                                    <?php } ?>
                                    <a style="color: blue;"
                                       href="mailto:<?= $customertData['accountmanagerEmail'] ?>"><?= $customertData['accountmanagerEmail'] ?></a><br/>

                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
<?php } ?>

<?php
$mailBody = ob_get_clean();
?>
<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <?php if (!empty($customertData['var_password'])) { ?>
                <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                       style="width: 600px; margin: 0 auto 0 auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                    <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                    <?php
                    echo $mailBody;
                    ?>
                    <?php
                    $mailTemplatedata['fname'] = $customertData['var_fname'];
                    $mailTemplatedata['lname'] = $customertData['var_lname'];

                    $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>

                </table>
            <?php } else { ?>
                <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                       style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                    <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                    <?php
                    echo $mailBody;
                    ?>
                    <?php
                    $mailTemplatedata['fname'] = $customertData['var_fname'];
                    $mailTemplatedata['lname'] = $customertData['var_lname'];
                    $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
                </table>
            <?php } ?>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
