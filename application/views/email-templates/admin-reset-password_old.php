<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<meta name="description" content="">
	<meta name="author" content="">
	<title>ProSapient</title>

	<style>
		body {
			font-family: 'Open Sans', sans-serif;
			font-size: 13px;
		}

		a {
			font-family: 'Open Sans', sans-serif;
			text-decoration: none;
			color: #fff;
			font-size: 13px;
		}

		p {
			font-family: 'Open Sans', sans-serif;
			font-size: 13px;
		}

		[style*="Open Sans"] {
			font-family: 'Open Sans', Arial, sans-serif !important;
		}
	</style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">

	<tr>
		<td style="padding: 0 25px;">
			<table cellpadding="0" cellspacing="0" style="width: 100%;">
				<tr>
					<td>
						<table cellpadding="0" cellspacing="0" style="width: 100%;">
							<tr>
								<td>
									<label style="margin: 0; font-weight: bold;font-family: 'Open Sans', sans-serif;">
										<strong style="font-size: 14px; font-family: 'Open Sans', sans-serif;">
											Dear <?= $forgot_data['var_name']; ?>
											,</strong></label></td>
							</tr>
							<tr>
								<td style="height: 10px"></td>
							</tr>
							<tr>
								<td style="font-family: 'Open Sans', sans-serif;">Thank you for joining
									proSapient.
								</td>
							</tr>
							<tr>
								<td style="height: 15px"></td>
							</tr>
							<tr>
								<td style="font-family: 'Open Sans', sans-serif;">Your username is your
									email address and we require you to set a password which you can do
									by following this link :
								</td>
							</tr>
							<tr>
								<td style="height: 15px"></td>
							</tr>
							<tr>
								<td style="font-family: 'Open Sans', sans-serif; text-align: center; height: 50px;">
									Email is <?= $forgot_data['var_email']; ?>
									<br/>

									Password is <?= $reset_password; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td style="height: 30px"></td>
				</tr>
			</table>
		</td>
	</tr>



<!--Preheader-->
<div class="preheader"
	 style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td width="100%" align="center">

		</td>
	</tr>
</table>
</body>
</html>
<!--  -->
