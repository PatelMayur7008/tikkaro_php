<tr>
    <td style="text-align: center; padding: 15px 20px; background-color: #0d011f; box-shadow: 0 -4px 3px -4px #000; text-align: center;">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td style="text-align: center; color: #fff; font-size: 12px;font-family: 'Open Sans', sans-serif;">This
                    email was
                    intended
                    for <?= $userName ?>
                </td>
            </tr>
            <tr>
                <td style="height: 12px"></td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a href="<?php echo base_url(); ?>" style="display: inline-block; width: 90px; height: 21px;">
                        <img style="width: 90px; height: 21px; display: block;" src="<?php echo base_url() ?>public/assets/images/logo-1.png"
                             alt="" width="90" height="21px"/></a>
                </td>
            </tr>
            <tr>
                <td style="height: 8px"></td>
            </tr>
            <tr>
                <td style="text-align: center;">
                    <a style="color: #fff;font-family: 'Open Sans', sans-serif; font-size: 12px; text-decoration: none; padding: 0 8px 0 0; border-right: solid 1px #fff;"
                       href="javascript:;">Unsubscribe</a>
                    <a style="color: #fff;font-family: 'Open Sans', sans-serif; font-size: 12px; text-decoration: none; padding: 0 5px;"
                       href="javascript:;">View in browser</a>
                </td>
            </tr>
        </table>
    </td>
</tr>