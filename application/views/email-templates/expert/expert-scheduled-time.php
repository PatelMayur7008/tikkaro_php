
<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ProSapient</title>


    <style>
        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">
<?php
ob_start();
?>
<tr>
    <td style="padding: 0 50px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <label style="margin: 0; font-weight: bold;font-family: 'Open Sans', sans-serif;">
                                    <strong style="font-family: 'Open Sans', sans-serif; font-size: 13px;">
                                        Dear <?= ($checkClientAnonymous == 'Yes') ? $timeGiven[0]['expetsFname'] . ' ' . $timeGiven[0]['expetsLname'] : $userName; ?>
                                        ,</strong>
                                </label></td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td style=" font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $username; ?>
                                consultation <?php if ($checkClientAnonymous == 'Yes') {
                                    if ($projectName['enum_client_anonymous'] == 'NO') { ?>
                                        (with
                                        <span style=" font-size: 13px;"><?= $timeGiven[0]['var_company_name']; ?></span>)
                                    <?php }
                                } else { ?>
                                    (with
                                    <span style=" font-size: 13px;"><?= $timeGiven[0]['expetsFname'] . ' ' . $timeGiven[0]['expetsLname']; ?></span>)
                                <?php } ?>
                                has been
                                confirmed
                                for <?= $projectName['var_project_name']; ?>!
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <?php
                            $dataTime = date('Y-m-d', strtotime($timeGiven[0]['dt_date_availability'])) . ' ' . date('H:i:s', strtotime($timeGiven[0]['dt_time_availability']));
                            if ($mailType == 'Expert') {
                                $timezone = getExpertTimeZone($timeGiven[0]['fk_expert']);
                                if (!$timezone) {
                                    $timezone = 'UTC';
                                }
                                $convertDate = convert_date($dataTime, 'UTC', $timezone);
                            } else if ($mailType == 'Customer') {
                                $timezone = getClientTimeZone($timeGiven[0]['fk_client']);
                                if (!$timezone) {
                                    $timezone = 'UTC';
                                }
                                $convertDate = convert_date($dataTime, 'UTC', $timezone);
                            }


                            ?>
                            <th colspan="3"
                                style="background-color: #787281; font-weight: bold; border-bottom: solid 2px #E2E2E2; font-family: 'Open Sans',sans-serif; text-align: center; color: #fff; padding: 3px;">
                                <strong style="font-size: 13px; font-family: 'Open Sans',sans-serif; "><?= date('D', strtotime($convertDate)); ?> <?= date('d/m/y', strtotime($convertDate)) ?></strong>
                            </th>
                        </tr>
                        <tr>
                            <?php $cenvertedTime = date('H:i:s', strtotime('+1 hour', strtotime($convertDate))); ?>
                            <td colspan="2"
                                style="background-color: #0D011F; border-bottom: solid 2px #E2E2E2;font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px; text-align: center;">
                                <strong style="font-size: 13px; font-family: 'Open Sans',sans-serif; "><?= date('h:i A', strtotime($convertDate)); ?>
                                    - <?= date('h:i A', strtotime($cenvertedTime)); ?>
                                </strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="background-color: #0D011F; width:50%; border-right: solid 2px #E2E2E2;font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px; text-align: center;">
                                <strong style="font-size: 13px; font-family: 'Open Sans',sans-serif; ">
                                    UK Dial number:
                                    <span><?= formatNumber($callsData['var_call_number']); ?></span></strong>
                            </td>
                            <td
                                    rowspan="2"
                                    style="background-color: #0D011F; width:50%; color: #fff; padding: 3px;font-family: 'Open Sans', sans-serif; font-weight: bold; text-align: center;">
                                <strong style="font-size: 13px; font-family: 'Open Sans',sans-serif; ">
                                    Passcode: <span><?= $callsData['var_passcode']; ?></span> </strong>
                            </td>
                        </tr>

                        <?php
                        if(!empty($callsData['var_us_call_number'])){?>
                        <tr>
                            <td style="background-color: #0D011F; width:50%; border-right: solid 2px #E2E2E2;font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px; text-align: center;">
                                <strong style="font-size: 13px; font-family: 'Open Sans',sans-serif; ">
                                    US Dial number:
                                    <span><?= formatNumber($callsData['var_us_call_number'],"us"); ?></span></strong>
                        </td>

                        </tr>
                        <?php }
                        ?>

                    </table>
                </td>
            </tr>

            <tr>
                <td style="height: 30px"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">

                        <tr>
                            <td style="width:50%; text-align:center; padding:0 5px">
                                <?php $urlString = base64_encode(json_encode(['clientConfirmationId' => $clientConfirmationId, 'callId' => $callsData['id']]));

                                if ($mailType == 'Expert') {
                                    $flag = 'flag=E';
                                } else {
                                    $flag = 'flag=C';
                                }

                                ?>
                                <a href="<?php echo base_url() . 'scheduling/addToOutLook/?data=' . $urlString . '&' . $flag ?>"
                                   style="background-color:#fff; font-family: 'Open Sans', sans-serif;border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 250px;padding: 5px;text-align: center;text-decoration: none;">Add
                                    to Outlook</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
						<tr>
							<td style="height: 30px"></td>
						</tr>
						<tr>
							<?php
							if(!empty($expertData['var_position']) && $expertData['var_company']){
								$expertdetail = ' - '.$expertData['var_position'].' at '.$expertData['var_company'];
							}
							else{
								$expertdetail = '';
							}
							?>
							<td style="font-weight:bold;font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $expertData['var_fname'].' '.$expertData['var_lname'].$expertdetail ?>
							</td>
						</tr>
						<tr>
							<td style="font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $expertData['txt_bio']?>
							</td>
						</tr>
                        <tr>
                            <td style="height: 30px"></td>
                        </tr>
                        <tr>
                            <td style=" font-size: 13px; font-family: 'Open Sans', sans-serif;">Please
                                contact us if you need anything else
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 30px"></td>
                        </tr>
                        <tr>
                            <td style=" font-size: 13px; font-family: 'Open Sans', sans-serif;">Best,
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px"></td>
                        </tr>
                        <tr>
                            <td style="font-size: 13px; font-family: 'Open Sans', sans-serif;">
                                <span style="font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $timeGiven[0]['stafName']; ?></span>,
                                Associate<br/>
                                <a style="color: blue; font-size: 13px;"
                                   href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                <?php if ($timeGiven[0]['staffPhone']) { ?>
                                    <span style="font-size: 13px; font-family: 'Open Sans', sans-serif;"><?= $timeGiven[0]['staffPhone']; ?></span>
                                    <br/>
                                <?php } ?>
                                <a style="color: blue; font-size: 13px; font-family: 'Open Sans', sans-serif;"
                                   href="mailto:<?= $timeGiven[0]['staffEmail']; ?>"><?= $timeGiven[0]['staffEmail']; ?></a><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
        </table>
    </td>
</tr>
<?php
$mailBody = ob_get_clean();
?>
<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                   style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                <?php
                echo $mailBody;
                ?>
                <?php
                if ($mailType == 'Expert') {
                    $mailTemplatedata['fname'] = $timeGiven[0]['expetsFname'];
                    $mailTemplatedata['lname'] = $timeGiven[0]['expetsLname'];
                }
                else{
                    $mailTemplatedata['fname'] = $userName;
                    $mailTemplatedata['lname']="";

                }
                $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
