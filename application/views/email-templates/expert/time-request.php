<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ProSapient</title>
    <style>

        @font-face {
            font-family: 'Open Sans';
            font-style: normal;
            font-weight: 400;
            src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
        }

        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">
<?php
ob_start();
?>
<tr>
    <td style="padding: 0 25px;">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td>
                                <label style="margin: 0; font-weight: bold;font-family: 'Open Sans', sans-serif;">
                                    <strong style="font-family: 'Open Sans', sans-serif; font-size: 13px;">
                                        Dear <?= $expertsData[0]['expertsfname'] . ' ' . $expertsData[0]['expertslname']; ?>
                                        ,</strong>
                                </label></td>
                        </tr>
                        <tr>
                            <td style="height: 10px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">
                                One of our clients
                                <?php if ($expertsData[0]['clientAnonymous'] == 'NO') { ?>
                                    (<span><?= $expertsData[0]['var_company_name']; ?></span>)
                                <?php } ?>
                                has requested a consultation on the following project:
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 15px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">
                                <!--                                    Project Title<br />-->
                                <span><?= $expertsData[0]['var_project_name'] ?></span></td>
                        </tr>
                        <tr>
                            <td style="height: 15px"></td>
                        </tr>
<!--                        <tr>-->
<!--                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">-->
<!--                                                                    Project Description<br />-->
<!--                                <span>--><?//= nl2br($expertsData[0]['txt_description']); ?><!--</span></td>-->
<!--                        </tr>-->
<!--                        <tr>-->
<!--                            <td style="height: 15px"></td>-->
<!--                        </tr>-->
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">Please
                                let us know your
                                availability
                                by
                                clicking the button below or responding via email
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">
                        <tr>
                            <td style="width:100%; text-align:center; padding:0 5px">
                                <?php $urlString = base64_encode(json_encode(['expertsHasProjectId' => $expertsData[0]["id"]])); ?>
                                <a href="<?php echo base_url() . 'expert/availability/?data=' . $urlString ?>"
                                   style="background-color:#fff; font-family: 'Open Sans', sans-serif;  border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 250px;padding: 5px;text-align: center;text-decoration: none;">Provide
                                    Times</a>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" style="width: 100%;">

                        <tr>
                            <td style="height: 30px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">Best,
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 20px"></td>
                        </tr>
                        <tr>
                            <td style="font-family: 'Open Sans', sans-serif; font-size: 13px;">
                                <span><?= $expertsData[0]['stafname']; ?></span>, Associate<br/>
                                <a style="color: blue; font-size: 13px;"
                                   href="https://proSapient.com"><span>https://proSapient.com</span></a><br/>
                                <?php if ($expertsData[0]['staffphone']) { ?>
                                    <span><?= $expertsData[0]['staffphone']; ?></span><br/>
                                <?php } ?>
                                <a style="color: blue; font-size: 13px;"
                                   href="mailto:<?= $expertsData[0]['staffemail']; ?>"><?= $expertsData[0]['staffemail']; ?></a><br/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 30px"></td>
            </tr>
        </table>
    </td>
</tr>
<?php
$mailBody = ob_get_clean();
?>
<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                   style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                <?php
                echo $mailBody;
                ?>
                <?php
                $mailTemplatedata['fname'] = $expertsData[0]['expertsfname'];
                $mailTemplatedata['lname'] = $expertsData[0]['expertslname'];
                $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
