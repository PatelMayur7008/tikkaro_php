<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ProSapient</title>

    <style>
        body {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        a {
            font-family: 'Open Sans', sans-serif;
            text-decoration: none;
            color: #fff;
            font-size: 13px;
        }

        p {
            font-family: 'Open Sans', sans-serif;
            font-size: 13px;
        }

        [style*="Open Sans"] {
            font-family: 'Open Sans', Arial, sans-serif !important;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">
<?php
ob_start();
?>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    </td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="text-align:center;font-family: 'Open Sans', sans-serif;"><?= strtoupper($expertname) ?> has signed the terms and conditions
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif; text-align: center; height: 50px;">

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">

                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'Open Sans', sans-serif;">
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
<?php
$mailBody = ob_get_clean();
?>
<!--Preheader-->
<div class="preheader"
     style="display:none;font-size:1px;color:#ffffff;line-height:1px;max-height:0px;max-width:0px;opacity:0;overflow:hidden;">
    <?php
    echo strip_tags($mailBody);
    ?>
</div>
<!-- end preheader-->
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td width="100%" align="center">
            <?php if (!empty($expertData[0]['var_password'])) { ?>
                <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                       style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                    <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                    <?php
                    echo $mailBody;
                    ?>
                    <?php
                    $mailTemplatedata['fname'] = $expertData[0]['var_fname'];
                    $mailTemplatedata['lname'] = $expertData[0]['var_lname'];

                    $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
                </table>
            <?php } else { ?>
                <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
                       style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
                    <?php $this->load->view('email-templates/includes/bodyheader'); ?>
                    <?php
                    echo $mailBody;
                    ?>
                    <?php
                    $mailTemplatedata['fname'] = $expertData[0]['var_fname'];
                    $mailTemplatedata['lname'] = $expertData[0]['var_lname'];

                    $this->load->view('email-templates/includes/bodyfooter', $mailTemplatedata); ?>
                </table>
            <?php } ?>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
