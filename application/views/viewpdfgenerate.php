
<!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Service" ng-app="app" class="">
    <head>
        <title>Free Invoice Generator by Invoiced</title>

        <style>
            body {
                margin-left: 0px;
                margin-top: 0px;
                margin-right: 0px;
                margin-bottom: 0px;
                font-family:arial; 
                font-size:11px;
                box-sizing: border-box;
            }
            table{

                width: 100%;
            }
            .invoice-header{
                table-layout:fixed;
            }
            .fixedContainer{
                /*width: 590px;*/
                margin: 0 auto;
                /*border: 1px solid #ccc;*/
            }
            .bill-head{
                background-color:#000000;
                color:#fff;
            }
            td,th{
                height: 22px;
                line-height: 22px;
            }
            th{
                font-weight: 600;
            }
            .invoice-info{
                font-size:10px;
            }
            .invoice-info tr td:last-child{
                border:1px solid #ccc;
                border-bottom:0;
            }
            .invoice-info tr:last-child td:last-child{
                border-bottom:1px solid #ccc;
            }


            /*            .middle-contain tbody tr td{
                            padding:0 10px;
                            border-right:1px solid #c0c0c0;
                        }*/
            /*            .middle-contain tbody tr:nth-child(2){
                            background-color: #ccc;
                        }*/
            .note-title table tr th{
                background:#000;
                color:#fff;
                padding:0 10px;
            }
            .even{
                background-color: #c0c0c0;
            }
            .even .blank{
                background-color: #fff;
                border-right: 1px solid #c0c0c0;
            }
            .odd .blank{
                background-color: #fff;
                border-right: 1px solid #c0c0c0;
            }
        </style>


    </head>
    <body>
        <div class="fixedContainer">
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="header">
                <tr>
                    <td align="right" valign="middle" style="font-size: 25px;font-weight: bold;color:#000080;">&nbsp;</td>
                </tr>
                <tr>
                    <td align="right" valign="middle" style="font-size: 30px;font-weight: bold;color:#000080;">Invoice</td>
                </tr>
            </table>
            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="header">
                <tr>
                    <td class="bill-head" align="left" valign="middle" style="background-color: #000;width: 50%">&nbsp;&nbsp;<b>Bill To:</b></td>
                    <td style="width: 5%" align="left" valign="middle"></td>
                    <td style="width: 10%" align="left" valign="middle"></td>
                    <td style="width: 35%" align="left" valign="middle"></td>
                </tr>
                <tr>
                    <td style="color: red;" align="left" valign="top">&nbsp;&nbsp;Address</td>
                    <td align="left" valign="middle"></td>
                    <td colspan="2" align="left" valign="middle">
                        <table class="invoice-info" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Date: &nbsp;</td>
                                <td style="border: 1px solid #ccc;border-bottom: none;margin-top: 3px;" align="center" valign="middle">August 29, 2017</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Invoice #: &nbsp;</td>
                                <td style="border: 1px solid #ccc;border-bottom: none;color: red;" align="center" valign="middle">PROSAP-XXX</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Customer ID: &nbsp; </td>
                                <td style="border: 1px solid #ccc;border-bottom: none;" align="center" valign="middle">PROSAPIENT</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Payment Due by: &nbsp;</td>
                                <td style="border: 1px solid #ccc;border-bottom: none;" align="center" valign="middle">September 23, 2017</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Hourly Rate: &nbsp;</td>
                                <td style="border: 1px solid #ccc;border-bottom: none;" align="center" valign="middle">650.00</td>
                            </tr>
                            <tr>
                                <td style="font-size: 12px;" align="right" valign="middle">Currency: &nbsp; </td>
                                <td style="border: 1px solid #ccc;" align="center" valign="middle">USD</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="middle">&nbsp;</td>
                    <td align="left" valign="middle"></td>
                    <td align="left" valign="middle"></td>
                    <td align="left" valign="middle"></td>
                </tr>
            </table>
            <table class="middle-contain" width="100%" border="0" cellspacing="0" cellpadding="0">
                <thead>
                    <tr>
                        <th style="background-color: #000;color: #fff;width: 17%;line-height: 17px;" align="left" valign="middle">&nbsp;&nbsp;<b>Date</b></th>
                        <th style="background-color: #000;color: #fff;width: 15%;line-height: 17px;text-align: center;" align="center" valign="middle"><b>Lenth (mins)</b> </th>
                        <th style="background-color: #000;color: #fff;width: 33%;line-height: 17px;" align="left" valign="middle">&nbsp;&nbsp;<b>Client</b></th>
                        <th style="background-color: #fff;color: #fff;width: 15%;line-height: 17px;" align="left" valign="middle">&nbsp;</th>
                        <th style="background-color: #000;color: #fff;width: 20%;line-height: 17px;" align="left" valign="middle">&nbsp;&nbsp;<b>Line Total</b> </th>
                    </tr>
                </thead>
                <tbody>

                    <?php
                    for ($i = 0; $i <= 14; $i++) {
                        if (($i % 2) == 0) {
                            $class = 'odd';
                        } else {
                            $class = 'even';
                        }
                        ?>

                        <tr class="<?= $class; ?>">
                            <td style="border: 1px solid #c0c0c0;border-bottom: none; width: 17%;" align="left" valign="middle">&nbsp; 3 -jul-17 </td>
                            <td style="border: 1px solid #c0c0c0;border-bottom: none;width: 15%;" align="center" valign="middle">60</td>
                            <td style="border: 1px solid #c0c0c0;border-bottom: none;width: 33%;" align="left" valign="middle">&nbsp;&nbsp;Palamon</td>
                            <td class="blank" style="width: 15%;" align="left" valign="middle">&nbsp;</td>
                            <td style="border: 1px solid #c0c0c0;border-bottom: none;width: 20%;" align="right" valign="middle">650.00&nbsp;&nbsp;</td>
                        </tr>
                    <?php }
                    ?>


                </tbody>
            </table>
            <div class="" style="page-break-inside: avoid;">
                <table class="footer-notes" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 50%;background-color: #000;color:#fff;" class="note-title" valign="middle">&nbsp;<b>Special Notes and Instructions</b>
                        </td>
                        <td style="width: 15%;" valign="middle">&nbsp;</td>
                        <td style="width: 12%;" valign="middle"><b>&nbsp;Subtotal</b></td>
                        <td style="width: 3%;" valign="middle">&nbsp;</td>
                        <td style="width: 20%;border: 1px solid #c0c0c0;" class="total" align="right" valign="middle">650.00&nbsp;&nbsp;</td>
                    </tr>
                </table>
                <table class="footer-notes" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td style="width: 50%;border: 1px solid #c0c0c0;line-height: 1.2;" class="note-title" valign="middle" align="center;">
                            <table style="margin:0 auto;" width="90%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td align="left;" style="line-height: 1.4;">
                                        <br/><br/>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,

                                        <br/>
                                    </td>
                                </tr>
                            </table>

                        </td>
                        <td style="width: 15%;" valign="middle">&nbsp;</td>
                        <td style="width: 12%;" valign="middle">&nbsp;</td>
                        <td style="width: 3%;" valign="middle">&nbsp;</td>
                        <td style="width: 20%;" class="" align="right" valign="middle">&nbsp;</td>
                    </tr>
                </table>
            </div>





        </div>
    </body>
</html>
