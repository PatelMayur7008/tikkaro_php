<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }

    .selectwidth select {
        width: 225px !important;
    }
</style>

<?php
$companyArray = array();
foreach ($companyData as $value) {
    array_push($companyArray, array('value' => $value['Company_id'], 'text' => $value['company_name']));
}


$statusArray = array();
$status = array('YES' => 'Enable', 'NO' => 'Disable');
foreach ($status as $key=>$value) {
    array_push($statusArray, array('value' => $key, 'text' => $value));
}
?>
<script type="text/javascript">
    var gender = '<?= json_encode($GenderArray);?>';
    var status = '<?= json_encode($statusArray);?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= bussinessMenu('overview', $bussinessId) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Name:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Name"
                                       data-placeholder="Enter Name"
                                       data-table="bussiness"
                                       data-required="true"
                                       data-field="var_name"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $BussinessData['id']; ?>"
                                    ><?= $BussinessData['var_name']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Email:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Email "
                                       data-placeholder="Enter Email "
                                       data-table="bussiness"
                                       data-required="true"
                                       data-field="var_email"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $BussinessData['id']; ?>"
                                    ><?= $BussinessData['var_email']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Phone:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Phone"
                                       data-placeholder="Enter Phone"
                                       data-table="bussiness"
                                       data-required="true"
                                       data-field="var_phone"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $BussinessData['id']; ?>"
                                    ><?= $BussinessData['var_phone']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Note:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Note"
                                       data-placeholder="Enter Note"
                                       data-table="bussiness"
                                       data-required="false"
                                       data-field="txt_note"
                                       data-type="textarea"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $BussinessData['id']; ?>"
                                    ><?= $BussinessData['txt_note']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4"><b>Category:</b></label>
                                    <div class="col-md-8">
                                        <label class="control-label">
                                            <?php
                                            $catarr = array();
                                            for($i=0;$i<count($BussinessCategory);$i++){
                                                        array_push($catarr,$BussinessCategory[$i]['var_name']);
                                            }
                                            $catString = implode(',',$catarr);
                                            ?>
                                            <a href="javascript:;"
                                               class="cusComapnine editable editable-click"> <?= (!empty($catString)) ? $catString : '<span style="color: #DD1144"><i>Empty</i></span>' ?></a>
                                        </label>
                                        <div class="input-icon Tagstoshow projectOverview" style="display: none;">
                                            <select id="multiple" data-placeholder="Select Competitors"
                                                    class="form-control select2-multiple var_category"
                                                    name="var_category[]" multiple>
                                                <?php
                                                for ($k = 0; $k < count($Allcategory); $k++) {
                                                    $selected = "";
                                                    for ($j = 0; $j < count($BussinessCategory); $j++) {
                                                        if ($Allcategory[$k]['id'] == $BussinessCategory[$j]['id']) {
                                                            $selected = "selected = 'selected'";
                                                            break;
                                                        }
                                                    }
                                                    ?>
                                                    <option <?= $selected ?>
                                                            value="<?= $Allcategory[$k]['id'] ?>"><?= $Allcategory[$k]['name'] ?></option>
                                                    <?php ?>
                                                <?php } ?>
                                            </select>
                                            <div class="editable-buttons cusCompanyTags">
                                                <button type="submit" class="btn blue editable-submit updateMultipleSelection"
                                                        data-table="client_has_project"
                                                        data-field="var_competitors"
                                                        data-id="<?= $BussinessData['id']; ?>"><i class="fa fa-check"></i>
                                                </button>
                                                <button type="button" class="btn default editable-cancel cancleTags"><i
                                                            class="fa fa-times"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>




                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Status:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Status"
                                       data-table="bussiness"
                                       data-required="false"
                                       data-field="enum_enable"
                                       data-type="select"
                                       data-value="<?= $BussinessData['enum_enable']; ?>"
                                       data-value-source="status"
                                       data-id="<?= $BussinessData['id']; ?>">
                                        <?php
                                        if($BussinessData['enum_enable']=='YES'){
                                            echo 'Enable';
                                        }else{
                                            echo 'Disable';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <?php
                        //                        echo "<pre>";
                        //                        print_r($subscriptionData);
                        //                        exit;
                        if (!empty($subscriptionData)) {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label label <?= ($subscriptionData['enum_subscription_type'] == 'UNIT-BLOCK') ? 'label-danger' : 'label-black' ?>  label-sm "> <?= $subScriptionType->{$subscriptionData['enum_subscription_type']} ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscriptions Deadline:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= date('d/m/Y', strtotime($subscriptionData['dt_end_date'])) ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group <?= ($subscriptionData['enum_subscription_type'] == 'PAY-AS-YOU-GO') ? 'hide' : ''; ?>">
                                <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= $subscriptionData['int_no_of_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Current Subscription:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= ($subscriptionData['int_calls_completed']) ? $subscriptionData['int_calls_completed'] : '0' ?>
                                        calls </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_price_per_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_total_price'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"><?= ($subscriptionData['account_manager']) ? $subscriptionData['account_manager'] : $subscriptionData['admin'] ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php

                        }

                        ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
