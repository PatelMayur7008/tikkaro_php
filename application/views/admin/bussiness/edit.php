<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Staff
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editAccountManagerFrm"
              action="<?php echo admin_url() . 'staff/editKeyaccountmanager'; ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_username" name="var_usrname" placeholder="Enter Name"
                                   class="form-control" value="<?= $accountData['var_name']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="var_profile_image"
                                   value="<?= $accountData['var_profile_image']; ?>" class="form-control">
                            <input type="hidden" name="accountId" value="<?= $accountData['id']; ?>"
                                   class="form-control">
                            <input type="text" id="var_email" name="var_email" placeholder="Enter Email"
                                   class="form-control" value="<?= $accountData['var_email']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Password :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="password" id="var_password" name="var_password" placeholder="Enter Password"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone Number :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_phone" name="var_phone" placeholder="Enter Phone Number"
                                   class="form-control" value="<?= $accountData['bint_phone']; ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Team
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="team" name="team" data-placeholder="Select Team">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($teamData); $i++) {
                                    $selected = "";
                                    if ($teamData[$i]['id'] == $accountData['fk_team']) {
                                        $selected = "selected='selected'";
                                    }
                                    ?>

                                    <option value="<?= $teamData[$i]['id'] ?>" <?= $selected ?> ><?= $teamData[$i]['var_team_name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <!--                <div class="form-group">
                                    <label class="col-md-3 control-label">Password<span class="required" aria-required="true">*</span> :</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <input type="password" id="var_password" name="var_password" placeholder="Enter Password" class="form-control" value="<?= $accountData['var_password']; ?>"> 
                                        </div>
                                    </div>
                                </div>-->

                <div class="form-group">
                    <label class="col-md-3 control-label">Address :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea class="form-control" id="var_address" name="var_address"
                                      placeholder="Enter Address"
                                      rows="3"><?= $accountData['txt_address']; ?></textarea>
                        </div>
                    </div>
                </div>

                <!--                <div class="form-group">
                                    <label class="col-md-3 control-label">Manage DOB<span class="required" aria-required="true">*</span> :</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <input  id="var_Manage_Dob" name="var_manage_dob"  placeholder="Enter Date Of Birth"class="form-control form-control-inline  date-picker" size="20" type="text" value="<?= date('Y-m-d', strtotime($accountData['dt_dob'])); ?>" >
                                        </div>
                                    </div>
                                </div>-->
                <div class="form-group">
                    <label class="control-label col-md-3">Profile Pic:</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="<?= base_url() . KEY_ACCOUNT_PROFILE_IMG ?>/<?= $accountData['var_profile_image']; ?>"
                                     alt=""/></div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="profile_pic"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                    Remove </a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'bussiness'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>