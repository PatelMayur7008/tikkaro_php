<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_GEOLOCATION_KEY; ?>&v=3.exp&sensor=false&libraries=places"></script>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Bussiness
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addBussinessFrm"
              action="<?php echo admin_url() . 'bussiness/add'; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Bussiness Name<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_name" name="var_name" placeholder="Enter Bussiness Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Category
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" multiple id="Category" name="Category[]"
                                    data-placeholder="Select Category">
                                <option value=""></option>
                                <?php
                                for($i=0;$i<count($category);$i++) {
                                    ?>
                                    <option value="<?=$category[$i]['id']?>"> <?= $category[$i]['var_name']?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_phone" name="var_phone" placeholder="Enter Phone"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Email<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_email" name="var_email" placeholder="Enter Email"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Note:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea id="Note" name="note" placeholder="Enter Note" class="form-control"> </textarea>
                        </div>
                    </div>
                </div>




            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'bussiness'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>