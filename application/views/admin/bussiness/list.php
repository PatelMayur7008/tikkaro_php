

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Bussiness List </span>
                    </div>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?php echo admin_url() . 'bussiness/add'; ?>">
                        <i class="fa fa-plus"></i> Add Bussiness
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="bussiness_list">
                        <thead>
                            <tr role="row" class="heading">
                                <th> Bussiness Code</th>
                                <th> Name </th>
                                <th> Category /ies </th>
                                <th> Phone </th>
                                <th> Email </th>
                                <th> Note </th>
                                <th> Status </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
