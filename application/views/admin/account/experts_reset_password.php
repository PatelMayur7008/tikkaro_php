<script type="text/javascript">
    var pagetype = 'resetuserpassword';
</script>
<div class="content">
    <form class="login-form" id="UserResetPass" action="<?= base_url().'experts_reset_password/resetPassword'?>" method="post">
        <h3 class="form-title font-green">Reset Password</h3>

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix"  type="hidden" name="expertsId" id="expertsId" value="<?php echo $id;?>"/>
            <input class="form-control form-control-solid placeholder-no-fix"  type="password" autocomplete="off" placeholder="Enter Password" id="password" name="password" /> </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Confirm Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" name="confirmpass" id="confirmpassword" placeholder="Enter Confirm Password"/> </div>
        <div class="form-actions">
            
            <button type="submit" class="btn green uppercase" value="login">Save</button>

        </div>
    </form>
</div>