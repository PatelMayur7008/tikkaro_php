<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Service
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addServiceFrm"
              action="<?php echo admin_url() . 'service/add'; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Service Title<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_title" name="var_title" placeholder="Enter Service Title"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Category
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="fk_category" name="fk_category"
                                    data-placeholder="Select Category">
                                <option value=""></option>
                                <?php
                                for ($j = 0; $j < count($categoryData); $j++) {
                                    ?>
                                    <option value="<?= $categoryData[$j]['id'] ?>"><?= $categoryData[$j]['var_name'] ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'service'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>