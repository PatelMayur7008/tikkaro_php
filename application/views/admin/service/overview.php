<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }

    .selectwidth select {
        width: 225px !important;
    }
</style>
<?php

$statusArray = array();
$status = array('YES' => 'Enable', 'NO' => 'Disable');
foreach ($status as $key=>$value) {
    array_push($statusArray, array('value' => $key, 'text' => $value));
}

$catArray = array();
$status = $this->db->get_where('master_category',array('enum_type'=>'SERVICE'))->result_array();
foreach ($status as $key=>$value) {
    array_push($catArray, array('value' => $value['id'], 'text' => $value['var_name']));
}
?>
<script type="text/javascript">
    var status = '<?= json_encode($statusArray);?>';
    var category = '<?= json_encode($catArray);?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= serviceMenu('overview', $serviceId) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Service Title:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Service Title"
                                       data-placeholder="Enter Service Title"
                                       data-table="services"
                                       data-required="true"
                                       data-field="var_title"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $serviceData['id']; ?>"
                                    ><?= $serviceData['var_title']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Category:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Category"
                                       data-table="services"
                                       data-required="true"
                                       data-field="fk_category"
                                       data-type="select"
                                       data-value="<?= $serviceData['fk_category']; ?>"
                                       data-value-source="category"
                                       data-id="<?= $serviceData['id']; ?>">
                                        <?php echo $serviceData['var_name']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Status:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Status"
                                       data-table="services"
                                       data-required="false"
                                       data-field="enum_enable"
                                       data-type="select"
                                       data-value="<?= $serviceData['enum_enable']; ?>"
                                       data-value-source="status"
                                       data-id="<?= $serviceData['id']; ?>">
                                        <?php
                                        if($serviceData['enum_enable']=='YES'){
                                            echo 'Enable';
                                        }else{
                                            echo 'Disable';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
