<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }

    .selectwidth select {
        width: 225px !important;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_GEOLOCATION_KEY; ?>&v=3.exp&sensor=false&libraries=places"></script>


<?php

$GenderArray = array();
$gender = array('M' => 'Male', 'F' => 'Female');
foreach ($gender as $key=>$value) {
    array_push($GenderArray, array('value' => $key, 'text' => $value));
}

$statusArray = array();
$status = array('YES' => 'Enable', 'NO' => 'Disable');
foreach ($status as $key=>$value) {
    array_push($statusArray, array('value' => $key, 'text' => $value));
}
?>
<script type="text/javascript">
    var gender = '<?= json_encode($GenderArray);?>';
    var status = '<?= json_encode($statusArray);?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= userMenu('overview', $userId) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User First Name:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter First Name"
                                       data-placeholder="Enter First Name"
                                       data-table="users"
                                       data-required="true"
                                       data-field="var_fname"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $userData['id']; ?>"
                                    ><?= $userData['var_fname']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User Last Name:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Last Name"
                                       data-placeholder="Enter Last Name"
                                       data-table="users"
                                       data-required="true"
                                       data-field="var_lname"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $userData['id']; ?>"
                                    ><?= $userData['var_lname']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User Email:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Email Name"
                                       data-placeholder="Enter Email Name"
                                       data-table="users"
                                       data-required="true"
                                       data-validate-email="true"
                                       data-field="var_email"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $userData['id']; ?>"
                                    ><?= $userData['var_email']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User Phone:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Phone"
                                       data-placeholder="Enter Phone"
                                       data-table="users"
                                       data-required="true"
                                       data-validate-number="true"
                                       data-field="bint_phone"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $userData['id']; ?>"
                                    ><?= $userData['bint_phone']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Pincode:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Pincode"
                                       data-placeholder="Enter Pincode"
                                       data-table="users"
                                       data-required="true"
                                       data-validate-number="true"
                                       data-field="int_areacode"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $userData['id']; ?>"
                                    ><?= $userData['int_areacode']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User DOB:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;"
                                       class="editable editable-click"> <?= (!empty($userData['dt_dob']))? date('d/m/Y', strtotime($userData['dt_dob'])) : '<span style="color: #DD1144"><i>Empty</i></span>'?></a>
                                </label>
                                <div class="input-icon dateToshow" style="display: none; top:-15px;">
                                    <input type="text" id="compliance_date" name="user_dob"
                                           placeholder="Enter User DOB"
                                           class="form-control date-picker" data-date-format="dd/mm/yyyy"
                                           value="<?= (!empty($userData['dt_dob']))? date('d/m/Y', strtotime($userData['dt_dob'])):''; ?>">
                                    <div class="editable-buttons cusDateValue">
                                        <button type="submit"
                                                class="btn blue editable-submit updateDates"
                                                data-table="client_has_project" data-id="<?= $userData['id']; ?>">
                                            <i class="fa fa-check"></i></button>
                                        <button type="button"
                                                class="btn default editable-cancel cancelDates"><i
                                                    class="fa fa-times"></i></button>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Gender:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Gender"
                                       data-table="users"
                                       data-required="false"
                                       data-field="enum_geneder"
                                       data-type="select"
                                       data-value="<?= $userData['enum_geneder']; ?>"
                                       data-value-source="gender"
                                       data-id="<?= $userData['id']; ?>">
                                        <?php
                                        if($userData['enum_geneder']=='M'){
                                            echo 'Male';
                                        }else{
                                            echo 'Female';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                                <div class="form-group ">
                                    <label class="control-label col-md-4">Profile Pic:</label>
                                    <div class="col-md-8">
                                        <div class="fileinput fileinput-new userProfileImages"
                                             data-provides="fileinput">
                                            <div class="fileinput-preview thumbnail userProfilePicture"
                                                 data-trigger="fileinput" style="width: 200px; height: 150px;">
                                                <label class="control-label expertsBio">
                                                    <?php
                                                    if (!empty($userData['var_profile_image'])) {
                                                        $imgURL = base_url(USER_PROFILE_IMG) . "/" . $userData['var_profile_image'];
                                                    } else {
                                                        $imgURL = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                                    }
                                                    ?>
                                                    <img class="userProfile" src="<?= $imgURL ?>"
                                                         style="max-width: 200px; max-height: 150px;">
                                                </label>

                                            </div>
                                            <div >
                                                <input type="hidden" name="user_id" class='user_id'
                                                       value="<?= $userData['id'] ?>">
                                                <input type="hidden" name="var_profile_image" class='oldImage'
                                                       value="<?= $userData['var_profile_image'] ?>">
                                                <input type="file" name="profile_pic" id="userProfileImage"
                                                       class="userProfileImage" style="visibility: hidden"> </span>
                                                <div class="editable-buttons cusPicValue" style="display: none;">
                                                    <button type="submit"
                                                            class="btn blue editable-submit updatePics"
                                                            data-table="users" data-id="<?= $userData['id']; ?>">
                                                        <i class="fa fa-check"></i></button>
                                                    <button type="button"
                                                            class="btn default editable-cancel cancelPics"><i
                                                                class="fa fa-times"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Status:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Status"
                                       data-table="users"
                                       data-required="false"
                                       data-field="enum_enable"
                                       data-type="select"
                                       data-value="<?= $userData['enum_enable']; ?>"
                                       data-value-source="status"
                                       data-id="<?= $userData['id']; ?>">
                                        <?php
                                        if($userData['enum_enable']=='YES'){
                                            echo 'Enable';
                                        }else{
                                            echo 'Disable';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <?php
                        //                        echo "<pre>";
                        //                        print_r($subscriptionData);
                        //                        exit;
                        if (!empty($subscriptionData)) {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label label <?= ($subscriptionData['enum_subscription_type'] == 'UNIT-BLOCK') ? 'label-danger' : 'label-black' ?>  label-sm "> <?= $subScriptionType->{$subscriptionData['enum_subscription_type']} ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscriptions Deadline:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= date('d/m/Y', strtotime($subscriptionData['dt_end_date'])) ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group <?= ($subscriptionData['enum_subscription_type'] == 'PAY-AS-YOU-GO') ? 'hide' : ''; ?>">
                                <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= $subscriptionData['int_no_of_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Current Subscription:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= ($subscriptionData['int_calls_completed']) ? $subscriptionData['int_calls_completed'] : '0' ?>
                                        calls </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_price_per_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_total_price'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"><?= ($subscriptionData['account_manager']) ? $subscriptionData['account_manager'] : $subscriptionData['admin'] ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php

                        }

                        ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
