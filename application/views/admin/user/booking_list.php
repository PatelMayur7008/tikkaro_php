<style type="text/css">
    .compliance > td {
        background-color: #d82735 !important;
        color: #fff;
    }

    table.dataTable .compliance td.sorting_1 {
        background-color: #d82735 !important;
        color: #fff;
    }

    .datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
        z-index: 99999999999 !important;
    }

    .dataTables_filter {
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= userMenu('booking', $userId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase"> Booking List </span>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="user_id" id="userId" value="<?= $userId ?>" />
                    <table class="table table-striped table-bordered table-hover" id="booking_list">
                        <thead>
                        <tr role="row" class="heading">
                            <th> Booking Date</th>
                            <th> Store Name</th>
                            <th> Price </th>
                            <th> Service </th>
                            <th> Timeslot </th>
                            <th> Booking Status </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



