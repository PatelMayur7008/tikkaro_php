<style type="text/css">
    .compliance > td {
        background-color: #d82735 !important;
        color: #fff;
    }

    table.dataTable .compliance td.sorting_1 {
        background-color: #d82735 !important;
        color: #fff;
    }

    .datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
        z-index: 99999999999 !important;
    }

    .dataTables_filter {
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= userMenu('store', $userId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Store List </span>
                    </div>
                </div>
<!--                <div class="actions">-->
<!--                    <a class="btn btn-circle btn-default btn-sm" href="--><?//= admin_url('stores/add_holiday/'.$storeId)?><!--">-->
<!--                        <i class="fa fa-plus"></i> Add Holiday-->
<!--                    </a>-->
<!--                </div>-->
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="user_id" id="userId" value="<?= $userId ?>" />
                    <table class="table table-striped table-bordered table-hover" id="user_list">
                        <thead>
                        <tr role="row" class="heading">
                            <th> Name</th>
                            <th> Email</th>
                            <th> Note </th>
                            <th> Address </th>
                            <th> Contact No </th>
                            <th> WeekOff Day</th>
                            <th> noofBook seat </th>
                            <th> facebook link </th>
                            <th> twitter link </th>
                            <th> instagram link </th>
                            <th> Whats app No </th>
                            <th> Rating</th>
                            <th> Store Type </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



