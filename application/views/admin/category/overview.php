<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }

    .selectwidth select {
        width: 225px !important;
    }
</style>
<?php

$statusArray = array();
$status = array('YES' => 'Enable', 'NO' => 'Disable');
foreach ($status as $key=>$value) {
    array_push($statusArray, array('value' => $key, 'text' => $value));
}


$typeArray = array();
$status = array('SERVICE' => 'Service', 'STORE' => 'Store');
foreach ($status as $key=>$value) {
    array_push($typeArray, array('value' => $key, 'text' => $value));
}
?>
<script type="text/javascript">
    var status = '<?= json_encode($statusArray);?>';
    var type = '<?= json_encode($typeArray);?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= categoryMenu('overview', $category_id) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Category Name:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Category Name"
                                       data-placeholder="Enter Category Name"
                                       data-table="master_category"
                                       data-required="true"
                                       data-field="var_name"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $categoryData['id']; ?>"
                                    ><?= $categoryData['var_name']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Type:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Type"
                                       data-table="master_category"
                                       data-required="true"
                                       data-field="enum_type"
                                       data-type="select"
                                       data-value="<?= $categoryData['enum_type']; ?>"
                                       data-value-source="type"
                                       data-id="<?= $categoryData['id']; ?>">
                                        <?php
                                        if($categoryData['enum_type']=='SERVICE'){
                                            echo 'Service';
                                        }else{
                                            echo 'Store';
                                        }
                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Status:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Status"
                                       data-table="master_category"
                                       data-required="true"
                                       data-field="enum_enable"
                                       data-type="select"
                                       data-value="<?= $categoryData['enum_enable']; ?>"
                                       data-value-source="status"
                                       data-id="<?= $categoryData['id']; ?>">
                                        <?php
                                        if($categoryData['enum_enable']=='YES'){
                                            echo 'Enable';
                                        }else{
                                            echo 'Disable';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
