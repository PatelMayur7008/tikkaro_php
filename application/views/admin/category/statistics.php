<style type="text/css">
	.compliance > td {
		background-color: #d82735 !important;
		color: #fff;
	}

	table.dataTable .compliance td.sorting_1 {
		background-color: #d82735 !important;
		color: #fff;
	}

	.datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
		z-index: 99999999999 !important;
	}
</style>
<script>
	var CallsFilterByClientId = <?= $clientId; ?>;
</script>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="tabbable-line boxless tabbable-reversed">
			<?php $this->load->helper('common_tab_helper'); ?>
			<?= staffMenu('statistics', $staffId) ?>
			<div class="tab-content">
				<?php $this->load->helper('comman_data_helper'); ?>
				<?= staffData($staffData) ?>
				<div class="portlet light portlet-fit portlet-datatable bordered">
					<div class="portlet-title">
						<div class="col-md-3" style="padding-left: 0">
							<div class="caption">
								<i class="icon-equalizer font-blue-hoki"></i>
								<span class="caption-subject font-green-haze sbold uppercase"> Statistics List </span>
							</div>
						</div>

					</div>
					<div class="portlet-body">
						<div class="portlet-body">
							<table class="table table-striped table-bordered table-hover" id="statistics_list" data-id="<?= $statisticsId; ?>">
								<thead>
								<tr role="row" class="heading">
									<th> Date</th>
									<th> Prospects Added</th>
									<th> Prospect Interactions</th>
									<th> Date</th>
								</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<style>
	#call_list_wrapper table.table > thead th:nth-child(3),
	#call_list_wrapper table.table > tbody td:nth-child(3) {
		display: none !important;
	}
</style>

