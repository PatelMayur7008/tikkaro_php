<div class="row">
    <div class="col-md-12">

        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-green-haze bold uppercase">Profile Account</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">Personal Info</a>
                                </li>
                                <li class="">
                                    <a href="#tab_1_2" data-toggle="tab">Change Profile Picture</a>
                                </li>
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">Change Password</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form id="admin_infor" method="post" class="form-horizontal" name="admin_info"
                                          action="<?php echo admin_url() . 'Profile/editDetail'; ?>"
                                          enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">First Name</label>
                                            <div class="col-md-4">
                                                <input type="text" placeholder="Enter First Name"
                                                       value="<?= $profiles['var_fname']; ?>" name="fname" id="fname"
                                                       class="form-control"/>
                                                <input type="hidden"
                                                       value="<?= $profiles['id']; ?>" name="id" id="id"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Last Name</label>
                                            <div class="col-md-4">
                                                <input type="text" placeholder="Enter Last Name"
                                                       value="<?= $profiles['var_lname']; ?>" name="lname" id="lname"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Email Address</label>
                                            <div class="col-md-4">
                                                <input type="text" name="email" id="email"
                                                       value="<?= $profiles['var_email']; ?>" placeholder="Enter Email"
                                                    <?= $readOnly; ?> class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Phone no.</label>
                                            <div class="col-md-4">
                                                <input type="text" name="phone" id="phone"
                                                       value="<?= $profiles['bint_phone']; ?>"
                                                       placeholder="Enter Phone number"
                                                       class="form-control"/>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Address</label>
                                            <div class="col-md-4">
                                                <textarea class="form-control" rows="3" id="address" name="address"
                                                          placeholder="Enter Address"><?= $profiles['txt_address']; ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Manage DOB
                                                :</label>
                                            <div class="col-md-8">
                                                <div class="input-icon right">
                                                    <input id="var_Manage_Dob"  name="var_manage_dob" placeholder="Enter Date Of Birth" for
                                                           class="form-control form-control-inline  date-picker" size="20" type="text" value="<?= $profiles['dt_dob']?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-offset-3 col-md-4">
                                            <input class="btn green btn-circle" type="submit" value="Update">
                                            <a href="javascript:window.location.reload();"
                                               class="btn default btn-circle"> Cancel </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->

                                <!-- CHANGE PROFILE PICTURE TAB -->
                                <div class="tab-pane" id="tab_1_2">
                                    <form method="POST" role="form" class="form-horizontal" id="change_picture"
                                          action="<?php echo admin_url() . 'Profile/editPicture'; ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Profile picture</label>
                                            <div class="col-md-4">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail"
                                                         style="width: 200px; height: 150px;">
                                                        <?php
                                                            $profilePICPath = ADMIN_PROFILE_IMG . '/' . $profiles['var_profile_image'];
                                                            $profilePIC = base_url($profilePICPath);
                                                            $isExists = file_exists($profilePICPath);
                                                            $isFile = is_file($profilePICPath);
                                                            if ($profiles['var_profile_image'] === NULL OR trim($profiles['var_profile_image']) === '' OR $isExists === FALSE OR $isFile === FALSE) {
                                                                $profilePIC = base_url(NO_IMAGE);
                                                            }

                                                        ?>
                                                        <input type="hidden" name="admin_id" value="<?= $profiles['id']?>"/>
                                                        <img src="<?= $profilePIC; ?>"
                                                             alt=""/>
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 200px; max-height: 150px;"></div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" class="profilePicture"
                                                                   name="profilePicture">
                                                            <input type="file" name="old_profilePicture"
                                                                   value="<?= $profiles['var_profile_image']; ?>">
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists"
                                                           data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-offset-3 col-md-4">
                                            <input class="btn green btn-circle" type="submit" value="Update">
                                            <a href="javascript:window.location.reload();"
                                               class="btn default btn-circle"> Cancel </a>
                                        </div>

                                    </form>
                                </div>
                                <!-- END PROFILE PICTURE TAB -->

                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="tab_1_3">
                                    <form method="POST" role="form" class="form-horizontal" id="change_password"
                                          action="<?php echo admin_url() . 'Profile/editPassword'; ?>">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Current Password</label>
                                            <div class="col-md-4">
                                                <input type="hidden" name="admin_id" value="<?= $profiles['id'] ?>" />
                                                <input type="password" name="old_pwd" id="old_pwd"
                                                       class="form-control"/>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">New Password</label>
                                            <div class="col-md-4">
                                                <input type="password" name="new_pwd" id="new_pwd"
                                                       class="form-control"/>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Re-type New Password</label>
                                            <div class="col-md-4">
                                                <input type="password" name="conf_pwd" id="conf_pwd"
                                                       class="form-control"/>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="col-md-offset-3 col-md-4">
                                            <input class="btn green btn-circle" type="submit" value="Update">
                                            <a href="javascript:window.location.reload();"
                                               class="btn default btn-circle"> Cancel </a>
                                        </div>

                                    </form>
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>