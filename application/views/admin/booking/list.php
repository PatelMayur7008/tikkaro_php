<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Booking List </span>
                    </div>
                </div>

<!--                <div class="actions">-->
<!--                    <a class="btn btn-circle btn-default btn-sm" href="--><?php //echo admin_url() . 'service/add'; ?><!--">-->
<!--                        <i class="fa fa-plus"></i> Add Service-->
<!--                    </a>-->
<!--                </div>-->
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="booking_list">
                        <thead>
                            <tr role="row" class="heading">
                                <th> Id</th>
                                <th> User Name </th>
                                <th> Store Name</th>
                                <th> Price </th>
                                <th> Services </th>
                                <th> Timeslots </th>
                                <th> Booking Date </th>
                                <th> Booking Status </th>
                                <th> Action </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
<div id="cancel_model" style="display: none" class="modal fade cancel_model in" style="padding-top: 20px; display: block;" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Cancel Confirmation</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Cancel ?...</h3>
                <input type="hidden" name="bookingId" class="bookingId"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-circle cancel_booking_model" data-dismiss="modal">Close</button>
                <button type="submit" class="btn red CancelBooking btn-circle"><i class="fa fa-check"></i>Delete
                </button>
            </div>
        </div>
    </div>
</div>