<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Timeslot
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addTimeslotFrm"
              action="<?php echo admin_url() . 'time-slot/add'; ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Timeslot:</label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepicker">
                            <input type="text" name="time_slot" size="16" readonly class="form-control">
                            <span class="input-group-btn">
							<button class="btn default date-set" type="button"><i
                                                                class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'time-slot'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>