<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Timeslot
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editTimeslotFrm"
              action="<?php echo admin_url() . 'time-slot/edit'; ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-2 control-label">Timeslot:</label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepicker">
                            <input type="text" name="time_slot" size="16" readonly value="<?= $time_slot['slot_time'] ?>" class="form-control">

                            <span class="input-group-btn">
							<button class="btn default date-set" type="button"><i
                                                                class="fa fa-calendar"></i></button>
                            </span>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="id"  value="<?= $time_slot['id'] ?>" class="form-control">


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'time-slot'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>