<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Store Timeslot
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addStoreTimeslotFrm"
              action="<?php echo admin_url() . 'stores/add_timeslot'; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Timeslot<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="store_id" value="<?= $storeId ?>">
                            <select class="form-control select2me" id="timeslot" name="fk_timeslot" data-placeholder="Select Timeslot">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($timeslot_list); $i++) {
                                    ?>

                                    <option value="<?= $timeslot_list[$i]['id'] ?>"><?= $timeslot_list[$i]['slot_time'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/time_slot/'.$storeId; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>