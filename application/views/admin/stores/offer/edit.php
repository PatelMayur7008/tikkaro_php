<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>edit Offer
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editOfferFrm"
              action="<?php echo admin_url() . 'stores/edit_offer/'.$offer_data['offers']['id']; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Name <span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="store_id" value="<?= $storeId ?>">
                            <input type="hidden" name="offer_id" value="<?= $offer_data['offers']['id'] ?>">
                            <input type="text" id="var_name" name="var_name" value="<?= $offer_data['offers']['var_name'] ?>" placeholder="Enter Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Services
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" multiple id="services" name="services[]"
                                    data-placeholder="Select Services">
                                <option value=""></option>
                                <?php
                                for($i=0;$i<count($service);$i++) {
                                    $selected ="";
                                    for($j=0;$j<count($offer_data['offers']['services']);$j++) {
                                        if ($service[$i]['fk_service'] ==$offer_data['offers']['services'][$j]['id']) {
                                            $selected = "selected = 'selected'";
                                            break;
                                        }
                                    }
                                    ?>
                                    <option <?=$selected?> value="<?= $service[$i]['fk_service'] ?>"> <?= $service[$i]['var_title'] ?></option>
                                    <?php

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Start Date
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input id="var_str_date" name="var_str_date" placeholder="Enter Start Date" for
                                   class="form-control form-control-inline  date-picker" size="20" type="text" value="<?= date('d/m/Y',strtotime($offer_data['offers']['dt_start_date'])) ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">End Date
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input id="var_end_date" name="var_end_date" placeholder="Enter End Date" for
                                   class="form-control form-control-inline  date-picker" size="20" type="text" value="<?= date('d/m/Y',strtotime($offer_data['offers']['dt_end_date'])) ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Price<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_price" value="<?= $offer_data['offers']['var_price'] ?>" name="var_price" placeholder="Enter Price"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Note:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea id="Note" name="note" placeholder="Enter Note" class="form-control"> <?= $offer_data['offers']['txt_note'] ?></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Status
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me"  id="enum_status" name="enum_status"
                                    data-placeholder="Select Services">
                                <option value=""></option>
                                <option <?php echo ($offer_data['offers']['enum_enable'] == 'YES') ? 'selected': ''; ?> value="YES"> Enable</option>
                                <option <?php echo ($offer_data['offers']['enum_enable'] == 'No') ? 'selected': ''; ?>  value="NO"> Disable</option>

                            </select>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/offer/'.$storeId; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>