<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_GEOLOCATION_KEY; ?>&v=3.exp&sensor=false&libraries=places"></script>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Store
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" id="addStoreFrm"
              action="<?php echo admin_url() . 'stores/add'; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Store Title<span class="required"
                                                                           aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_title" name="var_title" placeholder="Enter Store Title"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone Number<span class="required"
                                                                            aria-required="true">*</span>:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_phone" name="var_phone" placeholder="Enter Phone Number"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Address <span class="required" aria-required="true">*</span>:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_address" name="var_address" placeholder="Enter Address"
                                   class="form-control">
                            <input type="hidden" id="address" name="address"
                                   class="form-control">
                            <input type="hidden" id="lat" name="lat"
                                   class="form-control">
                            <input type="hidden" id="lng" name="lng"
                                   class="form-control">
                            <input type="hidden" id="pincode" name="pincode"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">WeekOff Day:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="int_weekoff" name="int_weekoff"
                                    data-placeholder="Select WeekOff Day">
                                <option value=""></option>
                                <?php
                                $week = getAllWeekDays();
                                foreach ($week as $key => $val) {
                                    ?>
                                    <option value="<?= $key ?>"><?= $val ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">NO OF BOOK SEAT:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="int_no_book_seats" name="int_no_book_seats"
                                   placeholder="Enter NO OF BOOK SEAT"
                                   class="form-control">
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">Rating:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_rating" name="var_rating" placeholder="Enter Rating"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Store Type
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="store_type" name="store_type"
                                    data-placeholder="Select Store Type">
                                <option value=""></option>
                                <option value="MEN">Men</option>
                                <option value="WOMEN">Women</option>
                                <option value="BOTH">Both</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Select Business
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="bussiness_id" name="bussiness_id"
                                    data-placeholder="Select Business">
                                <option value=""></option>
                                <?php
                                for ($j = 0; $j < count($bussinessData); $j++) {
                                    ?>
                                    <option value="<?= $bussinessData[$j]['id'] ?>"><?= $bussinessData[$j]['var_name'] ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Select Category
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="fk_category" name="fk_category"
                                    data-placeholder="Select Category">
                                <option value=""></option>
                                <?php
                                for ($j = 0; $j < count($categoryData); $j++) {
                                    ?>
                                    <option value="<?= $categoryData[$j]['id'] ?>"><?= $categoryData[$j]['var_name'] ?></option>
                                    <?php
                                }
                                ?>

                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Store logo:</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="store_logo"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                    Remove </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>