<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Holiday
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editHolidayFrm"
              action="<?php echo admin_url() . 'stores/edit_holiday/'.$holiday; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Holiday Date
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input id="var_str_date" name="var_holiday_date" placeholder="Enter Holiday Date"
                                   class="form-control form-control-inline  date-picker" size="20" type="text" value="<?= date('d/m/Y',strtotime($holiday_data['dt_holiday_date'])) ?>">
                            <input type="hidden" name="storeId" value="<?= $storeId; ?>">
                            <input type="hidden" name="holiday_id" value="<?= $holiday; ?>">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Opening Time:</label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepicker">
                            <input type="text" name="opening_time" id="opening_time" size="16" value="<?= $holiday_data['dt_start_time']?>" readonly class="form-control">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Closing Time:</label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepicker">
                            <input type="text" name="closing_time" id="closing_time" size="16" value="<?= $holiday_data['dt_end_time']?>" readonly class="form-control">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Note:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea id="Note" name="note" placeholder="Enter Note" class="form-control"><?= $holiday_data['txt_note']?> </textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/holiday_manager/'.$storeId; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>