<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }

    .selectwidth select {
        width: 225px !important;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=<?php echo GOOGLE_GEOLOCATION_KEY; ?>&v=3.exp&sensor=false&libraries=places"></script>


<?php
$bussinessArr = array();
$bussiness = $this->db->get('bussiness')->result_array();
foreach ($bussiness as $key=>$value) {
    array_push($bussinessArr, array('value' => $value['id'], 'text' => $value['var_name']));
}

$storeTypeArray = array();
$type = array('MEN' => 'Men', 'WOMEN' => 'Women','BOTH'=>'Both');
foreach ($type as $key=>$value) {
    array_push($storeTypeArray, array('value' => $key, 'text' => $value));
}

$weekArray = array();
$weeks = getAllWeekDays();
foreach($weeks as $key=>$val){
    array_push($weekArray, array('value' => $key, 'text' => $val));
}

$statusArray = array();
$status = array('YES' => 'Enable', 'NO' => 'Disable');
foreach ($status as $key=>$value) {
    array_push($statusArray, array('value' => $key, 'text' => $value));
}


$catArray = array();
$status = $this->db->get_where('master_category',array('enum_type'=>'STORE'))->result_array();
foreach ($status as $key=>$value) {
    array_push($catArray, array('value' => $value['id'], 'text' => $value['var_name']));
}
?>
<script type="text/javascript">
    var type = '<?= json_encode($storeTypeArray);?>';
    var status = '<?= json_encode($statusArray);?>';
    var bussiness = '<?= json_encode($bussinessArr);?>';
    var weeks = '<?= json_encode($weekArray) ?>';
    var category = '<?= json_encode($catArray) ?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= storeMenu('overview', $storeId) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Store Title:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Store Title"
                                       data-placeholder="Enter Store Title"
                                       data-table="stores"
                                       data-required="true"
                                       data-field="var_title"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $storeData['id']; ?>"
                                    ><?= $storeData['var_title']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Bussiness:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Bussiness"
                                       data-table="stores"
                                       data-required="true"
                                       data-field="fk_bussiness"
                                       data-type="select"
                                       data-value="<?= $storeData['fk_bussiness']; ?>"
                                       data-value-source="bussiness"
                                       data-id="<?= $storeData['id']; ?>">
                                        <?php
                                        echo $storeData['bussinessName'];
                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Store Code:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                  <?= $storeData['var_store_code']?>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User Phone:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Phone"
                                       data-placeholder="Enter Phone"
                                       data-table="stores"
                                       data-required="true"
                                       data-validate-number="true"
                                       data-field="bint_phone"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $storeData['id']; ?>"
                                    ><?= $storeData['bint_phone']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>User Address:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="address"><?= $storeData['txt_address']; ?>
                                    </a>
                                    <div style="display: none" class="addressDiv">
                                        <div class="editable-input " style="position: relative;width:275px;">
                                            <input type="text" class="form-control addressAuto" name="var_address" id="var_address" placeholder="Enter address">
                                            <input type="hidden" name="lng" id="lng" class="lng" />
                                            <input type="hidden" name="lat" id="lat" class="lat">
                                            <input type="hidden" name="pincode" id="pincode" class="pincode">
                                            <input type="hidden" name="formated_address" id="formated_address" class="formated_address">
                                            <input type="hidden" name="u_id" id="store_id" class="store_id" value="<?= $storeData['id']; ?>">
                                        </div>
                                        <div class="editable-buttons">
                                            <button type="submit" class="btn blue editable-submit addressSubmit"><i class="fa fa-check"></i></button>
                                        </div>
                                        <button type="button" class="btn default editable-cancel cancel-add"><i class="fa fa-times"></i></button>
                                    </div>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Rating:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Rating"
                                       data-placeholder="Enter Rating"
                                       data-table="stores"
                                       data-required="true"
                                       data-field="var_rating"
                                       data-input-class="form-control cuswidth var_address"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $storeData['id']; ?>"
                                    ><?= $storeData['var_rating']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Category:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter category"
                                       data-table="stores"
                                       data-required="true"
                                       data-field="fk_category"
                                       data-type="select"
                                       data-value="<?= $storeData['fk_category']; ?>"
                                       data-value-source="category"
                                       data-id="<?= $storeData['id']; ?>">
                                        <?php
                                        echo $storeData['catname'];
                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Week Off Day:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Week Off Day"
                                       data-table="stores"
                                       data-required="false"
                                       data-field="int_weekoff"
                                       data-type="select"
                                       data-value="<?= $storeData['int_weekoff']; ?>"
                                       data-value-source="weeks"
                                       data-id="<?= $storeData['id']; ?>">
                                        <?php
                                        echo getDayNameFromNumber($storeData['int_weekoff']);
                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>No Of Book Seat:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter No Of Book Seat"
                                       data-placeholder="Enter No Of Book Seat"
                                       data-table="stores"
                                       data-required="true"
                                       data-validate-number="true"
                                       data-field="int_no_book_seats"
                                       data-input-class="form-control cuswidth var_address"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $storeData['id']; ?>"
                                    ><?= $storeData['int_no_book_seats']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Store Type:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Store"
                                       data-table="stores"
                                       data-required="true"
                                       data-field="enum_store_type"
                                       data-type="select"
                                       data-value="<?= $storeData['enum_store_type']; ?>"
                                       data-value-source="type"
                                       data-id="<?= $storeData['id']; ?>">
                                        <?php
                                        echo $storeData['enum_store_type'];
                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>



                        <div class="form-group ">
                            <label class="control-label col-md-4"><b>Store Logo:</b></label>
                            <div class="col-md-8">
                                <div class="fileinput fileinput-new userProfileImages"
                                     data-provides="fileinput">
                                    <div class="fileinput-preview thumbnail userProfilePicture"
                                         data-trigger="fileinput" style="width: 200px; height: 150px;">
                                        <label class="control-label expertsBio">
                                            <?php
                                            if (!empty( $storeData['var_logo'])) {
                                                $imgURL = base_url(STORE_LOGO_IMG) . "/" . $storeData['var_logo'];
                                            } else {
                                                $imgURL = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                                            }
                                            ?>
                                            <img class="userProfile" src="<?= $imgURL ?>"
                                                 style="max-width: 200px; max-height: 150px;">
                                        </label>

                                    </div>
                                    <div >
                                        <input type="hidden" name="user_id" class='user_id'
                                               value="<?= $storeData['id'] ?>">
                                        <input type="hidden" name="var_profile_image" class='oldImage'
                                               value="<?= $storeData['var_logo'] ?>">
                                        <input type="file" name="profile_pic" id="userProfileImage"
                                               class="userProfileImage" style="visibility: hidden"> </span>
                                        <div class="editable-buttons cusPicValue" style="display: none;">
                                            <button type="submit"
                                                    class="btn blue editable-submit updatePics"
                                                    data-table="users" data-id="<?= $storeData['id']; ?>">
                                                <i class="fa fa-check"></i></button>
                                            <button type="button"
                                                    class="btn default editable-cancel cancelPics"><i
                                                        class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Status:</b></label>
                            <div class="col-md-8 selectwidth">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Status"
                                       data-table="stores"
                                       data-required="false"
                                       data-field="enum_enable"
                                       data-type="select"
                                       data-value="<?= $storeData['enum_enable']; ?>"
                                       data-value-source="status"
                                       data-id="<?= $storeData['id']; ?>">
                                        <?php
                                        if($storeData['enum_enable']=='YES'){
                                            echo 'Enable';
                                        }else{
                                            echo 'Disable';
                                        }

                                        ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <?php
                        //                        echo "<pre>";
                        //                        print_r($subscriptionData);
                        //                        exit;
                        if (!empty($subscriptionData)) {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label label <?= ($subscriptionData['enum_subscription_type'] == 'UNIT-BLOCK') ? 'label-danger' : 'label-black' ?>  label-sm "> <?= $subScriptionType->{$subscriptionData['enum_subscription_type']} ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscriptions Deadline:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= date('d/m/Y', strtotime($subscriptionData['dt_end_date'])) ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group <?= ($subscriptionData['enum_subscription_type'] == 'PAY-AS-YOU-GO') ? 'hide' : ''; ?>">
                                <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= $subscriptionData['int_no_of_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Current Subscription:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= ($subscriptionData['int_calls_completed']) ? $subscriptionData['int_calls_completed'] : '0' ?>
                                        calls </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_price_per_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_total_price'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"><?= ($subscriptionData['account_manager']) ? $subscriptionData['account_manager'] : $subscriptionData['admin'] ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php

                        }

                        ?>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
