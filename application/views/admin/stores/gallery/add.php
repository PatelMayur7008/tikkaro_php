<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="col-md-3" style="padding-left: 0">
					<div class="caption">
						<i class="icon-settings font-green-haze"></i>
						<span class="caption-subject font-green-haze sbold uppercase"> Upload Image </span>
					</div>
				</div>

			</div>

			<div class="portlet-body">
				<div class="portlet-body">
					<form name="uploadimages" id="uploadimagesfrm" method="post" enctype="multipart/form-data"
						  action="<?php echo admin_url() . 'stores/uploadImages/'.$store_id; ?>" class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-md-4">Upload Image :</label>
							<div class="col-md-7">
                                <input type="hidden" name="store_code" value="<?= $store_id ?>">
							<input type="file" name="image_file[]" multiple="multiple">
							</div>
						</div>
						<div class="form-action" style="text-align: center;">
							<button type="submit" class="btn green btn-circle submit"> Submit</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
