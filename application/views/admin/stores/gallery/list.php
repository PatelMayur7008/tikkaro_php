
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= storeMenu('gallery', $storeId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Gallery</span>
                    </div>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?= admin_url('stores/add_image/'.$storeId)?>">
                        <i class="fa fa-plus"></i> Add Image
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="store_id" class="storeId" value="<?= $storeId ?>">
                    <div class="row">
                        <div class="col-md-12">
<!--                            --><?php // print_array($gallery_data); ?>
                            <?php  for($i=0;$i<count($gallery_data);$i++){
                                if($gallery_data[$i]['enum_ismain'] == 'YES'){
                                $src =  base_url().STORE_IMG.'/'. $gallery_data[$i]['var_file'];
                                ?>
                            <div class="col-md-4">
                                <img style="height: 100% !important;width: 100%;" src="<?= $src ?>">
                                <a href="javascript:;" class="delete-image" data-file="<?= $gallery_data[$i]['var_file'] ?>" data-galleryId="<?= $gallery_data[$i]['id']?>">Delete</a>
                                <a href="javascript:;" class="make-profile" data-storeId="<?= $storeId ?>" data-imageId="<?= $gallery_data[$i]['id'] ?>" >Make Image as Profile</a>
                            </div>
                            <?php } }?>
<!--                            <div class="col-md-4">-->
<!--                                <img style="height: 100% !important;width: 100%;" src="http://lofrev.net/wp-content/photos/2017/05/swissair_logo_1.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-4">-->
<!--                                <img style="height: 100% !important;width: 100%;" src="http://lofrev.net/wp-content/photos/2017/05/Whatsapp-Vector-Logo-2.png">-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #call_list_wrapper table.table > thead th:nth-child(6),
    #call_list_wrapper table.table > tbody td:nth-child(6) {
        display: none !important;
    }
</style>



