<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Store Service
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editStoreServiceFrm"
              action="<?php echo admin_url() . 'stores/edit_service/'.$service_id; ?>" class="form-horizontal">
            <div class="form-body">

                <!--                <div class="form-group">-->
                <!--                    <label class="col-md-3 control-label">Service<span class="required" aria-required="true">*</span>-->
                <!--                        :</label>-->
                <!--                    <div class="col-md-8">-->
                <!--                        <div class="input-icon right">-->
                <input type="hidden" name="service_id" value="<?= $service_id ?>">
                <input type="hidden" name="fk_store" value="<?= $service_detail['fk_store'] ?>">
                <!--                            <select class="form-control select2me" id="service" name="fk_service" data-placeholder="Select Service">-->
                <!--                                <option value=""></option>-->
                <!--                                --><?php
                //                                for ($i = 0; $i < count($service_list); $i++) {
                //                                    ?>
                <!---->
                <!--                                    <option value="--><? //= $service_list[$i]['id'] ?><!--">-->
                <? //= $service_list[$i]['var_title'] ?><!--</option>-->
                <!--                                    --><?php
                //                                }
                //                                ?>
                <!--                            </select>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                </div>-->

                <div class="form-group">
                    <label class="col-md-3 control-label">Price<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_price" name="var_price" placeholder="Enter Price"
                                   value="<?= $service_detail['var_price'] ?>"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Status
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="enum_enable" name="enum_enable"
                                    data-placeholder="Select Status">
                                <option value=""></option>
                                <?php
                                $statusArr = array('YES'=>'Enable','NO'=>'Disable');
                                foreach($statusArr as $key=>$val){
                                    $selected = ($service_detail['enum_enable'] == $key) ?'selected':'';
                                ?>
                                <option <?= $selected ?> value="<?= $key?>"><?= $val ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Service Time:</label>
                    <div class="col-md-4">
                        <div class="input-group date datetimepicker">
                            <input type="text" name="service_time" id="opening_time" size="16" value="<?= $service_detail['dt_service_time'] ?>"  readonly class="form-control">
                            <span class="input-group-btn">
                            </span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/services/'.$service_detail['fk_store']; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>