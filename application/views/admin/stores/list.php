<style type="text/css">
	.compliance > td {
		background-color: #d82735 !important;
		color: #fff;
	}

	table.dataTable .compliance td.sorting_1 {
		background-color: #d82735 !important;
		color: #fff;
	}

	.datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
		z-index: 99999999999 !important;
	}
</style>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="col-md-3" style="padding-left: 0">
					<div class="caption">
						<i class="icon-settings font-green-haze"></i>
						<span class="caption-subject font-green-haze sbold uppercase"> Store List </span>
					</div>
				</div>
				<div class="actions">
					<a class="btn btn-circle btn-default btn-sm"
					   href="<?= admin_url() . 'stores/add' ?>">
						<i class="fa fa-plus"></i> Add Store
					</a>
				</div>

			</div>
			<div class="portlet-body">
				<div class="portlet-body">
					<table class="table table-striped table-bordered table-hover" id="stores_list">
						<thead>
						<tr role="row" class="heading">
							<th> Title</th>
							<th> Address</th>
							<th> Phone</th>
							<th> Rating</th>
							<th> Category </th>
							<th> Store Type</th>
							<th> NO Of Seats </th>
							<th> Week Off </th>
							<th> Total Members </th>
							<th> Total Customer </th>
							<th> Total Service </th>
							<th> Store Status</th>
							<th> Action</th>
                            <th> id</th>
						</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="createCallModal" tabindex="-1" role="basic" aria-hidden="true"
	 style="z-index: 999999999 !important;">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Create Call</h4>
			</div>
			<form name="frmcreateCall" id="frmcreateCall" action="" method="post">
				<div class="modal-body">

					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4">
								<div class="form-group">

									<select class="form-control select2me client" name="client"
											data-placeholder="Select Client">
										<option value=""></option>
										<?php for ($i = 0; $i < count($allClient); $i++) { ?>
											<option
												value="<?= $allClient[$i]['id'] ?>" <?php if ($clientId == $allClient[$i]['id']) {
												echo "selected='selected'";
											} ?>
												data-account-id="<?= $allClient[$i]['fk_assign_account_manager'] ?>"><?= $allClient[$i]['var_company_name']; ?></option>
										<?php } ?>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<select class="form-control select2me project" name="project"
											data-placeholder="Select Project" id="clientProject">
										<option value=""></option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<select class="form-control select2me experts" name="experts"
											data-placeholder="Select experts" id="Projectexperts">
										<option value=""></option>
									</select>
								</div>
								<div class="clearfix"></div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<div class="input-group date datetimepicker ajaxDatePicker">
										<input type="text" size="16" readonly class="form-control calldatetime">
										<span class="input-group-btn">
													<button class="btn default date-set" type="button"><i
															class="fa fa-calendar"></i></button>
												</span>
									</div>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-12">
								<label class="TimeZoneDesc" ></label>
							</div>
						</div>
					</div>

				</div>
				<div class="modal-footer">
					<button type="button" class="btn green btn-circle createCall"> Create</button>
				</div>
			</form>
		</div>
	</div>
	<!-- /.modal-content -->
</div>
<div class="modal fade" id="editTranscriptModel" tabindex="-1" role="basic" aria-hidden="true"
	 style="z-index: 999999999 !important;">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Publish Transcription</h4>
			</div>
			<div class="modal-body">
				<div class="form-body">
					<h5>Update transcript</h5>
					<textarea class="form-control" id="txt_transcription" name="txt_transcription" rows="15"
							  autocomplete="off"></textarea>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn green btn-circle btnedittranscriptpopup"> Submit</button>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>

<div class="modal fade" id="publishModal" tabindex="-1" role="basic" aria-hidden="true"
	 style="z-index: 999999999 !important;">
	<div class="modal-dialog" style="width: 60%;">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Publish Transcription</h4>
			</div>
			<div class="modal-body">
				<div class="form-body">
					<h3>Are you sure you want to publishc a call transcription?</h3>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn green btn-circle btnpublishcallpopup"> Submit</button>
			</div>
		</div>
	</div>
	<!-- /.modal-content -->
</div>

