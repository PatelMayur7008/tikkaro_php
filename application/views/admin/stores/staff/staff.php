<style type="text/css">
    .compliance > td {
        background-color: #d82735 !important;
        color: #fff;
    }

    table.dataTable .compliance td.sorting_1 {
        background-color: #d82735 !important;
        color: #fff;
    }

    .datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
        z-index: 99999999999 !important;
    }

    .dataTables_filter {
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= storeMenu('staff', $storeId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Staff List </span>
                    </div>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?= admin_url('stores/add_staff/'.$storeId)?>">
                        <i class="fa fa-plus"></i> Add Staff
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="store_id" class="storeId" value="<?= $storeId ?>">
                    <table class="table table-striped table-bordered table-hover" id="staff_list">
                        <thead>
                        <tr role="row" class="heading">
                            <th> Name </th>
                            <th> Phone </th>
                            <th> Start Date</th>
                            <th> End Date </th>
                            <th> IsFrontView</th>
                            <th> Is current</th>
                            <th> Status </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #call_list_wrapper table.table > thead th:nth-child(6),
    #call_list_wrapper table.table > tbody td:nth-child(6) {
        display: none !important;
    }
</style>



