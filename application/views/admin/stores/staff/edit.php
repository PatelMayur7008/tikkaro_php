<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Staff
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editStorestaffFrm"
              action="<?php echo admin_url() . 'stores/edit_staff/'.$staff_data['id']; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Name <span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="store_id" value="<?= $storeId ?>">
                            <input type="hidden" name="staff_id" value="<?= $staff_data['id'] ?>">
                            <input type="text" id="var_name" name="var_name" placeholder="Enter Name"
                                   value="<?= $staff_data['var_name'] ?>"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Phone<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_phone" name="var_phone" placeholder="Enter Phone"
                                   value="<?= $staff_data['var_phone'] ?>" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Start Date
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input id="var_str_date" name="var_str_date" placeholder="Enter Start Date" for
                                   value="<?= date('d/m/Y', strtotime($staff_data['dt_startdate'])) ?>"
                                   class="form-control form-control-inline  date-picker" size="20" type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">End Date
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input id="var_end_date" name="var_end_date" placeholder="Enter End Date" for
                                   value="<?= (!empty($staff_data['dt_enddate']))? date('d/m/Y', strtotime($staff_data['dt_enddate'])) : '' ?>"
                                   class="form-control form-control-inline  date-picker" size="20" type="text">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Profile Pic:</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">

                                <?php if (!empty($staff_data['var_image'])) { ?>
                                    <input type="hidden" name="old_image" value="<?= $staff_data['var_image'] ?>" >
                                    <img src="<?= base_url().STAFF_PROFILE_IMG .'/'.$staff_data['var_image']?>" alt=""/>
                                <?php } else { ?>
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                                <?php } ?>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="profile_pic"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                    Remove </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Is FrontView
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <?php
                            $checked = ($staff_data['enum_isFrontView'] == 'YES') ? 'checked' : '';
                            ?>
                            <input type="checkbox" id="is_front" name="is_front" <?= $checked ?>
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">is Current
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <?php
                            $checked = ($staff_data['enum_current'] == 'YES') ? 'checked' : '';
                            ?>
                            <input type="checkbox" id="is_current" name="is_current" <?= $checked ?>
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">is Active
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <?php
                            $checked = ($staff_data['enum_active'] == 'YES') ? 'checked' : '';
                            ?>
                            <input type="checkbox" id="is_active" name="is_active" <?= $checked ?>
                                   class="form-control">
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/staff/'.$storeId; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>