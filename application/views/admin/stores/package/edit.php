<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Package
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editPackageFrm"
              action="<?php echo admin_url() . 'stores/edit_package/'.$package_data['packages'][0]['id']; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Name <span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="store_id" value="<?= $storeId ?>">
                            <input type="hidden" name="package_id" value="<?= $package_data['packages'][0]['id']  ?>">
                            <input type="text" id="var_name" name="var_name"  value="<?= $package_data['packages'][0]['var_name'] ?>" placeholder="Enter Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Services
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" multiple id="services" name="services[]"
                                    data-placeholder="Select Services">
                                <option value=""></option>
                                <?php
                                for($i=0;$i<count($service);$i++) {
                                    $selected ="";
                                    for($j=0;$j<count($package_data['packages'][0]['services']);$j++) {
                                        if ($service[$i]['fk_service'] == $package_data['packages'][0]['services'][$j]['id']) {
                                            $selected = "selected = 'selected'";
                                            break;
                                        }
                                        }
                                        ?>
                                        <option <?=$selected?> value="<?= $service[$i]['fk_service'] ?>"> <?= $service[$i]['var_title'] ?></option>
                                        <?php

                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Price<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_price" name="var_price" value="<?= $package_data['packages'][0]['var_price'] ?>" placeholder="Enter Price"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Note:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea id="Note" name="note" placeholder="Enter Note" class="form-control"><?= $package_data['packages'][0]['txt_note'] ?> </textarea>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Status
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me"  id="enum_status" name="enum_status"
                                    data-placeholder="Select Services">
                                <option value=""></option>
                                    <option <?php echo ($package_data['packages'][0]['enum_enable'] == 'YES') ? 'selected': ''; ?> value="YES"> Enable</option>
                                    <option <?php echo ($package_data['packages'][0]['enum_enable'] == 'No') ? 'selected': ''; ?>  value="NO"> Disable</option>

                            </select>
                        </div>
                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/package/'.$storeId; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>