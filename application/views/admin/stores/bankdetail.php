<style type="text/css">
    .datetimepicker.datetimepicker-dropdown-bottom-left.dropdown-menu {
        z-index: 99999999999 !important;
    }

    .dataTables_filter {
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= storeMenu('bankdetail', $storeId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Bank Details List </span>
                    </div>
                </div>
                <?php if(empty($bankData)){ ?>
                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?= admin_url('stores/updateBankDetail/'.$storeId)?>">
                        <i class="fa fa-plus"></i> Add Bank Details
                    </a>
                </div>
                <?php }?>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="store_id" class="storeId" value="<?= $storeId ?>">
                    <table class="table table-striped table-bordered table-hover" id="bank_list">
                        <thead>
                        <tr role="row" class="heading">
                            <th> Account No </th>
                            <th> Ifsc code </th>
                            <th> Name </th>
                            <th> Bank Name </th>
                            <th> Branch Name </th>
                            <th> Address </th>
                            <th> Account Type </th>
                            <th> Card Type </th>
                            <th> Card No </th>
                            <th> Action </th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>