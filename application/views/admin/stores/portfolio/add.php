<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="portlet light portlet-fit portlet-datatable bordered">
			<div class="portlet-title">
				<div class="col-md-3" style="padding-left: 0">
					<div class="caption">
						<i class="icon-settings font-green-haze"></i>
						<span class="caption-subject font-green-haze sbold uppercase"> Add Portfolio </span>
					</div>
				</div>

			</div>

			<div class="portlet-body">
				<div class="portlet-body">
					<form name="addPortfolio" id="addPortfoliofrm" method="post" enctype="multipart/form-data"
						  action="<?php echo admin_url() . 'stores/add_portfolio/'.$store_id; ?>" class="form-horizontal">
						<div class="form-group">
							<label class="control-label col-md-3">Upload Image :</label>
							<div class="col-md-8">
                                <input type="hidden" name="store_code" value="<?= $store_id ?>">
							<input type="file" name="image_file" >
							</div>
						</div>
                        <div class="form-group">
                            <label class="col-md-3 control-label">Var Title<span class="required" aria-required="true">*</span>
                                :</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <input type="text" id="var_title" name="var_title" placeholder="Enter Title" class="form-control">
                                </div>
                            </div>
                        </div>
						<div class="form-action" style="text-align: center;">
							<button type="submit" class="btn green btn-circle submit"> Submit</button>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
