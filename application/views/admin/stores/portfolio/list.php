
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= storeMenu('portfolio', $storeId) ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Portfolio</span>
                    </div>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?= admin_url('stores/add_portfolio/'.$storeId)?>">
                        <i class="fa fa-plus"></i> Add Portfolio
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <input type="hidden" name="store_id" class="storeId" value="<?= $storeId ?>">
                    <div class="row">
                        <div class="col-md-12">
<!--                            --><?php // print_array($portfolio_data); ?>
                            <?php  for($i=0;$i<count($portfolio_data);$i++){
                               $src =  base_url().PORTFOLIO_IMG.'/'. $portfolio_data[$i]['var_file'];
                                ?>
                            <div class="col-md-4">
                                <img style="height: 100% !important;width: 100%;" src="<?= $src ?>">
                                <span><?= $portfolio_data[$i]['var_title'] ?></span>
<!--                                <span>-->
<!--                                    <a title="Delete" class="btn blue btn-xs" data-toggle="modal" href="--><?//= admin_url('stores/deletePortfolio/').$portfolio_data[$i]['id'] ?><!--"><i class="fa fa-trash"></i> Delete </a>-->
<!--                                </span>-->
                            </div>
                            <?php  }?>
<!--                            <div class="col-md-4">-->
<!--                                <img style="height: 100% !important;width: 100%;" src="http://lofrev.net/wp-content/photos/2017/05/swissair_logo_1.png">-->
<!--                            </div>-->
<!--                            <div class="col-md-4">-->
<!--                                <img style="height: 100% !important;width: 100%;" src="http://lofrev.net/wp-content/photos/2017/05/Whatsapp-Vector-Logo-2.png">-->
<!--                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #call_list_wrapper table.table > thead th:nth-child(6),
    #call_list_wrapper table.table > tbody td:nth-child(6) {
        display: none !important;
    }
</style>



