

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Edit Store Timing
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="editStoreTimingFrm"
              action="<?php echo admin_url() . 'stores/edit_timing/'.$timing_id; ?>" class="form-horizontal">
            <div class="form-body">
                <input type="hidden" name="timing_id" value="<?= $timing_id ?>">
                <input type="hidden" name="fk_store" value="<?= $timing_detail['fk_store'] ?>">

                <div class="form-group">
                    <label class="col-md-3 control-label">Day<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <label class="col-md-3 control-label" style="text-align: left">
                                <?= getDayNameFromNumber($timing_detail['int_week']) ?>
                                </label>
                        </div>
                    </div>
                </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Opening Time:</label>
                        <div class="col-md-4">
                            <div class="input-group date datetimepicker">
                                <input type="text" name="opening_time" id="opening_time" size="16" value="<?= $timing_detail['dt_open_time'] ?>" readonly class="form-control">
                                <span class="input-group-btn">
                            </span>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Closing Time:</label>
                        <div class="col-md-4">
                            <div class="input-group date datetimepicker">
                                <input type="text" name="closing_time" id="closing_time" size="16" value="<?= $timing_detail['dt_close_time'] ?>" readonly class="form-control">
                                <span class="input-group-btn">
                            </span>
                            </div>
                        </div>
                    </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'stores/timing/'.$timing_detail['fk_store']; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>