<!--[if lt IE 9]>
<script src="<?= base_url(); ?>public/assets/global/plugins/respond.min.js?v=<?= REVISED_VERSION; ?>"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/excanvas.min.js?v=<?= REVISED_VERSION; ?>"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->

<script src="<?= base_url(); ?>public/assets/global/plugins/jquery.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap/js/bootstrap.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/js.cookie.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/jquery.blockui.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/uniform/jquery.uniform.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/jquery-validation/js/jquery.validate.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap-toastr/toastr.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/scripts/datatable.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/datatables/datatables.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/jquery.mockjax.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!--<script src="--><? //= base_url(); ?><!--public/assets/global/plugins/select2/js/select2.full.min.js?v=<?= FRONT_REVISED_VERSION; ?>"" type="text/javascript"></script>-->

<script src="<?= base_url(); ?>public/assets/global/plugins/select2/js/select2.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<?php
if (!empty($js_plugin)) {
    foreach ($js_plugin as $value) {
        ?>
        <script src="<?= base_url(); ?>public/assets/global/plugins/<?php echo $value ?>?v=<?= REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<!-- BEGIN THEME GLOBAL SCRIPTS -->

<script src="<?= base_url(); ?>public/assets/global/scripts/app.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/global/plugins/moment.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?= base_url(); ?>public/assets/javascripts/common_function.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>

<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= base_url(); ?>public/assets/layouts/layout/scripts/layout.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<!--<script src="--><? //= base_url(); ?><!--public/assets/layouts/layout/scripts/demo.js?v=--><? //= REVISED_VERSION; ?><!--"-->
<!--type="text/javascript"></script>-->
<!-- END THEME LAYOUT SCRIPTS -->

<?php
if (!empty($js)) {
    foreach ($js as $value) {
        ?>
        <script src="<?php echo base_url(); ?>public/assets/javascripts/<?php echo $value ?>?v=<?= REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<script src="<?php echo base_url(); ?>public/assets/pages/scripts/components-date-time-pickers.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script src="<?php echo base_url(); ?>public/assets/pages/scripts/table-datatables-managed.min.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        App.init();
        <?php
        if (!empty($init)) {
            foreach ($init as $value) {
                echo $value . ';';
            }
        }
        ?>
    });
</script>
<?php
if ($this->session->flashdata('status') != '' && $this->session->flashdata('message') != '') {
    ?>
    <script>
        Toastr.init("<?= $this->session->flashdata('status'); ?>", "<?= $this->session->flashdata('message'); ?>", "");
    </script>
    <?php
}
?>
<!--start  delete model-->
<div id="myModal_autocomplete" class="modal fade delete_modal" style="padding-top: 20px;" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-circle btn-default" data-dismiss="modal">Close</button>
                <button type="submit" id="btndelete" data-url=""
                        class="btn red btn-circle btndelete delete-records-modal"><i class="fa fa-check"></i>Delete
                </button>
            </div>
        </div>
    </div>
</div>

<!--End  delete model-->

<!--/* myModal_autocomplete */-->
<!--start  delete model-->
<div id="delete_model" class="modal fade delete_modal" style="padding-top: 20px;" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Delete Confirmation</h4>
            </div>
            <div class="modal-body">
                <h3>Are you Sure to Delete ?...</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-circle" data-dismiss="modal">Close</button>
                <button type="submit" id="btndelete" data-url=""
                        class="btn red btndelete delete-records-modal btn-circle"><i class="fa fa-check"></i>Delete
                </button>
            </div>
        </div>
    </div>
</div>

<!--End  delete model-->
<!--start  delete model-->
<div id="unblock_experts" class="modal fade unblock_experts" style="padding-top: 20px;" role="dialog"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Expert Unblock Confirmation</h4>
            </div>
            <div class="modal-body">
                <h4>Are you Sure to unblock this experts ?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-circle" data-dismiss="modal">Close</button>
                <button type="submit" id="experts_unblock"
                        class="btn blue btn-circle"><i class="fa fa-check"></i> Unblock
                </button>
            </div>
        </div>
    </div>
</div>

<!--End  delete model-->


<!-- ADD NOTES MODEL -->


<!--Block Model-->

<div class="modal fade" id="block_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;" id="block-model">
    <div class="modal-dialog">
        <form class="form-horizontal" id="block_frm" name="block_frm" method="POST">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Block</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group" class="block">
                            <label class="col-md-3 control-label">Date :</label>
                            <div class="col-md-7">
                                <div class="input-group input-large date-picker input-daterange"
                                     data-date-format="dd/mm/yyyy">
                                    <input class="form-control from" name="from" type="text">
                                    <span class="input-group-addon"> to </span>
                                    <input class="form-control to" name="to" type="text">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-6 md-checkbox">
                                <b>Or</b>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-offset-5 md-checkbox">
                                <input type="checkbox" id="checkbox1_2" name="block" class="md-check checkbox1">
                                <label for="checkbox1_2">
                                    <span></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Block permanent </label>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label">Reason <span class="required"> * </span>:</label>
                            <div class="col-md-7">
                                <div class="input-icon right">
                                    <input type="hidden" name="expertId" id="expertBlockId">
                                    <textarea class="form-control" id="txt_reason" name="txt_reason"
                                              placeholder="Enter Reason" rows="3"></textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger btn-circle block">Block
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>

<!--end Block model-->

