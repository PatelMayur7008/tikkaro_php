<div class="page-footer">
    <div class="page-footer-inner"> <?= date('Y') ?> &copy; <?= get_project_name(); ?>.</div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>

<div class="modal fade" id="cancelcallModal" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Call Cancel</h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <h3>Are you sure you want to cancel a call?</h3>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-circle btncancellcallpopup"> Submit</button>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>

<div class="modal fade" id="transcriptionModal" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog" style="width: 60%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Transcription</h4>
            </div>
            <div class="modal-body">
                <div class="form-body">
                    <textarea style="width: 100%; resize: vertical; height: 67vh" rows="5" name="transcriptiontxt" id="transcriptiontxt"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn green btn-circle btntranscriptionpopup"> Submit</button>
            </div>
        </div>
    </div>
    <!-- /.modal-content -->
</div>
