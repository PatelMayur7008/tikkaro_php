-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2018 at 06:05 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikkaro`
--

-- --------------------------------------------------------

--
-- Table structure for table `tk_admin`
--

CREATE TABLE `tk_admin` (
  `id` int(11) NOT NULL,
  `var_fname` varchar(100) NOT NULL,
  `var_lname` varchar(100) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `var_password` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `txt_address` text,
  `dt_dob` datetime DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_admin`
--

INSERT INTO `tk_admin` (`id`, `var_fname`, `var_lname`, `var_email`, `var_password`, `bint_phone`, `var_profile_image`, `txt_address`, `dt_dob`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@admin.com', 'e6e061838856bf47e1de730719fb2609', '9727190205', 'Profile_1515325777.jpg', 'Lorem Ipsum text here...123', '1994-05-19 00:00:00', 'YES', '2017-11-03 00:00:00', '2018-01-07 11:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barbers`
--

CREATE TABLE `tk_barbers` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `txt_address` text,
  `var_rating` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barbers`
--

INSERT INTO `tk_barbers` (`id`, `var_name`, `bint_phone`, `txt_address`, `var_rating`, `created_at`, `updated_at`) VALUES
(1, 'Jignesh vanand', '8487546789', 'Lorem ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(2, 'Rutvik Vanand', '5454878952', 'Lorem Ipsum text here', 4, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(3, 'Nirav patel', '9854894556', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(4, 'Mayur Patel', '7894855896', 'Lorem ipsum text here..', 4, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(5, 'Gaurav Prajapati', '13213132132132', 'Lorem Ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35'),
(6, 'Jayesh Mojidra', '54654654321321', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barber_types`
--

CREATE TABLE `tk_barber_types` (
  `id` int(11) NOT NULL,
  `var_type` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barber_types`
--

INSERT INTO `tk_barber_types` (`id`, `var_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53'),
(2, 'Worker', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking`
--

CREATE TABLE `tk_booking` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `dt_booking_date` datetime NOT NULL,
  `var_price` varchar(100) NOT NULL,
  `enum_book_type` enum('CANCEL','COMPLETED','UPCOMING') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking`
--

INSERT INTO `tk_booking` (`id`, `fk_store`, `fk_user`, `dt_booking_date`, `var_price`, `enum_book_type`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-12 03:09:18', '500', 'UPCOMING', '2018-01-10 22:43:46', '2018-01-20 18:10:41'),
(2, 2, 1, '2018-01-12 06:43:22', '2000', 'CANCEL', '2018-01-10 23:19:07', '2018-01-11 19:26:57'),
(3, 2, 1, '2018-01-12 00:11:53', '120', 'COMPLETED', '2018-01-12 00:11:53', '2018-01-27 15:16:55'),
(4, 2, 19, '2018-01-27 07:24:24', '500', 'COMPLETED', '2018-01-27 00:06:41', '2018-01-26 18:36:41'),
(5, 2, 19, '2018-01-27 00:00:00', '200', 'CANCEL', '2018-01-27 00:06:41', '2018-01-26 18:36:41'),
(6, 2, 19, '2018-01-27 00:00:00', '5000', 'UPCOMING', '2018-01-27 00:09:50', '2018-01-26 18:39:50'),
(7, 2, 19, '2018-01-27 00:00:00', '5000', 'CANCEL', '2018-01-27 00:34:50', '2018-01-26 19:06:51'),
(8, 2, 20, '2018-01-24 00:00:00', '2000', 'CANCEL', '2018-01-27 00:34:50', '2018-02-12 17:00:56'),
(9, 2, 20, '2018-01-27 00:00:00', '8000', 'COMPLETED', '2018-01-27 00:35:40', '2018-02-12 17:01:02'),
(10, 1, 21, '2018-02-18 00:35:40', '6000', 'UPCOMING', '2018-01-27 00:35:40', '2018-02-18 09:19:52'),
(11, 3, 20, '2018-02-18 00:36:18', '8000', 'UPCOMING', '2018-01-27 00:36:18', '2018-02-18 09:18:51'),
(12, 3, 21, '2018-02-18 00:36:18', '750', 'COMPLETED', '2018-01-27 00:36:18', '2018-02-18 09:19:00');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_service`
--

CREATE TABLE `tk_booking_has_service` (
  `id` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_service`
--

INSERT INTO `tk_booking_has_service` (`id`, `fk_booking`, `fk_service`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-01-10 22:45:13', '2018-01-10 17:15:13'),
(2, 1, 3, '2018-01-10 22:45:13', '2018-01-10 17:15:13'),
(3, 2, 3, '2018-01-12 00:15:35', '2018-01-11 18:45:35'),
(4, 3, 5, '2018-01-12 00:15:35', '2018-01-11 18:45:35'),
(5, 4, 2, '2018-01-27 00:07:10', '2018-01-26 18:37:10'),
(6, 5, 2, '2018-01-27 00:07:10', '2018-01-26 18:37:10'),
(7, 6, 3, '2018-01-27 00:10:14', '2018-01-26 18:40:14'),
(8, 6, 9, '2018-01-27 00:10:14', '2018-01-26 18:40:14'),
(9, 4, 6, '2018-01-27 00:30:48', '2018-01-26 19:00:48'),
(10, 4, 9, '2018-01-27 00:30:48', '2018-01-26 19:00:48'),
(11, 5, 3, '2018-01-27 00:32:36', '2018-01-26 19:02:36'),
(12, 5, 11, '2018-01-27 00:32:36', '2018-01-26 19:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_timeslot`
--

CREATE TABLE `tk_booking_has_timeslot` (
  `id` int(11) NOT NULL,
  `fk_timeslot` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_timeslot`
--

INSERT INTO `tk_booking_has_timeslot` (`id`, `fk_timeslot`, `fk_booking`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-10 22:45:45', '2018-01-10 17:15:45'),
(2, 3, 1, '2018-01-10 22:45:45', '2018-01-20 17:13:35'),
(3, 4, 10, '2018-01-12 00:16:18', '2018-01-27 15:38:46'),
(4, 6, 3, '2018-01-12 00:16:18', '2018-01-27 15:17:30'),
(5, 11, 3, '2018-01-27 00:07:32', '2018-01-27 16:48:56'),
(6, 3, 5, '2018-01-27 00:07:32', '2018-01-26 18:37:32'),
(7, 3, 6, '2018-01-27 00:10:38', '2018-01-26 18:40:38'),
(8, 15, 6, '2018-01-27 00:10:38', '2018-01-26 18:40:38'),
(9, 5, 4, '2018-01-27 00:31:42', '2018-01-26 19:01:42'),
(10, 37, 2, '2018-01-27 00:31:42', '2018-01-27 15:23:13'),
(11, 38, 2, '2018-01-27 00:33:03', '2018-01-27 15:38:31'),
(12, 48, 5, '2018-01-27 00:33:03', '2018-01-26 19:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness`
--

CREATE TABLE `tk_bussiness` (
  `id` int(11) NOT NULL,
  `var_bussiness_code` varchar(100) DEFAULT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_phone` varchar(50) DEFAULT NULL,
  `int_otp` int(11) DEFAULT NULL,
  `var_email` varchar(100) DEFAULT NULL,
  `txt_note` text,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness`
--

INSERT INTO `tk_bussiness` (`id`, `var_bussiness_code`, `var_name`, `var_phone`, `int_otp`, `var_email`, `txt_note`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'ABC123', 'Barber', '9856321456', 511839, 'barber@gmail.com', 'tes notasdasdasdsadsadsad', 'YES', '2018-01-22 22:17:52', '2018-01-22 16:47:52'),
(4, 'test123', 'test 789', 'test456@gmail.com', NULL, '7778788778', 'asfsd sdfads asdfa s ', 'YES', '2018-01-22 18:11:09', '2018-01-22 18:11:09'),
(5, 'SVj360', 'tesetest', 'weser@sfsgsd.com', NULL, '87545454545', 'gsdfgsdfgdfg ', 'YES', '2018-02-07 17:24:01', '2018-02-07 17:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_category`
--

CREATE TABLE `tk_bussiness_category` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_category`
--

INSERT INTO `tk_bussiness_category` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'test First category', '2018-01-22 22:16:27', '2018-01-22 16:46:27'),
(2, 'test Second category', '2018-01-22 22:16:27', '2018-01-22 16:46:27'),
(3, 'test Third category', '2018-01-22 22:16:54', '2018-01-22 16:46:54'),
(4, 'test Fourth category', '2018-01-22 22:16:54', '2018-01-22 16:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_has_category`
--

CREATE TABLE `tk_bussiness_has_category` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_has_category`
--

INSERT INTO `tk_bussiness_has_category` (`id`, `fk_bussiness`, `fk_category`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-01-22 22:25:22', '2018-01-22 16:55:22'),
(2, 1, 2, '2018-01-22 22:25:22', '2018-01-22 16:55:22'),
(5, 4, 2, '0000-00-00 00:00:00', '2018-01-22 18:11:09'),
(6, 4, 4, '0000-00-00 00:00:00', '2018-01-22 18:11:09'),
(7, 5, 1, '2018-02-07 17:24:01', '2018-02-07 17:24:01'),
(8, 5, 2, '2018-02-07 17:24:02', '2018-02-07 17:24:02'),
(9, 5, 3, '2018-02-07 17:24:02', '2018-02-07 17:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `tk_extra_facility`
--

CREATE TABLE `tk_extra_facility` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_extra_facility`
--

INSERT INTO `tk_extra_facility` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'WIFI', 'YES', '2018-01-09 23:14:11', '2018-01-28 09:12:23'),
(2, 'AC', 'YES', '2018-01-09 23:14:11', '2018-01-09 17:44:11'),
(3, 'Game Area', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32'),
(4, 'Movie Zone', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_master_category`
--

CREATE TABLE `tk_master_category` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `enum_type` enum('SERVICE','STORE') NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_master_category`
--

INSERT INTO `tk_master_category` (`id`, `var_name`, `enum_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'TEst one category', 'SERVICE', 'YES', '2018-02-21 23:08:47', '2018-02-21 17:38:47'),
(2, 'Test two cat2', 'STORE', 'NO', '2018-02-21 23:09:26', '2018-02-21 17:39:26'),
(3, 'test cat 456', 'STORE', 'YES', '2018-02-21 18:00:21', '2018-02-21 18:00:21'),
(4, 'test service cat1', 'SERVICE', 'YES', '2018-02-22 16:35:06', '2018-02-22 16:35:06'),
(5, 'test service cat2', 'SERVICE', 'YES', '2018-02-22 16:35:21', '2018-02-22 16:35:21');

-- --------------------------------------------------------

--
-- Table structure for table `tk_migrations`
--

CREATE TABLE `tk_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_migrations`
--

INSERT INTO `tk_migrations` (`version`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `tk_offer_has_service`
--

CREATE TABLE `tk_offer_has_service` (
  `id` int(11) NOT NULL,
  `fk_offer` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_offer_has_service`
--

INSERT INTO `tk_offer_has_service` (`id`, `fk_offer`, `fk_service`, `var_price`, `var_offer_price`, `created_at`, `updated_at`) VALUES
(21, 4, 1, '150', '333', '2018-02-17 19:58:33', '2018-02-17 19:58:33'),
(22, 4, 4, '2500', '99', '2018-02-17 19:58:34', '2018-02-17 19:58:34'),
(25, 5, 1, '50', '50', '2018-02-18 09:29:45', '2018-02-18 09:29:45');

-- --------------------------------------------------------

--
-- Table structure for table `tk_package_has_service`
--

CREATE TABLE `tk_package_has_service` (
  `id` int(11) NOT NULL,
  `fk_package` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_package_has_service`
--

INSERT INTO `tk_package_has_service` (`id`, `fk_package`, `fk_service`, `var_price`, `created_at`, `updated_at`) VALUES
(10, 2, 1, '78', '2018-01-28 17:17:18', '2018-01-28 17:17:18'),
(11, 2, 6, '85', '2018-01-28 17:17:19', '2018-01-28 17:17:19'),
(23, 5, 2, '90', '2018-01-30 17:16:15', '2018-01-30 17:16:15'),
(24, 5, 3, '80', '2018-01-30 17:16:15', '2018-01-30 17:16:15'),
(72, 1, 3, '111', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(73, 1, 2, '11111111', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(74, 1, 1, '11', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(77, 6, 1, '150', '2018-02-02 18:06:49', '2018-02-02 18:06:49'),
(78, 6, 10, '333', '2018-02-02 18:06:49', '2018-02-02 18:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `tk_services`
--

CREATE TABLE `tk_services` (
  `id` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_services`
--

INSERT INTO `tk_services` (`id`, `fk_category`, `var_title`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hair cut', 'YES', '2017-11-04 00:00:00', '2018-02-21 18:16:16'),
(2, 1, 'Saving', 'YES', '2017-11-04 00:00:00', '2018-02-21 18:16:44'),
(3, 1, 'Clipper Cut', 'YES', '2017-11-04 00:00:00', '2018-02-21 18:16:44'),
(4, 5, 'The Sides', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:55:22'),
(5, 1, 'Hot Towel Head Shave', 'YES', '2017-11-04 00:00:00', '2018-02-21 18:16:44'),
(6, 1, 'Hot Towel Face Shave', 'YES', '2017-11-04 00:00:00', '2018-02-21 18:16:44'),
(8, 1, 'asdasd456fgdf', 'YES', '2018-01-04 18:47:42', '2018-02-21 18:16:44'),
(9, 1, 'Hair cut 123', 'YES', '0000-00-00 00:00:00', '2018-02-21 18:16:44'),
(10, 1, 'Hair cut 456', 'YES', '2018-01-19 18:47:40', '2018-02-21 18:16:44'),
(11, 1, 'Hair spa', 'YES', '2018-01-22 18:11:20', '2018-02-21 18:16:44'),
(12, 5, 'test service 777', 'YES', '2018-02-22 16:38:49', '2018-02-22 16:58:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_setting`
--

CREATE TABLE `tk_setting` (
  `id` int(11) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_setting`
--

INSERT INTO `tk_setting` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'var_fb_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(2, 'var_insta_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(3, 'var_twitter_link', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24'),
(4, 'var_whatsapp_no', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24');

-- --------------------------------------------------------

--
-- Table structure for table `tk_stores`
--

CREATE TABLE `tk_stores` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `fk_category` int(11) DEFAULT NULL,
  `var_title` varchar(255) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `txt_note` text,
  `var_store_code` varchar(255) NOT NULL,
  `int_otp` int(11) DEFAULT NULL,
  `txt_address` text,
  `var_logo` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) DEFAULT NULL,
  `var_longitude` varchar(255) DEFAULT NULL,
  `int_areacode` int(11) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `int_weekoff` int(11) DEFAULT NULL,
  `int_no_book_seats` int(11) DEFAULT NULL,
  `var_fb_link` varchar(255) DEFAULT NULL,
  `var_twitter_link` varchar(255) DEFAULT NULL,
  `var_instagram_link` varchar(255) DEFAULT NULL,
  `var_whatsapp_no` varchar(50) DEFAULT NULL,
  `var_rating` varchar(20) DEFAULT NULL,
  `enum_store_type` enum('MEN','WOMEN','BOTH') NOT NULL DEFAULT 'MEN',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_stores`
--

INSERT INTO `tk_stores` (`id`, `fk_bussiness`, `fk_category`, `var_title`, `var_email`, `txt_note`, `var_store_code`, `int_otp`, `txt_address`, `var_logo`, `var_latitude`, `var_longitude`, `int_areacode`, `bint_phone`, `int_weekoff`, `int_no_book_seats`, `var_fb_link`, `var_twitter_link`, `var_instagram_link`, `var_whatsapp_no`, `var_rating`, `enum_store_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 'Hairitage Unisex Salon', NULL, NULL, 'caz347', NULL, 'GF.10, Dev-preet Complex Opp.Brewberrys Coffee Bar N.F.D Circle, Cross Raod Bodakdev, Bodakdev, Ahmedabad, Gujarat 380054', 'store_logo_1518952488.jpg', '23.043459', '72.520108', NULL, '456798231312', 1, 1, NULL, NULL, NULL, NULL, '5', 'BOTH', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(2, 1, 2, 'Truefitt & Hill', 'testing@gmail.com', 'testy', 'ABCGHitk007', 444879, '77, Western Express Hwy, Tanji Nagar, Kurar Village, Malad East, Mumbai, Maharashtra 400097, India', 'store_logo_1518952478.jpg', '19.188591', '72.85870299999999', 400097, '8884445454', 1, 4, 'www.fb.com/test', 'www.twitter.com/test', 'www.insta.com/test', '88866633', '4', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(3, 1, 2, 'Classic Hair Care', NULL, NULL, 'bio412', NULL, 'Newyork Trade Centre, S.G. Highway, Thaltej, Ahmedabad, Gujarat 380059', 'store_logo_1518952450.png', '23.040054', '72.513088', NULL, '565565656', 0, 1, NULL, NULL, NULL, NULL, '3.3', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(4, 1, 2, 'Ai Shree Khodiyar Hair Style', NULL, NULL, 'FIq180', NULL, 'Maharana Pratap Rd, Vishwas City 1, Chanakyapuri, Ahmedabad, Gujarat 380061', 'store_logo_1518952436.png', '23.080365', '72.535798', NULL, '213265483141', 3, 1, NULL, NULL, NULL, NULL, '4.1', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(5, 1, 2, 'Vaibhav Hair Art', NULL, NULL, 'uVP369', NULL, 'Asopalav Apartment, Mangal Murti, Naranpur Telephone Exchange Road, Naranpura, Ahmedabad, Gujarat 380063', 'store_logo_1518952425.jpg', '23.060193', '72.547983', NULL, '798798564564', 2, 1, NULL, NULL, NULL, NULL, NULL, 'MEN', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(6, 1, 2, 'Deepkala Hair Art', NULL, NULL, 'Atr568', NULL, '12/B Janta Market Society, Vejalpur, Ahmedabad, Gujarat 380051', 'store_logo_1518952411.jpg', '23.003413', '72.518396', NULL, '989565656512', 5, 1, NULL, NULL, NULL, NULL, NULL, 'WOMEN', 'YES', '2017-11-04 00:00:00', '2018-02-22 16:28:49'),
(7, 1, 2, 'Example Store', NULL, NULL, 'hJC397', NULL, '120, New Link Road, Kandivali West, Mumbai, Maharashtra, India', 'store_logo_1518952401.jpg', '19.206802', '72.83462499999996', 400067, '98654124', 5, 10, NULL, NULL, NULL, NULL, '2.2', 'MEN', 'YES', '2018-01-11 19:19:59', '2018-02-22 16:28:49'),
(11, 1, 2, 'TEST STORE', NULL, NULL, 'itT300', NULL, '154, Western Express Highway, Park Society, Mumbai, Maharashtra, India', 'store_logo_1518952389.jpg', '19.0974079', '72.85294099999999', 400057, '566565', 0, 12, NULL, NULL, NULL, NULL, '3.5', 'WOMEN', 'YES', '2018-01-24 16:53:15', '2018-02-22 16:28:49'),
(12, 1, 2, 'TEST LOGO IMG', NULL, NULL, 'XWa516', NULL, '785, Outer Ring Road, Nagarjun Enclave, 1st Phase, JP Nagar, Bengaluru, Karnataka, India', 'store_logo_1518952374.jpg', '12.906821', '77.57725800000003', 560078, '9856321456', NULL, 40, NULL, NULL, NULL, NULL, '1.3', 'MEN', 'YES', '2018-02-17 08:36:49', '2018-02-22 16:28:49'),
(23, 1, 2, 'test store', NULL, NULL, 'PQB715', NULL, '157, Hosur Road, Konappana Agrahara, Electronic City, Bengaluru, Karnataka, India', 'store_logo_1519229774.jpg', '12.8562688', '77.66444960000001', 560100, '6666666666', NULL, 3, NULL, NULL, NULL, NULL, '1.3', 'BOTH', 'YES', '2018-02-21 16:16:14', '2018-02-22 16:28:49'),
(24, 1, 3, 'asdasd', NULL, NULL, 'oFb542', NULL, 'sfsdsdfasfasd', 'store_logo_1519236836.jpg', NULL, NULL, NULL, '9856321456', NULL, 4, NULL, NULL, NULL, NULL, '1.3', 'MEN', 'YES', '2018-02-21 18:13:56', '2018-02-22 17:03:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_bank_details`
--

CREATE TABLE `tk_store_has_bank_details` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_accountno` varchar(255) NOT NULL,
  `var_ifsc_code` varchar(255) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `var_bank_name` varchar(500) NOT NULL,
  `var_branch_name` varchar(500) NOT NULL,
  `txt_address` text NOT NULL,
  `enum_acc_type` enum('S','C') NOT NULL COMMENT '''S''=>''Saving'',''C''=>''Current''',
  `var_debit_card_type` varchar(255) NOT NULL,
  `var_card_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_bank_details`
--

INSERT INTO `tk_store_has_bank_details` (`id`, `fk_store`, `var_accountno`, `var_ifsc_code`, `var_name`, `var_bank_name`, `var_branch_name`, `txt_address`, `enum_acc_type`, `var_debit_card_type`, `var_card_number`, `created_at`, `updated_at`) VALUES
(1, 6, '999885554', 'unb0012', 'jay shah', 'union bank', 'vastral', 'vastral', 'C', 'debit', '123456987412', '2018-01-12 17:41:45', '2018-01-12 17:42:11'),
(3, 2, 'hdfhdfh7878121212', 'fsdfa', 'TEST', 'state bank of india', 'VASTRAL', 'TRST', '', 'D', 'asdasdasdgsdgfhfdhdfhdfhfd', '2018-01-27 13:38:34', '2018-02-14 16:45:16'),
(4, 7, '778788778', 'icici785412', 'test', 'test', 'test', 'setset', 'S', 'dfsdfgdfg', '7854124587', '2018-01-28 09:14:11', '2018-01-28 09:14:11'),
(5, 5, 'strore 5 bank1', 'IFSC778', 'TEst name', 'Axis Bank', 'Ashram road', 'Ashram road', 'S', 'AXIS BANK DEbit card', '789645123652', '2018-01-30 16:06:41', '2018-01-30 16:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_barber`
--

CREATE TABLE `tk_store_has_barber` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_barber` int(11) NOT NULL,
  `fk_barber_type` int(11) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_barber`
--

INSERT INTO `tk_store_has_barber` (`id`, `fk_store`, `fk_barber`, `fk_barber_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(2, 1, 2, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(3, 2, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(4, 2, 4, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(5, 3, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(6, 4, 4, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(7, 5, 5, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57'),
(8, 6, 6, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_customer`
--

CREATE TABLE `tk_store_has_customer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_customer`
--

INSERT INTO `tk_store_has_customer` (`id`, `fk_store`, `fk_user`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-26 22:58:41', '2018-01-26 17:28:41'),
(2, 2, 19, '2018-01-26 22:58:41', '2018-01-26 17:28:41'),
(3, 2, 20, '2018-01-26 22:58:56', '2018-01-26 17:28:56'),
(4, 5, 19, '2018-01-27 01:15:21', '2018-01-26 19:45:21'),
(5, 11, 19, '2018-01-27 01:15:21', '2018-01-26 19:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_facility`
--

CREATE TABLE `tk_store_has_facility` (
  `id` int(11) NOT NULL,
  `fk_facility` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_facility`
--

INSERT INTO `tk_store_has_facility` (`id`, `fk_facility`, `fk_store`, `created_at`, `updated_at`) VALUES
(26, 1, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(27, 2, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(28, 3, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_gallery`
--

CREATE TABLE `tk_store_has_gallery` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_parent` int(11) DEFAULT NULL,
  `var_file` varchar(500) NOT NULL,
  `var_width` varchar(50) DEFAULT NULL,
  `var_height` varchar(50) DEFAULT NULL,
  `enum_isProfile` enum('YES','NO') DEFAULT NULL,
  `enum_ismain` enum('YES','NO') DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_gallery`
--

INSERT INTO `tk_store_has_gallery` (`id`, `fk_store`, `fk_parent`, `var_file`, `var_width`, `var_height`, `enum_isProfile`, `enum_ismain`, `enum_enable`, `created_at`, `updated_at`) VALUES
(19, 2, NULL, '0973ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:27:56', '2018-01-31 17:27:56'),
(20, 2, NULL, '1443ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'NO', '2018-01-31 17:27:56', '2018-01-31 17:27:56'),
(21, 2, NULL, '0529ABCGHitk007.jpg', NULL, NULL, 'YES', 'NO', 'YES', '2018-01-31 17:27:57', '2018-01-31 17:27:57'),
(22, 2, NULL, '6057ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'YES', '2018-01-31 17:27:59', '2018-01-31 17:27:59'),
(23, 2, NULL, '3243ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'YES', '2018-01-31 17:35:33', '2018-01-31 17:35:33'),
(24, 2, NULL, '1876ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'YES', '2018-01-31 17:37:12', '2018-01-31 17:37:12'),
(25, 2, NULL, '4515ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'YES', '2018-01-31 17:37:12', '2018-01-31 17:37:12'),
(28, 2, NULL, '5277ABCGHitk007.jpg', NULL, NULL, 'NO', 'NO', 'NO', '2018-01-31 17:47:06', '2018-01-31 17:47:06'),
(32, 2, NULL, 'store_1517507786.jpg', NULL, NULL, 'NO', 'NO', 'YES', '2018-02-01 17:56:26', '2018-02-01 17:56:26'),
(38, 2, NULL, 'images-11517593306.jpg', '259', '194', 'NO', 'YES', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(39, 2, NULL, 'images-11517593306_100x74.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(40, 2, NULL, 'images-11517593306_200x149.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(41, 2, NULL, 'images-11517593306_500x374.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(42, 2, NULL, 'images-51517593331.jpg', '275', '183', 'NO', 'YES', 'YES', '2018-02-02 17:42:11', '2018-02-02 17:42:11'),
(43, 2, NULL, 'images-51517593331_100x66.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-02 17:42:11', '2018-02-02 17:42:11'),
(44, 2, NULL, 'images-51517593331_200x133.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-02 17:42:12', '2018-02-02 17:42:12'),
(45, 2, NULL, 'images-51517593331_500x332.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-02 17:42:12', '2018-02-02 17:42:12'),
(46, 12, NULL, 'images-11518857674.jpg', '259', '194', 'NO', 'YES', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(47, 12, NULL, 'images-11518857674_100x74.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(48, 12, NULL, 'images-11518857674_200x149.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(49, 12, NULL, 'images-11518857674_500x374.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_holidays`
--

CREATE TABLE `tk_store_has_holidays` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `dt_holiday_date` date NOT NULL,
  `dt_start_time` time NOT NULL,
  `dt_end_time` time NOT NULL,
  `txt_note` text NOT NULL,
  `enum_holiday` enum('YES','NO') DEFAULT 'NO',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_holidays`
--

INSERT INTO `tk_store_has_holidays` (`id`, `fk_store`, `dt_holiday_date`, `dt_start_time`, `dt_end_time`, `txt_note`, `enum_holiday`, `created_at`, `updated_at`) VALUES
(18, 2, '2018-01-25', '03:41:00', '03:41:00', 'dfdsfd', 'YES', '2018-01-26 22:07:07', '2018-01-26 22:29:33'),
(19, 2, '2018-01-24', '03:41:00', '03:41:00', 'dfdsfd24', 'NO', '2018-01-26 22:07:22', '2018-01-26 22:30:52'),
(23, 2, '2018-01-27', '04:00:00', '04:00:00', 'dsfsdfsdf', 'YES', '2018-01-26 22:26:20', '2018-01-26 22:26:20'),
(24, 2, '2018-01-22', '04:38:00', '04:38:00', 'dsdasd', 'YES', '2018-01-26 23:08:29', '2018-01-26 23:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_offer`
--

CREATE TABLE `tk_store_has_offer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(500) NOT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `dt_start_date` date NOT NULL,
  `dt_end_date` date NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_offer`
--

INSERT INTO `tk_store_has_offer` (`id`, `fk_store`, `var_name`, `var_price`, `var_offer_price`, `txt_note`, `dt_start_date`, `dt_end_date`, `enum_enable`, `created_at`, `updated_at`) VALUES
(4, 2, 'test offer 2222', '2650', '432', 'extra notes changes222', '2018-12-07', '2018-02-15', 'YES', '2018-01-29 17:16:09', '2018-01-29 17:16:09'),
(5, 1, 'jghgjh', '50', '50', '', '2018-02-18', '2018-02-28', 'YES', '2018-02-18 09:23:52', '2018-02-18 09:23:52');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_package`
--

CREATE TABLE `tk_store_has_package` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_package`
--

INSERT INTO `tk_store_has_package` (`id`, `fk_store`, `var_name`, `var_price`, `txt_note`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 2, 'Diwali packege', '2500', 'This is Diwali special packege', 'YES', '2018-01-28 14:55:52', '2018-01-28 09:25:52'),
(2, 6, 'test pack 2', '33', 'extra notes', 'YES', '2018-01-28 17:11:45', '2018-01-28 17:11:45'),
(5, 5, 'TEst Pack for store 5', '250000', ' Spacial pack   ', 'NO', '2018-01-30 16:07:34', '2018-01-30 16:07:34'),
(6, 2, 'new package', '633', 'test', 'YES', '2018-02-02 18:06:49', '2018-02-02 18:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_services`
--

CREATE TABLE `tk_store_has_services` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(11) NOT NULL,
  `dt_service_time` time DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_services`
--

INSERT INTO `tk_store_has_services` (`id`, `fk_store`, `fk_service`, `var_price`, `dt_service_time`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:05'),
(2, 1, 2, '20', '01:00:00', 'YES', '2017-11-04 00:00:00', '2018-02-18 09:36:25'),
(3, 1, 3, '12', '01:15:00', 'YES', '2017-11-04 00:00:00', '2018-02-18 09:36:58'),
(7, 3, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:06:12'),
(8, 3, 2, '100', '00:00:20', 'YES', '2017-11-04 00:00:00', '2018-01-19 17:00:12'),
(9, 3, 3, '150', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(10, 4, 4, '200', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(11, 4, 5, '300', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(12, 4, 6, '550', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(13, 5, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:08:42'),
(14, 5, 2, '100', '00:00:25', 'YES', '2017-11-04 00:00:00', '2018-01-19 17:00:27'),
(15, 5, 3, '200', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(16, 6, 4, '300', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(17, 6, 5, '400', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:37'),
(18, 7, 6, '500', NULL, 'YES', '2017-11-04 00:00:00', '2018-02-09 18:41:19'),
(19, 3, 4, '500', NULL, 'YES', '2018-01-06 19:54:11', '2018-01-06 19:54:11'),
(24, 2, 1, '150', '12:00:00', 'YES', '2018-01-19 23:35:02', '2018-02-17 20:49:20'),
(25, 2, 2, '1500', '01:50:00', 'YES', '2018-01-19 23:35:02', '2018-01-29 16:48:43'),
(28, 2, 4, '222', '01:15:00', 'YES', '0000-00-00 00:00:00', '2018-02-17 20:51:58'),
(30, 2, 3, '3', '03:15:00', 'YES', '0000-00-00 00:00:00', '2018-02-17 11:23:18'),
(31, 11, 11, '200', '00:12:00', 'NO', '0000-00-00 00:00:00', '2018-02-09 18:42:13'),
(35, 2, 9, '480', '00:30:00', 'NO', '0000-00-00 00:00:00', '2018-02-17 11:15:54'),
(36, 2, 6, '22', '01:15:00', 'NO', '0000-00-00 00:00:00', '2018-02-17 20:52:20');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_setting`
--

CREATE TABLE `tk_store_has_setting` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_setting` int(11) NOT NULL,
  `var_setting_val` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_setting`
--

INSERT INTO `tk_store_has_setting` (`id`, `fk_store`, `fk_setting`, `var_setting_val`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'www.facebook.com/hairsallon', 'YES', '2018-01-17 00:17:56', '2018-01-16 18:47:56'),
(2, 2, 4, '985632145', 'YES', '2018-01-17 00:18:18', '2018-01-16 18:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_staff`
--

CREATE TABLE `tk_store_has_staff` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(255) DEFAULT NULL,
  `var_phone` varchar(100) DEFAULT NULL,
  `var_image` varchar(500) DEFAULT NULL,
  `dt_startdate` datetime DEFAULT NULL,
  `dt_enddate` datetime DEFAULT NULL,
  `enum_isFrontView` enum('YES','NO') DEFAULT NULL,
  `enum_active` enum('YES','NO') DEFAULT NULL,
  `enum_current` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_staff`
--

INSERT INTO `tk_store_has_staff` (`id`, `fk_store`, `var_name`, `var_phone`, `var_image`, `dt_startdate`, `dt_enddate`, `enum_isFrontView`, `enum_active`, `enum_current`, `created_at`, `updated_at`) VALUES
(1, 1, 'tushar123', '9856321456', NULL, '2017-12-15 00:00:00', '2017-12-17 00:00:00', 'YES', 'YES', 'YES', '2018-01-08 18:49:48', '2018-02-18 12:44:08'),
(3, 2, 'Test1', '9856321456', NULL, '2017-01-03 00:00:00', '2018-01-16 00:00:00', 'YES', 'YES', 'YES', '2018-01-16 23:41:58', '2018-01-17 18:32:06'),
(4, 2, 'nirav', '1111111855', NULL, '2018-01-17 00:00:00', '2018-01-17 00:00:00', 'NO', 'YES', 'YES', '2018-01-16 19:10:54', '2018-02-17 14:53:53'),
(5, 5, 'neasrfd', '5555', NULL, '2018-01-17 00:00:00', '2018-01-31 00:00:00', 'NO', 'NO', 'NO', '2018-01-16 19:13:07', '2018-01-28 09:01:22'),
(6, 2, 'Test staff', '8777667779', NULL, '2018-01-17 00:00:00', '2018-01-17 00:00:00', 'YES', 'YES', 'YES', '2018-01-17 17:17:20', '2018-02-17 14:19:01'),
(8, 1, 'eeeee', '33333', NULL, '2017-02-01 00:00:00', '2018-01-02 00:00:00', 'YES', 'YES', 'YES', '0000-00-00 00:00:00', '2018-02-18 12:43:42'),
(9, 2, 'test image', '9856321458', 'staff_1518200134.jpg', '2017-12-01 00:00:00', '2018-02-28 00:00:00', 'YES', 'YES', 'YES', '2018-02-09 16:52:26', '2018-02-17 14:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timeslots`
--

CREATE TABLE `tk_store_has_timeslots` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_timeslot` int(11) NOT NULL,
  `int_week_day` int(11) DEFAULT NULL,
  `enum_enable` enum('YES','NO') DEFAULT NULL,
  `enum_isfull` enum('YES','NO') DEFAULT NULL,
  `int_book_limit` int(11) DEFAULT NULL,
  `int_book_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timeslots`
--

INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(1, 23, 1, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(2, 23, 2, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(3, 23, 3, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(4, 23, 4, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(5, 23, 5, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(6, 23, 6, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(7, 23, 7, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:15', '2018-02-21 16:26:50'),
(8, 23, 8, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(9, 23, 9, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 17:05:19'),
(10, 23, 10, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(11, 23, 11, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(12, 23, 12, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(13, 23, 13, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(14, 23, 14, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(15, 23, 15, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(16, 23, 16, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(17, 23, 17, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(18, 23, 18, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(19, 23, 19, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(20, 23, 20, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(21, 23, 21, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(22, 23, 22, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:16', '2018-02-21 16:26:50'),
(23, 23, 23, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(24, 23, 24, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(25, 23, 25, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(26, 23, 26, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(27, 23, 27, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(28, 23, 28, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(29, 23, 29, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(30, 23, 30, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(31, 23, 31, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(32, 23, 32, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(33, 23, 33, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 17:05:20'),
(34, 23, 34, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 17:05:20'),
(35, 23, 35, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 17:05:20'),
(36, 23, 36, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 17:05:20'),
(37, 23, 37, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(38, 23, 38, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(39, 23, 39, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(40, 23, 40, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:17', '2018-02-21 16:26:50'),
(41, 23, 41, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(42, 23, 42, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(43, 23, 43, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(44, 23, 44, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(45, 23, 45, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(46, 23, 46, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(47, 23, 47, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(48, 23, 48, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(49, 23, 49, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(50, 23, 50, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(51, 23, 51, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(52, 23, 52, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(53, 23, 53, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(54, 23, 54, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(55, 23, 55, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(56, 23, 56, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(57, 23, 57, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(58, 23, 58, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:18', '2018-02-21 16:26:50'),
(59, 23, 59, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(60, 23, 60, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(61, 23, 61, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(62, 23, 62, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(63, 23, 63, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(64, 23, 64, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(65, 23, 65, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(66, 23, 66, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(67, 23, 67, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(68, 23, 68, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(69, 23, 69, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(70, 23, 70, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(71, 23, 71, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(72, 23, 72, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(73, 23, 73, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(74, 23, 74, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(75, 23, 75, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(76, 23, 76, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(77, 23, 77, 0, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(78, 23, 78, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(79, 23, 79, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(80, 23, 80, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:19', '2018-02-21 16:26:50'),
(81, 23, 81, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(82, 23, 82, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(83, 23, 83, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(84, 23, 84, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(85, 23, 85, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(86, 23, 86, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(87, 23, 87, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(88, 23, 88, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(89, 23, 89, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(90, 23, 90, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(91, 23, 91, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(92, 23, 92, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(93, 23, 93, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(94, 23, 94, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(95, 23, 95, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(96, 23, 96, 0, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(97, 23, 1, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(98, 23, 2, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(99, 23, 3, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(100, 23, 4, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:20', '2018-02-21 16:26:50'),
(101, 23, 5, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(102, 23, 6, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(103, 23, 7, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(104, 23, 8, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(105, 23, 9, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(106, 23, 10, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(107, 23, 11, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(108, 23, 12, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(109, 23, 13, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(110, 23, 14, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(111, 23, 15, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(112, 23, 16, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(113, 23, 17, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(114, 23, 18, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(115, 23, 19, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(116, 23, 20, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(117, 23, 21, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(118, 23, 22, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(119, 23, 23, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(120, 23, 24, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(121, 23, 25, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(122, 23, 26, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:21', '2018-02-21 16:26:50'),
(123, 23, 27, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(124, 23, 28, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(125, 23, 29, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(126, 23, 30, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(127, 23, 31, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(128, 23, 32, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(129, 23, 33, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(130, 23, 34, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(131, 23, 35, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(132, 23, 36, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(133, 23, 37, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(134, 23, 38, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(135, 23, 39, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(136, 23, 40, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(137, 23, 41, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(138, 23, 42, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(139, 23, 43, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(140, 23, 44, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(141, 23, 45, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(142, 23, 46, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:22', '2018-02-21 16:26:50'),
(143, 23, 47, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(144, 23, 48, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(145, 23, 49, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(146, 23, 50, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(147, 23, 51, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(148, 23, 52, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(149, 23, 53, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(150, 23, 54, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(151, 23, 55, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(152, 23, 56, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(153, 23, 57, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(154, 23, 58, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(155, 23, 59, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(156, 23, 60, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(157, 23, 61, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(158, 23, 62, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(159, 23, 63, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(160, 23, 64, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(161, 23, 65, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(162, 23, 66, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(163, 23, 67, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:23', '2018-02-21 16:26:50'),
(164, 23, 68, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(165, 23, 69, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(166, 23, 70, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(167, 23, 71, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(168, 23, 72, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(169, 23, 73, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(170, 23, 74, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(171, 23, 75, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(172, 23, 76, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(173, 23, 77, 1, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(174, 23, 78, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(175, 23, 79, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(176, 23, 80, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(177, 23, 81, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(178, 23, 82, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(179, 23, 83, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(180, 23, 84, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(181, 23, 85, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(182, 23, 86, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(183, 23, 87, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(184, 23, 88, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(185, 23, 89, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(186, 23, 90, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(187, 23, 91, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:24', '2018-02-21 16:26:50'),
(188, 23, 92, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(189, 23, 93, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(190, 23, 94, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(191, 23, 95, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(192, 23, 96, 1, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(193, 23, 1, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(194, 23, 2, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(195, 23, 3, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(196, 23, 4, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(197, 23, 5, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(198, 23, 6, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(199, 23, 7, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(200, 23, 8, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(201, 23, 9, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(202, 23, 10, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(203, 23, 11, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:25', '2018-02-21 16:26:50'),
(204, 23, 12, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(205, 23, 13, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(206, 23, 14, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(207, 23, 15, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(208, 23, 16, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(209, 23, 17, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(210, 23, 18, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(211, 23, 19, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(212, 23, 20, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(213, 23, 21, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(214, 23, 22, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(215, 23, 23, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(216, 23, 24, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(217, 23, 25, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(218, 23, 26, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(219, 23, 27, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(220, 23, 28, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(221, 23, 29, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(222, 23, 30, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(223, 23, 31, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(224, 23, 32, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:26', '2018-02-21 16:26:50'),
(225, 23, 33, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(226, 23, 34, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(227, 23, 35, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(228, 23, 36, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(229, 23, 37, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(230, 23, 38, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(231, 23, 39, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(232, 23, 40, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(233, 23, 41, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(234, 23, 42, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(235, 23, 43, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(236, 23, 44, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(237, 23, 45, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(238, 23, 46, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(239, 23, 47, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(240, 23, 48, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(241, 23, 49, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(242, 23, 50, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(243, 23, 51, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(244, 23, 52, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(245, 23, 53, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(246, 23, 54, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(247, 23, 55, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(248, 23, 56, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(249, 23, 57, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:27', '2018-02-21 16:26:50'),
(250, 23, 58, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(251, 23, 59, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(252, 23, 60, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(253, 23, 61, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(254, 23, 62, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(255, 23, 63, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(256, 23, 64, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(257, 23, 65, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(258, 23, 66, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(259, 23, 67, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(260, 23, 68, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(261, 23, 69, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(262, 23, 70, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(263, 23, 71, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(264, 23, 72, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(265, 23, 73, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(266, 23, 74, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(267, 23, 75, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(268, 23, 76, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(269, 23, 77, 2, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(270, 23, 78, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(271, 23, 79, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(272, 23, 80, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:28', '2018-02-21 16:26:50'),
(273, 23, 81, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(274, 23, 82, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(275, 23, 83, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(276, 23, 84, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(277, 23, 85, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(278, 23, 86, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(279, 23, 87, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(280, 23, 88, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(281, 23, 89, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(282, 23, 90, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(283, 23, 91, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(284, 23, 92, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(285, 23, 93, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(286, 23, 94, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(287, 23, 95, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(288, 23, 96, 2, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(289, 23, 1, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(290, 23, 2, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(291, 23, 3, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(292, 23, 4, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(293, 23, 5, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:29', '2018-02-21 16:26:50'),
(294, 23, 6, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(295, 23, 7, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(296, 23, 8, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(297, 23, 9, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(298, 23, 10, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(299, 23, 11, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(300, 23, 12, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(301, 23, 13, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(302, 23, 14, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(303, 23, 15, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(304, 23, 16, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(305, 23, 17, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(306, 23, 18, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(307, 23, 19, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(308, 23, 20, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(309, 23, 21, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(310, 23, 22, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(311, 23, 23, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(312, 23, 24, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:30', '2018-02-21 16:26:50'),
(313, 23, 25, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(314, 23, 26, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(315, 23, 27, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(316, 23, 28, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(317, 23, 29, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(318, 23, 30, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(319, 23, 31, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(320, 23, 32, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(321, 23, 33, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(322, 23, 34, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(323, 23, 35, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(324, 23, 36, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(325, 23, 37, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(326, 23, 38, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(327, 23, 39, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(328, 23, 40, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(329, 23, 41, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(330, 23, 42, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(331, 23, 43, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(332, 23, 44, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(333, 23, 45, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(334, 23, 46, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(335, 23, 47, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(336, 23, 48, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(337, 23, 49, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:31', '2018-02-21 16:26:50'),
(338, 23, 50, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(339, 23, 51, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(340, 23, 52, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(341, 23, 53, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(342, 23, 54, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(343, 23, 55, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(344, 23, 56, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(345, 23, 57, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(346, 23, 58, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(347, 23, 59, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(348, 23, 60, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(349, 23, 61, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(350, 23, 62, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(351, 23, 63, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(352, 23, 64, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(353, 23, 65, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(354, 23, 66, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(355, 23, 67, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(356, 23, 68, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(357, 23, 69, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(358, 23, 70, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(359, 23, 71, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(360, 23, 72, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(361, 23, 73, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:32', '2018-02-21 16:26:50'),
(362, 23, 74, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(363, 23, 75, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(364, 23, 76, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(365, 23, 77, 3, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(366, 23, 78, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(367, 23, 79, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(368, 23, 80, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(369, 23, 81, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(370, 23, 82, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(371, 23, 83, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(372, 23, 84, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(373, 23, 85, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(374, 23, 86, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(375, 23, 87, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(376, 23, 88, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(377, 23, 89, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(378, 23, 90, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(379, 23, 91, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(380, 23, 92, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:33', '2018-02-21 16:26:50'),
(381, 23, 93, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(382, 23, 94, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(383, 23, 95, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(384, 23, 96, 3, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(385, 23, 1, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(386, 23, 2, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(387, 23, 3, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(388, 23, 4, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(389, 23, 5, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(390, 23, 6, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(391, 23, 7, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(392, 23, 8, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(393, 23, 9, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(394, 23, 10, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(395, 23, 11, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(396, 23, 12, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(397, 23, 13, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(398, 23, 14, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(399, 23, 15, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:34', '2018-02-21 16:26:50'),
(400, 23, 16, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(401, 23, 17, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(402, 23, 18, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(403, 23, 19, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(404, 23, 20, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(405, 23, 21, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(406, 23, 22, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(407, 23, 23, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(408, 23, 24, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(409, 23, 25, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(410, 23, 26, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(411, 23, 27, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(412, 23, 28, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(413, 23, 29, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(414, 23, 30, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(415, 23, 31, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(416, 23, 32, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(417, 23, 33, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(418, 23, 34, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:35', '2018-02-21 16:26:50'),
(419, 23, 35, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(420, 23, 36, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(421, 23, 37, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(422, 23, 38, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(423, 23, 39, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(424, 23, 40, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(425, 23, 41, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(426, 23, 42, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(427, 23, 43, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(428, 23, 44, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(429, 23, 45, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(430, 23, 46, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(431, 23, 47, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(432, 23, 48, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(433, 23, 49, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(434, 23, 50, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(435, 23, 51, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(436, 23, 52, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(437, 23, 53, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(438, 23, 54, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:36', '2018-02-21 16:26:50'),
(439, 23, 55, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(440, 23, 56, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(441, 23, 57, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(442, 23, 58, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(443, 23, 59, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(444, 23, 60, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(445, 23, 61, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(446, 23, 62, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(447, 23, 63, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(448, 23, 64, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(449, 23, 65, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(450, 23, 66, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(451, 23, 67, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(452, 23, 68, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(453, 23, 69, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(454, 23, 70, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(455, 23, 71, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(456, 23, 72, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:37', '2018-02-21 16:26:50'),
(457, 23, 73, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(458, 23, 74, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(459, 23, 75, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(460, 23, 76, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(461, 23, 77, 4, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(462, 23, 78, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(463, 23, 79, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(464, 23, 80, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(465, 23, 81, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(466, 23, 82, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(467, 23, 83, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(468, 23, 84, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(469, 23, 85, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(470, 23, 86, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(471, 23, 87, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(472, 23, 88, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(473, 23, 89, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(474, 23, 90, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:38', '2018-02-21 16:26:50'),
(475, 23, 91, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(476, 23, 92, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(477, 23, 93, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(478, 23, 94, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(479, 23, 95, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(480, 23, 96, 4, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(481, 23, 1, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(482, 23, 2, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(483, 23, 3, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(484, 23, 4, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(485, 23, 5, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(486, 23, 6, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(487, 23, 7, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(488, 23, 8, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(489, 23, 9, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(490, 23, 10, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:39', '2018-02-21 16:26:50'),
(491, 23, 11, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(492, 23, 12, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(493, 23, 13, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(494, 23, 14, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(495, 23, 15, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(496, 23, 16, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(497, 23, 17, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(498, 23, 18, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(499, 23, 19, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(500, 23, 20, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(501, 23, 21, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(502, 23, 22, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(503, 23, 23, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(504, 23, 24, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(505, 23, 25, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(506, 23, 26, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(507, 23, 27, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(508, 23, 28, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:40', '2018-02-21 16:26:50'),
(509, 23, 29, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(510, 23, 30, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(511, 23, 31, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(512, 23, 32, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(513, 23, 33, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(514, 23, 34, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(515, 23, 35, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(516, 23, 36, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(517, 23, 37, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(518, 23, 38, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(519, 23, 39, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(520, 23, 40, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(521, 23, 41, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(522, 23, 42, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(523, 23, 43, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(524, 23, 44, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(525, 23, 45, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(526, 23, 46, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(527, 23, 47, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(528, 23, 48, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(529, 23, 49, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:41', '2018-02-21 16:26:50'),
(530, 23, 50, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(531, 23, 51, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(532, 23, 52, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(533, 23, 53, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(534, 23, 54, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(535, 23, 55, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(536, 23, 56, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(537, 23, 57, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(538, 23, 58, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(539, 23, 59, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(540, 23, 60, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(541, 23, 61, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(542, 23, 62, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(543, 23, 63, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(544, 23, 64, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(545, 23, 65, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(546, 23, 66, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(547, 23, 67, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(548, 23, 68, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(549, 23, 69, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(550, 23, 70, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:42', '2018-02-21 16:26:50'),
(551, 23, 71, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(552, 23, 72, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(553, 23, 73, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(554, 23, 74, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(555, 23, 75, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(556, 23, 76, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(557, 23, 77, 5, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(558, 23, 78, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(559, 23, 79, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(560, 23, 80, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(561, 23, 81, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(562, 23, 82, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(563, 23, 83, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(564, 23, 84, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(565, 23, 85, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(566, 23, 86, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(567, 23, 87, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(568, 23, 88, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(569, 23, 89, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(570, 23, 90, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(571, 23, 91, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:43', '2018-02-21 16:26:50'),
(572, 23, 92, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(573, 23, 93, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(574, 23, 94, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(575, 23, 95, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(576, 23, 96, 5, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(577, 23, 1, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(578, 23, 2, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(579, 23, 3, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(580, 23, 4, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(581, 23, 5, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(582, 23, 6, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(583, 23, 7, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(584, 23, 8, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(585, 23, 9, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(586, 23, 10, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(587, 23, 11, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(588, 23, 12, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(589, 23, 13, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(590, 23, 14, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:44', '2018-02-21 16:26:50'),
(591, 23, 15, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(592, 23, 16, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(593, 23, 17, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(594, 23, 18, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(595, 23, 19, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(596, 23, 20, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(597, 23, 21, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(598, 23, 22, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50');
INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(599, 23, 23, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(600, 23, 24, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(601, 23, 25, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(602, 23, 26, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(603, 23, 27, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(604, 23, 28, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(605, 23, 29, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(606, 23, 30, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(607, 23, 31, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(608, 23, 32, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(609, 23, 33, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(610, 23, 34, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(611, 23, 35, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:45', '2018-02-21 16:26:50'),
(612, 23, 36, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(613, 23, 37, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(614, 23, 38, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(615, 23, 39, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(616, 23, 40, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(617, 23, 41, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(618, 23, 42, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(619, 23, 43, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(620, 23, 44, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(621, 23, 45, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(622, 23, 46, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(623, 23, 47, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(624, 23, 48, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(625, 23, 49, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(626, 23, 50, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(627, 23, 51, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(628, 23, 52, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(629, 23, 53, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(630, 23, 54, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(631, 23, 55, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(632, 23, 56, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(633, 23, 57, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(634, 23, 58, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:46', '2018-02-21 16:26:50'),
(635, 23, 59, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(636, 23, 60, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(637, 23, 61, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(638, 23, 62, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(639, 23, 63, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(640, 23, 64, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(641, 23, 65, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(642, 23, 66, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(643, 23, 67, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(644, 23, 68, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(645, 23, 69, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(646, 23, 70, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(647, 23, 71, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(648, 23, 72, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(649, 23, 73, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(650, 23, 74, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(651, 23, 75, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(652, 23, 76, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(653, 23, 77, 6, 'YES', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(654, 23, 78, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(655, 23, 79, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:47', '2018-02-21 16:26:50'),
(656, 23, 80, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(657, 23, 81, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(658, 23, 82, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(659, 23, 83, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(660, 23, 84, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(661, 23, 85, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(662, 23, 86, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(663, 23, 87, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(664, 23, 88, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(665, 23, 89, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(666, 23, 90, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(667, 23, 91, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(668, 23, 92, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(669, 23, 93, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(670, 23, 94, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(671, 23, 95, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(672, 23, 96, 6, 'NO', 'NO', 3, NULL, '2018-02-21 16:16:48', '2018-02-21 16:26:50'),
(673, 24, 1, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:56', '2018-02-22 17:03:53'),
(674, 24, 2, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:56', '2018-02-22 17:03:53'),
(675, 24, 3, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:56', '2018-02-22 17:03:53'),
(676, 24, 4, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(677, 24, 5, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(678, 24, 6, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(679, 24, 7, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(680, 24, 8, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(681, 24, 9, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(682, 24, 10, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(683, 24, 11, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(684, 24, 12, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(685, 24, 13, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(686, 24, 14, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(687, 24, 15, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(688, 24, 16, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(689, 24, 17, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(690, 24, 18, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(691, 24, 19, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(692, 24, 20, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:57', '2018-02-22 17:03:53'),
(693, 24, 21, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(694, 24, 22, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(695, 24, 23, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(696, 24, 24, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(697, 24, 25, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(698, 24, 26, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(699, 24, 27, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(700, 24, 28, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(701, 24, 29, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(702, 24, 30, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(703, 24, 31, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(704, 24, 32, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(705, 24, 33, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(706, 24, 34, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(707, 24, 35, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(708, 24, 36, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:58', '2018-02-22 17:03:53'),
(709, 24, 37, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(710, 24, 38, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(711, 24, 39, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(712, 24, 40, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(713, 24, 41, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(714, 24, 42, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(715, 24, 43, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(716, 24, 44, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(717, 24, 45, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(718, 24, 46, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(719, 24, 47, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(720, 24, 48, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(721, 24, 49, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(722, 24, 50, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(723, 24, 51, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(724, 24, 52, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(725, 24, 53, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(726, 24, 54, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(727, 24, 55, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(728, 24, 56, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:13:59', '2018-02-22 17:03:53'),
(729, 24, 57, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(730, 24, 58, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(731, 24, 59, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(732, 24, 60, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(733, 24, 61, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(734, 24, 62, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(735, 24, 63, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(736, 24, 64, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(737, 24, 65, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(738, 24, 66, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(739, 24, 67, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(740, 24, 68, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(741, 24, 69, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(742, 24, 70, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(743, 24, 71, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(744, 24, 72, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(745, 24, 73, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(746, 24, 74, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(747, 24, 75, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(748, 24, 76, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:00', '2018-02-22 17:03:53'),
(749, 24, 77, 0, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(750, 24, 78, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(751, 24, 79, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(752, 24, 80, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(753, 24, 81, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(754, 24, 82, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(755, 24, 83, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(756, 24, 84, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(757, 24, 85, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(758, 24, 86, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(759, 24, 87, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(760, 24, 88, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(761, 24, 89, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(762, 24, 90, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(763, 24, 91, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(764, 24, 92, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(765, 24, 93, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(766, 24, 94, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(767, 24, 95, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(768, 24, 96, 0, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:01', '2018-02-22 17:03:53'),
(769, 24, 1, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(770, 24, 2, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(771, 24, 3, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(772, 24, 4, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(773, 24, 5, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(774, 24, 6, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(775, 24, 7, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(776, 24, 8, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(777, 24, 9, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(778, 24, 10, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(779, 24, 11, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(780, 24, 12, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(781, 24, 13, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(782, 24, 14, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(783, 24, 15, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(784, 24, 16, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(785, 24, 17, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(786, 24, 18, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(787, 24, 19, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:02', '2018-02-22 17:03:53'),
(788, 24, 20, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(789, 24, 21, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(790, 24, 22, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(791, 24, 23, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(792, 24, 24, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(793, 24, 25, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(794, 24, 26, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(795, 24, 27, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(796, 24, 28, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(797, 24, 29, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(798, 24, 30, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(799, 24, 31, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(800, 24, 32, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(801, 24, 33, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(802, 24, 34, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(803, 24, 35, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(804, 24, 36, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(805, 24, 37, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:03', '2018-02-22 17:03:53'),
(806, 24, 38, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(807, 24, 39, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(808, 24, 40, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(809, 24, 41, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(810, 24, 42, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(811, 24, 43, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(812, 24, 44, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(813, 24, 45, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(814, 24, 46, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(815, 24, 47, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(816, 24, 48, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(817, 24, 49, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(818, 24, 50, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(819, 24, 51, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(820, 24, 52, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(821, 24, 53, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(822, 24, 54, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(823, 24, 55, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(824, 24, 56, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(825, 24, 57, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:04', '2018-02-22 17:03:53'),
(826, 24, 58, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(827, 24, 59, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(828, 24, 60, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(829, 24, 61, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(830, 24, 62, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(831, 24, 63, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(832, 24, 64, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(833, 24, 65, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(834, 24, 66, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(835, 24, 67, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(836, 24, 68, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(837, 24, 69, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(838, 24, 70, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(839, 24, 71, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(840, 24, 72, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(841, 24, 73, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(842, 24, 74, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(843, 24, 75, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(844, 24, 76, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(845, 24, 77, 1, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(846, 24, 78, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(847, 24, 79, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:05', '2018-02-22 17:03:53'),
(848, 24, 80, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(849, 24, 81, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(850, 24, 82, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(851, 24, 83, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(852, 24, 84, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(853, 24, 85, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(854, 24, 86, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(855, 24, 87, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(856, 24, 88, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(857, 24, 89, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(858, 24, 90, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(859, 24, 91, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(860, 24, 92, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(861, 24, 93, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(862, 24, 94, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(863, 24, 95, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:06', '2018-02-22 17:03:53'),
(864, 24, 96, 1, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(865, 24, 1, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(866, 24, 2, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(867, 24, 3, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(868, 24, 4, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(869, 24, 5, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(870, 24, 6, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(871, 24, 7, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(872, 24, 8, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(873, 24, 9, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(874, 24, 10, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(875, 24, 11, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(876, 24, 12, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(877, 24, 13, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(878, 24, 14, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(879, 24, 15, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(880, 24, 16, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(881, 24, 17, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(882, 24, 18, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(883, 24, 19, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(884, 24, 20, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:07', '2018-02-22 17:03:53'),
(885, 24, 21, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(886, 24, 22, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(887, 24, 23, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(888, 24, 24, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(889, 24, 25, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(890, 24, 26, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(891, 24, 27, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(892, 24, 28, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(893, 24, 29, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(894, 24, 30, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(895, 24, 31, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(896, 24, 32, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(897, 24, 33, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(898, 24, 34, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(899, 24, 35, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(900, 24, 36, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(901, 24, 37, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(902, 24, 38, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(903, 24, 39, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(904, 24, 40, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:08', '2018-02-22 17:03:53'),
(905, 24, 41, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(906, 24, 42, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(907, 24, 43, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(908, 24, 44, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(909, 24, 45, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(910, 24, 46, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(911, 24, 47, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(912, 24, 48, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(913, 24, 49, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(914, 24, 50, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(915, 24, 51, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(916, 24, 52, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(917, 24, 53, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(918, 24, 54, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(919, 24, 55, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(920, 24, 56, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(921, 24, 57, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(922, 24, 58, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(923, 24, 59, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(924, 24, 60, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:09', '2018-02-22 17:03:53'),
(925, 24, 61, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(926, 24, 62, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(927, 24, 63, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(928, 24, 64, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(929, 24, 65, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(930, 24, 66, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(931, 24, 67, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(932, 24, 68, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(933, 24, 69, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(934, 24, 70, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(935, 24, 71, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(936, 24, 72, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(937, 24, 73, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(938, 24, 74, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(939, 24, 75, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(940, 24, 76, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:10', '2018-02-22 17:03:53'),
(941, 24, 77, 2, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(942, 24, 78, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(943, 24, 79, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(944, 24, 80, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(945, 24, 81, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(946, 24, 82, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(947, 24, 83, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(948, 24, 84, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(949, 24, 85, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(950, 24, 86, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(951, 24, 87, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(952, 24, 88, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(953, 24, 89, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(954, 24, 90, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(955, 24, 91, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(956, 24, 92, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(957, 24, 93, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:11', '2018-02-22 17:03:53'),
(958, 24, 94, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(959, 24, 95, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(960, 24, 96, 2, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(961, 24, 1, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(962, 24, 2, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(963, 24, 3, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(964, 24, 4, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(965, 24, 5, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(966, 24, 6, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(967, 24, 7, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(968, 24, 8, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(969, 24, 9, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(970, 24, 10, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(971, 24, 11, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(972, 24, 12, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(973, 24, 13, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(974, 24, 14, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(975, 24, 15, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(976, 24, 16, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:12', '2018-02-22 17:03:53'),
(977, 24, 17, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(978, 24, 18, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(979, 24, 19, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(980, 24, 20, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(981, 24, 21, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(982, 24, 22, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(983, 24, 23, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(984, 24, 24, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(985, 24, 25, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(986, 24, 26, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(987, 24, 27, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(988, 24, 28, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(989, 24, 29, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(990, 24, 30, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(991, 24, 31, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(992, 24, 32, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(993, 24, 33, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(994, 24, 34, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:13', '2018-02-22 17:03:53'),
(995, 24, 35, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(996, 24, 36, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(997, 24, 37, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(998, 24, 38, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(999, 24, 39, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1000, 24, 40, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1001, 24, 41, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1002, 24, 42, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1003, 24, 43, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1004, 24, 44, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1005, 24, 45, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1006, 24, 46, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1007, 24, 47, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1008, 24, 48, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1009, 24, 49, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1010, 24, 50, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1011, 24, 51, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1012, 24, 52, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:14', '2018-02-22 17:03:53'),
(1013, 24, 53, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1014, 24, 54, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1015, 24, 55, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1016, 24, 56, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1017, 24, 57, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1018, 24, 58, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1019, 24, 59, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1020, 24, 60, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1021, 24, 61, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1022, 24, 62, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1023, 24, 63, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1024, 24, 64, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1025, 24, 65, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1026, 24, 66, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1027, 24, 67, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1028, 24, 68, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1029, 24, 69, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1030, 24, 70, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:15', '2018-02-22 17:03:53'),
(1031, 24, 71, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1032, 24, 72, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1033, 24, 73, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1034, 24, 74, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1035, 24, 75, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1036, 24, 76, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1037, 24, 77, 3, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1038, 24, 78, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1039, 24, 79, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1040, 24, 80, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1041, 24, 81, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1042, 24, 82, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1043, 24, 83, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1044, 24, 84, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1045, 24, 85, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1046, 24, 86, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1047, 24, 87, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1048, 24, 88, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1049, 24, 89, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:16', '2018-02-22 17:03:53'),
(1050, 24, 90, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1051, 24, 91, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1052, 24, 92, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1053, 24, 93, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1054, 24, 94, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1055, 24, 95, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1056, 24, 96, 3, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1057, 24, 1, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1058, 24, 2, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1059, 24, 3, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1060, 24, 4, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1061, 24, 5, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1062, 24, 6, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1063, 24, 7, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1064, 24, 8, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1065, 24, 9, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1066, 24, 10, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1067, 24, 11, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1068, 24, 12, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:17', '2018-02-22 17:03:53'),
(1069, 24, 13, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1070, 24, 14, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1071, 24, 15, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1072, 24, 16, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1073, 24, 17, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1074, 24, 18, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1075, 24, 19, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1076, 24, 20, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1077, 24, 21, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1078, 24, 22, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1079, 24, 23, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1080, 24, 24, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1081, 24, 25, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1082, 24, 26, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1083, 24, 27, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1084, 24, 28, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1085, 24, 29, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1086, 24, 30, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1087, 24, 31, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1088, 24, 32, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1089, 24, 33, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:18', '2018-02-22 17:03:53'),
(1090, 24, 34, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1091, 24, 35, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1092, 24, 36, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1093, 24, 37, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1094, 24, 38, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1095, 24, 39, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1096, 24, 40, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1097, 24, 41, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1098, 24, 42, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1099, 24, 43, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1100, 24, 44, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1101, 24, 45, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1102, 24, 46, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1103, 24, 47, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1104, 24, 48, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1105, 24, 49, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1106, 24, 50, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1107, 24, 51, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1108, 24, 52, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1109, 24, 53, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1110, 24, 54, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1111, 24, 55, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:19', '2018-02-22 17:03:53'),
(1112, 24, 56, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1113, 24, 57, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1114, 24, 58, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1115, 24, 59, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1116, 24, 60, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1117, 24, 61, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1118, 24, 62, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1119, 24, 63, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1120, 24, 64, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1121, 24, 65, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1122, 24, 66, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1123, 24, 67, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1124, 24, 68, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1125, 24, 69, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1126, 24, 70, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1127, 24, 71, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1128, 24, 72, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1129, 24, 73, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1130, 24, 74, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1131, 24, 75, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1132, 24, 76, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1133, 24, 77, 4, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:20', '2018-02-22 17:03:53'),
(1134, 24, 78, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1135, 24, 79, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1136, 24, 80, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1137, 24, 81, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1138, 24, 82, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1139, 24, 83, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1140, 24, 84, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1141, 24, 85, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1142, 24, 86, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1143, 24, 87, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1144, 24, 88, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1145, 24, 89, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1146, 24, 90, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1147, 24, 91, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1148, 24, 92, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1149, 24, 93, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1150, 24, 94, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1151, 24, 95, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1152, 24, 96, 4, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:21', '2018-02-22 17:03:53'),
(1153, 24, 1, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1154, 24, 2, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1155, 24, 3, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1156, 24, 4, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1157, 24, 5, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1158, 24, 6, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1159, 24, 7, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1160, 24, 8, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1161, 24, 9, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1162, 24, 10, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1163, 24, 11, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1164, 24, 12, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1165, 24, 13, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1166, 24, 14, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1167, 24, 15, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1168, 24, 16, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:22', '2018-02-22 17:03:53'),
(1169, 24, 17, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1170, 24, 18, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1171, 24, 19, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1172, 24, 20, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1173, 24, 21, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1174, 24, 22, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1175, 24, 23, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1176, 24, 24, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1177, 24, 25, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1178, 24, 26, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1179, 24, 27, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1180, 24, 28, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1181, 24, 29, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1182, 24, 30, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1183, 24, 31, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1184, 24, 32, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1185, 24, 33, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1186, 24, 34, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1187, 24, 35, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:23', '2018-02-22 17:03:53'),
(1188, 24, 36, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1189, 24, 37, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1190, 24, 38, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1191, 24, 39, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1192, 24, 40, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1193, 24, 41, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53');
INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(1194, 24, 42, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1195, 24, 43, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1196, 24, 44, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1197, 24, 45, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1198, 24, 46, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1199, 24, 47, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1200, 24, 48, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1201, 24, 49, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1202, 24, 50, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1203, 24, 51, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1204, 24, 52, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:24', '2018-02-22 17:03:53'),
(1205, 24, 53, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1206, 24, 54, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1207, 24, 55, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1208, 24, 56, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1209, 24, 57, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1210, 24, 58, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1211, 24, 59, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1212, 24, 60, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1213, 24, 61, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1214, 24, 62, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1215, 24, 63, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1216, 24, 64, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1217, 24, 65, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1218, 24, 66, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1219, 24, 67, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1220, 24, 68, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1221, 24, 69, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1222, 24, 70, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1223, 24, 71, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1224, 24, 72, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1225, 24, 73, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:25', '2018-02-22 17:03:53'),
(1226, 24, 74, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1227, 24, 75, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1228, 24, 76, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1229, 24, 77, 5, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1230, 24, 78, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1231, 24, 79, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1232, 24, 80, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1233, 24, 81, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1234, 24, 82, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1235, 24, 83, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1236, 24, 84, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1237, 24, 85, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1238, 24, 86, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1239, 24, 87, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1240, 24, 88, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1241, 24, 89, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1242, 24, 90, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1243, 24, 91, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1244, 24, 92, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:26', '2018-02-22 17:03:53'),
(1245, 24, 93, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1246, 24, 94, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1247, 24, 95, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1248, 24, 96, 5, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1249, 24, 1, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1250, 24, 2, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1251, 24, 3, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1252, 24, 4, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1253, 24, 5, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1254, 24, 6, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1255, 24, 7, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1256, 24, 8, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1257, 24, 9, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1258, 24, 10, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1259, 24, 11, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1260, 24, 12, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1261, 24, 13, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1262, 24, 14, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:27', '2018-02-22 17:03:53'),
(1263, 24, 15, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1264, 24, 16, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1265, 24, 17, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1266, 24, 18, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1267, 24, 19, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1268, 24, 20, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1269, 24, 21, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1270, 24, 22, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1271, 24, 23, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1272, 24, 24, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1273, 24, 25, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1274, 24, 26, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1275, 24, 27, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1276, 24, 28, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1277, 24, 29, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1278, 24, 30, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1279, 24, 31, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1280, 24, 32, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1281, 24, 33, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:28', '2018-02-22 17:03:53'),
(1282, 24, 34, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1283, 24, 35, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1284, 24, 36, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1285, 24, 37, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1286, 24, 38, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1287, 24, 39, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1288, 24, 40, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1289, 24, 41, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1290, 24, 42, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1291, 24, 43, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1292, 24, 44, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1293, 24, 45, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1294, 24, 46, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1295, 24, 47, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1296, 24, 48, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1297, 24, 49, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1298, 24, 50, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:29', '2018-02-22 17:03:53'),
(1299, 24, 51, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1300, 24, 52, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1301, 24, 53, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1302, 24, 54, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1303, 24, 55, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1304, 24, 56, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1305, 24, 57, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1306, 24, 58, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1307, 24, 59, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1308, 24, 60, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1309, 24, 61, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1310, 24, 62, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1311, 24, 63, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1312, 24, 64, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1313, 24, 65, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1314, 24, 66, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1315, 24, 67, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1316, 24, 68, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1317, 24, 69, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:30', '2018-02-22 17:03:53'),
(1318, 24, 70, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1319, 24, 71, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1320, 24, 72, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1321, 24, 73, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1322, 24, 74, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1323, 24, 75, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1324, 24, 76, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1325, 24, 77, 6, 'YES', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1326, 24, 78, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1327, 24, 79, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1328, 24, 80, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1329, 24, 81, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1330, 24, 82, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1331, 24, 83, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1332, 24, 84, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1333, 24, 85, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1334, 24, 86, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1335, 24, 87, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1336, 24, 88, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:31', '2018-02-22 17:03:53'),
(1337, 24, 89, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1338, 24, 90, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1339, 24, 91, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1340, 24, 92, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1341, 24, 93, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1342, 24, 94, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1343, 24, 95, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53'),
(1344, 24, 96, 6, 'NO', 'NO', 3, NULL, '2018-02-21 18:14:32', '2018-02-22 17:03:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timing`
--

CREATE TABLE `tk_store_has_timing` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `int_week` int(11) NOT NULL,
  `dt_open_time` time NOT NULL,
  `dt_close_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timing`
--

INSERT INTO `tk_store_has_timing` (`id`, `fk_store`, `int_week`, `dt_open_time`, `dt_close_time`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '07:30:36', '18:30:36', '2018-01-18 22:55:44', '2018-02-16 17:21:34'),
(2, 2, 2, '08:14:01', '19:37:00', '2018-01-18 22:55:44', '2018-01-18 17:25:44'),
(3, 2, 3, '06:15:26', '13:43:00', '2018-01-18 22:57:07', '2018-01-18 17:27:07'),
(4, 2, 4, '07:43:00', '17:37:00', '2018-01-18 22:57:07', '2018-01-18 17:27:07'),
(5, 2, 5, '06:22:27', '16:42:43', '2018-01-18 22:57:52', '2018-01-18 17:27:52'),
(6, 2, 6, '10:50:00', '20:48:00', '2018-01-18 22:57:52', '2018-01-19 17:52:15'),
(7, 2, 0, '08:00:00', '18:00:00', '2018-01-18 23:14:10', '2018-01-21 10:28:46'),
(9, 11, 0, '08:00:00', '20:00:00', '2018-01-24 16:53:15', '2018-01-24 16:55:52'),
(10, 11, 1, '08:00:00', '19:00:00', '2018-01-24 16:53:15', '2018-01-24 16:53:15'),
(11, 11, 2, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(12, 11, 3, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(13, 11, 4, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(14, 11, 5, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(15, 11, 6, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(16, 12, 0, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(17, 12, 1, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(18, 12, 2, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(19, 12, 3, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(20, 12, 4, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50'),
(21, 12, 5, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50'),
(22, 12, 6, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50'),
(93, 23, 0, '10:00:00', '20:00:00', '2018-02-21 16:16:15', '2018-02-21 17:05:19'),
(94, 23, 1, '09:00:00', '20:00:00', '2018-02-21 16:16:20', '2018-02-21 16:16:20'),
(95, 23, 2, '09:00:00', '20:00:00', '2018-02-21 16:16:25', '2018-02-21 16:16:25'),
(96, 23, 3, '09:00:00', '20:00:00', '2018-02-21 16:16:29', '2018-02-21 16:16:29'),
(97, 23, 4, '09:00:00', '20:00:00', '2018-02-21 16:16:34', '2018-02-21 16:16:34'),
(98, 23, 5, '09:00:00', '20:00:00', '2018-02-21 16:16:39', '2018-02-21 16:16:39'),
(99, 23, 6, '09:00:00', '20:00:00', '2018-02-21 16:16:44', '2018-02-21 16:16:44'),
(100, 24, 0, '09:00:00', '20:00:00', '2018-02-21 18:13:56', '2018-02-21 18:13:56'),
(101, 24, 1, '09:00:00', '20:00:00', '2018-02-21 18:14:01', '2018-02-21 18:14:01'),
(102, 24, 2, '09:00:00', '20:00:00', '2018-02-21 18:14:07', '2018-02-21 18:14:07'),
(103, 24, 3, '09:00:00', '20:00:00', '2018-02-21 18:14:12', '2018-02-21 18:14:12'),
(104, 24, 4, '09:00:00', '20:00:00', '2018-02-21 18:14:17', '2018-02-21 18:14:17'),
(105, 24, 5, '09:00:00', '20:00:00', '2018-02-21 18:14:21', '2018-02-21 18:14:21'),
(106, 24, 6, '09:00:00', '20:00:00', '2018-02-21 18:14:27', '2018-02-21 18:14:27');

-- --------------------------------------------------------

--
-- Table structure for table `tk_timeslots`
--

CREATE TABLE `tk_timeslots` (
  `id` int(11) NOT NULL,
  `slot_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_timeslots`
--

INSERT INTO `tk_timeslots` (`id`, `slot_time`, `created_at`, `updated_at`) VALUES
(1, '01:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(2, '01:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(3, '01:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(4, '01:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(5, '02:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(6, '02:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(7, '02:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(8, '02:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(9, '09:45:00', '2017-12-15 00:00:00', '2018-01-05 18:37:23'),
(10, '03:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(11, '03:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(12, '03:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(13, '04:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(14, '04:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(15, '04:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(16, '04:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(17, '05:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(18, '05:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(19, '05:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(20, '05:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(21, '06:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(22, '06:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(23, '23:10:00', '2017-12-15 00:00:00', '2018-01-12 18:17:20'),
(24, '06:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(25, '07:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(26, '07:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(27, '07:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(28, '07:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(29, '08:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(30, '08:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(31, '08:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(32, '08:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(33, '09:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(34, '09:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(35, '09:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(36, '09:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(37, '10:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(38, '10:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(39, '10:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(40, '10:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(41, '11:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(42, '11:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(43, '11:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(44, '11:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(45, '12:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(46, '12:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(47, '12:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(48, '12:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(49, '13:00:00', '0000-00-00 00:00:00', '2017-12-15 18:44:27'),
(50, '13:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(51, '13:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(52, '13:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(53, '14:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(54, '14:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(55, '14:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(56, '14:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(57, '15:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(58, '15:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(59, '15:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(60, '15:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(61, '16:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(62, '16:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(63, '16:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(64, '16:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(65, '17:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(66, '17:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(67, '17:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(68, '17:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(69, '18:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(70, '18:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(71, '18:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(72, '18:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(73, '19:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(74, '19:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(75, '19:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(76, '19:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(77, '20:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(78, '20:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(79, '20:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(80, '20:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(81, '21:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(82, '21:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(83, '21:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(84, '21:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(85, '22:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(86, '22:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(87, '22:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(88, '22:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(89, '23:00:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(90, '23:15:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(91, '23:30:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(92, '23:45:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(93, '00:00:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(94, '00:15:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(95, '00:30:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(96, '00:45:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_users`
--

CREATE TABLE `tk_users` (
  `id` int(255) NOT NULL,
  `var_fname` varchar(255) DEFAULT NULL,
  `var_lname` varchar(255) DEFAULT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) NOT NULL,
  `int_otp` int(6) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `int_areacode` int(11) NOT NULL,
  `dt_dob` date DEFAULT NULL,
  `enum_geneder` enum('M','F') NOT NULL DEFAULT 'M',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_users`
--

INSERT INTO `tk_users` (`id`, `var_fname`, `var_lname`, `var_email`, `bint_phone`, `int_otp`, `var_profile_image`, `int_areacode`, `dt_dob`, `enum_geneder`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'test fname', 'test lname', 'test@test.com', '9727190205', 235689, NULL, 382415, '1993-01-10', 'M', 'YES', '2017-11-04 10:34:55', '2018-01-03 17:31:55'),
(19, 'testestsetset', 'setsetest', 'estests@sfds.com', '1222222', NULL, NULL, 395008, '1992-01-10', 'F', 'YES', '2018-01-03 23:18:04', '2018-02-17 19:17:35'),
(20, 'fisrt name12', 'last name12', 'first+101@gmail.com', '999999999', NULL, 'user_1515321722.jpg', 9856325, '1994-10-02', 'M', 'YES', '2018-01-04 16:49:59', '2018-02-18 16:48:28'),
(21, 'j user', 'j user', 'juser@gmail.com', '9924545965', 119051, 'user_1515780630.jpg', 395008, '2018-01-16', 'M', 'YES', '2018-01-12 18:10:30', '2018-02-18 10:00:22'),
(22, 'test 18022018', 'test 18022018', 'resr@dsf.co', '9856321469', NULL, 'user_1518971070.jpg', 998555, '1990-02-01', 'M', 'YES', '2018-02-18 16:24:30', '2018-02-18 16:26:30');

-- --------------------------------------------------------

--
-- Table structure for table `tk_week`
--

CREATE TABLE `tk_week` (
  `id` int(11) NOT NULL,
  `var_name` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_week`
--

INSERT INTO `tk_week` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'sunday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(2, 'monday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(3, 'tuesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(4, 'wednesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(5, 'thursday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(6, 'friday', '2017-12-15 00:00:00', '2017-12-15 17:49:06'),
(7, 'saturday', '2017-12-15 00:00:00', '2017-12-15 17:50:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tk_admin`
--
ALTER TABLE `tk_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking` (`fk_booking`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_timeslot` (`fk_timeslot`),
  ADD KEY `fk_booking` (`fk_booking`);

--
-- Indexes for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_master_category`
--
ALTER TABLE `tk_master_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_offer` (`fk_offer`),
  ADD KEY `fk_service` (`fk_service`),
  ADD KEY `fk_offer_2` (`fk_offer`);

--
-- Indexes for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_package` (`fk_package`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_setting`
--
ALTER TABLE `tk_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_setting` (`fk_setting`),
  ADD KEY `fk_store_2` (`fk_store`,`fk_setting`);

--
-- Indexes for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_timeslot` (`fk_timeslot`);

--
-- Indexes for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_id` (`fk_store`);

--
-- Indexes for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_users`
--
ALTER TABLE `tk_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_week`
--
ALTER TABLE `tk_week`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tk_admin`
--
ALTER TABLE `tk_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_booking`
--
ALTER TABLE `tk_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_master_category`
--
ALTER TABLE `tk_master_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `tk_services`
--
ALTER TABLE `tk_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_setting`
--
ALTER TABLE `tk_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_stores`
--
ALTER TABLE `tk_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1345;
--
-- AUTO_INCREMENT for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `tk_users`
--
ALTER TABLE `tk_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tk_week`
--
ALTER TABLE `tk_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD CONSTRAINT `tk_booking_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD CONSTRAINT `tk_booking_has_service_ibfk_1` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_1` FOREIGN KEY (`fk_timeslot`) REFERENCES `tk_timeslots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_2` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking_has_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_1` FOREIGN KEY (`fk_bussiness`) REFERENCES `tk_bussiness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_2` FOREIGN KEY (`fk_category`) REFERENCES `tk_bussiness_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD CONSTRAINT `tk_offer_has_service_ibfk_1` FOREIGN KEY (`fk_offer`) REFERENCES `tk_store_has_offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD CONSTRAINT `tk_package_has_service_ibfk_1` FOREIGN KEY (`fk_package`) REFERENCES `tk_store_has_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_package_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD CONSTRAINT `tk_services_ibfk_1` FOREIGN KEY (`fk_category`) REFERENCES `tk_master_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD CONSTRAINT `tk_stores_ibfk_1` FOREIGN KEY (`fk_bussiness`) REFERENCES `tk_bussiness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_stores_ibfk_2` FOREIGN KEY (`fk_category`) REFERENCES `tk_master_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD CONSTRAINT `tk_store_has_customer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_customer_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD CONSTRAINT `tk_store_has_gallery_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD CONSTRAINT `tk_store_has_offer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD CONSTRAINT `tk_store_has_package_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD CONSTRAINT `tk_store_has_services_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_services_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD CONSTRAINT `tk_store_has_setting_ibfk_1` FOREIGN KEY (`fk_setting`) REFERENCES `tk_setting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD CONSTRAINT `tk_store_has_staff_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  ADD CONSTRAINT `tk_store_has_timeslots_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_timeslots_ibfk_2` FOREIGN KEY (`fk_timeslot`) REFERENCES `tk_timeslots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD CONSTRAINT `tk_store_has_timing_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
