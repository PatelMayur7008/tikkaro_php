-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 19, 2018 at 08:40 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikkaro`
--

-- --------------------------------------------------------

--
-- Table structure for table `tk_admin`
--

CREATE TABLE `tk_admin` (
  `id` int(11) NOT NULL,
  `var_fname` varchar(100) NOT NULL,
  `var_lname` varchar(100) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `var_password` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `txt_address` text,
  `dt_dob` datetime DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_admin`
--

INSERT INTO `tk_admin` (`id`, `var_fname`, `var_lname`, `var_email`, `var_password`, `bint_phone`, `var_profile_image`, `txt_address`, `dt_dob`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@admin.com', 'e6e061838856bf47e1de730719fb2609', '9727190205', 'Profile_1515325777.jpg', 'Lorem Ipsum text here...123', '1994-05-19 00:00:00', 'YES', '2017-11-03 00:00:00', '2018-01-07 11:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barbers`
--

CREATE TABLE `tk_barbers` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `txt_address` text,
  `var_rating` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barbers`
--

INSERT INTO `tk_barbers` (`id`, `var_name`, `bint_phone`, `txt_address`, `var_rating`, `created_at`, `updated_at`) VALUES
(1, 'Jignesh vanand', '8487546789', 'Lorem ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(2, 'Rutvik Vanand', '5454878952', 'Lorem Ipsum text here', 4, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(3, 'Nirav patel', '9854894556', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(4, 'Mayur Patel', '7894855896', 'Lorem ipsum text here..', 4, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(5, 'Gaurav Prajapati', '13213132132132', 'Lorem Ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35'),
(6, 'Jayesh Mojidra', '54654654321321', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barber_types`
--

CREATE TABLE `tk_barber_types` (
  `id` int(11) NOT NULL,
  `var_type` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barber_types`
--

INSERT INTO `tk_barber_types` (`id`, `var_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53'),
(2, 'Worker', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking`
--

CREATE TABLE `tk_booking` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `dt_booking_date` datetime NOT NULL,
  `var_price` varchar(100) NOT NULL,
  `enum_book_type` enum('CANCEL','COMPLETED','UPCOMING') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking`
--

INSERT INTO `tk_booking` (`id`, `fk_store`, `fk_user`, `dt_booking_date`, `var_price`, `enum_book_type`, `created_at`, `updated_at`) VALUES
(10, 24, 26, '2018-04-07 00:00:00', '150', 'UPCOMING', '2018-04-06 00:00:00', '2018-04-06 19:00:04'),
(11, 24, 26, '2018-04-07 00:00:00', '150', 'UPCOMING', '2018-04-06 00:00:00', '2018-04-06 20:55:54'),
(12, 24, 26, '2018-04-10 00:00:00', '55', 'UPCOMING', '2018-04-06 00:00:00', '2018-04-06 20:58:51'),
(13, 24, 26, '2018-04-17 00:00:00', '55', 'UPCOMING', '2018-04-06 00:00:00', '2018-04-06 20:59:46'),
(14, 24, 26, '2018-04-19 00:00:00', '55', 'UPCOMING', '2018-04-06 00:00:00', '2018-04-06 21:02:49'),
(15, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:22:34'),
(16, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:47'),
(17, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:51'),
(18, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(19, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:56'),
(20, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(21, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:24:59'),
(22, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(23, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(24, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:25:04'),
(25, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(26, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(27, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(28, 25, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(29, 24, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:55:40'),
(30, 24, 26, '2018-04-18 00:00:00', '60', 'UPCOMING', '2018-04-18 00:00:00', '2018-04-18 18:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_service`
--

CREATE TABLE `tk_booking_has_service` (
  `id` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_service`
--

INSERT INTO `tk_booking_has_service` (`id`, `fk_booking`, `fk_service`, `created_at`, `updated_at`) VALUES
(8, 10, 13, '2018-04-06 00:00:00', '2018-04-06 19:00:04'),
(9, 11, 13, '2018-04-06 00:00:00', '2018-04-06 20:55:55'),
(10, 12, 13, '2018-04-06 00:00:00', '2018-04-06 20:58:51'),
(11, 13, 13, '2018-04-06 00:00:00', '2018-04-06 20:59:46'),
(12, 14, 13, '2018-04-06 00:00:00', '2018-04-06 21:02:50'),
(13, 15, 13, '2018-04-18 00:00:00', '2018-04-18 18:22:34'),
(14, 16, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:47'),
(15, 17, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:51'),
(16, 18, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(17, 19, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:57'),
(18, 20, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(19, 21, 13, '2018-04-18 00:00:00', '2018-04-18 18:24:59'),
(20, 22, 13, '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(21, 23, 13, '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(22, 24, 13, '2018-04-18 00:00:00', '2018-04-18 18:25:05'),
(23, 25, 13, '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(24, 26, 13, '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(25, 27, 13, '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(26, 28, 13, '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(27, 29, 13, '2018-04-18 00:00:00', '2018-04-18 18:55:40'),
(28, 30, 13, '2018-04-18 00:00:00', '2018-04-18 18:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_timeslot`
--

CREATE TABLE `tk_booking_has_timeslot` (
  `id` int(11) NOT NULL,
  `fk_timeslot` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_timeslot`
--

INSERT INTO `tk_booking_has_timeslot` (`id`, `fk_timeslot`, `fk_booking`, `created_at`, `updated_at`) VALUES
(10, 33, 10, '2018-04-06 00:00:00', '2018-04-06 19:00:05'),
(11, 34, 10, '2018-04-06 00:00:00', '2018-04-06 19:00:05'),
(12, 9, 11, '2018-04-06 00:00:00', '2018-04-06 20:55:55'),
(13, 36, 11, '2018-04-06 00:00:00', '2018-04-06 20:55:55'),
(14, 37, 11, '2018-04-06 00:00:00', '2018-04-06 20:55:55'),
(15, 9, 12, '2018-04-06 00:00:00', '2018-04-06 20:58:52'),
(16, 36, 12, '2018-04-06 00:00:00', '2018-04-06 20:58:52'),
(17, 37, 12, '2018-04-06 00:00:00', '2018-04-06 20:58:52'),
(18, 9, 13, '2018-04-06 00:00:00', '2018-04-06 20:59:46'),
(19, 36, 13, '2018-04-06 00:00:00', '2018-04-06 20:59:46'),
(20, 37, 13, '2018-04-06 00:00:00', '2018-04-06 20:59:46'),
(21, 9, 14, '2018-04-06 00:00:00', '2018-04-06 21:02:50'),
(22, 36, 14, '2018-04-06 00:00:00', '2018-04-06 21:02:50'),
(23, 37, 14, '2018-04-06 00:00:00', '2018-04-06 21:02:50'),
(24, 9, 15, '2018-04-18 00:00:00', '2018-04-18 18:22:34'),
(25, 36, 15, '2018-04-18 00:00:00', '2018-04-18 18:22:34'),
(26, 37, 15, '2018-04-18 00:00:00', '2018-04-18 18:22:34'),
(27, 38, 15, '2018-04-18 00:00:00', '2018-04-18 18:22:35'),
(28, 9, 16, '2018-04-18 00:00:00', '2018-04-18 18:24:48'),
(29, 36, 16, '2018-04-18 00:00:00', '2018-04-18 18:24:48'),
(30, 37, 16, '2018-04-18 00:00:00', '2018-04-18 18:24:48'),
(31, 38, 16, '2018-04-18 00:00:00', '2018-04-18 18:24:48'),
(32, 9, 17, '2018-04-18 00:00:00', '2018-04-18 18:24:52'),
(33, 36, 17, '2018-04-18 00:00:00', '2018-04-18 18:24:52'),
(34, 37, 17, '2018-04-18 00:00:00', '2018-04-18 18:24:52'),
(35, 38, 17, '2018-04-18 00:00:00', '2018-04-18 18:24:52'),
(36, 9, 18, '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(37, 36, 18, '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(38, 37, 18, '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(39, 38, 18, '2018-04-18 00:00:00', '2018-04-18 18:24:55'),
(40, 9, 19, '2018-04-18 00:00:00', '2018-04-18 18:24:57'),
(41, 36, 19, '2018-04-18 00:00:00', '2018-04-18 18:24:57'),
(42, 37, 19, '2018-04-18 00:00:00', '2018-04-18 18:24:57'),
(43, 38, 19, '2018-04-18 00:00:00', '2018-04-18 18:24:57'),
(44, 9, 20, '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(45, 36, 20, '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(46, 37, 20, '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(47, 38, 20, '2018-04-18 00:00:00', '2018-04-18 18:24:58'),
(48, 9, 21, '2018-04-18 00:00:00', '2018-04-18 18:24:59'),
(49, 36, 21, '2018-04-18 00:00:00', '2018-04-18 18:24:59'),
(50, 37, 21, '2018-04-18 00:00:00', '2018-04-18 18:25:00'),
(51, 38, 21, '2018-04-18 00:00:00', '2018-04-18 18:25:00'),
(52, 9, 22, '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(53, 36, 22, '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(54, 37, 22, '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(55, 38, 22, '2018-04-18 00:00:00', '2018-04-18 18:25:01'),
(56, 9, 23, '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(57, 36, 23, '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(58, 37, 23, '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(59, 38, 23, '2018-04-18 00:00:00', '2018-04-18 18:25:03'),
(60, 9, 24, '2018-04-18 00:00:00', '2018-04-18 18:25:05'),
(61, 36, 24, '2018-04-18 00:00:00', '2018-04-18 18:25:05'),
(62, 37, 24, '2018-04-18 00:00:00', '2018-04-18 18:25:05'),
(63, 38, 24, '2018-04-18 00:00:00', '2018-04-18 18:25:05'),
(64, 9, 25, '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(65, 36, 25, '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(66, 37, 25, '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(67, 38, 25, '2018-04-18 00:00:00', '2018-04-18 18:26:57'),
(68, 9, 26, '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(69, 36, 26, '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(70, 37, 26, '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(71, 38, 26, '2018-04-18 00:00:00', '2018-04-18 18:26:59'),
(72, 9, 27, '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(73, 36, 27, '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(74, 37, 27, '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(75, 38, 27, '2018-04-18 00:00:00', '2018-04-18 18:27:00'),
(76, 9, 28, '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(77, 36, 28, '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(78, 37, 28, '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(79, 38, 28, '2018-04-18 00:00:00', '2018-04-18 18:30:57'),
(80, 9, 29, '2018-04-18 00:00:00', '2018-04-18 18:55:40'),
(81, 36, 29, '2018-04-18 00:00:00', '2018-04-18 18:55:40'),
(82, 37, 29, '2018-04-18 00:00:00', '2018-04-18 18:55:41'),
(83, 9, 30, '2018-04-18 00:00:00', '2018-04-18 18:55:42'),
(84, 36, 30, '2018-04-18 00:00:00', '2018-04-18 18:55:42'),
(85, 37, 30, '2018-04-18 00:00:00', '2018-04-18 18:55:42');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness`
--

CREATE TABLE `tk_bussiness` (
  `id` int(11) NOT NULL,
  `var_bussiness_code` varchar(100) DEFAULT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_phone` varchar(50) DEFAULT NULL,
  `int_otp` int(11) DEFAULT NULL,
  `var_email` varchar(100) DEFAULT NULL,
  `txt_note` text,
  `var_pushy_device_id` varchar(255) DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness`
--

INSERT INTO `tk_bussiness` (`id`, `var_bussiness_code`, `var_name`, `var_phone`, `int_otp`, `var_email`, `txt_note`, `var_pushy_device_id`, `enum_enable`, `created_at`, `updated_at`) VALUES
(6, 'YAB276', 'Barber Shop', '9999999999', 390014, 'testbarber@gmail.com', ' ', '05a6321f900ac76b2e1ec5', 'YES', '2018-04-01 09:54:09', '2018-04-01 09:54:09'),
(7, 'noi424', 'Chamunda Hair Style', '8888888888', 384250, 'chamunda@gmail.com', ' ', '05a6321f900ac76b2e1ec5', 'YES', '2018-04-04 18:02:19', '2018-04-04 18:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_category`
--

CREATE TABLE `tk_bussiness_category` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_category`
--

INSERT INTO `tk_bussiness_category` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'Barber', '2018-01-22 22:16:27', '2018-01-22 16:46:27');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_has_category`
--

CREATE TABLE `tk_bussiness_has_category` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_has_category`
--

INSERT INTO `tk_bussiness_has_category` (`id`, `fk_bussiness`, `fk_category`, `created_at`, `updated_at`) VALUES
(10, 6, 1, '2018-04-01 09:54:09', '2018-04-01 09:54:09'),
(11, 7, 1, '2018-04-04 18:02:19', '2018-04-04 18:02:19');

-- --------------------------------------------------------

--
-- Table structure for table `tk_extra_facility`
--

CREATE TABLE `tk_extra_facility` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_extra_facility`
--

INSERT INTO `tk_extra_facility` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'WIFI', 'YES', '2018-01-09 23:14:11', '2018-01-28 09:12:23'),
(2, 'AC', 'YES', '2018-01-09 23:14:11', '2018-01-09 17:44:11'),
(3, 'Game Area', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32'),
(4, 'Movie Zone', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_master_category`
--

CREATE TABLE `tk_master_category` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `enum_type` enum('SERVICE','STORE') NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_master_category`
--

INSERT INTO `tk_master_category` (`id`, `var_name`, `enum_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(6, 'Hair Saloon', 'STORE', 'YES', '2018-04-01 09:58:50', '2018-04-01 09:58:50'),
(7, 'Nail Spa', 'STORE', 'YES', '2018-04-01 09:59:04', '2018-04-01 09:59:04'),
(8, 'Brows and lashes', 'STORE', 'YES', '2018-04-01 09:59:33', '2018-04-01 09:59:33'),
(9, 'Hair Stylists', 'STORE', 'YES', '2018-04-01 09:59:56', '2018-04-01 09:59:56'),
(10, 'Spa and massage', 'STORE', 'YES', '2018-04-01 10:00:15', '2018-04-01 10:00:15'),
(11, 'Body art', 'STORE', 'YES', '2018-04-01 10:00:28', '2018-04-01 10:00:28'),
(12, 'Bridal makeup', 'STORE', 'YES', '2018-04-01 10:00:48', '2018-04-01 10:00:48'),
(13, 'Ladies beauty parlors', 'STORE', 'YES', '2018-04-01 10:01:10', '2018-04-01 10:01:10'),
(14, 'Other', 'STORE', 'YES', '2018-04-01 10:01:26', '2018-04-01 10:01:26'),
(15, 'Shave', 'SERVICE', 'YES', '2018-04-01 10:15:17', '2018-04-01 10:15:17'),
(16, 'Skin care', 'SERVICE', 'YES', '2018-04-01 10:15:37', '2018-04-01 10:15:37'),
(17, 'Hair care', 'SERVICE', 'YES', '2018-04-01 10:15:53', '2018-04-01 10:15:53'),
(18, 'Body care', 'SERVICE', 'YES', '2018-04-01 10:16:24', '2018-04-01 10:16:24');

-- --------------------------------------------------------

--
-- Table structure for table `tk_migrations`
--

CREATE TABLE `tk_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_migrations`
--

INSERT INTO `tk_migrations` (`version`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `tk_offer_has_service`
--

CREATE TABLE `tk_offer_has_service` (
  `id` int(11) NOT NULL,
  `fk_offer` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tk_package_has_service`
--

CREATE TABLE `tk_package_has_service` (
  `id` int(11) NOT NULL,
  `fk_package` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_package_has_service`
--

INSERT INTO `tk_package_has_service` (`id`, `fk_package`, `fk_service`, `var_price`, `created_at`, `updated_at`) VALUES
(80, 8, 17, '100', '2018-04-01 11:24:30', '2018-04-01 11:24:30'),
(81, 8, 19, '500', '2018-04-01 11:24:30', '2018-04-01 11:24:30'),
(83, 9, 13, '50', '2018-04-01 11:25:29', '2018-04-01 11:25:29'),
(84, 9, 24, '100', '2018-04-01 11:25:29', '2018-04-01 11:25:29'),
(85, 10, 13, '100', '2018-04-04 18:33:06', '2018-04-04 18:33:06'),
(86, 10, 22, '210', '2018-04-04 18:33:06', '2018-04-04 18:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `tk_portfolio_has_review`
--

CREATE TABLE `tk_portfolio_has_review` (
  `id` int(11) NOT NULL,
  `fk_portfolio` int(11) NOT NULL,
  `txt_review` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_portfolio_has_review`
--

INSERT INTO `tk_portfolio_has_review` (`id`, `fk_portfolio`, `txt_review`, `created_at`, `updated_at`) VALUES
(1, 2, 'test', '2018-04-19 23:11:04', '2018-04-19 17:41:04'),
(2, 2, 'sdfasdfadsfasdf', '2018-04-19 23:11:04', '2018-04-19 17:41:04'),
(3, 2, 'testing review', '2018-04-19 06:10:32', '2018-04-19 18:10:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_services`
--

CREATE TABLE `tk_services` (
  `id` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_services`
--

INSERT INTO `tk_services` (`id`, `fk_category`, `var_title`, `enum_enable`, `created_at`, `updated_at`) VALUES
(13, 17, 'Hair cut', 'YES', '2018-04-01 10:16:43', '2018-04-01 10:16:43'),
(14, 15, 'Shaving', 'YES', '2018-04-01 10:16:55', '2018-04-01 10:17:50'),
(15, 15, 'Beard trim', 'YES', '2018-04-01 10:17:17', '2018-04-01 10:17:17'),
(16, 15, 'Beard spa', 'YES', '2018-04-01 10:17:39', '2018-04-01 10:17:39'),
(17, 16, 'Face wash', 'YES', '2018-04-01 10:18:06', '2018-04-01 10:18:06'),
(18, 16, 'Face massage', 'YES', '2018-04-01 10:18:24', '2018-04-01 10:18:24'),
(19, 16, 'Facial', 'YES', '2018-04-01 10:18:37', '2018-04-01 10:18:37'),
(20, 16, 'Face clean-up', 'YES', '2018-04-01 10:18:51', '2018-04-01 10:18:51'),
(21, 17, 'Oil massage', 'YES', '2018-04-01 10:19:15', '2018-04-01 10:19:15'),
(22, 17, 'Hair style', 'YES', '2018-04-01 10:19:29', '2018-04-01 10:19:29'),
(23, 17, 'Kids haircut', 'YES', '2018-04-01 10:19:50', '2018-04-01 10:19:50'),
(24, 17, 'Hair color', 'YES', '2018-04-01 10:20:06', '2018-04-01 10:20:06');

-- --------------------------------------------------------

--
-- Table structure for table `tk_setting`
--

CREATE TABLE `tk_setting` (
  `id` int(11) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_setting`
--

INSERT INTO `tk_setting` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'var_fb_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(2, 'var_insta_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(3, 'var_twitter_link', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24'),
(4, 'var_whatsapp_no', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24');

-- --------------------------------------------------------

--
-- Table structure for table `tk_stores`
--

CREATE TABLE `tk_stores` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `fk_category` int(11) DEFAULT NULL,
  `var_title` varchar(255) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `txt_note` text,
  `var_store_code` varchar(255) NOT NULL,
  `int_otp` int(11) DEFAULT NULL,
  `txt_address` text,
  `var_logo` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) DEFAULT NULL,
  `var_longitude` varchar(255) DEFAULT NULL,
  `int_areacode` int(11) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `int_weekoff` int(11) DEFAULT NULL,
  `int_no_book_seats` int(11) DEFAULT NULL,
  `var_fb_link` varchar(255) DEFAULT NULL,
  `var_twitter_link` varchar(255) DEFAULT NULL,
  `var_instagram_link` varchar(255) DEFAULT NULL,
  `var_whatsapp_no` varchar(50) DEFAULT NULL,
  `var_rating` varchar(20) DEFAULT NULL,
  `enum_store_type` enum('MEN','WOMEN','BOTH') NOT NULL DEFAULT 'MEN',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_stores`
--

INSERT INTO `tk_stores` (`id`, `fk_bussiness`, `fk_category`, `var_title`, `var_email`, `txt_note`, `var_store_code`, `int_otp`, `txt_address`, `var_logo`, `var_latitude`, `var_longitude`, `int_areacode`, `bint_phone`, `int_weekoff`, `int_no_book_seats`, `var_fb_link`, `var_twitter_link`, `var_instagram_link`, `var_whatsapp_no`, `var_rating`, `enum_store_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(24, 6, 6, 'Om Hair Saloon', 'null', 'null', 'ujR938', NULL, 'Arbudanagar, Mahadev Nagar Tekra, Ahmedabad, Gujarat, India', 'receipt_pic1523036517.jpg', '23.0083427', '72.64791059999993', 380038, '9999999999', 6, 2, NULL, NULL, NULL, '9999999999', NULL, 'MEN', 'YES', '2018-04-01 10:04:06', '2018-04-06 17:41:57'),
(25, 7, 6, 'Chamunda Hair Style', NULL, NULL, 'liw713', NULL, '2, Arbuda Nagar Road, Mahadev Nagar Tekra, Ahmedabad, Gujarat, India', 'store_logo_1522865019.jpg', '23.0116601', '72.64482420000002', 380024, '8888888888', 0, 3, NULL, NULL, NULL, '8888888888', '5', 'BOTH', 'YES', '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(26, 6, 6, 'dsfasdfasd', NULL, NULL, 'gEx987', NULL, 'Test Yantra Software Solutions India Pvt Ltd, Gandhi Bazaar, Basavanagudi, Bengaluru, Karnataka, India', NULL, '12.9480282', '77.57300959999998', 560004, '9898989898', 0, 3, NULL, NULL, NULL, NULL, NULL, 'MEN', 'YES', '2018-04-06 19:51:26', '2018-04-06 19:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_bank_details`
--

CREATE TABLE `tk_store_has_bank_details` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_accountno` varchar(255) NOT NULL,
  `var_ifsc_code` varchar(255) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `var_bank_name` varchar(500) NOT NULL,
  `var_branch_name` varchar(500) NOT NULL,
  `txt_address` text NOT NULL,
  `enum_acc_type` enum('S','C') NOT NULL COMMENT '''S''=>''Saving'',''C''=>''Current''',
  `var_debit_card_type` varchar(255) NOT NULL,
  `var_card_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_bank_details`
--

INSERT INTO `tk_store_has_bank_details` (`id`, `fk_store`, `var_accountno`, `var_ifsc_code`, `var_name`, `var_bank_name`, `var_branch_name`, `txt_address`, `enum_acc_type`, `var_debit_card_type`, `var_card_number`, `created_at`, `updated_at`) VALUES
(1, 6, '999885554', 'unb0012', 'jay shah', 'union bank', 'vastral', 'vastral', 'C', 'debit', '123456987412', '2018-01-12 17:41:45', '2018-01-12 17:42:11'),
(3, 2, 'hdfhdfh7878121212', 'fsdfa', 'TEST', 'state bank of india', 'VASTRAL', 'TRST', '', 'D', 'asdasdasdgsdgfhfdhdfhdfhfd', '2018-01-27 13:38:34', '2018-02-14 16:45:16'),
(4, 7, '778788778', 'icici785412', 'test', 'test', 'test', 'setset', 'S', 'dfsdfgdfg', '7854124587', '2018-01-28 09:14:11', '2018-01-28 09:14:11'),
(5, 5, 'strore 5 bank1', 'IFSC778', 'TEst name', 'Axis Bank', 'Ashram road', 'Ashram road', 'S', 'AXIS BANK DEbit card', '789645123652', '2018-01-30 16:06:41', '2018-01-30 16:06:50'),
(6, 1, 'asfasdfa', 'sdfasdf', 'asdfsd', 'fasdf', 'asdfasdfa', 'sdfasdfasdf', '', 'C', 'sfasfasdfsdf', '2018-02-26 17:27:20', '2018-02-26 17:27:20'),
(7, 24, '985632145544454', '54d5sfsf', 'asdfasdfsdfa', 'sdfasdf', 'asdf', '985632145544454', '', 'D', '98565466565656', '2018-03-06 16:44:10', '2018-04-01 11:11:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_barber`
--

CREATE TABLE `tk_store_has_barber` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_barber` int(11) NOT NULL,
  `fk_barber_type` int(11) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_barber`
--

INSERT INTO `tk_store_has_barber` (`id`, `fk_store`, `fk_barber`, `fk_barber_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(2, 1, 2, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(3, 2, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(4, 2, 4, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(5, 3, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(6, 4, 4, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(7, 5, 5, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57'),
(8, 6, 6, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_customer`
--

CREATE TABLE `tk_store_has_customer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_facility`
--

CREATE TABLE `tk_store_has_facility` (
  `id` int(11) NOT NULL,
  `fk_facility` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_facility`
--

INSERT INTO `tk_store_has_facility` (`id`, `fk_facility`, `fk_store`, `created_at`, `updated_at`) VALUES
(26, 1, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(27, 2, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(28, 3, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(45, 0, 1, '0000-00-00 00:00:00', '2018-03-30 17:10:56'),
(46, 0, 24, '0000-00-00 00:00:00', '2018-04-06 17:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_gallery`
--

CREATE TABLE `tk_store_has_gallery` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_parent` int(11) DEFAULT NULL,
  `var_file` varchar(500) NOT NULL,
  `var_width` varchar(50) DEFAULT NULL,
  `var_height` varchar(50) DEFAULT NULL,
  `enum_isProfile` enum('YES','NO') DEFAULT NULL,
  `enum_ismain` enum('YES','NO') DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_gallery`
--

INSERT INTO `tk_store_has_gallery` (`id`, `fk_store`, `fk_parent`, `var_file`, `var_width`, `var_height`, `enum_isProfile`, `enum_ismain`, `enum_enable`, `created_at`, `updated_at`) VALUES
(25, 24, NULL, 'ujR9381522581377.jpg', '750', '440', 'NO', 'YES', 'NO', '2018-04-01 11:16:17', '2018-04-01 11:16:17'),
(26, 24, 25, 'ujR9381522581377_100x58.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-01 11:16:17', '2018-04-01 11:16:17'),
(27, 24, 25, 'ujR9381522581377_200x117.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-01 11:16:17', '2018-04-01 11:16:17'),
(28, 24, 25, 'ujR9381522581377_500x293.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-01 11:16:18', '2018-04-01 11:16:18'),
(29, 24, NULL, 'ujR9381522581383.jpg', '774', '1032', 'YES', 'YES', 'YES', '2018-04-01 11:16:23', '2018-04-01 11:16:23'),
(30, 24, 29, 'ujR9381522581383_75x100.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-01 11:16:23', '2018-04-01 11:16:23'),
(31, 24, 29, 'ujR9381522581383_150x200.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-01 11:16:23', '2018-04-01 11:16:23'),
(32, 24, 29, 'ujR9381522581383_375x500.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-01 11:16:24', '2018-04-01 11:16:24'),
(33, 24, NULL, 'ujR9381522581384.jpg', '750', '540', 'NO', 'YES', 'YES', '2018-04-01 11:16:24', '2018-04-01 11:16:24'),
(34, 24, 33, 'ujR9381522581384_100x72.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-01 11:16:24', '2018-04-01 11:16:24'),
(35, 24, 33, 'ujR9381522581384_200x144.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-01 11:16:24', '2018-04-01 11:16:24'),
(36, 24, 33, 'ujR9381522581384_500x360.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-01 11:16:24', '2018-04-01 11:16:24'),
(37, 24, NULL, 'ujR9381522583156.jpg', '928', '628', 'NO', 'YES', 'YES', '2018-04-01 11:45:56', '2018-04-01 11:45:56'),
(38, 24, 37, 'ujR9381522583156_100x67.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-01 11:45:56', '2018-04-01 11:45:56'),
(39, 24, 37, 'ujR9381522583156_200x135.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-01 11:45:56', '2018-04-01 11:45:56'),
(40, 24, 37, 'ujR9381522583156_500x338.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-01 11:45:56', '2018-04-01 11:45:56'),
(41, 25, NULL, 'liw7131522866560.jpg', '704', '1140', 'NO', 'YES', 'YES', '2018-04-04 18:29:20', '2018-04-04 18:29:20'),
(42, 25, 41, 'liw7131522866560_61x100.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-04 18:29:20', '2018-04-04 18:29:20'),
(43, 25, 41, 'liw7131522866560_123x200.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-04 18:29:20', '2018-04-04 18:29:20'),
(44, 25, 41, 'liw7131522866560_308x500.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-04 18:29:20', '2018-04-04 18:29:20'),
(49, 25, NULL, 'liw7131522866587.jpg', '696', '563', 'YES', 'YES', 'YES', '2018-04-04 18:29:47', '2018-04-04 18:29:47'),
(50, 25, 49, 'liw7131522866587_100x80.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-04-04 18:29:47', '2018-04-04 18:29:47'),
(51, 25, 49, 'liw7131522866587_200x161.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-04-04 18:29:47', '2018-04-04 18:29:47'),
(52, 25, 49, 'liw7131522866587_500x404.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-04-04 18:29:47', '2018-04-04 18:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_holidays`
--

CREATE TABLE `tk_store_has_holidays` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `dt_holiday_date` date NOT NULL,
  `dt_start_time` time NOT NULL,
  `dt_end_time` time NOT NULL,
  `txt_note` text NOT NULL,
  `enum_holiday` enum('YES','NO') DEFAULT 'NO',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_holidays`
--

INSERT INTO `tk_store_has_holidays` (`id`, `fk_store`, `dt_holiday_date`, `dt_start_time`, `dt_end_time`, `txt_note`, `enum_holiday`, `created_at`, `updated_at`) VALUES
(18, 2, '2018-01-25', '03:41:00', '03:41:00', 'dfdsfd', 'YES', '2018-01-26 22:07:07', '2018-01-26 22:29:33'),
(19, 2, '2018-01-24', '03:41:00', '03:41:00', 'dfdsfd24', 'NO', '2018-01-26 22:07:22', '2018-01-26 22:30:52'),
(23, 2, '2018-01-27', '04:00:00', '04:00:00', 'dsfsdfsdf', 'YES', '2018-01-26 22:26:20', '2018-01-26 22:26:20'),
(24, 2, '2018-01-22', '04:38:00', '04:38:00', 'dsdasd', 'YES', '2018-01-26 23:08:29', '2018-01-26 23:08:29'),
(25, 1, '2018-02-26', '22:45:00', '22:45:00', 'asdfsfasf', 'NO', '2018-02-26 17:15:41', '2018-02-26 17:15:41'),
(26, 1, '2018-02-27', '22:46:00', '22:46:00', 'fsadfasdfasd', 'NO', '2018-02-26 17:16:18', '2018-02-26 17:16:18'),
(27, 24, '2018-03-01', '07:30:00', '22:30:00', ' fasdfadsf ', 'NO', '2018-03-06 16:49:49', '2018-03-06 16:50:04'),
(28, 24, '2018-04-04', '08:37:00', '16:37:00', 'Test note', 'YES', '2018-04-01 11:08:39', '2018-04-01 11:10:03');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_offer`
--

CREATE TABLE `tk_store_has_offer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(500) NOT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `dt_start_date` date NOT NULL,
  `dt_end_date` date NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_package`
--

CREATE TABLE `tk_store_has_package` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_package`
--

INSERT INTO `tk_store_has_package` (`id`, `fk_store`, `var_name`, `var_price`, `txt_note`, `enum_enable`, `created_at`, `updated_at`) VALUES
(8, 24, 'Face Pack', '600', '', 'YES', '2018-04-01 11:24:30', '2018-04-01 11:24:30'),
(9, 24, 'Hait Care Pack', '150', 'All hair tritment', 'YES', '2018-04-01 11:25:02', '2018-04-01 11:25:02'),
(10, 25, 'Dulha Package', '310', 'This will incluse all services ', 'YES', '2018-04-04 18:33:06', '2018-04-04 18:33:06');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_portfolio`
--

CREATE TABLE `tk_store_has_portfolio` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `var_file` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_portfolio`
--

INSERT INTO `tk_store_has_portfolio` (`id`, `fk_store`, `var_title`, `var_file`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 24, 'test portfolio', '', 'YES', '2018-04-17 18:57:45', '2018-04-17 18:57:45'),
(2, 25, 'test portfolio', 'portfolio_1524076522.jpg', 'YES', '2018-04-18 18:35:22', '2018-04-18 18:35:22');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_review`
--

CREATE TABLE `tk_store_has_review` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `int_review` int(11) NOT NULL,
  `txt_text` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_services`
--

CREATE TABLE `tk_store_has_services` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(11) NOT NULL,
  `dt_service_time` time DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_services`
--

INSERT INTO `tk_store_has_services` (`id`, `fk_store`, `fk_service`, `var_price`, `dt_service_time`, `enum_enable`, `created_at`, `updated_at`) VALUES
(43, 24, 13, '55', '00:15:00', 'YES', '2018-04-01 10:21:05', '2018-04-06 17:40:42'),
(44, 24, 14, '40', '00:15:00', 'YES', '2018-04-01 10:21:23', '2018-04-01 10:21:23'),
(45, 24, 19, '300', '01:00:00', 'YES', '2018-04-01 10:21:42', '2018-04-01 10:21:42'),
(46, 24, 17, '100', '00:30:00', 'YES', '2018-04-01 10:22:00', '2018-04-01 10:22:00'),
(47, 24, 21, '50', '00:30:00', 'YES', '2018-04-01 10:22:17', '2018-04-01 10:22:17'),
(48, 24, 22, '100', '00:30:00', 'YES', '2018-04-01 10:22:33', '2018-04-01 10:22:33'),
(49, 24, 23, '40', '00:15:00', 'YES', '2018-04-01 10:22:53', '2018-04-01 10:22:53'),
(50, 24, 15, '20', '00:15:00', 'YES', '2018-04-01 10:23:05', '2018-04-01 10:23:05'),
(52, 24, 24, '100', '00:30:00', 'NO', '2018-04-01 11:01:02', '2018-04-06 17:41:01'),
(53, 25, 13, '60', '00:30:00', 'YES', '2018-04-04 18:04:20', '2018-04-04 18:04:42'),
(54, 25, 22, '100', '00:30:00', 'YES', '2018-04-04 18:05:00', '2018-04-04 18:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_setting`
--

CREATE TABLE `tk_store_has_setting` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_setting` int(11) NOT NULL,
  `var_setting_val` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_setting`
--

INSERT INTO `tk_store_has_setting` (`id`, `fk_store`, `fk_setting`, `var_setting_val`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'www.facebook.com/hairsallon', 'YES', '2018-01-17 00:17:56', '2018-01-16 18:47:56'),
(2, 2, 4, '985632145', 'YES', '2018-01-17 00:18:18', '2018-01-16 18:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_staff`
--

CREATE TABLE `tk_store_has_staff` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(255) DEFAULT NULL,
  `var_phone` varchar(100) DEFAULT NULL,
  `var_image` varchar(500) DEFAULT NULL,
  `dt_startdate` datetime DEFAULT NULL,
  `dt_enddate` datetime DEFAULT NULL,
  `enum_isFrontView` enum('YES','NO') DEFAULT NULL,
  `enum_active` enum('YES','NO') DEFAULT NULL,
  `enum_current` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_staff`
--

INSERT INTO `tk_store_has_staff` (`id`, `fk_store`, `var_name`, `var_phone`, `var_image`, `dt_startdate`, `dt_enddate`, `enum_isFrontView`, `enum_active`, `enum_current`, `created_at`, `updated_at`) VALUES
(14, 24, 'Prince Patel', '9874563212', 'staff_1522578331.jpeg', '2018-04-01 00:00:00', '1970-01-01 00:00:00', 'YES', 'YES', 'YES', '2018-04-01 10:25:31', '2018-04-04 05:29:52'),
(15, 24, 'Mayur Patel', '9877754621', 'staff_1522578357.jpeg', '2018-03-05 00:00:00', NULL, 'YES', 'YES', 'YES', '2018-04-01 10:25:57', '2018-04-01 10:25:57'),
(16, 25, 'Ronak Patel', '9854512112', '', '2018-04-01 00:00:00', '2018-04-28 00:00:00', 'YES', 'YES', 'YES', '2018-04-04 18:10:03', '2018-04-04 18:10:03'),
(17, 24, 'Nirav', '9687845056', 'receipt_pic1522999508.jpg', '2018-04-01 00:00:00', '2018-04-13 00:00:00', 'YES', 'YES', 'YES', '2018-04-06 07:25:09', '2018-04-06 07:25:09');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timeslots`
--

CREATE TABLE `tk_store_has_timeslots` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_timeslot` int(11) NOT NULL,
  `int_week_day` int(11) DEFAULT NULL,
  `enum_enable` enum('YES','NO') DEFAULT NULL,
  `enum_isfull` enum('YES','NO') DEFAULT NULL,
  `int_book_limit` int(11) DEFAULT NULL,
  `int_book_count` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timeslots`
--

INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(673, 24, 1, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(674, 24, 2, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(675, 24, 3, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(676, 24, 4, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(677, 24, 5, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(678, 24, 6, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(679, 24, 7, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(680, 24, 8, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(681, 24, 9, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:30:01'),
(682, 24, 10, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(683, 24, 11, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(684, 24, 12, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(685, 24, 13, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(686, 24, 14, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(687, 24, 15, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(688, 24, 16, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(689, 24, 17, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(690, 24, 18, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(691, 24, 19, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(692, 24, 20, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(693, 24, 21, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(694, 24, 22, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(695, 24, 23, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(696, 24, 24, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(697, 24, 25, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(698, 24, 26, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(699, 24, 27, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(700, 24, 28, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(701, 24, 29, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(702, 24, 30, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(703, 24, 31, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(704, 24, 32, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(705, 24, 33, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(706, 24, 34, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(707, 24, 35, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(708, 24, 36, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(709, 24, 37, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(710, 24, 38, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(711, 24, 39, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(712, 24, 40, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(713, 24, 41, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(714, 24, 42, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(715, 24, 43, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(716, 24, 44, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(717, 24, 45, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(718, 24, 46, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(719, 24, 47, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(720, 24, 48, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(721, 24, 49, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(722, 24, 50, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(723, 24, 51, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(724, 24, 52, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(725, 24, 53, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(726, 24, 54, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(727, 24, 55, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(728, 24, 56, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(729, 24, 57, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(730, 24, 58, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(731, 24, 59, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(732, 24, 60, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(733, 24, 61, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(734, 24, 62, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(735, 24, 63, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(736, 24, 64, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(737, 24, 65, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(738, 24, 66, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(739, 24, 67, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(740, 24, 68, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(741, 24, 69, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(742, 24, 70, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(743, 24, 71, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(744, 24, 72, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(745, 24, 73, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(746, 24, 74, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(747, 24, 75, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(748, 24, 76, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(749, 24, 77, 0, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(750, 24, 78, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(751, 24, 79, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(752, 24, 80, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(753, 24, 81, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(754, 24, 82, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(755, 24, 83, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(756, 24, 84, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(757, 24, 85, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(758, 24, 86, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(759, 24, 87, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(760, 24, 88, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(761, 24, 89, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(762, 24, 90, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(763, 24, 91, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(764, 24, 92, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(765, 24, 93, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(766, 24, 94, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(767, 24, 95, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(768, 24, 96, 0, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(769, 24, 1, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(770, 24, 2, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(771, 24, 3, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(772, 24, 4, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(773, 24, 5, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(774, 24, 6, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(775, 24, 7, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(776, 24, 8, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(777, 24, 9, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(778, 24, 10, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(779, 24, 11, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(780, 24, 12, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(781, 24, 13, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(782, 24, 14, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(783, 24, 15, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(784, 24, 16, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(785, 24, 17, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(786, 24, 18, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(787, 24, 19, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(788, 24, 20, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(789, 24, 21, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(790, 24, 22, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(791, 24, 23, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(792, 24, 24, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(793, 24, 25, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(794, 24, 26, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(795, 24, 27, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(796, 24, 28, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(797, 24, 29, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(798, 24, 30, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(799, 24, 31, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(800, 24, 32, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(801, 24, 33, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(802, 24, 34, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(803, 24, 35, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(804, 24, 36, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(805, 24, 37, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(806, 24, 38, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(807, 24, 39, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(808, 24, 40, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(809, 24, 41, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(810, 24, 42, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(811, 24, 43, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(812, 24, 44, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(813, 24, 45, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(814, 24, 46, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(815, 24, 47, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(816, 24, 48, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(817, 24, 49, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(818, 24, 50, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(819, 24, 51, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(820, 24, 52, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(821, 24, 53, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(822, 24, 54, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(823, 24, 55, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(824, 24, 56, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(825, 24, 57, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(826, 24, 58, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(827, 24, 59, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(828, 24, 60, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(829, 24, 61, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(830, 24, 62, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(831, 24, 63, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(832, 24, 64, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(833, 24, 65, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(834, 24, 66, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(835, 24, 67, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(836, 24, 68, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(837, 24, 69, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(838, 24, 70, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(839, 24, 71, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(840, 24, 72, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(841, 24, 73, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(842, 24, 74, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(843, 24, 75, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(844, 24, 76, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(845, 24, 77, 1, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(846, 24, 78, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(847, 24, 79, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(848, 24, 80, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(849, 24, 81, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(850, 24, 82, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(851, 24, 83, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(852, 24, 84, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(853, 24, 85, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(854, 24, 86, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(855, 24, 87, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(856, 24, 88, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(857, 24, 89, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(858, 24, 90, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(859, 24, 91, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(860, 24, 92, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(861, 24, 93, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(862, 24, 94, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(863, 24, 95, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(864, 24, 96, 1, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(865, 24, 1, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(866, 24, 2, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(867, 24, 3, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(868, 24, 4, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(869, 24, 5, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(870, 24, 6, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(871, 24, 7, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(872, 24, 8, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(873, 24, 9, 2, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-06 20:59:46'),
(874, 24, 10, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(875, 24, 11, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(876, 24, 12, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(877, 24, 13, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(878, 24, 14, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(879, 24, 15, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(880, 24, 16, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(881, 24, 17, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(882, 24, 18, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(883, 24, 19, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(884, 24, 20, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(885, 24, 21, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(886, 24, 22, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(887, 24, 23, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(888, 24, 24, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(889, 24, 25, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(890, 24, 26, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(891, 24, 27, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(892, 24, 28, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(893, 24, 29, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(894, 24, 30, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(895, 24, 31, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(896, 24, 32, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(897, 24, 33, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(898, 24, 34, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(899, 24, 35, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(900, 24, 36, 2, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-06 20:59:46'),
(901, 24, 37, 2, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-06 20:59:46'),
(902, 24, 38, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(903, 24, 39, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(904, 24, 40, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(905, 24, 41, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(906, 24, 42, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(907, 24, 43, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(908, 24, 44, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(909, 24, 45, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(910, 24, 46, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(911, 24, 47, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(912, 24, 48, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(913, 24, 49, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(914, 24, 50, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(915, 24, 51, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(916, 24, 52, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(917, 24, 53, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(918, 24, 54, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(919, 24, 55, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(920, 24, 56, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(921, 24, 57, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(922, 24, 58, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(923, 24, 59, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(924, 24, 60, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(925, 24, 61, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(926, 24, 62, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(927, 24, 63, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(928, 24, 64, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(929, 24, 65, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(930, 24, 66, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(931, 24, 67, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(932, 24, 68, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(933, 24, 69, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(934, 24, 70, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(935, 24, 71, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(936, 24, 72, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(937, 24, 73, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(938, 24, 74, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(939, 24, 75, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(940, 24, 76, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(941, 24, 77, 2, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(942, 24, 78, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(943, 24, 79, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(944, 24, 80, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(945, 24, 81, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(946, 24, 82, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(947, 24, 83, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(948, 24, 84, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(949, 24, 85, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(950, 24, 86, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(951, 24, 87, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(952, 24, 88, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(953, 24, 89, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(954, 24, 90, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(955, 24, 91, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(956, 24, 92, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(957, 24, 93, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(958, 24, 94, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(959, 24, 95, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(960, 24, 96, 2, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(961, 24, 1, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(962, 24, 2, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(963, 24, 3, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(964, 24, 4, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(965, 24, 5, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(966, 24, 6, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(967, 24, 7, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(968, 24, 8, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(969, 24, 9, 3, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-18 18:55:42'),
(970, 24, 10, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(971, 24, 11, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(972, 24, 12, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(973, 24, 13, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(974, 24, 14, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(975, 24, 15, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(976, 24, 16, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(977, 24, 17, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(978, 24, 18, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(979, 24, 19, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(980, 24, 20, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(981, 24, 21, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(982, 24, 22, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(983, 24, 23, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(984, 24, 24, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(985, 24, 25, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(986, 24, 26, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(987, 24, 27, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(988, 24, 28, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(989, 24, 29, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(990, 24, 30, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(991, 24, 31, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(992, 24, 32, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(993, 24, 33, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(994, 24, 34, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(995, 24, 35, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(996, 24, 36, 3, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-18 18:55:42'),
(997, 24, 37, 3, 'YES', 'YES', 2, 2, '2018-04-01 10:04:06', '2018-04-18 18:55:42'),
(998, 24, 38, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(999, 24, 39, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1000, 24, 40, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1001, 24, 41, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1002, 24, 42, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1003, 24, 43, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1004, 24, 44, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1005, 24, 45, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1006, 24, 46, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1007, 24, 47, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1008, 24, 48, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1009, 24, 49, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1010, 24, 50, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1011, 24, 51, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1012, 24, 52, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1013, 24, 53, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1014, 24, 54, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1015, 24, 55, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1016, 24, 56, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1017, 24, 57, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1018, 24, 58, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1019, 24, 59, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1020, 24, 60, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1021, 24, 61, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1022, 24, 62, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1023, 24, 63, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1024, 24, 64, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1025, 24, 65, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1026, 24, 66, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1027, 24, 67, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1028, 24, 68, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1029, 24, 69, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1030, 24, 70, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1031, 24, 71, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1032, 24, 72, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1033, 24, 73, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1034, 24, 74, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1035, 24, 75, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1036, 24, 76, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1037, 24, 77, 3, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1038, 24, 78, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1039, 24, 79, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1040, 24, 80, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1041, 24, 81, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1042, 24, 82, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1043, 24, 83, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1044, 24, 84, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1045, 24, 85, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1046, 24, 86, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1047, 24, 87, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1048, 24, 88, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1049, 24, 89, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1050, 24, 90, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1051, 24, 91, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1052, 24, 92, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1053, 24, 93, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1054, 24, 94, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1055, 24, 95, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1056, 24, 96, 3, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1057, 24, 1, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1058, 24, 2, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1059, 24, 3, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1060, 24, 4, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1061, 24, 5, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1062, 24, 6, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1063, 24, 7, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1064, 24, 8, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1065, 24, 9, 4, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 21:02:49'),
(1066, 24, 10, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1067, 24, 11, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1068, 24, 12, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1069, 24, 13, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1070, 24, 14, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1071, 24, 15, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1072, 24, 16, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1073, 24, 17, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1074, 24, 18, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1075, 24, 19, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1076, 24, 20, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1077, 24, 21, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1078, 24, 22, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1079, 24, 23, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1080, 24, 24, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1081, 24, 25, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1082, 24, 26, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1083, 24, 27, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1084, 24, 28, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1085, 24, 29, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1086, 24, 30, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1087, 24, 31, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1088, 24, 32, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1089, 24, 33, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1090, 24, 34, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1091, 24, 35, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1092, 24, 36, 4, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 21:02:49'),
(1093, 24, 37, 4, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 21:02:49'),
(1094, 24, 38, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1095, 24, 39, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1096, 24, 40, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1097, 24, 41, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1098, 24, 42, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1099, 24, 43, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1100, 24, 44, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1101, 24, 45, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1102, 24, 46, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1103, 24, 47, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1104, 24, 48, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1105, 24, 49, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1106, 24, 50, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1107, 24, 51, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1108, 24, 52, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1109, 24, 53, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1110, 24, 54, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1111, 24, 55, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1112, 24, 56, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1113, 24, 57, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1114, 24, 58, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1115, 24, 59, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1116, 24, 60, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1117, 24, 61, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1118, 24, 62, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1119, 24, 63, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1120, 24, 64, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1121, 24, 65, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1122, 24, 66, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1123, 24, 67, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1124, 24, 68, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1125, 24, 69, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1126, 24, 70, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1127, 24, 71, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1128, 24, 72, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1129, 24, 73, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1130, 24, 74, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1131, 24, 75, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1132, 24, 76, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1133, 24, 77, 4, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1134, 24, 78, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1135, 24, 79, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1136, 24, 80, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1137, 24, 81, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1138, 24, 82, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1139, 24, 83, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1140, 24, 84, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1141, 24, 85, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1142, 24, 86, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1143, 24, 87, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1144, 24, 88, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1145, 24, 89, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1146, 24, 90, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1147, 24, 91, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1148, 24, 92, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1149, 24, 93, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1150, 24, 94, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1151, 24, 95, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1152, 24, 96, 4, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1153, 24, 1, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1154, 24, 2, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1155, 24, 3, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1156, 24, 4, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1157, 24, 5, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1158, 24, 6, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1159, 24, 7, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1160, 24, 8, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1161, 24, 9, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:31:11'),
(1162, 24, 10, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1163, 24, 11, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1164, 24, 12, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1165, 24, 13, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1166, 24, 14, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1167, 24, 15, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1168, 24, 16, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1169, 24, 17, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1170, 24, 18, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1171, 24, 19, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1172, 24, 20, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1173, 24, 21, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1174, 24, 22, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1175, 24, 23, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1176, 24, 24, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1177, 24, 25, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1178, 24, 26, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1179, 24, 27, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1180, 24, 28, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1181, 24, 29, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1182, 24, 30, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1183, 24, 31, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1184, 24, 32, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1185, 24, 33, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:57:41'),
(1186, 24, 34, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:57:45'),
(1187, 24, 35, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:31:23'),
(1188, 24, 36, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-06 18:31:29'),
(1189, 24, 37, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1190, 24, 38, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1191, 24, 39, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1192, 24, 40, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1193, 24, 41, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1194, 24, 42, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1195, 24, 43, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1196, 24, 44, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1197, 24, 45, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1198, 24, 46, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1199, 24, 47, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1200, 24, 48, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1201, 24, 49, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1202, 24, 50, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1203, 24, 51, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1204, 24, 52, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1205, 24, 53, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1206, 24, 54, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1207, 24, 55, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1208, 24, 56, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1209, 24, 57, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1210, 24, 58, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1211, 24, 59, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1212, 24, 60, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1213, 24, 61, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1214, 24, 62, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1215, 24, 63, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1216, 24, 64, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1217, 24, 65, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1218, 24, 66, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1219, 24, 67, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1220, 24, 68, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1221, 24, 69, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1222, 24, 70, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1223, 24, 71, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1224, 24, 72, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1225, 24, 73, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1226, 24, 74, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1227, 24, 75, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1228, 24, 76, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1229, 24, 77, 5, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1230, 24, 78, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1231, 24, 79, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1232, 24, 80, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1233, 24, 81, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1234, 24, 82, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1235, 24, 83, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1236, 24, 84, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1237, 24, 85, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1238, 24, 86, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1239, 24, 87, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1240, 24, 88, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1241, 24, 89, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1242, 24, 90, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1243, 24, 91, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1244, 24, 92, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1245, 24, 93, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1246, 24, 94, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1247, 24, 95, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1248, 24, 96, 5, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1249, 24, 1, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1250, 24, 2, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1251, 24, 3, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1252, 24, 4, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1253, 24, 5, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1254, 24, 6, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1255, 24, 7, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1256, 24, 8, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1257, 24, 9, 6, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 20:55:54'),
(1258, 24, 10, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1259, 24, 11, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1260, 24, 12, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1261, 24, 13, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1262, 24, 14, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1263, 24, 15, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1264, 24, 16, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1265, 24, 17, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1266, 24, 18, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06');
INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(1267, 24, 19, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1268, 24, 20, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1269, 24, 21, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1270, 24, 22, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1271, 24, 23, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1272, 24, 24, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1273, 24, 25, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1274, 24, 26, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1275, 24, 27, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1276, 24, 28, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1277, 24, 29, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1278, 24, 30, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1279, 24, 31, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1280, 24, 32, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1281, 24, 33, 6, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 19:00:04'),
(1282, 24, 34, 6, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 19:00:04'),
(1283, 24, 35, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1284, 24, 36, 6, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 20:55:54'),
(1285, 24, 37, 6, 'YES', 'NO', 2, 1, '2018-04-01 10:04:06', '2018-04-06 20:55:54'),
(1286, 24, 38, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1287, 24, 39, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1288, 24, 40, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1289, 24, 41, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1290, 24, 42, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1291, 24, 43, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1292, 24, 44, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1293, 24, 45, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1294, 24, 46, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1295, 24, 47, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1296, 24, 48, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1297, 24, 49, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1298, 24, 50, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1299, 24, 51, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1300, 24, 52, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1301, 24, 53, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1302, 24, 54, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1303, 24, 55, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1304, 24, 56, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1305, 24, 57, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1306, 24, 58, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1307, 24, 59, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1308, 24, 60, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1309, 24, 61, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1310, 24, 62, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1311, 24, 63, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1312, 24, 64, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1313, 24, 65, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1314, 24, 66, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1315, 24, 67, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1316, 24, 68, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1317, 24, 69, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1318, 24, 70, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1319, 24, 71, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1320, 24, 72, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1321, 24, 73, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1322, 24, 74, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1323, 24, 75, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1324, 24, 76, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1325, 24, 77, 6, 'YES', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1326, 24, 78, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1327, 24, 79, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1328, 24, 80, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1329, 24, 81, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1330, 24, 82, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1331, 24, 83, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1332, 24, 84, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1333, 24, 85, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1334, 24, 86, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1335, 24, 87, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1336, 24, 88, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1337, 24, 89, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1338, 24, 90, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1339, 24, 91, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1340, 24, 92, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1341, 24, 93, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1342, 24, 94, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1343, 24, 95, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1344, 24, 96, 6, 'NO', 'NO', 2, NULL, '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(1345, 25, 1, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1346, 25, 2, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1347, 25, 3, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1348, 25, 4, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1349, 25, 5, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1350, 25, 6, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1351, 25, 7, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1352, 25, 8, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1353, 25, 9, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1354, 25, 10, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1355, 25, 11, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1356, 25, 12, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1357, 25, 13, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1358, 25, 14, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1359, 25, 15, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1360, 25, 16, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1361, 25, 17, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1362, 25, 18, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1363, 25, 19, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1364, 25, 20, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1365, 25, 21, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1366, 25, 22, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1367, 25, 23, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1368, 25, 24, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1369, 25, 25, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1370, 25, 26, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1371, 25, 27, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1372, 25, 28, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1373, 25, 29, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1374, 25, 30, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1375, 25, 31, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1376, 25, 32, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1377, 25, 33, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1378, 25, 34, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1379, 25, 35, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1380, 25, 36, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1381, 25, 37, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1382, 25, 38, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1383, 25, 39, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1384, 25, 40, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1385, 25, 41, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1386, 25, 42, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1387, 25, 43, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1388, 25, 44, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1389, 25, 45, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1390, 25, 46, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1391, 25, 47, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1392, 25, 48, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1393, 25, 49, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1394, 25, 50, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1395, 25, 51, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1396, 25, 52, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1397, 25, 53, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1398, 25, 54, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1399, 25, 55, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1400, 25, 56, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1401, 25, 57, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1402, 25, 58, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1403, 25, 59, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1404, 25, 60, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1405, 25, 61, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1406, 25, 62, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1407, 25, 63, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1408, 25, 64, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1409, 25, 65, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1410, 25, 66, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1411, 25, 67, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1412, 25, 68, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1413, 25, 69, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1414, 25, 70, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1415, 25, 71, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1416, 25, 72, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1417, 25, 73, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1418, 25, 74, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1419, 25, 75, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1420, 25, 76, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1421, 25, 77, 0, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1422, 25, 78, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1423, 25, 79, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1424, 25, 80, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1425, 25, 81, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1426, 25, 82, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1427, 25, 83, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1428, 25, 84, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1429, 25, 85, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1430, 25, 86, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1431, 25, 87, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1432, 25, 88, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1433, 25, 89, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1434, 25, 90, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1435, 25, 91, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1436, 25, 92, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1437, 25, 93, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1438, 25, 94, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1439, 25, 95, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1440, 25, 96, 0, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1441, 25, 1, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1442, 25, 2, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1443, 25, 3, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1444, 25, 4, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1445, 25, 5, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1446, 25, 6, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1447, 25, 7, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1448, 25, 8, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1449, 25, 9, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1450, 25, 10, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1451, 25, 11, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1452, 25, 12, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1453, 25, 13, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1454, 25, 14, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1455, 25, 15, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1456, 25, 16, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1457, 25, 17, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1458, 25, 18, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1459, 25, 19, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1460, 25, 20, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1461, 25, 21, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1462, 25, 22, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1463, 25, 23, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1464, 25, 24, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1465, 25, 25, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1466, 25, 26, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1467, 25, 27, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1468, 25, 28, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1469, 25, 29, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1470, 25, 30, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1471, 25, 31, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1472, 25, 32, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1473, 25, 33, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1474, 25, 34, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1475, 25, 35, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1476, 25, 36, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1477, 25, 37, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1478, 25, 38, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1479, 25, 39, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1480, 25, 40, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1481, 25, 41, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1482, 25, 42, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1483, 25, 43, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1484, 25, 44, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1485, 25, 45, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1486, 25, 46, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1487, 25, 47, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1488, 25, 48, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1489, 25, 49, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1490, 25, 50, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1491, 25, 51, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1492, 25, 52, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1493, 25, 53, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1494, 25, 54, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1495, 25, 55, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1496, 25, 56, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1497, 25, 57, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1498, 25, 58, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1499, 25, 59, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1500, 25, 60, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1501, 25, 61, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1502, 25, 62, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1503, 25, 63, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1504, 25, 64, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1505, 25, 65, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1506, 25, 66, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1507, 25, 67, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1508, 25, 68, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1509, 25, 69, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1510, 25, 70, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1511, 25, 71, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1512, 25, 72, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1513, 25, 73, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1514, 25, 74, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1515, 25, 75, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1516, 25, 76, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1517, 25, 77, 1, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1518, 25, 78, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1519, 25, 79, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1520, 25, 80, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1521, 25, 81, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1522, 25, 82, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1523, 25, 83, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1524, 25, 84, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1525, 25, 85, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1526, 25, 86, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1527, 25, 87, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1528, 25, 88, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1529, 25, 89, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1530, 25, 90, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1531, 25, 91, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1532, 25, 92, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1533, 25, 93, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1534, 25, 94, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1535, 25, 95, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1536, 25, 96, 1, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1537, 25, 1, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1538, 25, 2, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1539, 25, 3, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1540, 25, 4, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1541, 25, 5, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1542, 25, 6, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1543, 25, 7, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1544, 25, 8, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1545, 25, 9, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1546, 25, 10, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1547, 25, 11, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1548, 25, 12, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1549, 25, 13, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1550, 25, 14, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1551, 25, 15, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1552, 25, 16, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1553, 25, 17, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1554, 25, 18, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1555, 25, 19, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1556, 25, 20, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1557, 25, 21, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1558, 25, 22, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1559, 25, 23, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1560, 25, 24, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1561, 25, 25, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1562, 25, 26, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1563, 25, 27, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1564, 25, 28, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1565, 25, 29, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1566, 25, 30, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1567, 25, 31, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1568, 25, 32, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1569, 25, 33, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1570, 25, 34, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1571, 25, 35, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1572, 25, 36, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1573, 25, 37, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1574, 25, 38, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1575, 25, 39, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1576, 25, 40, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1577, 25, 41, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1578, 25, 42, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1579, 25, 43, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1580, 25, 44, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1581, 25, 45, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1582, 25, 46, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1583, 25, 47, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1584, 25, 48, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1585, 25, 49, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1586, 25, 50, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1587, 25, 51, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1588, 25, 52, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1589, 25, 53, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1590, 25, 54, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1591, 25, 55, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1592, 25, 56, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1593, 25, 57, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1594, 25, 58, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1595, 25, 59, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1596, 25, 60, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1597, 25, 61, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1598, 25, 62, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1599, 25, 63, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1600, 25, 64, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1601, 25, 65, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1602, 25, 66, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1603, 25, 67, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1604, 25, 68, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1605, 25, 69, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1606, 25, 70, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1607, 25, 71, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1608, 25, 72, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1609, 25, 73, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1610, 25, 74, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1611, 25, 75, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1612, 25, 76, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1613, 25, 77, 2, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1614, 25, 78, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1615, 25, 79, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1616, 25, 80, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1617, 25, 81, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1618, 25, 82, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1619, 25, 83, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1620, 25, 84, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1621, 25, 85, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1622, 25, 86, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1623, 25, 87, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1624, 25, 88, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1625, 25, 89, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1626, 25, 90, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1627, 25, 91, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1628, 25, 92, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1629, 25, 93, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1630, 25, 94, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1631, 25, 95, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1632, 25, 96, 2, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1633, 25, 1, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1634, 25, 2, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1635, 25, 3, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1636, 25, 4, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1637, 25, 5, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1638, 25, 6, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1639, 25, 7, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1640, 25, 8, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1641, 25, 9, 3, 'YES', 'YES', 3, 3, '2018-04-04 18:03:39', '2018-04-18 18:30:57'),
(1642, 25, 10, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1643, 25, 11, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1644, 25, 12, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1645, 25, 13, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1646, 25, 14, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1647, 25, 15, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1648, 25, 16, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1649, 25, 17, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1650, 25, 18, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1651, 25, 19, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1652, 25, 20, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1653, 25, 21, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1654, 25, 22, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1655, 25, 23, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1656, 25, 24, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1657, 25, 25, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1658, 25, 26, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1659, 25, 27, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1660, 25, 28, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1661, 25, 29, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1662, 25, 30, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1663, 25, 31, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1664, 25, 32, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1665, 25, 33, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1666, 25, 34, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1667, 25, 35, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1668, 25, 36, 3, 'YES', 'NO', 3, 14, '2018-04-04 18:03:39', '2018-04-18 18:30:57'),
(1669, 25, 37, 3, 'YES', 'NO', 3, 14, '2018-04-04 18:03:39', '2018-04-18 18:30:57'),
(1670, 25, 38, 3, 'YES', 'NO', 3, 14, '2018-04-04 18:03:39', '2018-04-18 18:30:57'),
(1671, 25, 39, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1672, 25, 40, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1673, 25, 41, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1674, 25, 42, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1675, 25, 43, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1676, 25, 44, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1677, 25, 45, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1678, 25, 46, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1679, 25, 47, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1680, 25, 48, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1681, 25, 49, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1682, 25, 50, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1683, 25, 51, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1684, 25, 52, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1685, 25, 53, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1686, 25, 54, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1687, 25, 55, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1688, 25, 56, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1689, 25, 57, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1690, 25, 58, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1691, 25, 59, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1692, 25, 60, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1693, 25, 61, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1694, 25, 62, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1695, 25, 63, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1696, 25, 64, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1697, 25, 65, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1698, 25, 66, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1699, 25, 67, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1700, 25, 68, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1701, 25, 69, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1702, 25, 70, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1703, 25, 71, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1704, 25, 72, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1705, 25, 73, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1706, 25, 74, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1707, 25, 75, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1708, 25, 76, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1709, 25, 77, 3, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1710, 25, 78, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1711, 25, 79, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1712, 25, 80, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1713, 25, 81, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1714, 25, 82, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1715, 25, 83, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1716, 25, 84, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1717, 25, 85, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1718, 25, 86, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1719, 25, 87, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1720, 25, 88, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1721, 25, 89, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1722, 25, 90, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1723, 25, 91, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1724, 25, 92, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1725, 25, 93, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1726, 25, 94, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1727, 25, 95, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1728, 25, 96, 3, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1729, 25, 1, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1730, 25, 2, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1731, 25, 3, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1732, 25, 4, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1733, 25, 5, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1734, 25, 6, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1735, 25, 7, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1736, 25, 8, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1737, 25, 9, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1738, 25, 10, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1739, 25, 11, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1740, 25, 12, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1741, 25, 13, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1742, 25, 14, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1743, 25, 15, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1744, 25, 16, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1745, 25, 17, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1746, 25, 18, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1747, 25, 19, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1748, 25, 20, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1749, 25, 21, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1750, 25, 22, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1751, 25, 23, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1752, 25, 24, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1753, 25, 25, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1754, 25, 26, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1755, 25, 27, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1756, 25, 28, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1757, 25, 29, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1758, 25, 30, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1759, 25, 31, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1760, 25, 32, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1761, 25, 33, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1762, 25, 34, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1763, 25, 35, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1764, 25, 36, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1765, 25, 37, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1766, 25, 38, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1767, 25, 39, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1768, 25, 40, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1769, 25, 41, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1770, 25, 42, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1771, 25, 43, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1772, 25, 44, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1773, 25, 45, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1774, 25, 46, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1775, 25, 47, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1776, 25, 48, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1777, 25, 49, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1778, 25, 50, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1779, 25, 51, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1780, 25, 52, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1781, 25, 53, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1782, 25, 54, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1783, 25, 55, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1784, 25, 56, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1785, 25, 57, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1786, 25, 58, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1787, 25, 59, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1788, 25, 60, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1789, 25, 61, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1790, 25, 62, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1791, 25, 63, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1792, 25, 64, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1793, 25, 65, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1794, 25, 66, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1795, 25, 67, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1796, 25, 68, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1797, 25, 69, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1798, 25, 70, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1799, 25, 71, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1800, 25, 72, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1801, 25, 73, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1802, 25, 74, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1803, 25, 75, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1804, 25, 76, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1805, 25, 77, 4, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1806, 25, 78, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1807, 25, 79, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1808, 25, 80, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1809, 25, 81, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1810, 25, 82, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1811, 25, 83, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1812, 25, 84, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1813, 25, 85, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1814, 25, 86, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1815, 25, 87, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1816, 25, 88, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1817, 25, 89, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1818, 25, 90, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1819, 25, 91, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1820, 25, 92, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1821, 25, 93, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1822, 25, 94, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1823, 25, 95, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1824, 25, 96, 4, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1825, 25, 1, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1826, 25, 2, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1827, 25, 3, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1828, 25, 4, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1829, 25, 5, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1830, 25, 6, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1831, 25, 7, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1832, 25, 8, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1833, 25, 9, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1834, 25, 10, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1835, 25, 11, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1836, 25, 12, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1837, 25, 13, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1838, 25, 14, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1839, 25, 15, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1840, 25, 16, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1841, 25, 17, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1842, 25, 18, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1843, 25, 19, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1844, 25, 20, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1845, 25, 21, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1846, 25, 22, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1847, 25, 23, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1848, 25, 24, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1849, 25, 25, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1850, 25, 26, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1851, 25, 27, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1852, 25, 28, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1853, 25, 29, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1854, 25, 30, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1855, 25, 31, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1856, 25, 32, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01');
INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(1857, 25, 33, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1858, 25, 34, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1859, 25, 35, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1860, 25, 36, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1861, 25, 37, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1862, 25, 38, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1863, 25, 39, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1864, 25, 40, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1865, 25, 41, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1866, 25, 42, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1867, 25, 43, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1868, 25, 44, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1869, 25, 45, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1870, 25, 46, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1871, 25, 47, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1872, 25, 48, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1873, 25, 49, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1874, 25, 50, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1875, 25, 51, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1876, 25, 52, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1877, 25, 53, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1878, 25, 54, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1879, 25, 55, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1880, 25, 56, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1881, 25, 57, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1882, 25, 58, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1883, 25, 59, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1884, 25, 60, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1885, 25, 61, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1886, 25, 62, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1887, 25, 63, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1888, 25, 64, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1889, 25, 65, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1890, 25, 66, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1891, 25, 67, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1892, 25, 68, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1893, 25, 69, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1894, 25, 70, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1895, 25, 71, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1896, 25, 72, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1897, 25, 73, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1898, 25, 74, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1899, 25, 75, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1900, 25, 76, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1901, 25, 77, 5, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1902, 25, 78, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1903, 25, 79, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1904, 25, 80, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1905, 25, 81, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1906, 25, 82, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1907, 25, 83, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1908, 25, 84, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1909, 25, 85, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1910, 25, 86, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1911, 25, 87, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1912, 25, 88, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1913, 25, 89, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1914, 25, 90, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1915, 25, 91, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1916, 25, 92, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1917, 25, 93, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1918, 25, 94, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1919, 25, 95, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1920, 25, 96, 5, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1921, 25, 1, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1922, 25, 2, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1923, 25, 3, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1924, 25, 4, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1925, 25, 5, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1926, 25, 6, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1927, 25, 7, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1928, 25, 8, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1929, 25, 9, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1930, 25, 10, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1931, 25, 11, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1932, 25, 12, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1933, 25, 13, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1934, 25, 14, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1935, 25, 15, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1936, 25, 16, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1937, 25, 17, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1938, 25, 18, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1939, 25, 19, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1940, 25, 20, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1941, 25, 21, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1942, 25, 22, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1943, 25, 23, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1944, 25, 24, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1945, 25, 25, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1946, 25, 26, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1947, 25, 27, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1948, 25, 28, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1949, 25, 29, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1950, 25, 30, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1951, 25, 31, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1952, 25, 32, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1953, 25, 33, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1954, 25, 34, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1955, 25, 35, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1956, 25, 36, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1957, 25, 37, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1958, 25, 38, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1959, 25, 39, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1960, 25, 40, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1961, 25, 41, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1962, 25, 42, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1963, 25, 43, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1964, 25, 44, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1965, 25, 45, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1966, 25, 46, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1967, 25, 47, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1968, 25, 48, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1969, 25, 49, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1970, 25, 50, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1971, 25, 51, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1972, 25, 52, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1973, 25, 53, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1974, 25, 54, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1975, 25, 55, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1976, 25, 56, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1977, 25, 57, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1978, 25, 58, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1979, 25, 59, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1980, 25, 60, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1981, 25, 61, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1982, 25, 62, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1983, 25, 63, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1984, 25, 64, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1985, 25, 65, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1986, 25, 66, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1987, 25, 67, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1988, 25, 68, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1989, 25, 69, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1990, 25, 70, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1991, 25, 71, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1992, 25, 72, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1993, 25, 73, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1994, 25, 74, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1995, 25, 75, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1996, 25, 76, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1997, 25, 77, 6, 'YES', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1998, 25, 78, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(1999, 25, 79, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2000, 25, 80, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2001, 25, 81, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2002, 25, 82, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2003, 25, 83, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2004, 25, 84, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2005, 25, 85, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2006, 25, 86, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2007, 25, 87, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2008, 25, 88, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2009, 25, 89, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2010, 25, 90, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2011, 25, 91, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2012, 25, 92, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2013, 25, 93, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2014, 25, 94, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2015, 25, 95, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2016, 25, 96, 6, 'NO', 'NO', 3, NULL, '2018-04-04 18:03:39', '2018-04-18 18:30:01'),
(2017, 26, 1, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2018, 26, 2, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2019, 26, 3, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2020, 26, 4, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2021, 26, 5, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2022, 26, 6, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2023, 26, 7, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2024, 26, 8, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2025, 26, 9, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2026, 26, 10, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2027, 26, 11, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2028, 26, 12, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2029, 26, 13, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2030, 26, 14, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2031, 26, 15, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2032, 26, 16, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2033, 26, 17, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2034, 26, 18, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:27', '2018-04-06 19:57:33'),
(2035, 26, 19, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2036, 26, 20, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2037, 26, 21, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2038, 26, 22, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2039, 26, 23, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2040, 26, 24, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2041, 26, 25, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2042, 26, 26, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2043, 26, 27, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2044, 26, 28, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2045, 26, 29, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2046, 26, 30, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2047, 26, 31, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2048, 26, 32, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2049, 26, 33, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2050, 26, 34, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2051, 26, 35, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2052, 26, 36, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2053, 26, 37, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2054, 26, 38, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:28', '2018-04-06 19:57:33'),
(2055, 26, 39, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2056, 26, 40, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2057, 26, 41, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2058, 26, 42, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2059, 26, 43, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2060, 26, 44, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2061, 26, 45, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2062, 26, 46, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2063, 26, 47, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2064, 26, 48, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2065, 26, 49, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2066, 26, 50, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2067, 26, 51, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2068, 26, 52, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:29', '2018-04-06 19:57:33'),
(2069, 26, 53, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2070, 26, 54, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2071, 26, 55, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2072, 26, 56, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2073, 26, 57, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2074, 26, 58, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2075, 26, 59, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2076, 26, 60, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2077, 26, 61, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2078, 26, 62, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2079, 26, 63, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2080, 26, 64, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2081, 26, 65, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2082, 26, 66, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2083, 26, 67, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2084, 26, 68, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2085, 26, 69, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2086, 26, 70, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:30', '2018-04-06 19:57:33'),
(2087, 26, 71, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2088, 26, 72, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2089, 26, 73, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2090, 26, 74, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2091, 26, 75, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2092, 26, 76, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2093, 26, 77, 0, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2094, 26, 78, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2095, 26, 79, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2096, 26, 80, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2097, 26, 81, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2098, 26, 82, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2099, 26, 83, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2100, 26, 84, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2101, 26, 85, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2102, 26, 86, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2103, 26, 87, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2104, 26, 88, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2105, 26, 89, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2106, 26, 90, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:31', '2018-04-06 19:57:33'),
(2107, 26, 91, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2108, 26, 92, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2109, 26, 93, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2110, 26, 94, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2111, 26, 95, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2112, 26, 96, 0, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2113, 26, 1, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2114, 26, 2, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2115, 26, 3, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2116, 26, 4, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2117, 26, 5, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2118, 26, 6, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2119, 26, 7, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2120, 26, 8, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2121, 26, 9, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2122, 26, 10, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:32', '2018-04-06 19:57:33'),
(2123, 26, 11, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2124, 26, 12, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2125, 26, 13, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2126, 26, 14, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2127, 26, 15, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2128, 26, 16, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2129, 26, 17, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2130, 26, 18, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2131, 26, 19, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2132, 26, 20, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2133, 26, 21, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2134, 26, 22, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2135, 26, 23, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2136, 26, 24, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2137, 26, 25, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2138, 26, 26, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2139, 26, 27, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2140, 26, 28, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2141, 26, 29, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2142, 26, 30, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:33', '2018-04-06 19:57:33'),
(2143, 26, 31, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2144, 26, 32, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2145, 26, 33, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2146, 26, 34, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2147, 26, 35, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2148, 26, 36, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2149, 26, 37, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2150, 26, 38, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2151, 26, 39, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2152, 26, 40, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2153, 26, 41, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2154, 26, 42, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2155, 26, 43, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2156, 26, 44, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2157, 26, 45, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2158, 26, 46, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2159, 26, 47, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2160, 26, 48, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2161, 26, 49, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2162, 26, 50, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2163, 26, 51, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2164, 26, 52, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:34', '2018-04-06 19:57:33'),
(2165, 26, 53, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2166, 26, 54, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2167, 26, 55, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2168, 26, 56, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2169, 26, 57, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2170, 26, 58, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2171, 26, 59, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2172, 26, 60, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2173, 26, 61, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2174, 26, 62, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2175, 26, 63, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2176, 26, 64, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2177, 26, 65, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2178, 26, 66, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2179, 26, 67, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2180, 26, 68, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2181, 26, 69, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2182, 26, 70, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2183, 26, 71, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2184, 26, 72, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2185, 26, 73, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2186, 26, 74, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:35', '2018-04-06 19:57:33'),
(2187, 26, 75, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2188, 26, 76, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2189, 26, 77, 1, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2190, 26, 78, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2191, 26, 79, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2192, 26, 80, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2193, 26, 81, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2194, 26, 82, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2195, 26, 83, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2196, 26, 84, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2197, 26, 85, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2198, 26, 86, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2199, 26, 87, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2200, 26, 88, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2201, 26, 89, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2202, 26, 90, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2203, 26, 91, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2204, 26, 92, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2205, 26, 93, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2206, 26, 94, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2207, 26, 95, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2208, 26, 96, 1, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:36', '2018-04-06 19:57:33'),
(2209, 26, 1, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2210, 26, 2, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2211, 26, 3, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2212, 26, 4, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2213, 26, 5, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2214, 26, 6, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2215, 26, 7, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2216, 26, 8, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2217, 26, 9, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2218, 26, 10, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2219, 26, 11, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2220, 26, 12, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2221, 26, 13, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2222, 26, 14, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:37', '2018-04-06 19:57:33'),
(2223, 26, 15, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2224, 26, 16, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2225, 26, 17, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2226, 26, 18, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2227, 26, 19, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2228, 26, 20, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2229, 26, 21, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2230, 26, 22, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2231, 26, 23, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2232, 26, 24, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2233, 26, 25, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2234, 26, 26, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2235, 26, 27, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2236, 26, 28, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2237, 26, 29, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2238, 26, 30, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2239, 26, 31, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2240, 26, 32, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2241, 26, 33, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:38', '2018-04-06 19:57:33'),
(2242, 26, 34, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2243, 26, 35, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2244, 26, 36, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2245, 26, 37, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2246, 26, 38, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2247, 26, 39, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2248, 26, 40, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2249, 26, 41, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2250, 26, 42, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2251, 26, 43, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2252, 26, 44, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2253, 26, 45, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2254, 26, 46, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2255, 26, 47, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2256, 26, 48, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2257, 26, 49, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2258, 26, 50, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2259, 26, 51, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2260, 26, 52, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2261, 26, 53, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:39', '2018-04-06 19:57:33'),
(2262, 26, 54, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2263, 26, 55, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2264, 26, 56, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2265, 26, 57, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2266, 26, 58, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2267, 26, 59, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2268, 26, 60, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2269, 26, 61, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2270, 26, 62, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2271, 26, 63, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2272, 26, 64, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2273, 26, 65, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2274, 26, 66, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2275, 26, 67, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2276, 26, 68, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2277, 26, 69, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2278, 26, 70, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2279, 26, 71, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2280, 26, 72, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2281, 26, 73, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2282, 26, 74, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:40', '2018-04-06 19:57:33'),
(2283, 26, 75, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2284, 26, 76, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2285, 26, 77, 2, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2286, 26, 78, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2287, 26, 79, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2288, 26, 80, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2289, 26, 81, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2290, 26, 82, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2291, 26, 83, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2292, 26, 84, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2293, 26, 85, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2294, 26, 86, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2295, 26, 87, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2296, 26, 88, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2297, 26, 89, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2298, 26, 90, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2299, 26, 91, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2300, 26, 92, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2301, 26, 93, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2302, 26, 94, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2303, 26, 95, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:41', '2018-04-06 19:57:33'),
(2304, 26, 96, 2, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2305, 26, 1, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2306, 26, 2, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2307, 26, 3, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2308, 26, 4, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2309, 26, 5, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2310, 26, 6, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2311, 26, 7, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2312, 26, 8, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2313, 26, 9, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2314, 26, 10, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2315, 26, 11, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2316, 26, 12, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2317, 26, 13, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2318, 26, 14, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:42', '2018-04-06 19:57:33'),
(2319, 26, 15, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2320, 26, 16, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2321, 26, 17, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2322, 26, 18, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2323, 26, 19, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2324, 26, 20, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2325, 26, 21, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2326, 26, 22, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2327, 26, 23, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2328, 26, 24, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2329, 26, 25, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2330, 26, 26, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2331, 26, 27, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2332, 26, 28, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2333, 26, 29, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2334, 26, 30, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2335, 26, 31, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2336, 26, 32, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2337, 26, 33, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2338, 26, 34, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2339, 26, 35, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2340, 26, 36, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:43', '2018-04-06 19:57:33'),
(2341, 26, 37, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2342, 26, 38, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2343, 26, 39, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2344, 26, 40, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2345, 26, 41, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2346, 26, 42, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2347, 26, 43, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2348, 26, 44, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2349, 26, 45, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2350, 26, 46, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2351, 26, 47, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2352, 26, 48, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2353, 26, 49, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2354, 26, 50, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2355, 26, 51, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2356, 26, 52, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2357, 26, 53, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2358, 26, 54, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2359, 26, 55, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2360, 26, 56, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:44', '2018-04-06 19:57:33'),
(2361, 26, 57, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2362, 26, 58, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2363, 26, 59, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2364, 26, 60, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2365, 26, 61, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2366, 26, 62, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2367, 26, 63, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2368, 26, 64, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2369, 26, 65, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2370, 26, 66, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2371, 26, 67, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2372, 26, 68, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2373, 26, 69, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2374, 26, 70, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2375, 26, 71, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2376, 26, 72, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2377, 26, 73, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2378, 26, 74, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2379, 26, 75, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2380, 26, 76, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:45', '2018-04-06 19:57:33'),
(2381, 26, 77, 3, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2382, 26, 78, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2383, 26, 79, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2384, 26, 80, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2385, 26, 81, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2386, 26, 82, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2387, 26, 83, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2388, 26, 84, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2389, 26, 85, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2390, 26, 86, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2391, 26, 87, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2392, 26, 88, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2393, 26, 89, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2394, 26, 90, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2395, 26, 91, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2396, 26, 92, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2397, 26, 93, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2398, 26, 94, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2399, 26, 95, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2400, 26, 96, 3, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2401, 26, 1, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:46', '2018-04-06 19:57:33'),
(2402, 26, 2, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2403, 26, 3, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2404, 26, 4, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2405, 26, 5, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2406, 26, 6, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2407, 26, 7, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2408, 26, 8, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2409, 26, 9, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2410, 26, 10, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2411, 26, 11, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2412, 26, 12, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2413, 26, 13, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2414, 26, 14, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2415, 26, 15, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2416, 26, 16, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2417, 26, 17, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2418, 26, 18, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2419, 26, 19, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2420, 26, 20, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2421, 26, 21, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2422, 26, 22, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:47', '2018-04-06 19:57:33'),
(2423, 26, 23, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2424, 26, 24, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2425, 26, 25, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2426, 26, 26, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2427, 26, 27, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2428, 26, 28, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2429, 26, 29, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2430, 26, 30, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2431, 26, 31, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2432, 26, 32, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2433, 26, 33, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2434, 26, 34, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2435, 26, 35, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2436, 26, 36, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2437, 26, 37, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2438, 26, 38, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2439, 26, 39, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2440, 26, 40, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2441, 26, 41, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2442, 26, 42, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2443, 26, 43, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2444, 26, 44, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:48', '2018-04-06 19:57:33'),
(2445, 26, 45, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2446, 26, 46, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33');
INSERT INTO `tk_store_has_timeslots` (`id`, `fk_store`, `fk_timeslot`, `int_week_day`, `enum_enable`, `enum_isfull`, `int_book_limit`, `int_book_count`, `created_at`, `updated_at`) VALUES
(2447, 26, 47, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2448, 26, 48, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2449, 26, 49, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2450, 26, 50, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2451, 26, 51, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2452, 26, 52, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2453, 26, 53, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2454, 26, 54, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2455, 26, 55, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2456, 26, 56, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2457, 26, 57, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2458, 26, 58, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2459, 26, 59, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2460, 26, 60, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2461, 26, 61, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2462, 26, 62, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2463, 26, 63, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2464, 26, 64, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2465, 26, 65, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2466, 26, 66, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:49', '2018-04-06 19:57:33'),
(2467, 26, 67, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2468, 26, 68, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2469, 26, 69, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2470, 26, 70, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2471, 26, 71, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2472, 26, 72, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2473, 26, 73, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2474, 26, 74, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2475, 26, 75, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2476, 26, 76, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2477, 26, 77, 4, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2478, 26, 78, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2479, 26, 79, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2480, 26, 80, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2481, 26, 81, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2482, 26, 82, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2483, 26, 83, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2484, 26, 84, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2485, 26, 85, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:50', '2018-04-06 19:57:33'),
(2486, 26, 86, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2487, 26, 87, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2488, 26, 88, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2489, 26, 89, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2490, 26, 90, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2491, 26, 91, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2492, 26, 92, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2493, 26, 93, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2494, 26, 94, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2495, 26, 95, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2496, 26, 96, 4, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2497, 26, 1, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:51', '2018-04-06 19:57:33'),
(2498, 26, 2, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2499, 26, 3, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2500, 26, 4, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2501, 26, 5, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2502, 26, 6, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2503, 26, 7, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2504, 26, 8, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2505, 26, 9, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2506, 26, 10, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2507, 26, 11, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2508, 26, 12, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2509, 26, 13, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2510, 26, 14, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2511, 26, 15, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2512, 26, 16, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2513, 26, 17, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:52', '2018-04-06 19:57:33'),
(2514, 26, 18, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2515, 26, 19, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2516, 26, 20, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2517, 26, 21, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2518, 26, 22, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2519, 26, 23, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2520, 26, 24, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2521, 26, 25, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2522, 26, 26, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2523, 26, 27, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2524, 26, 28, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2525, 26, 29, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2526, 26, 30, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2527, 26, 31, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2528, 26, 32, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2529, 26, 33, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2530, 26, 34, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2531, 26, 35, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2532, 26, 36, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2533, 26, 37, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:53', '2018-04-06 19:57:33'),
(2534, 26, 38, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2535, 26, 39, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2536, 26, 40, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2537, 26, 41, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2538, 26, 42, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2539, 26, 43, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2540, 26, 44, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2541, 26, 45, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2542, 26, 46, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2543, 26, 47, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2544, 26, 48, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2545, 26, 49, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2546, 26, 50, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2547, 26, 51, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2548, 26, 52, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2549, 26, 53, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2550, 26, 54, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2551, 26, 55, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2552, 26, 56, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2553, 26, 57, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2554, 26, 58, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:54', '2018-04-06 19:57:33'),
(2555, 26, 59, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2556, 26, 60, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2557, 26, 61, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2558, 26, 62, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2559, 26, 63, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2560, 26, 64, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2561, 26, 65, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2562, 26, 66, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2563, 26, 67, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2564, 26, 68, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2565, 26, 69, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2566, 26, 70, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2567, 26, 71, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2568, 26, 72, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2569, 26, 73, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2570, 26, 74, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2571, 26, 75, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2572, 26, 76, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2573, 26, 77, 5, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2574, 26, 78, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:55', '2018-04-06 19:57:33'),
(2575, 26, 79, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2576, 26, 80, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2577, 26, 81, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2578, 26, 82, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2579, 26, 83, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2580, 26, 84, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2581, 26, 85, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2582, 26, 86, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2583, 26, 87, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2584, 26, 88, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2585, 26, 89, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2586, 26, 90, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2587, 26, 91, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2588, 26, 92, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2589, 26, 93, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2590, 26, 94, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2591, 26, 95, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2592, 26, 96, 5, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2593, 26, 1, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:56', '2018-04-06 19:57:33'),
(2594, 26, 2, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2595, 26, 3, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2596, 26, 4, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2597, 26, 5, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2598, 26, 6, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2599, 26, 7, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2600, 26, 8, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2601, 26, 9, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2602, 26, 10, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2603, 26, 11, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2604, 26, 12, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2605, 26, 13, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2606, 26, 14, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2607, 26, 15, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2608, 26, 16, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2609, 26, 17, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:57', '2018-04-06 19:57:33'),
(2610, 26, 18, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2611, 26, 19, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2612, 26, 20, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2613, 26, 21, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2614, 26, 22, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2615, 26, 23, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2616, 26, 24, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2617, 26, 25, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2618, 26, 26, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2619, 26, 27, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2620, 26, 28, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2621, 26, 29, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2622, 26, 30, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2623, 26, 31, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2624, 26, 32, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2625, 26, 33, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2626, 26, 34, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2627, 26, 35, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2628, 26, 36, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2629, 26, 37, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2630, 26, 38, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:58', '2018-04-06 19:57:33'),
(2631, 26, 39, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2632, 26, 40, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2633, 26, 41, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2634, 26, 42, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2635, 26, 43, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2636, 26, 44, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2637, 26, 45, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2638, 26, 46, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2639, 26, 47, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2640, 26, 48, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2641, 26, 49, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2642, 26, 50, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2643, 26, 51, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2644, 26, 52, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2645, 26, 53, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2646, 26, 54, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2647, 26, 55, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2648, 26, 56, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2649, 26, 57, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2650, 26, 58, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:51:59', '2018-04-06 19:57:33'),
(2651, 26, 59, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2652, 26, 60, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2653, 26, 61, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2654, 26, 62, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2655, 26, 63, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2656, 26, 64, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2657, 26, 65, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2658, 26, 66, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2659, 26, 67, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2660, 26, 68, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2661, 26, 69, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2662, 26, 70, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2663, 26, 71, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2664, 26, 72, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2665, 26, 73, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2666, 26, 74, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2667, 26, 75, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2668, 26, 76, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2669, 26, 77, 6, 'YES', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2670, 26, 78, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2671, 26, 79, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2672, 26, 80, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2673, 26, 81, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2674, 26, 82, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:00', '2018-04-06 19:57:33'),
(2675, 26, 83, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2676, 26, 84, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2677, 26, 85, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2678, 26, 86, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2679, 26, 87, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2680, 26, 88, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2681, 26, 89, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2682, 26, 90, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2683, 26, 91, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2684, 26, 92, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2685, 26, 93, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2686, 26, 94, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2687, 26, 95, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33'),
(2688, 26, 96, 6, 'NO', 'NO', 0, NULL, '2018-04-06 19:52:01', '2018-04-06 19:57:33');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timing`
--

CREATE TABLE `tk_store_has_timing` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `int_week` int(11) NOT NULL,
  `dt_open_time` time NOT NULL,
  `dt_close_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timing`
--

INSERT INTO `tk_store_has_timing` (`id`, `fk_store`, `int_week`, `dt_open_time`, `dt_close_time`, `created_at`, `updated_at`) VALUES
(114, 24, 0, '09:00:00', '22:00:00', '2018-04-01 10:04:06', '2018-04-01 10:50:41'),
(115, 24, 1, '09:00:00', '21:00:00', '2018-04-01 10:04:06', '2018-04-01 10:55:52'),
(116, 24, 2, '09:00:00', '20:00:00', '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(117, 24, 3, '09:00:00', '20:00:00', '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(118, 24, 4, '09:00:00', '20:00:00', '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(119, 24, 5, '09:00:00', '21:00:00', '2018-04-01 10:04:06', '2018-04-01 10:51:58'),
(120, 24, 6, '09:00:00', '20:00:00', '2018-04-01 10:04:06', '2018-04-01 10:04:06'),
(121, 25, 0, '09:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:03:39'),
(122, 25, 1, '10:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:26:41'),
(123, 25, 2, '09:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:03:39'),
(124, 25, 3, '09:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:03:39'),
(125, 25, 4, '09:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:03:39'),
(126, 25, 5, '09:00:00', '20:00:00', '2018-04-04 18:03:39', '2018-04-04 18:03:39'),
(127, 25, 6, '09:00:00', '23:00:00', '2018-04-04 18:03:39', '2018-04-04 18:26:41'),
(128, 26, 0, '09:00:00', '20:00:00', '2018-04-06 19:51:27', '2018-04-06 19:51:27'),
(129, 26, 1, '09:00:00', '20:00:00', '2018-04-06 19:51:32', '2018-04-06 19:51:32'),
(130, 26, 2, '09:00:00', '20:00:00', '2018-04-06 19:51:37', '2018-04-06 19:51:37'),
(131, 26, 3, '09:00:00', '20:00:00', '2018-04-06 19:51:42', '2018-04-06 19:51:42'),
(132, 26, 4, '09:00:00', '20:00:00', '2018-04-06 19:51:46', '2018-04-06 19:51:46'),
(133, 26, 5, '09:00:00', '20:00:00', '2018-04-06 19:51:51', '2018-04-06 19:51:51'),
(134, 26, 6, '09:00:00', '20:00:00', '2018-04-06 19:51:56', '2018-04-06 19:51:56');

-- --------------------------------------------------------

--
-- Table structure for table `tk_timeslots`
--

CREATE TABLE `tk_timeslots` (
  `id` int(11) NOT NULL,
  `slot_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_timeslots`
--

INSERT INTO `tk_timeslots` (`id`, `slot_time`, `created_at`, `updated_at`) VALUES
(1, '01:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(2, '01:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(3, '01:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(4, '01:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(5, '02:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(6, '02:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(7, '02:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(8, '02:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(9, '09:45:00', '2017-12-15 00:00:00', '2018-01-05 18:37:23'),
(10, '03:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(11, '03:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(12, '03:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(13, '04:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(14, '04:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(15, '04:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(16, '04:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(17, '05:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(18, '05:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(19, '05:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(20, '05:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(21, '06:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(22, '06:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(23, '23:10:00', '2017-12-15 00:00:00', '2018-01-12 18:17:20'),
(24, '06:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(25, '07:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(26, '07:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(27, '07:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(28, '07:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(29, '08:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(30, '08:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(31, '08:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(32, '08:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(33, '09:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(34, '09:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(35, '09:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(36, '09:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(37, '10:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(38, '10:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(39, '10:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(40, '10:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(41, '11:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(42, '11:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(43, '11:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(44, '11:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(45, '12:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(46, '12:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(47, '12:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(48, '12:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(49, '13:00:00', '0000-00-00 00:00:00', '2017-12-15 18:44:27'),
(50, '13:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(51, '13:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(52, '13:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(53, '14:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(54, '14:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(55, '14:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(56, '14:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(57, '15:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(58, '15:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(59, '15:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(60, '15:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(61, '16:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(62, '16:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(63, '16:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(64, '16:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(65, '17:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(66, '17:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(67, '17:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(68, '17:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(69, '18:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(70, '18:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(71, '18:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(72, '18:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(73, '19:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(74, '19:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(75, '19:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(76, '19:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(77, '20:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(78, '20:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(79, '20:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(80, '20:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(81, '21:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(82, '21:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(83, '21:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(84, '21:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(85, '22:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(86, '22:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(87, '22:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(88, '22:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(89, '23:00:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(90, '23:15:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(91, '23:30:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(92, '23:45:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(93, '00:00:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(94, '00:15:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(95, '00:30:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(96, '00:45:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_users`
--

CREATE TABLE `tk_users` (
  `id` int(255) NOT NULL,
  `var_fname` varchar(255) DEFAULT NULL,
  `var_lname` varchar(255) DEFAULT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) NOT NULL,
  `int_otp` int(6) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) DEFAULT NULL,
  `var_longitude` varchar(255) DEFAULT NULL,
  `txt_address` text,
  `int_areacode` int(11) NOT NULL,
  `dt_dob` date DEFAULT NULL,
  `enum_geneder` enum('M','F') NOT NULL DEFAULT 'M',
  `enum_service_type` enum('MEN','WOMEN','BOTH') NOT NULL,
  `var_pushy_device_id` varchar(255) DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_users`
--

INSERT INTO `tk_users` (`id`, `var_fname`, `var_lname`, `var_email`, `bint_phone`, `int_otp`, `var_profile_image`, `var_latitude`, `var_longitude`, `txt_address`, `int_areacode`, `dt_dob`, `enum_geneder`, `enum_service_type`, `var_pushy_device_id`, `enum_enable`, `created_at`, `updated_at`) VALUES
(23, NULL, NULL, NULL, '9924545965', 206700, NULL, NULL, NULL, NULL, 395008, NULL, 'M', 'MEN', '2e4695bdca4b498f499535', 'YES', '2018-04-01 09:59:03', '2018-04-01 13:01:04'),
(24, 'User', 'Test', 'test@gmail.com', '9865212545', NULL, '', NULL, NULL, NULL, 382415, '1993-07-13', 'M', 'MEN', NULL, 'YES', '2018-04-01 11:33:28', '2018-04-01 11:33:28'),
(25, 'Prince', 'Patel', NULL, '927338373', 840926, NULL, NULL, NULL, NULL, 382415, '1994-07-13', 'M', 'MEN', NULL, 'YES', '2018-04-01 11:38:28', '2018-04-01 12:07:47'),
(26, 'Nirav', NULL, 'testing@testing.com', '9898653256', 710524, NULL, NULL, NULL, NULL, 380061, '1995-04-02', 'M', 'MEN', '2e4695bdca4b498f499535', 'YES', '2018-04-01 13:01:29', '2018-04-17 18:00:08'),
(27, NULL, NULL, NULL, '8141051571', 527761, NULL, NULL, NULL, NULL, 382415, NULL, 'M', 'MEN', '9f8fef49b02d0dfef5cd32', 'YES', '2018-04-01 13:38:14', '2018-04-06 17:29:18'),
(28, '', 'Test', NULL, '9999999888', 751301, NULL, NULL, NULL, NULL, 878787, '2018-04-04', 'M', 'WOMEN', '9f8fef49b02d0dfef5cd32', 'YES', '2018-04-04 18:36:38', '2018-04-04 18:44:01'),
(29, NULL, NULL, NULL, '395008', 416746, NULL, NULL, NULL, NULL, 395008, NULL, 'M', 'MEN', NULL, 'YES', '2018-04-06 17:52:20', '2018-04-06 17:52:20'),
(30, 'test', NULL, NULL, '9856325689', 444034, NULL, NULL, NULL, NULL, 0, NULL, 'M', 'MEN', NULL, 'YES', '2018-04-08 10:51:05', '2018-04-08 12:10:05');

-- --------------------------------------------------------

--
-- Table structure for table `tk_user_has_favorite_store`
--

CREATE TABLE `tk_user_has_favorite_store` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_user_has_favorite_store`
--

INSERT INTO `tk_user_has_favorite_store` (`id`, `fk_store`, `fk_user`, `created_at`, `updated_at`) VALUES
(7, 24, 26, '2018-04-02 06:23:18', '2018-04-02 06:23:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_week`
--

CREATE TABLE `tk_week` (
  `id` int(11) NOT NULL,
  `var_name` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_week`
--

INSERT INTO `tk_week` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'sunday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(2, 'monday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(3, 'tuesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(4, 'wednesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(5, 'thursday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(6, 'friday', '2017-12-15 00:00:00', '2017-12-15 17:49:06'),
(7, 'saturday', '2017-12-15 00:00:00', '2017-12-15 17:50:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tk_admin`
--
ALTER TABLE `tk_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking` (`fk_booking`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_timeslot` (`fk_timeslot`),
  ADD KEY `fk_booking` (`fk_booking`);

--
-- Indexes for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_master_category`
--
ALTER TABLE `tk_master_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_offer` (`fk_offer`),
  ADD KEY `fk_service` (`fk_service`),
  ADD KEY `fk_offer_2` (`fk_offer`);

--
-- Indexes for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_package` (`fk_package`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_portfolio_has_review`
--
ALTER TABLE `tk_portfolio_has_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_portfolio` (`fk_portfolio`);

--
-- Indexes for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_setting`
--
ALTER TABLE `tk_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_portfolio`
--
ALTER TABLE `tk_store_has_portfolio`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_review`
--
ALTER TABLE `tk_store_has_review`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_setting` (`fk_setting`),
  ADD KEY `fk_store_2` (`fk_store`,`fk_setting`);

--
-- Indexes for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_timeslot` (`fk_timeslot`);

--
-- Indexes for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_id` (`fk_store`);

--
-- Indexes for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_users`
--
ALTER TABLE `tk_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_user_has_favorite_store`
--
ALTER TABLE `tk_user_has_favorite_store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_week`
--
ALTER TABLE `tk_week`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tk_admin`
--
ALTER TABLE `tk_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_booking`
--
ALTER TABLE `tk_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_master_category`
--
ALTER TABLE `tk_master_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT for table `tk_portfolio_has_review`
--
ALTER TABLE `tk_portfolio_has_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tk_services`
--
ALTER TABLE `tk_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tk_setting`
--
ALTER TABLE `tk_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_stores`
--
ALTER TABLE `tk_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tk_store_has_portfolio`
--
ALTER TABLE `tk_store_has_portfolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_store_has_review`
--
ALTER TABLE `tk_store_has_review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2689;
--
-- AUTO_INCREMENT for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `tk_users`
--
ALTER TABLE `tk_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `tk_user_has_favorite_store`
--
ALTER TABLE `tk_user_has_favorite_store`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_week`
--
ALTER TABLE `tk_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD CONSTRAINT `tk_booking_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD CONSTRAINT `tk_booking_has_service_ibfk_1` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_1` FOREIGN KEY (`fk_timeslot`) REFERENCES `tk_timeslots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_2` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_1` FOREIGN KEY (`fk_bussiness`) REFERENCES `tk_bussiness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_2` FOREIGN KEY (`fk_category`) REFERENCES `tk_bussiness_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD CONSTRAINT `tk_offer_has_service_ibfk_1` FOREIGN KEY (`fk_offer`) REFERENCES `tk_store_has_offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD CONSTRAINT `tk_package_has_service_ibfk_1` FOREIGN KEY (`fk_package`) REFERENCES `tk_store_has_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_package_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_portfolio_has_review`
--
ALTER TABLE `tk_portfolio_has_review`
  ADD CONSTRAINT `tk_portfolio_has_review_ibfk_1` FOREIGN KEY (`fk_portfolio`) REFERENCES `tk_store_has_portfolio` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD CONSTRAINT `tk_services_ibfk_1` FOREIGN KEY (`fk_category`) REFERENCES `tk_master_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD CONSTRAINT `tk_stores_ibfk_1` FOREIGN KEY (`fk_bussiness`) REFERENCES `tk_bussiness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_stores_ibfk_2` FOREIGN KEY (`fk_category`) REFERENCES `tk_master_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD CONSTRAINT `tk_store_has_customer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_customer_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD CONSTRAINT `tk_store_has_gallery_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD CONSTRAINT `tk_store_has_offer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD CONSTRAINT `tk_store_has_package_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_portfolio`
--
ALTER TABLE `tk_store_has_portfolio`
  ADD CONSTRAINT `tk_store_has_portfolio_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_review`
--
ALTER TABLE `tk_store_has_review`
  ADD CONSTRAINT `tk_store_has_review_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_review_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD CONSTRAINT `tk_store_has_services_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_services_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD CONSTRAINT `tk_store_has_setting_ibfk_1` FOREIGN KEY (`fk_setting`) REFERENCES `tk_setting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD CONSTRAINT `tk_store_has_staff_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  ADD CONSTRAINT `tk_store_has_timeslots_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_timeslots_ibfk_2` FOREIGN KEY (`fk_timeslot`) REFERENCES `tk_timeslots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD CONSTRAINT `tk_store_has_timing_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_user_has_favorite_store`
--
ALTER TABLE `tk_user_has_favorite_store`
  ADD CONSTRAINT `tk_user_has_favorite_store_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_user_has_favorite_store_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
