-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 17, 2018 at 09:11 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tikkaro`
--

-- --------------------------------------------------------

--
-- Table structure for table `tk_admin`
--

CREATE TABLE `tk_admin` (
  `id` int(11) NOT NULL,
  `var_fname` varchar(100) NOT NULL,
  `var_lname` varchar(100) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `var_password` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `txt_address` text,
  `dt_dob` datetime DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_admin`
--

INSERT INTO `tk_admin` (`id`, `var_fname`, `var_lname`, `var_email`, `var_password`, `bint_phone`, `var_profile_image`, `txt_address`, `dt_dob`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@admin.com', 'e6e061838856bf47e1de730719fb2609', '9727190205', 'Profile_1515325777.jpg', 'Lorem Ipsum text here...123', '1994-05-19 00:00:00', 'YES', '2017-11-03 00:00:00', '2018-01-07 11:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barbers`
--

CREATE TABLE `tk_barbers` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `txt_address` text,
  `var_rating` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barbers`
--

INSERT INTO `tk_barbers` (`id`, `var_name`, `bint_phone`, `txt_address`, `var_rating`, `created_at`, `updated_at`) VALUES
(1, 'Jignesh vanand', '8487546789', 'Lorem ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(2, 'Rutvik Vanand', '5454878952', 'Lorem Ipsum text here', 4, '2017-11-04 00:00:00', '2017-11-04 07:46:30'),
(3, 'Nirav patel', '9854894556', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(4, 'Mayur Patel', '7894855896', 'Lorem ipsum text here..', 4, '2017-11-04 00:00:00', '2017-11-04 07:47:32'),
(5, 'Gaurav Prajapati', '13213132132132', 'Lorem Ipsum text here...', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35'),
(6, 'Jayesh Mojidra', '54654654321321', 'Lorem ipsum text here..', 5, '2017-11-04 00:00:00', '2017-11-04 09:13:35');

-- --------------------------------------------------------

--
-- Table structure for table `tk_barber_types`
--

CREATE TABLE `tk_barber_types` (
  `id` int(11) NOT NULL,
  `var_type` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_barber_types`
--

INSERT INTO `tk_barber_types` (`id`, `var_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Owner', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53'),
(2, 'Worker', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:20:53');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking`
--

CREATE TABLE `tk_booking` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `dt_booking_date` datetime NOT NULL,
  `var_price` varchar(100) NOT NULL,
  `enum_book_type` enum('CANCEL','COMPLETED','UPCOMING') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking`
--

INSERT INTO `tk_booking` (`id`, `fk_store`, `fk_user`, `dt_booking_date`, `var_price`, `enum_book_type`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-12 03:09:18', '500', 'UPCOMING', '2018-01-10 22:43:46', '2018-01-20 18:10:41'),
(2, 2, 1, '2018-01-12 06:43:22', '2000', 'CANCEL', '2018-01-10 23:19:07', '2018-01-11 19:26:57'),
(3, 2, 1, '2018-01-12 00:11:53', '120', 'COMPLETED', '2018-01-12 00:11:53', '2018-01-27 15:16:55'),
(4, 2, 19, '2018-01-27 07:24:24', '500', 'COMPLETED', '2018-01-27 00:06:41', '2018-01-26 18:36:41'),
(5, 2, 19, '2018-01-27 00:00:00', '200', 'CANCEL', '2018-01-27 00:06:41', '2018-01-26 18:36:41'),
(6, 2, 19, '2018-01-27 00:00:00', '5000', 'UPCOMING', '2018-01-27 00:09:50', '2018-01-26 18:39:50'),
(7, 2, 19, '2018-01-27 00:00:00', '5000', 'CANCEL', '2018-01-27 00:34:50', '2018-01-26 19:06:51'),
(8, 2, 20, '2018-01-24 00:00:00', '2000', 'CANCEL', '2018-01-27 00:34:50', '2018-02-12 17:00:56'),
(9, 2, 20, '2018-01-27 00:00:00', '8000', 'COMPLETED', '2018-01-27 00:35:40', '2018-02-12 17:01:02'),
(10, 2, 21, '2018-01-27 00:35:40', '6000', 'COMPLETED', '2018-01-27 00:35:40', '2018-01-26 19:07:06'),
(11, 2, 20, '2018-01-27 00:36:18', '8000', 'UPCOMING', '2018-01-27 00:36:18', '2018-02-12 17:01:07'),
(12, 2, 21, '2018-01-27 00:36:18', '750', 'COMPLETED', '2018-01-27 00:36:18', '2018-01-26 19:07:15');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_service`
--

CREATE TABLE `tk_booking_has_service` (
  `id` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_service`
--

INSERT INTO `tk_booking_has_service` (`id`, `fk_booking`, `fk_service`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '2018-01-10 22:45:13', '2018-01-10 17:15:13'),
(2, 1, 3, '2018-01-10 22:45:13', '2018-01-10 17:15:13'),
(3, 2, 3, '2018-01-12 00:15:35', '2018-01-11 18:45:35'),
(4, 3, 5, '2018-01-12 00:15:35', '2018-01-11 18:45:35'),
(5, 4, 2, '2018-01-27 00:07:10', '2018-01-26 18:37:10'),
(6, 5, 2, '2018-01-27 00:07:10', '2018-01-26 18:37:10'),
(7, 6, 3, '2018-01-27 00:10:14', '2018-01-26 18:40:14'),
(8, 6, 9, '2018-01-27 00:10:14', '2018-01-26 18:40:14'),
(9, 4, 6, '2018-01-27 00:30:48', '2018-01-26 19:00:48'),
(10, 4, 9, '2018-01-27 00:30:48', '2018-01-26 19:00:48'),
(11, 5, 3, '2018-01-27 00:32:36', '2018-01-26 19:02:36'),
(12, 5, 11, '2018-01-27 00:32:36', '2018-01-26 19:02:36');

-- --------------------------------------------------------

--
-- Table structure for table `tk_booking_has_timeslot`
--

CREATE TABLE `tk_booking_has_timeslot` (
  `id` int(11) NOT NULL,
  `fk_timeslot` int(11) NOT NULL,
  `fk_booking` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_booking_has_timeslot`
--

INSERT INTO `tk_booking_has_timeslot` (`id`, `fk_timeslot`, `fk_booking`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-10 22:45:45', '2018-01-10 17:15:45'),
(2, 3, 1, '2018-01-10 22:45:45', '2018-01-20 17:13:35'),
(3, 4, 10, '2018-01-12 00:16:18', '2018-01-27 15:38:46'),
(4, 6, 3, '2018-01-12 00:16:18', '2018-01-27 15:17:30'),
(5, 11, 3, '2018-01-27 00:07:32', '2018-01-27 16:48:56'),
(6, 3, 5, '2018-01-27 00:07:32', '2018-01-26 18:37:32'),
(7, 3, 6, '2018-01-27 00:10:38', '2018-01-26 18:40:38'),
(8, 15, 6, '2018-01-27 00:10:38', '2018-01-26 18:40:38'),
(9, 5, 4, '2018-01-27 00:31:42', '2018-01-26 19:01:42'),
(10, 37, 2, '2018-01-27 00:31:42', '2018-01-27 15:23:13'),
(11, 38, 2, '2018-01-27 00:33:03', '2018-01-27 15:38:31'),
(12, 48, 5, '2018-01-27 00:33:03', '2018-01-26 19:03:03');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness`
--

CREATE TABLE `tk_bussiness` (
  `id` int(11) NOT NULL,
  `var_bussiness_code` varchar(100) DEFAULT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_phone` varchar(50) DEFAULT NULL,
  `var_email` varchar(100) DEFAULT NULL,
  `txt_note` text,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness`
--

INSERT INTO `tk_bussiness` (`id`, `var_bussiness_code`, `var_name`, `var_phone`, `var_email`, `txt_note`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'ABC123', 'Barber', '9856321456', 'barber@gmail.com', 'tes notasdasdasdsadsadsad', 'YES', '2018-01-22 22:17:52', '2018-01-22 16:47:52'),
(4, 'test123', 'test 789', 'test456@gmail.com', '7778788778', 'asfsd sdfads asdfa s ', 'YES', '2018-01-22 18:11:09', '2018-01-22 18:11:09'),
(5, 'SVj360', 'tesetest', 'weser@sfsgsd.com', '87545454545', 'gsdfgsdfgdfg ', 'YES', '2018-02-07 17:24:01', '2018-02-07 17:24:01');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_category`
--

CREATE TABLE `tk_bussiness_category` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_category`
--

INSERT INTO `tk_bussiness_category` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'test First category', '2018-01-22 22:16:27', '2018-01-22 16:46:27'),
(2, 'test Second category', '2018-01-22 22:16:27', '2018-01-22 16:46:27'),
(3, 'test Third category', '2018-01-22 22:16:54', '2018-01-22 16:46:54'),
(4, 'test Fourth category', '2018-01-22 22:16:54', '2018-01-22 16:46:54');

-- --------------------------------------------------------

--
-- Table structure for table `tk_bussiness_has_category`
--

CREATE TABLE `tk_bussiness_has_category` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `fk_category` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_bussiness_has_category`
--

INSERT INTO `tk_bussiness_has_category` (`id`, `fk_bussiness`, `fk_category`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-01-22 22:25:22', '2018-01-22 16:55:22'),
(2, 1, 2, '2018-01-22 22:25:22', '2018-01-22 16:55:22'),
(5, 4, 2, '0000-00-00 00:00:00', '2018-01-22 18:11:09'),
(6, 4, 4, '0000-00-00 00:00:00', '2018-01-22 18:11:09'),
(7, 5, 1, '2018-02-07 17:24:01', '2018-02-07 17:24:01'),
(8, 5, 2, '2018-02-07 17:24:02', '2018-02-07 17:24:02'),
(9, 5, 3, '2018-02-07 17:24:02', '2018-02-07 17:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `tk_extra_facility`
--

CREATE TABLE `tk_extra_facility` (
  `id` int(11) NOT NULL,
  `var_name` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_extra_facility`
--

INSERT INTO `tk_extra_facility` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'WIFI', 'YES', '2018-01-09 23:14:11', '2018-01-28 09:12:23'),
(2, 'AC', 'YES', '2018-01-09 23:14:11', '2018-01-09 17:44:11'),
(3, 'Game Area', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32'),
(4, 'Movie Zone', 'YES', '2018-01-18 22:21:32', '2018-01-18 16:51:32');

-- --------------------------------------------------------

--
-- Table structure for table `tk_migrations`
--

CREATE TABLE `tk_migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tk_migrations`
--

INSERT INTO `tk_migrations` (`version`) VALUES
(0);

-- --------------------------------------------------------

--
-- Table structure for table `tk_offer_has_service`
--

CREATE TABLE `tk_offer_has_service` (
  `id` int(11) NOT NULL,
  `fk_offer` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_offer_has_service`
--

INSERT INTO `tk_offer_has_service` (`id`, `fk_offer`, `fk_service`, `var_price`, `var_offer_price`, `created_at`, `updated_at`) VALUES
(21, 4, 1, '150', '333', '2018-02-17 19:58:33', '2018-02-17 19:58:33'),
(22, 4, 4, '2500', '99', '2018-02-17 19:58:34', '2018-02-17 19:58:34');

-- --------------------------------------------------------

--
-- Table structure for table `tk_package_has_service`
--

CREATE TABLE `tk_package_has_service` (
  `id` int(11) NOT NULL,
  `fk_package` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_package_has_service`
--

INSERT INTO `tk_package_has_service` (`id`, `fk_package`, `fk_service`, `var_price`, `created_at`, `updated_at`) VALUES
(10, 2, 1, '78', '2018-01-28 17:17:18', '2018-01-28 17:17:18'),
(11, 2, 6, '85', '2018-01-28 17:17:19', '2018-01-28 17:17:19'),
(23, 5, 2, '90', '2018-01-30 17:16:15', '2018-01-30 17:16:15'),
(24, 5, 3, '80', '2018-01-30 17:16:15', '2018-01-30 17:16:15'),
(72, 1, 3, '111', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(73, 1, 2, '11111111', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(74, 1, 1, '11', '2018-02-02 16:41:30', '2018-02-02 16:41:30'),
(77, 6, 1, '150', '2018-02-02 18:06:49', '2018-02-02 18:06:49'),
(78, 6, 10, '333', '2018-02-02 18:06:49', '2018-02-02 18:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `tk_services`
--

CREATE TABLE `tk_services` (
  `id` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_services`
--

INSERT INTO `tk_services` (`id`, `var_title`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'Hair cut', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:42:38'),
(2, 'Saving', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:42:38'),
(3, 'Clipper Cut', 'YES', '2017-11-04 00:00:00', '2018-01-27 15:07:20'),
(4, 'The Sides', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:43:47'),
(5, 'Hot Towel Head Shave', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:44:22'),
(6, 'Hot Towel Face Shave', 'YES', '2017-11-04 00:00:00', '2017-11-04 07:44:22'),
(8, 'asdasd456fgdf', 'YES', '2018-01-04 18:47:42', '2018-01-04 19:08:06'),
(9, 'Hair cut 123', 'YES', '0000-00-00 00:00:00', '2018-01-19 17:51:30'),
(10, 'Hair cut 456', 'YES', '2018-01-19 18:47:40', '2018-01-19 18:47:40'),
(11, 'Hair spa', 'YES', '2018-01-22 18:11:20', '2018-01-22 18:11:20');

-- --------------------------------------------------------

--
-- Table structure for table `tk_setting`
--

CREATE TABLE `tk_setting` (
  `id` int(11) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_setting`
--

INSERT INTO `tk_setting` (`id`, `var_name`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'var_fb_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(2, 'var_insta_link', 'YES', '2018-01-17 00:16:45', '2018-01-16 18:46:45'),
(3, 'var_twitter_link', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24'),
(4, 'var_whatsapp_no', 'YES', '2018-01-17 00:17:24', '2018-01-16 18:47:24');

-- --------------------------------------------------------

--
-- Table structure for table `tk_stores`
--

CREATE TABLE `tk_stores` (
  `id` int(11) NOT NULL,
  `fk_bussiness` int(11) NOT NULL,
  `var_title` varchar(255) NOT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `txt_note` text,
  `var_store_code` varchar(255) NOT NULL,
  `int_otp` int(11) DEFAULT NULL,
  `txt_address` text,
  `var_logo` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) NOT NULL,
  `var_longitude` varchar(255) NOT NULL,
  `int_areacode` int(11) DEFAULT NULL,
  `bint_phone` varchar(20) DEFAULT NULL,
  `int_weekoff` int(11) DEFAULT NULL,
  `int_no_book_seats` int(11) DEFAULT NULL,
  `var_fb_link` varchar(255) DEFAULT NULL,
  `var_twitter_link` varchar(255) DEFAULT NULL,
  `var_instagram_link` varchar(255) DEFAULT NULL,
  `var_whatsapp_no` varchar(50) DEFAULT NULL,
  `var_rating` varchar(20) DEFAULT NULL,
  `enum_store_type` enum('MEN','WOMEN','BOTH') NOT NULL DEFAULT 'MEN',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_stores`
--

INSERT INTO `tk_stores` (`id`, `fk_bussiness`, `var_title`, `var_email`, `txt_note`, `var_store_code`, `int_otp`, `txt_address`, `var_logo`, `var_latitude`, `var_longitude`, `int_areacode`, `bint_phone`, `int_weekoff`, `int_no_book_seats`, `var_fb_link`, `var_twitter_link`, `var_instagram_link`, `var_whatsapp_no`, `var_rating`, `enum_store_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 'Hairitage Unisex Salon', NULL, NULL, 'caz347', NULL, 'GF.10, Dev-preet Complex Opp.Brewberrys Coffee Bar N.F.D Circle, Cross Raod Bodakdev, Bodakdev, Ahmedabad, Gujarat 380054', NULL, '23.043459', '72.520108', NULL, '456798231312', 2, 1, NULL, NULL, NULL, NULL, '5', 'BOTH', 'YES', '2017-11-04 00:00:00', '2018-01-22 18:22:48'),
(2, 1, 'Truefitt & Hill', 'testing@gmail.com', 'testy', 'ABCGHitk007', 444879, '77, Western Express Hwy, Tanji Nagar, Kurar Village, Malad East, Mumbai, Maharashtra 400097, India', NULL, '19.188591', '72.85870299999999', 400097, '8884445454', 5, 4, 'www.fb.com/test', 'www.twitter.com/test', 'www.insta.com/test', '88866633', '4', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-02-13 17:32:33'),
(3, 1, 'Classic Hair Care', NULL, NULL, 'bio412', NULL, 'Newyork Trade Centre, S.G. Highway, Thaltej, Ahmedabad, Gujarat 380059', NULL, '23.040054', '72.513088', NULL, '565565656', 0, 1, NULL, NULL, NULL, NULL, '3.3', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-01-22 18:22:55'),
(4, 1, 'Ai Shree Khodiyar Hair Style', NULL, NULL, 'FIq180', NULL, 'Maharana Pratap Rd, Vishwas City 1, Chanakyapuri, Ahmedabad, Gujarat 380061', NULL, '23.080365', '72.535798', NULL, '213265483141', 3, 1, NULL, NULL, NULL, NULL, '4.1', 'MEN', 'YES', '2017-11-04 00:00:00', '2018-01-22 18:23:01'),
(5, 1, 'Vaibhav Hair Art', NULL, NULL, 'uVP369', NULL, 'Asopalav Apartment, Mangal Murti, Naranpur Telephone Exchange Road, Naranpura, Ahmedabad, Gujarat 380063', NULL, '23.060193', '72.547983', NULL, '798798564564', 2, 1, NULL, NULL, NULL, NULL, NULL, 'MEN', 'YES', '2017-11-04 00:00:00', '2018-01-22 18:23:08'),
(6, 1, 'Deepkala Hair Art', NULL, NULL, 'Atr568', NULL, '12/B Janta Market Society, Vejalpur, Ahmedabad, Gujarat 380051', 'store_logo_1518862036.jpg', '23.003413', '72.518396', NULL, '989565656512', 5, 1, NULL, NULL, NULL, NULL, NULL, 'WOMEN', 'YES', '2017-11-04 00:00:00', '2018-02-17 10:07:16'),
(7, 1, 'Example Store', NULL, NULL, 'hJC397', NULL, '120, New Link Road, Kandivali West, Mumbai, Maharashtra, India', NULL, '19.206802', '72.83462499999996', 400067, '98654124', 5, 10, NULL, NULL, NULL, NULL, '2.2', 'MEN', 'YES', '2018-01-11 19:19:59', '2018-01-22 18:23:12'),
(11, 1, 'TEST STORE', NULL, NULL, 'itT300', NULL, '154, Western Express Highway, Park Society, Mumbai, Maharashtra, India', NULL, '19.0974079', '72.85294099999999', 400057, '566565', 0, 12, NULL, NULL, NULL, NULL, '3.5', 'WOMEN', 'YES', '2018-01-24 16:53:15', '2018-01-24 17:05:48'),
(12, 1, 'TEST LOGO IMG', NULL, NULL, 'XWa516', NULL, '785, Outer Ring Road, Nagarjun Enclave, 1st Phase, JP Nagar, Bengaluru, Karnataka, India', 'store_logo_1518857563.jpg', '12.906821', '77.57725800000003', 560078, '9856321456', NULL, 40, NULL, NULL, NULL, NULL, '1.3', 'MEN', 'YES', '2018-02-17 08:36:49', '2018-02-17 19:20:39');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_bank_details`
--

CREATE TABLE `tk_store_has_bank_details` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_accountno` varchar(255) NOT NULL,
  `var_ifsc_code` varchar(255) NOT NULL,
  `var_name` varchar(500) NOT NULL,
  `var_bank_name` varchar(500) NOT NULL,
  `var_branch_name` varchar(500) NOT NULL,
  `txt_address` text NOT NULL,
  `enum_acc_type` enum('S','C') NOT NULL COMMENT '''S''=>''Saving'',''C''=>''Current''',
  `var_debit_card_type` varchar(255) NOT NULL,
  `var_card_number` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_bank_details`
--

INSERT INTO `tk_store_has_bank_details` (`id`, `fk_store`, `var_accountno`, `var_ifsc_code`, `var_name`, `var_bank_name`, `var_branch_name`, `txt_address`, `enum_acc_type`, `var_debit_card_type`, `var_card_number`, `created_at`, `updated_at`) VALUES
(1, 6, '999885554', 'unb0012', 'jay shah', 'union bank', 'vastral', 'vastral', 'C', 'debit', '123456987412', '2018-01-12 17:41:45', '2018-01-12 17:42:11'),
(3, 2, 'hdfhdfh7878121212', 'fsdfa', 'TEST', 'state bank of india', 'VASTRAL', 'TRST', '', 'D', 'asdasdasdgsdgfhfdhdfhdfhfd', '2018-01-27 13:38:34', '2018-02-14 16:45:16'),
(4, 7, '778788778', 'icici785412', 'test', 'test', 'test', 'setset', 'S', 'dfsdfgdfg', '7854124587', '2018-01-28 09:14:11', '2018-01-28 09:14:11'),
(5, 5, 'strore 5 bank1', 'IFSC778', 'TEst name', 'Axis Bank', 'Ashram road', 'Ashram road', 'S', 'AXIS BANK DEbit card', '789645123652', '2018-01-30 16:06:41', '2018-01-30 16:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_barber`
--

CREATE TABLE `tk_store_has_barber` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_barber` int(11) NOT NULL,
  `fk_barber_type` int(11) NOT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_barber`
--

INSERT INTO `tk_store_has_barber` (`id`, `fk_store`, `fk_barber`, `fk_barber_type`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(2, 1, 2, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:10:39'),
(3, 2, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(4, 2, 4, 2, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:01'),
(5, 3, 3, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(6, 4, 4, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:11:44'),
(7, 5, 5, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57'),
(8, 6, 6, 1, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:13:57');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_customer`
--

CREATE TABLE `tk_store_has_customer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_user` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_customer`
--

INSERT INTO `tk_store_has_customer` (`id`, `fk_store`, `fk_user`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2018-01-26 22:58:41', '2018-01-26 17:28:41'),
(2, 2, 19, '2018-01-26 22:58:41', '2018-01-26 17:28:41'),
(3, 2, 20, '2018-01-26 22:58:56', '2018-01-26 17:28:56'),
(4, 5, 19, '2018-01-27 01:15:21', '2018-01-26 19:45:21'),
(5, 11, 19, '2018-01-27 01:15:21', '2018-01-26 19:45:21');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_facility`
--

CREATE TABLE `tk_store_has_facility` (
  `id` int(11) NOT NULL,
  `fk_facility` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_facility`
--

INSERT INTO `tk_store_has_facility` (`id`, `fk_facility`, `fk_store`, `created_at`, `updated_at`) VALUES
(26, 1, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(27, 2, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29'),
(28, 3, 2, '0000-00-00 00:00:00', '2018-01-22 18:13:29');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_gallery`
--

CREATE TABLE `tk_store_has_gallery` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_parent` int(11) DEFAULT NULL,
  `var_file` varchar(500) NOT NULL,
  `var_width` varchar(50) DEFAULT NULL,
  `var_height` varchar(50) DEFAULT NULL,
  `enum_isProfile` enum('YES','NO') DEFAULT NULL,
  `enum_ismain` enum('YES','NO') DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_gallery`
--

INSERT INTO `tk_store_has_gallery` (`id`, `fk_store`, `fk_parent`, `var_file`, `var_width`, `var_height`, `enum_isProfile`, `enum_ismain`, `enum_enable`, `created_at`, `updated_at`) VALUES
(19, 2, NULL, '0973ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:27:56', '2018-01-31 17:27:56'),
(20, 2, NULL, '1443ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'NO', '2018-01-31 17:27:56', '2018-01-31 17:27:56'),
(21, 2, NULL, '0529ABCGHitk007.jpg', NULL, NULL, 'YES', 'YES', 'YES', '2018-01-31 17:27:57', '2018-01-31 17:27:57'),
(22, 2, NULL, '6057ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:27:59', '2018-01-31 17:27:59'),
(23, 2, NULL, '3243ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:35:33', '2018-01-31 17:35:33'),
(24, 2, NULL, '1876ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:37:12', '2018-01-31 17:37:12'),
(25, 2, NULL, '4515ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-01-31 17:37:12', '2018-01-31 17:37:12'),
(28, 2, NULL, '5277ABCGHitk007.jpg', NULL, NULL, 'NO', 'YES', 'NO', '2018-01-31 17:47:06', '2018-01-31 17:47:06'),
(32, 2, NULL, 'store_1517507786.jpg', NULL, NULL, 'NO', 'YES', 'YES', '2018-02-01 17:56:26', '2018-02-01 17:56:26'),
(38, 2, NULL, 'images-11517593306.jpg', '259', '194', 'NO', 'YES', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(39, 2, NULL, 'images-11517593306_100x74.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(40, 2, NULL, 'images-11517593306_200x149.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(41, 2, NULL, 'images-11517593306_500x374.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-02 17:41:46', '2018-02-02 17:41:46'),
(42, 2, NULL, 'images-51517593331.jpg', '275', '183', 'NO', 'YES', 'YES', '2018-02-02 17:42:11', '2018-02-02 17:42:11'),
(43, 2, NULL, 'images-51517593331_100x66.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-02 17:42:11', '2018-02-02 17:42:11'),
(44, 2, NULL, 'images-51517593331_200x133.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-02 17:42:12', '2018-02-02 17:42:12'),
(45, 2, NULL, 'images-51517593331_500x332.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-02 17:42:12', '2018-02-02 17:42:12'),
(46, 12, NULL, 'images-11518857674.jpg', '259', '194', 'NO', 'YES', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(47, 12, NULL, 'images-11518857674_100x74.jpg', '100', '100', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(48, 12, NULL, 'images-11518857674_200x149.jpg', '200', '200', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34'),
(49, 12, NULL, 'images-11518857674_500x374.jpg', '500', '500', 'NO', 'NO', 'YES', '2018-02-17 08:54:34', '2018-02-17 08:54:34');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_holidays`
--

CREATE TABLE `tk_store_has_holidays` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `dt_holiday_date` date NOT NULL,
  `dt_start_time` time NOT NULL,
  `dt_end_time` time NOT NULL,
  `txt_note` text NOT NULL,
  `enum_holiday` enum('YES','NO') DEFAULT 'NO',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_holidays`
--

INSERT INTO `tk_store_has_holidays` (`id`, `fk_store`, `dt_holiday_date`, `dt_start_time`, `dt_end_time`, `txt_note`, `enum_holiday`, `created_at`, `updated_at`) VALUES
(18, 2, '2018-01-25', '03:41:00', '03:41:00', 'dfdsfd', 'YES', '2018-01-26 22:07:07', '2018-01-26 22:29:33'),
(19, 2, '2018-01-24', '03:41:00', '03:41:00', 'dfdsfd24', 'NO', '2018-01-26 22:07:22', '2018-01-26 22:30:52'),
(23, 2, '2018-01-27', '04:00:00', '04:00:00', 'dsfsdfsdf', 'YES', '2018-01-26 22:26:20', '2018-01-26 22:26:20'),
(24, 2, '2018-01-22', '04:38:00', '04:38:00', 'dsdasd', 'YES', '2018-01-26 23:08:29', '2018-01-26 23:08:29');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_offer`
--

CREATE TABLE `tk_store_has_offer` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(500) NOT NULL,
  `var_offer_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `dt_start_date` date NOT NULL,
  `dt_end_date` date NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_offer`
--

INSERT INTO `tk_store_has_offer` (`id`, `fk_store`, `var_name`, `var_price`, `var_offer_price`, `txt_note`, `dt_start_date`, `dt_end_date`, `enum_enable`, `created_at`, `updated_at`) VALUES
(4, 2, 'test offer 2222', '2650', '432', 'extra notes changes222', '2018-12-07', '2018-02-15', 'YES', '2018-01-29 17:16:09', '2018-01-29 17:16:09');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_package`
--

CREATE TABLE `tk_store_has_package` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(500) DEFAULT NULL,
  `var_price` varchar(50) DEFAULT NULL,
  `txt_note` text,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_package`
--

INSERT INTO `tk_store_has_package` (`id`, `fk_store`, `var_name`, `var_price`, `txt_note`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 2, 'Diwali packege', '2500', 'This is Diwali special packege', 'YES', '2018-01-28 14:55:52', '2018-01-28 09:25:52'),
(2, 6, 'test pack 2', '33', 'extra notes', 'YES', '2018-01-28 17:11:45', '2018-01-28 17:11:45'),
(5, 5, 'TEst Pack for store 5', '250000', ' Spacial pack   ', 'NO', '2018-01-30 16:07:34', '2018-01-30 16:07:34'),
(6, 2, 'new package', '633', 'test', 'YES', '2018-02-02 18:06:49', '2018-02-02 18:06:49');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_services`
--

CREATE TABLE `tk_store_has_services` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_service` int(11) NOT NULL,
  `var_price` varchar(11) NOT NULL,
  `dt_service_time` time DEFAULT NULL,
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_services`
--

INSERT INTO `tk_store_has_services` (`id`, `fk_store`, `fk_service`, `var_price`, `dt_service_time`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:05'),
(2, 1, 2, '100', '00:00:10', 'YES', '2017-11-04 00:00:00', '2018-01-19 17:00:20'),
(3, 1, 3, '150', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:03:35'),
(7, 3, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:06:12'),
(8, 3, 2, '100', '00:00:20', 'YES', '2017-11-04 00:00:00', '2018-01-19 17:00:12'),
(9, 3, 3, '150', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(10, 4, 4, '200', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:15'),
(11, 4, 5, '300', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(12, 4, 6, '550', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:07:46'),
(13, 5, 1, '50', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:08:42'),
(14, 5, 2, '100', '00:00:25', 'YES', '2017-11-04 00:00:00', '2018-01-19 17:00:27'),
(15, 5, 3, '200', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(16, 6, 4, '300', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:17'),
(17, 6, 5, '400', NULL, 'YES', '2017-11-04 00:00:00', '2017-11-04 09:09:37'),
(18, 7, 6, '500', NULL, 'YES', '2017-11-04 00:00:00', '2018-02-09 18:41:19'),
(19, 3, 4, '500', NULL, 'YES', '2018-01-06 19:54:11', '2018-01-06 19:54:11'),
(24, 2, 1, '150', '12:00:00', 'YES', '2018-01-19 23:35:02', '2018-02-14 16:41:25'),
(25, 2, 2, '1500', '01:50:00', 'YES', '2018-01-19 23:35:02', '2018-01-29 16:48:43'),
(28, 2, 4, '2500', '00:33:00', 'YES', '0000-00-00 00:00:00', '2018-01-30 18:11:45'),
(30, 2, 3, '3', '03:15:00', 'YES', '0000-00-00 00:00:00', '2018-02-17 11:23:18'),
(31, 11, 11, '200', '00:12:00', 'NO', '0000-00-00 00:00:00', '2018-02-09 18:42:13'),
(35, 2, 9, '480', '00:30:00', 'NO', '0000-00-00 00:00:00', '2018-02-17 11:15:54');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_setting`
--

CREATE TABLE `tk_store_has_setting` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `fk_setting` int(11) NOT NULL,
  `var_setting_val` varchar(500) NOT NULL,
  `enum_enable` enum('YES','NO') DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_setting`
--

INSERT INTO `tk_store_has_setting` (`id`, `fk_store`, `fk_setting`, `var_setting_val`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 2, 1, 'www.facebook.com/hairsallon', 'YES', '2018-01-17 00:17:56', '2018-01-16 18:47:56'),
(2, 2, 4, '985632145', 'YES', '2018-01-17 00:18:18', '2018-01-16 18:48:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_staff`
--

CREATE TABLE `tk_store_has_staff` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `var_name` varchar(255) DEFAULT NULL,
  `var_phone` varchar(100) DEFAULT NULL,
  `var_image` varchar(500) DEFAULT NULL,
  `dt_startdate` datetime DEFAULT NULL,
  `dt_enddate` datetime DEFAULT NULL,
  `enum_isFrontView` enum('YES','NO') DEFAULT NULL,
  `enum_active` enum('YES','NO') DEFAULT NULL,
  `enum_current` enum('YES','NO') DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_staff`
--

INSERT INTO `tk_store_has_staff` (`id`, `fk_store`, `var_name`, `var_phone`, `var_image`, `dt_startdate`, `dt_enddate`, `enum_isFrontView`, `enum_active`, `enum_current`, `created_at`, `updated_at`) VALUES
(1, 1, 'tushar123', '9856321456', NULL, '2017-12-15 23:23:17', '2017-12-17 23:23:17', 'YES', 'NO', 'YES', '2018-01-08 18:49:48', '2018-01-16 17:22:26'),
(3, 2, 'Test1', '9856321456', NULL, '2017-01-03 00:00:00', '2018-01-16 00:00:00', 'YES', 'YES', 'YES', '2018-01-16 23:41:58', '2018-01-17 18:32:06'),
(4, 2, 'nirav', '1111111855', NULL, '2018-01-17 00:00:00', '2018-01-17 00:00:00', 'NO', 'YES', 'YES', '2018-01-16 19:10:54', '2018-02-17 14:53:53'),
(5, 5, 'neasrfd', '5555', NULL, '2018-01-17 00:00:00', '2018-01-31 00:00:00', 'NO', 'NO', 'NO', '2018-01-16 19:13:07', '2018-01-28 09:01:22'),
(6, 2, 'Test staff', '8777667779', NULL, '2018-01-17 00:00:00', '2018-01-17 00:00:00', 'YES', 'YES', 'YES', '2018-01-17 17:17:20', '2018-02-17 14:19:01'),
(8, 1, 'eeeee', '33333', NULL, '2017-02-01 00:00:00', '2018-01-02 00:00:00', 'YES', 'YES', 'NO', '0000-00-00 00:00:00', '2018-01-28 08:58:42'),
(9, 2, 'test image', '9856321458', 'staff_1518200134.jpg', '2017-12-01 00:00:00', '2018-02-28 00:00:00', 'YES', 'YES', 'YES', '2018-02-09 16:52:26', '2018-02-17 14:50:47');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timeslots`
--

CREATE TABLE `tk_store_has_timeslots` (
  `id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `timeslot_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timeslots`
--

INSERT INTO `tk_store_has_timeslots` (`id`, `store_id`, `timeslot_id`, `created_at`, `updated_at`) VALUES
(1, 2, 3, '2018-01-07 12:24:26', '2018-01-07 06:54:26'),
(2, 2, 4, '2018-01-07 12:24:26', '2018-01-07 06:54:26'),
(3, 2, 6, '2018-01-07 12:24:52', '2018-01-07 06:54:52'),
(4, 2, 8, '2018-01-07 12:24:52', '2018-01-07 06:54:52'),
(5, 2, 12, '2018-01-07 12:25:17', '2018-01-07 06:55:17'),
(7, 2, 27, '2018-01-07 07:27:14', '2018-01-07 07:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `tk_store_has_timing`
--

CREATE TABLE `tk_store_has_timing` (
  `id` int(11) NOT NULL,
  `fk_store` int(11) NOT NULL,
  `int_week` int(11) NOT NULL,
  `dt_open_time` time NOT NULL,
  `dt_close_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_store_has_timing`
--

INSERT INTO `tk_store_has_timing` (`id`, `fk_store`, `int_week`, `dt_open_time`, `dt_close_time`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '07:30:36', '18:30:36', '2018-01-18 22:55:44', '2018-02-16 17:21:34'),
(2, 2, 2, '08:14:01', '19:37:00', '2018-01-18 22:55:44', '2018-01-18 17:25:44'),
(3, 2, 3, '06:15:26', '13:43:00', '2018-01-18 22:57:07', '2018-01-18 17:27:07'),
(4, 2, 4, '07:43:00', '17:37:00', '2018-01-18 22:57:07', '2018-01-18 17:27:07'),
(5, 2, 5, '06:22:27', '16:42:43', '2018-01-18 22:57:52', '2018-01-18 17:27:52'),
(6, 2, 6, '10:50:00', '20:48:00', '2018-01-18 22:57:52', '2018-01-19 17:52:15'),
(7, 2, 0, '08:00:00', '18:00:00', '2018-01-18 23:14:10', '2018-01-21 10:28:46'),
(9, 11, 0, '08:00:00', '20:00:00', '2018-01-24 16:53:15', '2018-01-24 16:55:52'),
(10, 11, 1, '08:00:00', '19:00:00', '2018-01-24 16:53:15', '2018-01-24 16:53:15'),
(11, 11, 2, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(12, 11, 3, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(13, 11, 4, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(14, 11, 5, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(15, 11, 6, '08:00:00', '19:00:00', '2018-01-24 16:53:16', '2018-01-24 16:53:16'),
(16, 12, 0, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(17, 12, 1, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(18, 12, 2, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(19, 12, 3, '08:00:00', '19:00:00', '2018-02-17 08:36:49', '2018-02-17 08:36:49'),
(20, 12, 4, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50'),
(21, 12, 5, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50'),
(22, 12, 6, '08:00:00', '19:00:00', '2018-02-17 08:36:50', '2018-02-17 08:36:50');

-- --------------------------------------------------------

--
-- Table structure for table `tk_timeslots`
--

CREATE TABLE `tk_timeslots` (
  `id` int(11) NOT NULL,
  `slot_time` time NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_timeslots`
--

INSERT INTO `tk_timeslots` (`id`, `slot_time`, `created_at`, `updated_at`) VALUES
(1, '01:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(2, '01:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(3, '01:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(4, '01:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(5, '02:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(6, '02:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(7, '02:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(8, '02:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(9, '09:45:00', '2017-12-15 00:00:00', '2018-01-05 18:37:23'),
(10, '03:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(11, '03:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(12, '03:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(13, '04:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(14, '04:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:42'),
(15, '04:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(16, '04:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(17, '05:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(18, '05:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(19, '05:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(20, '05:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(21, '06:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(22, '06:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(23, '23:10:00', '2017-12-15 00:00:00', '2018-01-12 18:17:20'),
(24, '06:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(25, '07:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(26, '07:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(27, '07:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(28, '07:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(29, '08:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(30, '08:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(31, '08:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(32, '08:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(33, '09:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(34, '09:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(35, '09:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:43'),
(36, '09:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(37, '10:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(38, '10:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(39, '10:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(40, '10:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(41, '11:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(42, '11:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(43, '11:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(44, '11:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(45, '12:00:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(46, '12:15:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(47, '12:30:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(48, '12:45:00', '2017-12-15 00:00:00', '2017-12-15 18:29:44'),
(49, '13:00:00', '0000-00-00 00:00:00', '2017-12-15 18:44:27'),
(50, '13:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(51, '13:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(52, '13:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(53, '14:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(54, '14:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(55, '14:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(56, '14:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(57, '15:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(58, '15:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(59, '15:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(60, '15:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(61, '16:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(62, '16:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(63, '16:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(64, '16:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(65, '17:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(66, '17:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(67, '17:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:28'),
(68, '17:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(69, '18:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(70, '18:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(71, '18:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(72, '18:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(73, '19:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(74, '19:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(75, '19:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(76, '19:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(77, '20:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(78, '20:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(79, '20:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(80, '20:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(81, '21:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(82, '21:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(83, '21:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(84, '21:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(85, '22:00:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(86, '22:15:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(87, '22:30:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(88, '22:45:00', '2017-12-15 00:00:00', '2017-12-15 18:44:29'),
(89, '23:00:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(90, '23:15:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(91, '23:30:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(92, '23:45:00', '2017-12-15 00:00:00', '2017-12-15 18:47:01'),
(93, '00:00:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(94, '00:15:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(95, '00:30:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18'),
(96, '00:45:00', '2017-12-15 00:00:00', '2017-12-15 18:51:18');

-- --------------------------------------------------------

--
-- Table structure for table `tk_users`
--

CREATE TABLE `tk_users` (
  `id` int(255) NOT NULL,
  `var_fname` varchar(255) DEFAULT NULL,
  `var_lname` varchar(255) DEFAULT NULL,
  `var_email` varchar(255) DEFAULT NULL,
  `bint_phone` varchar(20) NOT NULL,
  `int_otp` int(6) DEFAULT NULL,
  `var_profile_image` varchar(255) DEFAULT NULL,
  `var_latitude` varchar(255) DEFAULT NULL,
  `var_longitude` varchar(255) DEFAULT NULL,
  `int_areacode` int(11) NOT NULL,
  `txt_address` text,
  `dt_dob` date DEFAULT NULL,
  `enum_geneder` enum('M','F') NOT NULL DEFAULT 'M',
  `enum_enable` enum('YES','NO') NOT NULL DEFAULT 'YES',
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_users`
--

INSERT INTO `tk_users` (`id`, `var_fname`, `var_lname`, `var_email`, `bint_phone`, `int_otp`, `var_profile_image`, `var_latitude`, `var_longitude`, `int_areacode`, `txt_address`, `dt_dob`, `enum_geneder`, `enum_enable`, `created_at`, `updated_at`) VALUES
(1, 'test fname', 'test lname', 'test@test.com', '9727190205', 235689, NULL, NULL, NULL, 382415, 'test address', '1993-01-10', 'M', 'YES', '2017-11-04 10:34:55', '2018-01-03 17:31:55'),
(19, 'testestsetset', 'setsetest', 'estests@sfds.com', '1222222', NULL, NULL, '21.2132781', '72.8486878', 395008, 'Umiyadham Rd, Dharmishta Park Society, Varachha, Surat, Gujarat 395008, India', '1992-01-10', 'F', 'YES', '2018-01-03 23:18:04', '2018-02-17 19:17:35'),
(20, 'fisrt name12', 'last name12', 'first+101@gmail.com', '999999999', NULL, 'user_1515321722.jpg', '21.2132781', '72.8486878', 0, 'aa', '1994-10-02', 'M', 'YES', '2018-01-04 16:49:59', '2018-01-07 10:42:02'),
(21, 'j user', 'j user', 'juser@gmail.com', '9924545965', NULL, 'user_1515780630.jpg', '21.2132781', '72.8486878', 395008, 'Umiyadham Rd, Dharmishta Park Society, Varachha, Surat, Gujarat 395008, India', '2018-01-16', 'M', 'YES', '2018-01-12 18:10:30', '2018-01-27 17:38:27');

-- --------------------------------------------------------

--
-- Table structure for table `tk_week`
--

CREATE TABLE `tk_week` (
  `id` int(11) NOT NULL,
  `var_name` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tk_week`
--

INSERT INTO `tk_week` (`id`, `var_name`, `created_at`, `updated_at`) VALUES
(1, 'sunday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(2, 'monday', '2017-12-15 00:00:00', '2017-12-15 17:45:07'),
(3, 'tuesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(4, 'wednesday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(5, 'thursday', '2017-12-15 00:00:00', '2017-12-15 17:48:22'),
(6, 'friday', '2017-12-15 00:00:00', '2017-12-15 17:49:06'),
(7, 'saturday', '2017-12-15 00:00:00', '2017-12-15 17:50:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tk_admin`
--
ALTER TABLE `tk_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking` (`fk_booking`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_timeslot` (`fk_timeslot`),
  ADD KEY `fk_booking` (`fk_booking`);

--
-- Indexes for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`),
  ADD KEY `fk_category` (`fk_category`);

--
-- Indexes for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_offer` (`fk_offer`),
  ADD KEY `fk_service` (`fk_service`),
  ADD KEY `fk_offer_2` (`fk_offer`);

--
-- Indexes for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_package` (`fk_package`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_services`
--
ALTER TABLE `tk_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_setting`
--
ALTER TABLE `tk_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_stores`
--
ALTER TABLE `tk_stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bussiness` (`fk_bussiness`);

--
-- Indexes for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_user` (`fk_user`);

--
-- Indexes for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_service` (`fk_service`);

--
-- Indexes for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`),
  ADD KEY `fk_setting` (`fk_setting`),
  ADD KEY `fk_store_2` (`fk_store`,`fk_setting`);

--
-- Indexes for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_store` (`fk_store`);

--
-- Indexes for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD PRIMARY KEY (`id`),
  ADD KEY `store_id` (`fk_store`);

--
-- Indexes for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_users`
--
ALTER TABLE `tk_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tk_week`
--
ALTER TABLE `tk_week`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tk_admin`
--
ALTER TABLE `tk_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tk_barbers`
--
ALTER TABLE `tk_barbers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_barber_types`
--
ALTER TABLE `tk_barber_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_booking`
--
ALTER TABLE `tk_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_bussiness`
--
ALTER TABLE `tk_bussiness`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_bussiness_category`
--
ALTER TABLE `tk_bussiness_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tk_extra_facility`
--
ALTER TABLE `tk_extra_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;
--
-- AUTO_INCREMENT for table `tk_services`
--
ALTER TABLE `tk_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_setting`
--
ALTER TABLE `tk_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tk_stores`
--
ALTER TABLE `tk_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tk_store_has_bank_details`
--
ALTER TABLE `tk_store_has_bank_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_store_has_barber`
--
ALTER TABLE `tk_store_has_barber`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tk_store_has_facility`
--
ALTER TABLE `tk_store_has_facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `tk_store_has_holidays`
--
ALTER TABLE `tk_store_has_holidays`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tk_store_has_timeslots`
--
ALTER TABLE `tk_store_has_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tk_timeslots`
--
ALTER TABLE `tk_timeslots`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `tk_users`
--
ALTER TABLE `tk_users`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `tk_week`
--
ALTER TABLE `tk_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tk_booking`
--
ALTER TABLE `tk_booking`
  ADD CONSTRAINT `tk_booking_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_service`
--
ALTER TABLE `tk_booking_has_service`
  ADD CONSTRAINT `tk_booking_has_service_ibfk_1` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_booking_has_timeslot`
--
ALTER TABLE `tk_booking_has_timeslot`
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_1` FOREIGN KEY (`fk_timeslot`) REFERENCES `tk_timeslots` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_booking_has_timeslot_ibfk_2` FOREIGN KEY (`fk_booking`) REFERENCES `tk_booking_has_service` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_bussiness_has_category`
--
ALTER TABLE `tk_bussiness_has_category`
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_1` FOREIGN KEY (`fk_bussiness`) REFERENCES `tk_bussiness` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_bussiness_has_category_ibfk_2` FOREIGN KEY (`fk_category`) REFERENCES `tk_bussiness_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_offer_has_service`
--
ALTER TABLE `tk_offer_has_service`
  ADD CONSTRAINT `tk_offer_has_service_ibfk_1` FOREIGN KEY (`fk_offer`) REFERENCES `tk_store_has_offer` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_package_has_service`
--
ALTER TABLE `tk_package_has_service`
  ADD CONSTRAINT `tk_package_has_service_ibfk_1` FOREIGN KEY (`fk_package`) REFERENCES `tk_store_has_package` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_package_has_service_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_customer`
--
ALTER TABLE `tk_store_has_customer`
  ADD CONSTRAINT `tk_store_has_customer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_customer_ibfk_2` FOREIGN KEY (`fk_user`) REFERENCES `tk_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_gallery`
--
ALTER TABLE `tk_store_has_gallery`
  ADD CONSTRAINT `tk_store_has_gallery_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_offer`
--
ALTER TABLE `tk_store_has_offer`
  ADD CONSTRAINT `tk_store_has_offer_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_package`
--
ALTER TABLE `tk_store_has_package`
  ADD CONSTRAINT `tk_store_has_package_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_services`
--
ALTER TABLE `tk_store_has_services`
  ADD CONSTRAINT `tk_store_has_services_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tk_store_has_services_ibfk_2` FOREIGN KEY (`fk_service`) REFERENCES `tk_services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_setting`
--
ALTER TABLE `tk_store_has_setting`
  ADD CONSTRAINT `tk_store_has_setting_ibfk_1` FOREIGN KEY (`fk_setting`) REFERENCES `tk_setting` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_staff`
--
ALTER TABLE `tk_store_has_staff`
  ADD CONSTRAINT `tk_store_has_staff_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tk_store_has_timing`
--
ALTER TABLE `tk_store_has_timing`
  ADD CONSTRAINT `tk_store_has_timing_ibfk_1` FOREIGN KEY (`fk_store`) REFERENCES `tk_stores` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
